#ifndef TdcAnalysis_h
#define TdcAnalysis_h 1

#include "TdcManager.h"

#include <algorithm> // find
#include <iostream> // find
#include <map>
#include <string>
#include <tuple>
#include <vector>

#include "TGraph.h"
#include "TGraphErrors.h"
#include "TFile.h"
#include "HitMap.h"

namespace Analysis {
    struct HitDistribution {
        std::string prefix = "pre";

        HitMap* hitmap;
        HitMap* hitmap_nHitsSameChip;
        HitMap* hitmap_notCalibratedHits;
        HitMap* hitmap_calibratedMemoryCells;

        TH1D* h_cogZ;
        TH1D* h_cogX;
        TH1D* h_cogY;

        TH1D* h_posX;
        TH1D* h_posY;
        TH1D* h_posZ;

        TH1D* h_hitRadius;
        TH1D* h_hitRadius_smallLayer;
        TH1D* h_hitRadius_bigLayer;

        TH2D* h2_mip_over_hitRadius;
        TH2D* h2_mip_over_showerDepth;
        TH2D* h2_radius_over_showerDepth;
        TH2D* h2_radius_over_nHitsSameChip;
        TH2D* h2_radius_over_memCell;
        TH2D* h2_nHitsSameChip_over_memCell;

        TH1D* h_energyFraction_over_radius;
        TH1D* h_energyFraction_over_showerDepth;

        TH1D* h_nHits;
        TH1D* h_singleHitEnergy;
        TH1D* h_energySum;

        TH2D* h2_nHits_cogZ;

        TH1D* h_mean_nHitsPerLayer;
        TH1D* h_mean_energyPerLayer;
        TH1D* h_mean_nHitsPerChip;

        TH1D* h_nHitsSameChip;
        TH1D* h_memCell;

        TH1D* h_showerStart;
        TH1D* h_showerStart_vs_MCTruth;
        TH1D* h_MCTruthShowerStart;
        TH2D* h2_showerStart_vs_MCTruth;
        TH2D* h2_showerDepth_vs_Layer;
        TH2D* h2_trueShowerDepth_vs_Layer;

        TH1D* h_nCherenkovHits;
        TH1D* h_nT0Hits;
        TH1D* h_nTracks;

        std::map<int, TH1D*> mh_layer_nHits; // do this only once

        std::map<std::tuple<int, int, int>, int> m_ijk_calibratedHits;
        std::map<std::tuple<int, int, int>, int> m_ijk_notCalibratedHits;
    };

    struct T0s {
        std::map<int, TH1D*> mh_mip;         // mip values
        std::map<int, TH1D*> mh_tdc;         // tdc values XXX: not yet implemented
        std::map<int, std::map<int, TH1D*> > mh_ns_diff_preCorrection;     // difference in calibrated time
        std::map<std::pair<int, int>, TH1D*> mh_ns_diff;     // difference in calibrated time
    };

    struct Timing {
        HitMap* hitmap_fwhm; //
        // HitMap* hitmap_delay; // TODO: Implement

        TH1D* h_resolution_evtTime;
        TH1D* h_resolution_t0Time;
        TH1D* h_resolution_t0Time_withFit;
        TH1D* h_resolution_t0Time_normalized;

        std::map<int, TH1D*> mh_showerDepth_resolution;
        std::map<int, TH1D*> mh_radius_resolution;
        std::map<int, TH1D*> mh_radius_resolution_lastLayer;
        std::map<int, TH1D*> mh_radius_resolution_smallLayers;
        std::map<int, TH1D*> mh_radius_resolution_bigLayers;
        std::map<int, TH1D*> mh_chip_resolution;
        std::map<int, TH1D*> mh_nHitsSameChip_resolution;
        std::map<int, TH1D*> mh_memCell_resolution;

        std::map<int, TH2D*> mh2_layer_radius_resolution;
        std::map<int, TH2D*> mh2_layer_showerDepth_resolution;


        TH1D* h_lastLayerHitGeoRadius;
        TH1D* h_stripHitGeoRadius;

        TH1D* h_T3B_resolution;

        std::map<std::tuple<int, int, int>, TH1D*> mh_ijk_resolution;

        TH1D* h_shiftPerChip; // See if there is a shift left for the chips
        TH1D* h_number_of_late_hits; // number of hits that are later than 50ns after T0
        TH1D* h_fraction_of_late_hits; // fraction of hits that are later than 50ns

        TH1D* h_fraction_of_late_hits25; // fraction of hits that are later than 50ns
        TH1D* h_fraction_of_late_hits50; // fraction of hits that are later than 50ns
        TH1D* h_fraction_of_late_hits100; // fraction of hits that are later than 50ns

        TH2D* h2_time_over_radius;
        TH2D* h2_time_over_mip;
        TH2D* h2_time_over_layer;
        TH2D* h2_time_over_showerDepth;
        TH2D* h2_time_over_memCell;

        TH1D* h_meanTime_radius;
        TH1D* h_meanTime_mip;

        TH1D* h_energyFraction_over_time;


        TH1D* h_fLateHits_over_radius;
        TH1D* h_fLateHits_over_radius_smallLayers;
        TH1D* h_fLateHits_over_radius_bigLayers;
        TH1D* h_fLateHits_over_mip;
        TH1D* h_fLateHits_over_layer;
        TH1D* h_fLateHits_over_showerDepth;
        TH1D* h_fLateHits_over_radius25;
        TH1D* h_fLateHits_over_mip25;
        TH1D* h_fLateHits_over_layer25;
        TH1D* h_fLateHits_over_radius50;
        TH1D* h_fLateHits_over_mip50;
        TH1D* h_fLateHits_over_layer50;
        TH1D* h_fLateHits_over_radius100;
        TH1D* h_fLateHits_over_mip100;
        TH1D* h_fLateHits_over_layer100;

        TH1D* h_fLateHits_over_radius100_smallLayers;
        TH1D* h_fLateHits_over_radius100_bigLayers;

        TH1D* h_numberOfTimeGaps;

        TH2D* h2_radius_resolution_layer8_showerStart5;
        TH2D* h2_radius_resolution_layer9_showerStart5;
        TH2D* h2_radius_resolution_layer10_showerStart7;
        TH2D* h2_radius_resolution_layer11_showerStart7;
        TH2D* h2_radius_resolution_layer13_showerStart10;

        TH2D* h2_timeCorrelation_7_8;
        TH2D* h2_timeCorrelation_14_15;

        TH2D* h2_energy_vs_timing;
        TH1D* h_energy_inLateHits;
        TH1D* h_correlatedEnergy_inLateHits;

        TH1D* h_weightedCorrelatedLayerDifference;

        TH2D* h2_clusterSize_clusterTime;
        TH2D* h2_clusterEnergy_nDifferentLayers;

        TH2D* h2_cogCorrelationIJ;
        TH2D* h2_cogCorrelation_clusterEnergy;

        TH1D* h_doubleParticleTime;

        TGraphErrors* gr_timeFit_showerDepth_const;
        TGraphErrors* gr_timeFit_showerDepth_Tfast;
        TGraphErrors* gr_timeFit_showerDepth_Tslow;
        TGraphErrors* gr_timeFit_showerDepth_Afast;
        TGraphErrors* gr_timeFit_showerDepth_Aslow;

        TGraphErrors* gr_timeFit_showerDepth_Afast_over_Aslow;

        TGraphErrors* gr_timeMean_overShowerDepth;
        TGraphErrors* gr_timeMean_overShowerDepth_50To200;
        TGraph* gr_timeMedian_overShowerDepth;

        TGraphErrors* gr_timeFit_radius_const;
        TGraphErrors* gr_timeFit_radius_Tfast;
        TGraphErrors* gr_timeFit_radius_Tslow;
        TGraphErrors* gr_timeFit_radius_Afast;
        TGraphErrors* gr_timeFit_radius_Aslow;

        TGraphErrors* gr_timeFit_radius_Afast_over_Aslow;

        TGraphErrors* gr_timeMean_overRadius;
        TGraphErrors* gr_timeMean_overRadius_50To200;

        TGraphErrors* gr_timeMean_overRadius_50To200_smallLayers;
        TGraphErrors* gr_timeMean_overRadius_50To200_bigLayers;
        TGraphErrors* gr_timeMean_overRadius_50To200_lastLayer;

        std::map<int, TProfile*> mpr_layers_timeMean_overRadius_50To200;

        std::map<int, TGraphErrors*> mgr_layer_timeMean_overRadius_50To200;

        TGraph* gr_timeMedian_overRadius;

    };

    struct General {
        HitMap* hitmap_chips;
        HitMap* hitmap_brokenChannels;
    };

    // TODO: analyze calibration
    // TODO: calibrated hits hitmap
    // TODO: how well does the calibration work?

    struct Calibration {
        // HitMap* hitmap_calibratedChannels;

        TGraph* gr_timewalk_correction_mean; // before calibration
        TGraph* gr_timewalk_correction_median; // after calibration

        TGraph* gr_nHitsSameChip_rms;
        TGraph* gr_nHitsSameChip_shift_mean;
        TGraph* gr_nHitsSameChip_shift_median;
    };

};

class TdcAnalysis {
    public:
        TdcAnalysis();
        TdcAnalysis(TdcManager* tdcManager);

        void analyze_hitDistribution(std::string prefix);
        void write_hitDistribution(std::string prefix, TFile* ofile);

        void analyze_t0s();
        void write_t0s(TFile* ofile);

        void analyze_general();
        void write_general(TFile* ofile);

        void analyze_timing();
        void write_timing(TFile* ofile);

        void analyze_calibration(std::string prefix);
        void write_calibration(std::string prefix, TFile* ofile);

        void writeFigures(const char* fname);
        void analyze();

        void writeEventMaps(std::vector<Event*> v_eventsForHitMap);

        Analysis::HitDistribution* _fig_hitsPre;
        Analysis::HitDistribution* _fig_hits;

        Analysis::T0s* _fig_t0s;
        Analysis::General* _fig_general;
        Analysis::Timing* _fig_timing;
        Analysis::Calibration* _fig_calibration;

    private:

        void init_hitDistribution(Analysis::HitDistribution* figHits, std::string prefix);
        void init_t0s();
        void init_general();
        void init_timing();
        void init_calibration();

        bool isDoubleParticle(std::map<double, Hit*>& pm_dT_hits,
                              Event& evt,
                              double pStartTime,
                              double pWindowSize,
                              double pMinClusterSize,
                              double pClusterSizeCut,
                              double pCoGCorrelationCut);

        TdcManager* TM;

        int _nChannels;
        int _nMemoryCells;
        // int _bxPeriod;
        // double _rampDeadtime;

        std::map<int, int> _nChannelsX;
        std::map<int, int> _nChannelsY;

        int _nLayers;

        // std::vector< std::pair< int, int> > _v_T0;
        // std::vector< std::pair< int, int> > _v_broken_T0;
        // std::vector< std::pair< int, int> > _v_cherenkov;

        // std::vector< int > _v_broken_chips;

        std::vector<int> _v_chipIDs;

        std::map<std::tuple<int, int, int>, std::pair<int, int> > _m_ijk_to_ChipChan;


        std::vector<Event*> _v_eventsForHitMap;

        // std::vector<TGraph*> _v_gr_T0;
        // TGraph* _gr_timewalk;

        std::map<int, TH1D*> _vh_layer_sim_resolution;

        TH1D* _h_nHits = new TH1D("_h_nHits", "Number of Hits per Event", 100,0,100);
        TH1D* _h_t0s = new TH1D("_h_t0s", "T0 Difference for High Memory Cells", 2000,-100,100);

        std::tuple<double, double, double> _tw_coeff = std::make_tuple(-2.74,8.46,-1.09);

        std::map<int, int> _m_nHits_inT0s;

        // chipID, channel(0..36), memCell(0..15), bxID(0,1,2), v_T0times, v_tdcs
        // while channel 36 is the sum of all channels, memCell 0 is the sum of all memCells
        // and bxID 2 is the sum of both bxIDs
        // std::map<int, std::map<int, std::map<int, std::map<int, std::vector<std::pair<double, Hit&> > > > > > _m_tdcs;

        // bool fHasReadRootFile = false;
        // bool fHasReadT0Calibration = false;
        // bool fHasReadHardwareConnection = false;
        // bool fHasReported = false;

        bool fHasAnalyseTiming = false;
        bool fHasAnalyseGeneral = false;
        bool fHasAnalyseT0 = false;
        bool fHasAnalyseHitDistributionPre = false;
        bool fHasAnalyseHitDistribution = false;
        bool fHasAnalyseCalibration = false;

};

#endif
