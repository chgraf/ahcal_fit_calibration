#ifndef TdcFitCalibration_h
#define TdcFitCalibration_h 1

#include "TdcManager.h"

#include <algorithm> // find
#include <iostream>
#include <map>
#include <tuple>
#include <vector>

class TdcFitCalibration {
    public:
        TdcFitCalibration();
        TdcFitCalibration(TdcManager* tdcManager);

        void shiftSimulationTo0();
        void shiftChipsTo0();


        void fit_calibration();
        void writeFitCalibration(const char* fn_fitCalibration);

        void nonLinearityCorrection();
        void writeNonLinearityCorrection(const char* fn_nonLinearityCorrection);

        void doTimeWalkCorrection();
        void writeTimeWalkCorrection(const char* fn_timeWalkCorrection);

    private:

        TdcManager* TM;

        int _nChannels;
        int _nMemoryCells;
        int _bxPeriod;
        double _rampDeadtime;

        std::map<int, int> _nChannelsX;
        std::map<int, int> _nChannelsY;

        int _nLayers;

        std::vector< std::pair< int, int> > _v_T0;
        std::vector< std::pair< int, int> > _v_broken_T0;
        std::vector< std::pair< int, int> > _v_cherenkov;

        std::vector< int > _v_broken_chips;

        std::vector<int> _v_chipIDs;

        std::tuple<double, double, double> _tw_coeff = std::make_tuple(-2.74,8.46,-1.09);

        std::map<int, std::map<int, std::map<int, std::map<int, std::tuple<double, double, double> > > > > _m_calibration_values;

        std::map<int, std::map<int, std::tuple<double, double, double> > > _m_nonLinearityCorrection;

        // std::map<int, double> _m_simulationShiftByLayer;

        // std::map<int, double> _m_shift;

        std::map<int, double> _m_nHitsSameChipCorrection;

};

#endif
