#ifndef TdcManager_h
#define TdcManager_h 1

#include "HitMap.h"

#include <algorithm> // find
#include <functional> // reference_wrapper
#include <iostream> //
#include <map>
#include <tuple>
#include <string>
#include <vector>

#include "TGraph.h"
#include "TFile.h"


const std::string RED = "\033[0;31m";
const std::string GREEN = "\033[0;32m";
const std::string YELLOW = "\033[1;33m";
const std::string ORANGE = "\033[0;33m";
const std::string CYAN = "\033[0;36m";
const std::string MAGENTA = "\033[0;35m";
const std::string NORMAL = "\033[0m";


struct Hit {
    int eventID = -1;
    int bxID = -1;
    int chipID = -1;
    int memoryCell = -1;
    int channel = -1;
    int adc = -1;
    int tdc = -1;
    int I = 0;
    int J = 0;
    int K = 0;
    int module = 0;
    int chipNr = 0;
    double mip = -1.;
    double posX = 0;
    double posY = 0;
    double posZ = 0;

    int hitbit = -1;
    int T0ID = -1;
    bool isT0 = false;

    int nHitsSameChip = 0;

    int radius = -1;
    int showerDepth = -1;

    double ns = -1;
    double event_time = 0;

    bool passTiming = false;
};

struct Event {
    // T0s and Cherenkovs
    int ID = 0;
    int nT0s = -1;
    int nCherenkovs = -1;
    std::map<int, Hit> T0s; // easy access of the T0s

    //FIXME: is this still needed?
    std::vector<Hit> brokenT0s; // easy access of the broken T0s

    // statistics
    double cogZ = -1;
    double cogX = -1;
    double cogY = -1;
    double energySum = -1;
    int nTracks = -1;
    int showerStart = -1;
    int showerStartAllLayers = -1;
    int showerStartMCTruth = -1;
    // reference time
    double T0_time = 0.;
    double fractionOfLateHits = 0.;

    // stores all hits in the event
    std::vector<Hit> v_hits;
    std::vector<Hit*> v_hits_time;
};

struct Cuts {
    int nTracks_inEvent = -1;        // number of tracks required
    int maxHits_inEvent = -1;        // maximum number of hits that are in the event (without broken chips, T0s)
    int minHits_inEvent = -1;        // minimum number of hits that are in the event (without broken chips, T0s)
    double minCogZ_inEvent = -1;        // minimum CoG in Events
    double maxCogZ_inEvent = -1;        // maximum CoG in Events
    int minHits_beatingCogZ = -1;    // minimum number of hits that beat the CoG cut
    int nCherenkovs = -1;            // number of cherenkov hits
    int maxNHitsSameChip = 16;            // number of cherenkov hits

    bool excludeMemoryCellZero = true;            // should memoryCell 0 be excluded?

    double minEnergy_hits = 0.5;            // energy cut on hits
    double minTime_T0 = 0.5;            // time cut on hits
    double maxTime_T0 = 0.5;            // time cut on hits

    int minT0s = 0;                     // minimum numbers of T0s
    double maxTimeDifference_T0 = 0;    // maximum time difference between several T0s

    std::vector<double> t0_mip_cuts;
};

struct RootHit {
    Int_t nHits;
    Float_t cogZ;
    Float_t energySum;
    Int_t eventID = 0;
    Int_t BxID = 0;
    Int_t nTracks = -1;
    Int_t MCTruth_showerStart = -1;
    Float_t hitEnergy[10000];
    Float_t hitTime[10000];
    Int_t I[10000];
    Int_t J[10000];
    Int_t K[10000];
    Int_t type[10000];
    Float_t hitPos[30000];
    // Float_t hitPos[10000][3];
};

/* In principle this class should provide the infrastructure for TDC calibration
    and analysis and do some calculations, that have to be done often
*/
class TdcManager {
    public:
        TdcManager();

        // FIXME: Deprecated
        void readRootFile(const char* fn_root_hits);

        void readRootFileNewFormat(const char* fn_root_hits, int limit);
        void selectEvents();

        void readT0Calibration(const char* fn_T0_calibration);
        void readT0Correction(const char* fn_T0_correction);
        void processT0s();

        void readHardwareConnection(const char* fn_hardwareConnection);

        void readFitCalibration(const char* fn_fitCalibration);
        void extendFitCalibration(); // extends fit calibration to non-calibrated memoryCells
        void readNonLinearityCorrection(const char* fn_nonLinearityCorrection);
        void readTimeWalkCorrection(const char* fn_timeWalkCorrection);

        void doTimeSmearing();

        bool tdcToNs(Hit& hit, bool fNonLinearityCorrection, bool fTimewalkCorrection, bool fShiftTo0, bool fnHitSameChipCorrection);
        inline double twCorrection(double mip) {
            return (std::get<0>(_tw_coeff) + std::get<1>(_tw_coeff) * exp(mip*std::get<2>(_tw_coeff)));
        };

        void sortDataPerChannel();

        void calibrate(bool fNonLinearityCorrection, bool fTimewalkCorrection, bool fShiftTo0, bool fnHitsSameChipCorrection);

        // mu: Muon
        // p: Pion
        // e: Electron
        inline void setParticleType(std::string particle_type) {
            std::cout << std::endl;
            std::cout << "Particle Type: ";
            if(particle_type == "mu") std::cout << ORANGE;
            else if(particle_type == "e") std::cout << YELLOW;
            else if(particle_type == "pi") std::cout << CYAN;
            else std::cout << RED;
            std::cout << particle_type << NORMAL << std::endl;
            _particle_type = particle_type;
        }

        inline void setDataset(std::string dataset) {
            std::cout << std::endl;
            std::cout << "Data set: ";
            if(dataset == std::string("Aug15")) std::cout << CYAN;
            else if(dataset == std::string("Jul15")) std::cout << MAGENTA;
            else std::cout << RED;
            std::cout << dataset << NORMAL << std::endl;
            _dataset = dataset;
        }

        inline void setIsSimulatedData(bool isSimData) {
            std::cout << std::endl;
            std::cout << "Is Simulated Data: ";
            if(isSimData) std::cout << GREEN << "yes" << NORMAL << std::endl;
            else std::cout << RED << "no" << NORMAL << std::endl;

            _isSimulatedData = isSimData;
            if(isSimData) fHasReadRootFile = fHasReadT0Calibration = true;
        }

        inline void setEnergy(int e) {
            _energy = e;
        }

        inline int getEnergy() {
            return _energy;
        }

        inline std::string getDataset() {
            return _dataset;
        }

        inline bool isSimulatedData() {
            return _isSimulatedData;
        }


        inline bool isT0(std::pair<int, int> p) {
            return (std::find(_v_T0.begin(), _v_T0.end(), p) != _v_T0.end());
        }

        // returns the t0 ID, return -1 if the given chip, channel pair is no T0
        inline int getT0ID(std::pair<int, int> p) {
            auto it_t0 = std::find(_v_T0.begin(), _v_T0.end(), p);
            if(it_t0 == _v_T0.end()) {
                return -1;
            } else {
                return it_t0 - _v_T0.begin();
            }
        }

        inline bool isBrokenT0(std::pair<int, int> p) {
            return (std::find(_v_broken_T0.begin(), _v_broken_T0.end(), p) != _v_broken_T0.end());
        }

        inline bool isBrokenChip(int chipID) {
            return (std::find(_v_broken_chips.begin(), _v_broken_chips.end(), chipID) != _v_broken_chips.end());
        }

        inline bool isCherenkov(std::pair<int, int> p) {
            return (std::find(_v_cherenkov.begin(), _v_cherenkov.end(), p) != _v_cherenkov.end());
        }

        inline bool isCalibratedChannel(std::pair<int, int> p) {
            return (std::find(_v_calibrated_channels.begin(), _v_calibrated_channels.end(), p) != _v_calibrated_channels.end());
        }

        inline bool isCalibratedMemoryCell(std::pair<int, int> p_chipChan, int memoryCell) {
            return _m_calibrated_memoryCells[p_chipChan] >= memoryCell;
        }

        // returns the depth of the memoryCell that is calibrated for a given chip/channel pair
        inline int getCalibratedMemoryCell(std::pair<int, int> p) { return _m_calibrated_memoryCells[p]; }

        double calculateCogZ(Event& evt);

        std::map<int, double> getLayerPositions();

        inline bool isPion() {
            if(_particle_type == "pi") return true;
            else return false;
        }

        inline bool isElectron() {
            if(_particle_type == "e") return true;
            else return false;
        }

        inline bool isMuon() {
            if(_particle_type == "mu") return true;
            else return false;
        }

        inline bool isHitSmallLayer(Hit& hit) {
            if(hit.K < 11) return true;
            else return false;
        }

        inline bool isHitBigLayer(Hit& hit) {
            if(hit.K > 10) return true;
            else return false;
        }

        inline std::vector<int> getChipIDs() { return _v_chipIDs; }

        inline int getNChannels() { return _nChannels; }
        inline std::map<int, int> getNChannelsX() { return _nChannelsX; }
        inline std::map<int, int> getNChannelsY() { return _nChannelsY; }

        inline int getNMemoryCells() { return _nMemoryCells; }
        inline int getNLayers() { return _nLayers; }

        inline int getNT0s() { return _v_T0.size(); }
        inline std::vector< std::pair< int, int> > getT0s() { return _v_T0; }

        inline std::tuple<double, double, double> getTwCoeff() { return _tw_coeff; }

        inline std::map<int, double> getNHitsSameChipShift() { return _m_nHitsSameChipCorrection; }

        inline std::vector<int> getBrokenChips() { return _v_broken_chips; }
        inline void printBrokenChips() {
            std::cout << "\nBroken Chips:" << std::endl;
            for(int i: _v_broken_chips) std::cout << "\tChip " << i << std::endl;
            std::cout << "Total: " << _v_broken_chips.size() << " / " << _v_chipIDs.size() << std::endl;
        }
        inline void setBrokenChips(std::vector<int> v_broken_chips) { _v_broken_chips = v_broken_chips; }
        inline void setBrokenT0s(std::vector<std::pair<int, int> > v_broken_T0) { _v_broken_T0 = v_broken_T0; }
        inline void setT0s(std::vector<std::pair<int, int> > v_T0) { _v_T0 = v_T0; }
        inline void setCherenkov(std::vector<std::pair<int, int> > v_cherenkov) { _v_cherenkov = v_cherenkov; }

        inline void setDoT0Correction(bool doT0Correction) { fT0Correction = doT0Correction; }


        inline Cuts getCuts() { return _cuts; }
        inline void setCuts(Cuts cuts) { _cuts = cuts; }

        inline std::pair<int, int> convert_ijk_to_chipChan(int i, int j, int k) {
            return _m_ijk_to_ChipChan[std::make_tuple(i,j,k)];
        }

        inline std::tuple<int, int, int> convert_chipChan_to_ijk(int chipID, int channel) {
            return _m_ChipChan_to_ijk[std::make_pair(chipID, channel)];
        }

        // void writeFigures(TFile* ofile);


        void writeProcessedRootFile(const char* fname);


        std::vector<Event> _v_events;

        // chipID, channel(0..36), memCell(0..15), bxID(0,1,2), v_T0times, v_tdcs
        // while channel 36 is the sum of all channels, memCell 0 is the sum of all memCells
        // and bxID 2 is the sum of both bxIDs
        std::map<int, std::map<int, std::map<int, std::map<int, std::vector<std::pair<double, Hit&> > > > > > _m_tdcs;

        std::vector<TGraph*> _v_t0_dt_plots; // T0 plots
        TH1D* _h_t0s;


    private:

        Cuts _cuts; // set of cuts


        int _nChannels;
        int _nMemoryCells;
        int _bxPeriod;
        double _rampDeadtime;

        std::string _particle_type = "mu";
        bool _isSimulatedData = false;
        int _energy = 0;
        std::string _dataset = "Aug15";

        std::map<int, int> _nChannelsX;
        std::map<int, int> _nChannelsY;

        int _nLayers;

        std::map<std::tuple<int, int, int>, std::pair<int, int> > _m_ijk_to_ChipChan;
        std::map<std::pair<int, int>, std::tuple<int, int, int> > _m_ChipChan_to_ijk;


        std::vector< std::pair< int, int> > _v_T0;
        std::vector< std::pair< int, int> > _v_broken_T0;
        std::vector< std::pair< int, int> > _v_cherenkov;

        std::vector< int > _v_broken_chips;
        std::vector< std::pair<int, int> > _v_calibrated_channels;
        std::map<std::pair<int, int>, int> _m_calibrated_memoryCells;

        std::vector<int> _v_chipIDs;

        std::map<int, std::map<int, std::map<int, std::map<int, std::pair<double, double> > > > > _m_T0_calibration;
        // module, bxID
        std::map<int, std::map<int, std::tuple<double, double, double> > > _m_T0_correction;

        std::map<int, std::map<int, std::map<int, std::map<int, std::tuple<double, double, double> > > > > _m_calibration_values;

        std::map<int, std::map<int, std::tuple<double, double, double> > > _m_nonLinearityCorrection;

        std::map<int, double> _m_simulationShiftByLayer;

        std::map<int, double> _m_shift;

        std::map<int, double> _m_nHitsSameChipCorrection;

        std::tuple<double, double, double> _tw_coeff = std::make_tuple(-2.74,8.46,-1.09);
        // Some statistics and plots


        bool fHasReadRootFile = false;

        bool fT0Correction = false;
        bool fHasReadT0Correction = false;

        bool fHasReadT0Calibration = false;
        bool fHasReadHardwareConnection = false;
        bool fHasReported = false;

};

#endif
