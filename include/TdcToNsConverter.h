#ifndef TdcToNsConverter_h
#define TdcToNsConverter_h 1

#include "TdcManager.h"

#include <algorithm> // find
#include <iostream> // find
#include <map>
#include <tuple>
#include <vector>

#include "TGraph.h"
#include "HitMap.h"
#include "TdcManager.h"

class TdcToNsConverter {
    public:
        TdcToNsConverter();
        TdcToNsConverter(TdcManager* TM);

        void writeRootFile();

    private:
        TdcManager* TM;


};

#endif
