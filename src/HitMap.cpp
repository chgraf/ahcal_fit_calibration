#include "HitMap.h"

#include <cassert>

#include <algorithm>
#include <iostream>
#include <sstream>

#include "TCanvas.h"
#include "TPad.h"
#include "TH2D.h"
#include "TH1D.h"



HitMap::HitMap(std::map<int, int> nChannelsX, std::map<int, int> nChannelsY, std::string pName="") {
    this->initHitMap(nChannelsX, nChannelsY, pName);
}


HitMap::HitMap(std::map<int, std::vector<std::pair<int, int> > >* KIJContainer, std::string pName="") {


    std::map<int, int> nChannelsX;
    std::map<int, int> nChannelsY;

    for(auto& pk : *KIJContainer) {
        int k = pk.first;
        _vLayers.push_back(k);

        // _nChannelsX[k] = pk.second;

        // find maximum values for I and J
        std::vector<std::pair<int, int> > mLayer = pk.second;
        auto maxPair = std::max_element(
            std::begin(mLayer), std::end(mLayer),
            [] (std::pair<int, int> p1, std::pair<int, int> p2) {
                return ((p1.first <= p2.first) && (p1.second <= p2.second));
            }
        );
        // std::cout << k << "\t" << maxPair->first << "\t" << maxPair->second << std::endl;
        nChannelsX[k] = maxPair->first;
        nChannelsY[k] = maxPair->second;
    }

    this->initHitMap(nChannelsX, nChannelsY, pName);
}


void HitMap::initHitMap(std::map<int, int> nChannelsX, std::map<int, int> nChannelsY, std::string pName="") {
    assert(nChannelsX.size() == nChannelsY.size());

    // _nLayers = nChannelsX.size();
    _name = pName;
    _nChannelsX = nChannelsX;
    _nChannelsY = nChannelsY;
    assert(nChannelsX.size() == nChannelsY.size());

    if(_vLayers.size() == 0){
        for(unsigned int i=0; i < _nChannelsX.size(); i++){
            _vLayers.push_back(i);
        }
    }

    _nLayers = _vLayers.size();
    // std::cout << "Layers: " << _nLayers << std::endl;

    TH2D* h;
    std::stringstream h_name;
    std::stringstream h_title;

    // for(int k=0; k < _nLayers; k++) {
    for(auto& k : _vLayers) {
        h_name.str(std::string());
        h_title.str(std::string());
        h_name << "hm_" << pName << "_" << k;
        h_title << "HitMap " << pName << " (layer " << k << ")";
        h = new TH2D(h_name.str().c_str(), h_title.str().c_str(), _nChannelsX[k], 0, _nChannelsX[k], _nChannelsY[k], 0, _nChannelsY[k]);
        _mh2_layers[k] = h;

    }
    std::cout << "Created Hitmap: " << pName << std::endl;
    // std::cout << "size _vh2...: " << _mh2_layers.size() << std::endl;

}


void HitMap::fill(int i, int j, int k) {
    // assert(k < _nLayers);
    // assert(i < _nChannelsX[k]);
    // assert(j < _nChannelsY[k]);

    _mh2_layers[k]->Fill(i,j);
}

void HitMap::set(int i, int j, int k, double value) {
    // assert(k < _nLayers);
    // assert(i < _nChannelsX[k]);
    // assert(j < _nChannelsY[k]);

    int bin = _mh2_layers[k]->GetBin(i+1,j+1);
    _mh2_layers[k]->SetBinContent(bin, value);
}

void HitMap::commonZScale(double zMin, double zMax) {
    for(auto& e: _mh2_layers) {
        TH2D* h = e.second;
        h->GetZaxis()->SetRangeUser(zMin, zMax);
    }
}

void HitMap::commonZScale() {
    double minValue=0;
    double maxValue=0;

    for(auto& k: _vLayers) {
        for(int binX = 1; binX < _mh2_layers[k]->GetNbinsX()-1; binX++) {
            for(int binY = 1; binY < _mh2_layers[k]->GetNbinsY()-1; binY++) {
                int bin = _mh2_layers[k]->GetBin(binX, binY);
                if(minValue == 0 && maxValue == 0) {
                    minValue = _mh2_layers[k]->GetBinContent(bin);
                    maxValue = _mh2_layers[k]->GetBinContent(bin);
                    continue;
                }

                if(_mh2_layers[k]->GetBinContent(bin) > maxValue) {
                    maxValue = _mh2_layers[k]->GetBinContent(bin);
                } else if(_mh2_layers[k]->GetBinContent(bin) < minValue) {
                    minValue = _mh2_layers[k]->GetBinContent(bin);
                }
            }
        }

    }

    for(auto& e: _mh2_layers) {
        TH2D* h = e.second;
        h->GetZaxis()->SetRangeUser(minValue, maxValue);
    }
}


void HitMap::write() {
    TFile* o_file = new TFile("HitMap.root", "recreate");
    write(o_file);
    o_file->Close();
}

void HitMap::write(TFile* outputRootFile) {
    if(outputRootFile->IsOpen()) {
        // _nLayers = _mh2_layers.size();
        // _nLayers = _vLayers.size();
        // std::cout << "nLayers: " << _nLayers << std::endl;


        TCanvas* canvas = new TCanvas(_name.c_str(), _name.c_str());
        if(_nLayers > 1) {
            if(_nLayers == 2) canvas->Divide(1,2);
            else if(_nLayers <= 4) canvas->Divide(2,2);
            else if(_nLayers <= 6) canvas->Divide(3,2);
            else if(_nLayers <= 9) canvas->Divide(3,3);
            else if(_nLayers <= 12) canvas->Divide(4,3);
            else if(_nLayers <= 16) canvas->Divide(4,4);
            else if(_nLayers <= 20) canvas->Divide(5,4);
            else if(_nLayers <= 25) canvas->Divide(5,5);
            else {
                int x = (int)sqrt(_nLayers) + 1;
                canvas->Divide(x,x);
            }
        }

        // for(int k=0; k < _nLayers; k++) {
        for(auto& k : _vLayers) {
            TPad *pad = (TPad*)canvas->cd(k+1);
            if(_logZ) pad->SetLogz();
            _mh2_layers[k]->SetStats(0);
            _mh2_layers[k]->Draw("COLZ");
        }
        canvas->Write();

        // for(int k=0; k < _nLayers; k++) {
        // Enable this for writing all the histograms
        // for(auto& k : _vLayers) {
            // _mh2_layers[k]->Write();
            // std::cout << "writing: "  << _mh2_layers[k]->GetTitle() << std::endl;
        // }
    } else {
        std::cout << "Root File is not open" << std::endl;
    }
}
