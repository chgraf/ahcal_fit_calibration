#include "TdcManager.h"
#include "TdcFitCalibration.h"
#include "TdcToNsConverter.h"
#include "TdcAnalysis.h"

#include <iostream>

TdcManager* tdcManager = new TdcManager();


void initSpecialChannels() {
    std::vector<int> v_brokenChips;
    std::vector<std::pair<int, int> > v_broken_T0;
    std::vector<std::pair<int, int> > v_T0;
    std::vector<std::pair<int, int> > v_cherenkov;

    // TODO: This should not be hardcoded!
    // Layer 4 & 5 exclude for electrons and pions
    // They show a noisy behaviour if there are more hits in the same chip
    if(tdcManager->isElectron() || tdcManager->isPion()) {
        v_brokenChips.push_back(161);
        v_brokenChips.push_back(162);
        v_brokenChips.push_back(163);
        v_brokenChips.push_back(164);
        v_brokenChips.push_back(165);
        v_brokenChips.push_back(166);
        v_brokenChips.push_back(167);
        v_brokenChips.push_back(168);
    }

    // Layer 12
    v_brokenChips.push_back(170);
    v_brokenChips.push_back(171);
    v_brokenChips.push_back(172);
    v_brokenChips.push_back(173);
    v_brokenChips.push_back(174);
    v_brokenChips.push_back(175);
    v_brokenChips.push_back(176);
    // v_brokenChips.push_back(177);
    v_brokenChips.push_back(178);
    v_brokenChips.push_back(179);
    v_brokenChips.push_back(180);
    v_brokenChips.push_back(181);
    v_brokenChips.push_back(182);
    v_brokenChips.push_back(183);
    v_brokenChips.push_back(184);


    v_T0.push_back(std::pair<int, int>(201,29));
    v_T0.push_back(std::pair<int, int>(217,23));

    if(tdcManager->getDataset() == "Jul15") {
        v_T0.push_back(std::pair<int, int>(185,29));
    }


    v_broken_T0.push_back(std::pair<int, int>(169,29));
    v_broken_T0.push_back(std::pair<int, int>(177,23));
    v_broken_T0.push_back(std::pair<int, int>(211,6));

    if(tdcManager->getDataset() == "Aug15") {
        v_broken_T0.push_back(std::pair<int, int>(185,29));
    }
        // v_broken_T0.push_back(std::pair<int, int>(185,29));

    v_cherenkov.push_back(std::pair<int, int>(141,29));
    v_cherenkov.push_back(std::pair<int, int>(195,12));
    v_cherenkov.push_back(std::pair<int, int>(227,6));

    tdcManager->setT0s(v_T0);
    tdcManager->setBrokenT0s(v_broken_T0);
    tdcManager->setCherenkov(v_cherenkov);
    tdcManager->setBrokenChips(v_brokenChips);

}

void initCuts() {
    Cuts cuts;

    cuts.excludeMemoryCellZero = true;
    cuts.minEnergy_hits = 0.5;

    cuts.minTime_T0 = 500;
    cuts.maxTime_T0 = 3500;
    cuts.maxTimeDifference_T0 = 5;

    cuts.maxNHitsSameChip = 16;

    //XXX: T0 energy cuts need to be checked for each data set!!!
    if(tdcManager->getDataset() == "Aug15") {
        cuts.minT0s = 2;
        if(tdcManager->isPion() && tdcManager->getEnergy() > 10) {
            cuts.t0_mip_cuts.push_back(4.6);
            cuts.t0_mip_cuts.push_back(3.3);
        } else if(tdcManager->isPion() && tdcManager->getEnergy() == 10) {
            cuts.t0_mip_cuts.push_back(4.1);
            cuts.t0_mip_cuts.push_back(3.15);

        }
    } else if(tdcManager->getDataset() == "Jul15") {
        cuts.minT0s = 3;
        // cuts.t0_mip_cuts.push_back(0.);
        cuts.t0_mip_cuts.push_back(2.8);
        cuts.t0_mip_cuts.push_back(6.6);
        // cuts.t0_mip_cuts.push_back(0.);
        cuts.t0_mip_cuts.push_back(0.);
    }

    // Event selection
    if(tdcManager->isSimulatedData()) {
        cuts.nTracks_inEvent = -1; // TODO: actually this should be 1! Check if miptrackfinder is working for simulation
        cuts.nCherenkovs = -1;
    } else {
        if(tdcManager->isMuon()) {
            cuts.nTracks_inEvent = 1;
            cuts.nCherenkovs = -1;
        } else if (tdcManager->isElectron()) {
            cuts.nTracks_inEvent = -1;
            cuts.nCherenkovs = 1;
        } else if (tdcManager->isPion()) {
            cuts.nTracks_inEvent = -1;
            cuts.nCherenkovs = 1;
        }
    }

    if(tdcManager->isMuon()) {
        cuts.minHits_inEvent = 0;
        cuts.maxHits_inEvent = 20;
        cuts.minCogZ_inEvent = 0;
        cuts.maxCogZ_inEvent = -1;
        cuts.minHits_beatingCogZ = -1;
    } else if (tdcManager->isElectron()) {
        cuts.minHits_beatingCogZ = -1;
        if(tdcManager->getEnergy() == 20) {
            cuts.minHits_inEvent = 16;
            cuts.maxHits_inEvent = 60;
            cuts.minCogZ_inEvent = 0;
            cuts.maxCogZ_inEvent = 120;
        } else if(tdcManager->getEnergy() == 70) {
            cuts.minHits_inEvent = 50;
            cuts.maxHits_inEvent = 100;
            cuts.minCogZ_inEvent = 90;
            cuts.maxCogZ_inEvent = 120;
        }
    } else if (tdcManager->isPion()) {
        cuts.maxTime_T0 = 2500; // reduce max time, so that late hits are still in
        if(tdcManager->getEnergy() == 10) {
            cuts.minHits_inEvent = 20;
            cuts.maxHits_inEvent = -1;
            cuts.minCogZ_inEvent = 120;
            cuts.maxCogZ_inEvent = -1;
            cuts.minHits_beatingCogZ = 30;
        } else if(tdcManager->getEnergy() == 30) {
            cuts.minHits_inEvent = 20;
            cuts.maxHits_inEvent = -1;
            cuts.minCogZ_inEvent = 120;
            cuts.maxCogZ_inEvent = -1;
            cuts.minHits_beatingCogZ = 50;
        } else if(tdcManager->getEnergy() == 50) {
            cuts.minHits_inEvent = 20;
            cuts.maxHits_inEvent = -1;
            cuts.minCogZ_inEvent = 120;
            cuts.maxCogZ_inEvent = -1;
            cuts.minHits_beatingCogZ = 70;
        } else if(tdcManager->getEnergy() == 70) {
            cuts.minHits_inEvent = 20;
            cuts.maxHits_inEvent = -1;
            cuts.minCogZ_inEvent = 120;
            cuts.maxCogZ_inEvent = -1;
            cuts.minHits_beatingCogZ = 100;
            // cuts.minHits_inEvent = -1;
            // cuts.maxHits_inEvent = -1;
            // cuts.minCogZ_inEvent = -1;
            // cuts.maxCogZ_inEvent = -1;
            // cuts.minHits_beatingCogZ = -1;
        } else if(tdcManager->getEnergy() == 90) {
            cuts.minHits_inEvent = 20;
            cuts.maxHits_inEvent = -1;
            cuts.minCogZ_inEvent = 120;
            cuts.maxCogZ_inEvent = -1;
            cuts.minHits_beatingCogZ = 100;
        }
    }

    if(tdcManager->getDataset() == "Aug15") {

    } else if(tdcManager->getDataset() == "Jul15") {

    }

    tdcManager->setCuts(cuts);
}

void writeCalibrationConstants() {
    TdcFitCalibration* fitCalibration = new TdcFitCalibration(tdcManager);

    fitCalibration->fit_calibration();
    fitCalibration->writeFitCalibration("constants/fit_calibration_tmp.txt");

    tdcManager->readFitCalibration("./constants/fit_calibration_tmp.txt");
    // tdcManager->calibrate(true, true, true, true);

    fitCalibration->nonLinearityCorrection();
    fitCalibration->writeNonLinearityCorrection("constants/nonLinearityCorrection_tmp.txt");

    // void doTimeWalkCorrection();
    // void writeTimeWalkCorrection(const char* fn_timeWalkCorrection);

}



void writeCalibratedRootFile() {
    // TDCcali->readRootFileNewFormat("../Hits/muons_newFormat_Tracks.root");
    // TDCcali->readRootFileNewFormat("../Hits/pions_newFormat.root");
    // TDCcali->readRootFileNewFormat("../Hits/electrons_newFormat.root");

    // TDCcali->readRootFileNewFormat("/home/iwsatlas1/cgraf/Analysis/muons_test.root");

    // TDCcali->readRootFileNewFormat("../Hits/pions_newFormat.root");
    // TDCcali->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Electrons_Sim/electrons_004_hcal_noCorrection.root");
    // TDCcali->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Electrons_Sim/electrons_004_hcal_newTimeSmearing.root");
    // TDCcali->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Electrons_Sim/electrons_005_hcal_10GeV.root");
    // TDCcali->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Muons_Sim/muons_007_tracks_hcal_T0smearing.root");

    // TDCcali->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Muons_Sim/muons_007_noTracks_hcal_T0smearing.root");

    // TDCcali->readRootFile("../hits_pions_70GeV.root");
    // TDCcali->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_QGSP_BERT_HD_reco_hcal_002.root");

}

void analyze() {
    // tdcManager->calibrate(true, true, true, true);
    // analysis->analyze();

}


int main() {
    std::cout << "Start calibration..." << std::endl;

    bool doCalibration = false;

    // mu; e; pi;
    tdcManager->setParticleType("pi");
    tdcManager->setIsSimulatedData(true);
    tdcManager->setEnergy(70);
    // Aug15, Jul15
    tdcManager->setDataset("Aug15");
    // tdcManager->setDataset("Jul15");
    tdcManager->setDoT0Correction(false);

    initSpecialChannels();
    initCuts();

    tdcManager->printBrokenChips();

    if(tdcManager->getDataset() == std::string("Aug15") ) {
        tdcManager->readHardwareConnection("../Mapping/Hardware_Connection_ijk_to_chipchan_Aug.txt");
    } else if(tdcManager->getDataset() == std::string("Jul15") ) {
        tdcManager->readHardwareConnection("../Mapping/Hardware_Connection_ijk_to_chipchan_July.txt");
        // tdcManager->readHardwareConnection("../Mapping/Hardware_Connection_ijk_to_chipchan_Aug.txt");
    }

    if(doCalibration) std::cout << RED << "\nCalibration Mode!\n" << NORMAL << std::endl;

    TdcAnalysis* analysis = new TdcAnalysis(tdcManager);

    tdcManager->readT0Calibration("../tmp_calibration.txt");
    // tdcManager->readT0Calibration("./constants/T0_Eldwan.txt");
    // tdcManager->readT0Correction("T0_correction_eldwan.txt");

    if(not doCalibration) {
        tdcManager->readFitCalibration("./constants/fit_calibration_17_06_26.txt");
        // tdcManager->readFitCalibration("./constants/fit_calibration_17_08_11_july.txt");
        // tdcManager->readFitCalibration("./constants/fit_calibration_17_08_01_july.txt");
        // tdcManager->readFitCalibration("./fit_calibration_eldwan.txt");

        tdcManager->extendFitCalibration();

        tdcManager->readNonLinearityCorrection("./constants/nonLinearityCorrection_17_06_26.txt");
        // tdcManager->readNonLinearityCorrection("./constants/nonLinearityCorrection_17_08_11_july.txt");
        // tdcManager->readFitCalibration("./constants/nonLinearityCorrection_17_08_01_july.txt");
        // tdcManager->readNonLinearityCorrection("./nonLinearityCorrection_eldwan.txt");
    }


    if(not tdcManager->isSimulatedData()) {
        if(tdcManager->isMuon()) {
            // tdcManager->readRootFileNewFormat("../../2015_July_Testbeam_tdc/Track_24010.root", -1);
            if(tdcManager->getDataset() == std::string("Jul15")) {
                // tdcManager->readRootFileNewFormat("../../2015_July_Testbeam_tdc/JulyMuons20_79.root", -1);
                tdcManager->readRootFileNewFormat("../../2015_July_Testbeam_tdc/JulyMuons80_169.root", -1);
                tdcManager->readRootFileNewFormat("../../2015_July_Testbeam_tdc/JulyMuons169_204.root", -1);
                tdcManager->readRootFileNewFormat("../../2015_July_Testbeam_tdc/JulyMuons613-663.root", -1);

                // tdcManager->readRootFileNewFormat("../../2015_July_Testbeam_tdc/Track_24017.root", -1);
                // tdcManager->readRootFileNewFormat("../../2015_July_Testbeam_tdc/Track_24018.root", -1);
                // tdcManager->readRootFileNewFormat("../../2015_July_Testbeam_tdc/Track_24019.root", -1);
            } else if(tdcManager->getDataset() == std::string("Aug15") ) {
                tdcManager->readRootFileNewFormat("../Hits/muons_newFormat_Tracks.root", -1);
            }
            // tdcManager->readRootFileNewFormat("../Hits/17_05_15_hits_Muons_30040-30049.root", -1);
            // tdcManager->readRootFileNewFormat("../Hits/17_05_15_hits_Muons_30050-30059.root", -1);
            // tdcManager->readRootFileNewFormat("../Hits/17_05_15_hits_Muons_30060-30069.root", -1);
            // tdcManager->readRootFileNewFormat("../Hits/17_05_15_hits_Muons_30070-30077.root", -1);
            // tdcManager->readRootFileNewFormat("../Hits/17_05_15_hits_Muons_30445-30449.root", -1);
            // tdcManager->readRootFileNewFormat("../Hits/17_05_15_hits_Muons_30450-30454.root", -1);
            // tdcManager->readRootFileNewFormat("../Hits/17_05_15_hits_Muons_30460-30464.root", -1);
            // tdcManager->readRootFileNewFormat("../Hits/17_05_15_hits_Muons_30465-30469.root", -1);
            // tdcManager->readRootFileNewFormat("../Hits/17_05_15_hits_Muons_30470-30475.root", -1);
        } else if(tdcManager->isElectron()) {
            tdcManager->readRootFileNewFormat("../Hits/electrons_newFormat.root", -1);
        } else if(tdcManager->isPion()) {
            // tdcManager->readRootFileNewFormat("../Hits/pions_newFormat_validated.root", -1);
             //tdcManager->readRootFileNewFormat("../Hits/17_05_15_hits_Pions_10GeV.root ", -1);
             if(tdcManager->getEnergy() == 70) {
                if(tdcManager->getDataset() == std::string("Jul15")) {
                    tdcManager->readRootFileNewFormat("../../2015_July_Testbeam_tdc/pions_july_70GeV.root", -1);
                } else {
                    tdcManager->readRootFileNewFormat("../Hits/pions_newFormat.root", -1);
                }
             } else if(tdcManager->getEnergy() == 10) {
                 tdcManager->readRootFileNewFormat("../Hits/17_05_15_hits_Pions_10GeV.root", -1);
             } else if(tdcManager->getEnergy() == 30) {
                 tdcManager->readRootFileNewFormat("../Hits/17_07_28_hits_Pions_30GeV.root", -1);
             } else if(tdcManager->getEnergy() == 50) {
                 tdcManager->readRootFileNewFormat("../Hits/17_07_28_hits_Pions_50GeV.root", 1000000);
             } else if(tdcManager->getEnergy() == 90) {
                 tdcManager->readRootFileNewFormat("../Hits/17_07_28_hits_Pions_90GeV.root", -1);
             }
        }

    } else {
        if(tdcManager->isMuon()) {
            // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Muons_Sim/muons_009.root");
            tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Muons_Sim/muons_test_beam_profile.root", -1);
            // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Muons_Sim/muons_009.root", -1);
        } else if(tdcManager->isElectron()) {
            // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Electrons_Sim/electrons_008_10GeV_noSteel.root", -1);
            // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Electrons_Sim/electrons_009_10GeV.root", -1);
            // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Electrons_Sim/electrons_009_15GeV_noSteel.root", -1);
            // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Electrons_Sim/electrons_009_20GeV_noSteel.root", -1);
            // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Electrons_Sim/electrons_009_10GeV_noSteel.root", -1);
            tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Electrons_Sim/electrons_009_20GeV_withSteel.root", -1);
            // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Electrons_Sim/electrons_009_20GeV_noSteel.root", -1);
            // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Electrons_Sim/electrons_009_20GeV_withSteelFar.root", -1);
            //tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Electrons_Sim/electrons_009_70GeV.root", -1);
        } else if(tdcManager->isPion()) {
            // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_QGSP_BERT_HD_reco_hcal_004_beamSmearing.root", -1);
            // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_QGSP_BERT_HD_reco_hcal_005_noSteel.root", -1);
        //    tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_006_70GeV_withSteelFar.root", -1);
        //    tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_006_70GeV_moreSteel.root", -1);
            // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_006_70GeV_moreSteel.root", -1);
            //  tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_006_10GeV_withSteelFar.root", -1);
            //tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/kaon_reco_hcal_006_70GeV.root", -1);
            // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/anti_proton_reco_hcal_006_70GeV.root", -1);

            // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_006_QGSP_BERT.root", -1);
            // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_006_70GeV_steelNear_doppler.root", -1);
            // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_006_70GeV_steelNear.root", 0);
            if(tdcManager->getEnergy() == 90) {
                tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_006_90GeV_steelNear_positive.root", -1);
                 //tdcManager->readRootFileNewFormat("../Hits/17_08_08_Pions_QGSP_BERT_90GeV.root", -1);
            } else if(tdcManager->getEnergy() == 70) {
                // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_006_70GeV_steelNear_plus.root", -1);
                // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_006_70GeV_steelNear_noTimeSmearing.root", -1);
                // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_006_QBBC_70GeV_steelNear_positive.root", -1);
                // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_006_QBBC_70GeV_T3BTimeSmearing.root", -1);
                // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_006_QGSP_BERT_HP_70GeV_T3BTimeSmearing.root", -1);
                // LAST LAYER 38 wurde "ueberschrieben!!!!" Neu simulieren!!!!
                // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_006_QGSP_BERT_HP_70GeV_lastLayer38_T3BResolution.root", -1);
                // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_007_QGSP_BERT_HP_70GeV_lastLayer38_fullPlastic.root", -1);
                // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_007_QGSP_BERT_HP_70GeV_lastLayer30.root", -1);
                tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_007_QGSP_BERT_HP_70GeV_reference.root", -1);
                // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_007_QGSP_BERT_HP_70GeV_lastLayer38_fullPlastic_T3BResolution.root", -1);
                // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/testTruthShowerStart.root", -1);
                //   tdcManager->readRootFileNewFormat("../Hits/17_08_08_Pions_QGSP_BERT_70GeV.root", -1);
            } else if(tdcManager->getEnergy() == 50) {
                tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_006_50GeV_steelNear_positive.root", -1);
                 // tdcManager->readRootFileNewFormat("../Hits/17_08_08_Pions_QGSP_BERT_50GeV.root", -1);
            } else if(tdcManager->getEnergy() == 30) {
                tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_006_30GeV_steelNear_positive.root", -1);
                 //tdcManager->readRootFileNewFormat("../Hits/17_08_08_Pions_QGSP_BERT_30GeV.root", -1);
            } else if(tdcManager->getEnergy() == 10) {
                tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_006_10GeV_steelNear_positive.root", -1);
                 // tdcManager->readRootFileNewFormat("../Hits/17_08_08_Pions_QGSP_BERT_10GeV.root", -1);
            }
            // tdcManager->readRootFileNewFormat("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_reco_hcal_", -1);
        }

    }

    if(not doCalibration) {
        analysis->analyze_hitDistribution( std::string("pre") );
        analysis->analyze_t0s();
    }


    tdcManager->selectEvents();

    tdcManager->processT0s(); // Not needed for Simulation data

    // Only needed to test wether it is important if time smearing is happening before
    // or after reconstration (no, there is no difference)
    // tdcManager->doTimeSmearing();


    tdcManager->sortDataPerChannel(); // only needed for calibration? XXX Check this

    if(doCalibration) {
        writeCalibrationConstants();
    }

    if(not tdcManager->isSimulatedData()) {
        // tdcManager->calibrate(true, false, true, true);
        tdcManager->calibrate(true, false, true, true);
        analysis->analyze_calibration(std::string("timewalk"));

        // tdcManager->calibrate(true, true, true, false);
        tdcManager->calibrate(true, true, true, false);
        analysis->analyze_calibration(std::string("nHitsSameChip"));

        // tdcManager->calibrate(true, true, true, true);
        tdcManager->calibrate(true, true, true, true);
    }


    analysis->analyze_hitDistribution( std::string("hits") );
    analysis->analyze_general();
    analysis->analyze_timing();

    analysis->writeFigures("analysis.root");



    // tdcManager->writeProcessedRootFile("./../Hits/pions_newFormat_validated.root");

    // Simulation stuff
    // TDCcali->writeSimulationTiming("cali_output/mc_aug_muons_006.root");

    // TDCcali->sortDataPerChannel(); // Only needed if Fit calibration or nonLinearityCorrection

    // TDCcali->fit_calibration();
    // TDCcali->writeFitCalibration("fitCalibration_electrons");
    // TDCcali->readFitCalibration("./fitCalibration_eventTime.txt");
    // tdcManager->readFitCalibration("./fitCalibration_new.txt");

    // TDCcali->nonLinearityCorrection();
    // TDCcali->writeNonLinearityCorrection("nonLinearityCorrection_electrons.txt");
    // TDCcali->readNonLinearityCorrection("nonLinearityCorrection_eventTime.txt");
    // tdcManager->readNonLinearityCorrection("nonLinearityCorrection_new.txt");

    // tdcManager->shiftChipsTo0();
    // TDCcali->shiftSimulationTo0();

    // TDCcali->doTimeWalkCorrection();

    // tdcManager->analyze();

    // delete TDCcali;

    return 0;
}
