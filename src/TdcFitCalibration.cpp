#include "TdcFitCalibration.h"

#include <fstream>
#include <iostream>
#include <sstream>

//#include <omp.h>
#include "TF1.h"
#include "TFile.h"
#include "TGraph.h"
#include "TH1.h"
#include "TH2.h"
#include "THStack.h"
#include "TKDE.h"
#include "TMath.h"
#include "TTree.h"

// Initialization
TdcFitCalibration::TdcFitCalibration() {
    std::cout << std::endl;
    std::cout << "Creating TDC Calibration" << std::endl;
}

TdcFitCalibration::TdcFitCalibration(TdcManager* tdcManager) {
    TM = tdcManager;

    _v_chipIDs = TM->getChipIDs();
    _nChannels = TM->getNChannels();
    _nMemoryCells = TM->getNMemoryCells();

    TdcFitCalibration();
}



/* Fit calibration
 * Sometimes the robust fit crashes. I have no idea why this happens.
 * Possible solution: Try catch? Probably not...
 */

void TdcFitCalibration::fit_calibration() {
    // if(not fHasReadRootFile || not fHasReadT0Calibration) {
    //     std::cout << (fHasReadRootFile ? "T0 Calibration" : "Hit Root File");
    //     std::cout << " was not read in ... aborting." << std::endl;
    //     return;
    // }

    std::cout << "Do the fit calibration ..." << std::endl;

    TFile* o_file = new TFile("fits.root", "recreate");

    if(o_file->IsOpen()) {

        // #pragma omp parallel num_threads(5)
        // #pragma omp for
        for(unsigned int i=0; i < _v_chipIDs.size(); i++) {
            int chipID = _v_chipIDs[i];

            int totalHitsThisChip = 0;
            std::cout << "Processing hits in chip " << chipID;
            double chipSlope_even = -1;
            double chipSlope_odd = -1;
            bool fCalculatedChipSlope = false;

            for(int iMem = 0; iMem < 16; iMem++) {

                // Calculate slope for the chip, after all the memCell 1 were done
                if(iMem > 1 && not fCalculatedChipSlope) {
                    std::vector<double> v_chnSlopes_even;
                    std::vector<double> v_chnSlopes_odd;
                    for(int iChn = 0; iChn < _nChannels + 1; iChn++) {
                        double chnSlope_even = std::get<1>(_m_calibration_values[chipID][iChn][1][0]);
                        double chnSlope_odd = std::get<1>(_m_calibration_values[chipID][iChn][1][1]);
                        if(chnSlope_even > 0) v_chnSlopes_even.push_back(chnSlope_even);
                        if(chnSlope_odd > 0) v_chnSlopes_odd.push_back(chnSlope_odd);
                    }
                    // get the median of all slopes
                    if(v_chnSlopes_even.size() > 0) {
                        std::sort(v_chnSlopes_even.begin(), v_chnSlopes_even.end());
                        chipSlope_even = v_chnSlopes_even[floor(v_chnSlopes_even.size()/2)];
                    }

                    if(v_chnSlopes_odd.size() > 0) {
                        std::sort(v_chnSlopes_odd.begin(), v_chnSlopes_odd.end());
                        chipSlope_odd = v_chnSlopes_odd[floor(v_chnSlopes_odd.size()/2)];
                    }

                    fCalculatedChipSlope = true;
                    //repeat the first memory cell
                    iMem--;
                }

                for(int iChn = 0; iChn < _nChannels + 1; iChn++) {
                    // if(iChn == 36 || chipID == 154) continue; // fit is not working
                    for(int iBx=0; iBx < 2; iBx++) {
                        // if(iChn == 19 && chipID == 156 && iMem == 1 && iBx == 1) continue; // fit is not working
                        int nHits = TM->_m_tdcs[chipID][iChn][iMem][iBx].size();
                        totalHitsThisChip += nHits;
                        std::cout << chipID << " " << iChn << " " << iMem << " " << iBx << "\t" << nHits << std::endl;


                        if(nHits > 50) {
                            std::stringstream ss_title;
                            ss_title << chipID << " " << iChn << " " << iMem << " " << iBx;

                            std::vector<double> T0s;
                            std::vector<double> TDCs;
                            int  c = 0;
                            for(auto& p: TM->_m_tdcs[chipID][iChn][iMem][iBx]) {
                                if(c > 4000) break; //cap the number of entries in the fit. Not sure if this is necessary

                                double T0 = p.first;
                                double TDC = p.second.tdc;

                                //std::cout << T0 << "\t" << TDC << std::endl;

                                if(T0 > 500 && T0 < 3500 && TDC > 500 && TDC < 3000) {
                                    T0s.push_back(T0);
                                    TDCs.push_back(TDC);
                                    c++;
                                }
                            }

                            if(c < 50) continue;

                            TGraph* gr_ramp = new TGraph(TDCs.size()-2, &(TDCs[1]), &(T0s[1]));
                            gr_ramp->SetName(ss_title.str().c_str());
                            gr_ramp->SetTitle(ss_title.str().c_str());
                            gr_ramp->GetXaxis()->SetTitle("Hit [tdc]");
                            gr_ramp->GetYaxis()->SetTitle("T0 [ns]");
                            gr_ramp->SetMinimum(500);
                            gr_ramp->SetMaximum(3500);

                            TF1* f_lin = new TF1("f_lin", "pol1", 500, 3000);
                            f_lin->SetParameter(0,-1000.);
                            f_lin->SetParameter(1,1.6);

                            // XXX: Delete me
                            gr_ramp->Write();

                            // in the first iteration, do a robust fit,
                            // in the second, fix the slope parameter
                            gr_ramp->Fit("f_lin","Qrob=0.95", "", 500, 3000);

                            gr_ramp->Write();

                            // In the first iteration: take the results from the robust fit
                            double offset = f_lin->GetParameter(0);
                            double slope = f_lin->GetParameter(1);
                            // double quad = f_quad->GetParameter(2);
                            double quad = 0.;

                            auto tuple = std::make_tuple(offset, slope, quad);
                            _m_calibration_values[chipID][iChn][iMem][iBx] = tuple;

                            if(not fCalculatedChipSlope) {
                                delete gr_ramp;
                                delete f_lin;

                                continue;
                            }
                            // if we are in the second iteration, reject outliers

                            slope = iBx == 0 ? chipSlope_even: chipSlope_odd;

                            std::vector<double> T0s_rob;
                            std::vector<double> TDCs_rob;
                            if(offset != 0 && slope > 0) {
                                for(auto& p: TM->_m_tdcs[chipID][iChn][iMem][iBx]) {
                                    double T0 = p.first;
                                    double TDC = p.second.tdc;

                                    if(fabs(offset+TDC*slope - T0) < 10) {
                                        T0s_rob.push_back(T0);
                                        TDCs_rob.push_back(TDC);
                                    }

                                }
                            }

                            if(TDCs_rob.size() > 50) {

                                TGraph* gr_ramp_fix = new TGraph(TDCs_rob.size(), &(TDCs_rob[0]), &(T0s_rob[0]));
                                ss_title << " fix";
                                gr_ramp_fix->SetName(ss_title.str().c_str());
                                gr_ramp_fix->SetTitle(ss_title.str().c_str());
                                gr_ramp_fix->GetXaxis()->SetTitle("Hit [tdc]");
                                gr_ramp_fix->GetYaxis()->SetTitle("T0 [ns]");
                                gr_ramp_fix->SetMinimum(500);
                                gr_ramp_fix->SetMaximum(3000);

                                TF1* f_lin_fix = new TF1("f_lin_fix", "pol1", 0, 4000);
                                f_lin_fix->FixParameter(1, iBx==0 ? chipSlope_even : chipSlope_odd);

                                gr_ramp_fix->Fit("f_lin_fix","QB", "", 500, 3000);

                                gr_ramp_fix->Write();

                                offset = f_lin_fix->GetParameter(0);
                                slope = f_lin_fix->GetParameter(1);
                                // double quad = f_quad->GetParameter(2);
                                double quad = 0.;

                                auto new_tuple = std::make_tuple(offset, slope, quad);
                                _m_calibration_values[chipID][iChn][iMem][iBx] = new_tuple;

                                delete gr_ramp_fix;
                                delete f_lin_fix;
                            } else {
                                _m_calibration_values[chipID][iChn][iMem][iBx] = std::make_tuple(0,0,0);
                            }

                            delete gr_ramp;
                            delete f_lin;
                        }
                        else {
                            _m_calibration_values[chipID][iChn][iMem][iBx] = std::make_tuple(0,0,0);
                        }
                    }
                }
            }
            std::cout << "\twith " << totalHitsThisChip << "\tHits" << std::endl;
        }
    }

    o_file->Close();
}


void TdcFitCalibration::writeFitCalibration(const char* fn_fitCalibration) {

    std::cout << std::endl;
    std::cout << "Writing calibration constants to " << fn_fitCalibration << std::endl;

    std::ofstream out_file;
    out_file.open(fn_fitCalibration);

    out_file << "ChipID\tChannel\tMemoryCell\tBxID\tOffstet\tSlope\n";

    for(auto& chipID: _v_chipIDs) {
        for(int iChn=0; iChn < _nChannels+1; iChn++) {
            for(int iMem=0; iMem < 16; iMem++) {
                for(int iBx=0; iBx < 2; iBx++) {
                    auto tupel = _m_calibration_values[chipID][iChn][iMem][iBx];
                    double offset = std::get<0>(tupel);
                    double slope = std::get<1>(tupel);
                    if(offset != 0 && slope != 0) {
                        out_file << chipID << "\t" << iChn << "\t" << iMem << "\t" << iBx << "\t";
                        out_file << offset << "\t" << slope << "\n";
                    }
                }
            }
        }
    }

    out_file.close();

    return;
}


/*
    - fit_calibraiton has to be done before
    non linearity correction:
*/
void TdcFitCalibration::nonLinearityCorrection() {
    std::cout << std::endl;
    std::cout << "Do non Linearity Correction ..." << std::endl;

    TFile* o_file = new TFile("nonLin.root", "recreate");

    if(o_file->IsOpen()) {

        for(auto& chipID: _v_chipIDs) {
            for(int iBx= 0; iBx < 2; iBx++) {

                std::vector<double> T0s;
                std::vector<double> dTs;

                for(auto& p: TM->_m_tdcs[chipID][36][1][iBx]) {
                    double T0 = p.first;
                    Hit hit = p.second;

                    hit.ns = -1;
                    TM->tdcToNs(hit, false, false, false, false);

                    if(hit.ns > 0) {
                        T0s.push_back(hit.ns);
                        dTs.push_back(T0-hit.ns);
                    } else {
                        continue;
                    }
                }

                if(T0s.size() < 50) continue;

                std::stringstream ss_title;
                ss_title << chipID << " " << iBx;

                TGraph* gr_nonLin = new TGraph(T0s.size(), &(T0s[0]), &(dTs[0]));
                gr_nonLin->SetName(ss_title.str().c_str());
                gr_nonLin->SetTitle(ss_title.str().c_str());
                gr_nonLin->GetXaxis()->SetTitle("T0 - Hit [ns]");
                gr_nonLin->GetYaxis()->SetTitle("Hit [ns]");
                gr_nonLin->SetMinimum(-50);
                gr_nonLin->SetMaximum(50);

                TF1* f_quad = new TF1("f_quad", "pol2", 0, 4000);
                f_quad->SetParameter(0,0.);
                f_quad->SetParameter(1,0.);
                f_quad->SetParameter(2,0.);

                // in the first iteration, do a robust fit,
                // in the second, fix the slope parameter
                gr_nonLin->Fit("f_quad","Qrob=0.85", "", 500, 3000);
                gr_nonLin->Write();

                double a = f_quad->GetParameter(0);
                double b = f_quad->GetParameter(1);
                double c = f_quad->GetParameter(2);

                _m_nonLinearityCorrection[chipID][iBx] = std::make_tuple(a,b,c);

            }
        }
    }

    o_file->Close();
}


/*
    Write non Linearity Correction to file
*/
void TdcFitCalibration::writeNonLinearityCorrection(const char* fn_nonLinearity) {

    std::cout << std::endl;
    std::cout << "Writing nonLinearity correction constants to " << fn_nonLinearity << std::endl;

    std::ofstream out_file;
    out_file.open(fn_nonLinearity);

    out_file << "ChipID\tBxID\tConst\tLinear\tQuadratic\n";

    for(auto& chipID: _v_chipIDs) {
        for(int iBx=0; iBx < 2; iBx++) {
            auto tupel = _m_nonLinearityCorrection[chipID][iBx];
            double constant = std::get<0>(tupel);
            double lin = std::get<1>(tupel);
            double quad = std::get<2>(tupel);
            if(quad != 0) {
                out_file << chipID << "\t" << iBx << "\t";
                out_file << constant << "\t" << lin << "\t" << quad << "\n";
            }
        }
    }

    out_file.close();

    return;
}


/*
    - tdcToNs conversion is assumed to be done already
    TimeWalk Correction:
*/
void TdcFitCalibration::doTimeWalkCorrection() {
    std::cout << std::endl;
    std::cout << "Do time walk correction" << std::endl;

    double mip_stepsize = 0.1; // mip

    std::map<int, std::vector<double> > mv_mip_dTs;

    for(auto& evt: TM->_v_events) {
        double T0_time = evt.T0_time;
        if(T0_time == 0) continue;

        for(auto& hit: evt.v_hits) {
            TM->tdcToNs(hit, true, false, false, false);
                // hit.ns = hit.ns - _m_shift[hit.K];
            if(hit.ns > 500 && hit.ns < 3500) {
                int key = floor(hit.mip/mip_stepsize);
                mv_mip_dTs[key].push_back(hit.ns - T0_time);
            }
        }
    }

    std::vector<double> v_mip;
    std::vector<double> v_median_dTs;
    for(auto& m: mv_mip_dTs) {
        int key = m.first;
        std::vector<double> v_dTs = m.second;

        if(v_dTs.size() < 100) continue;

        //calculate median
        std::sort(v_dTs.begin(), v_dTs.end());
        double median_dT = v_dTs[floor(v_dTs.size()/2)];

        v_median_dTs.push_back(median_dT);
        v_mip.push_back(key*mip_stepsize);
    }

    // _gr_timewalk = new TGraph(v_mip.size(), &(v_mip[0]), &(v_median_dTs[0]));
    // _gr_timewalk->SetName("gr_timewalk");
    // _gr_timewalk->SetTitle("Time Walk");
    // _gr_timewalk->GetXaxis()->SetTitle("Mip");
    // _gr_timewalk->GetYaxis()->SetTitle("dT");
    //
    // TF1* f_exp = new TF1("f_exp", "[0]+[1]*TMath::Exp([2]*x)", 0, 10);
    // f_exp->SetParameter(0,-3.);
    // f_exp->SetParameter(1,-7.);
    // f_exp->SetParameter(2,-1.);
    // _gr_timewalk->Fit("f_exp", "Q", "", 0, 8);

    // double tw_a = f_exp->GetParameter(0);
    // double tw_b = f_exp->GetParameter(1);
    // double tw_c = f_exp->GetParameter(2);

    // _tw_coeff = std::make_tuple(tw_a, tw_b, tw_c);

    return;
}
