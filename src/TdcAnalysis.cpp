#include "TdcAnalysis.h"

#include <cassert>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <iterator>
#include <set>
#include <sstream>

#include "TF1.h"
#include "TFile.h"
#include "TGraph.h"
#include "TH1.h"
#include "TH2.h"
#include "THStack.h"
#include "TKDE.h"
#include "TMath.h"
#include "TMinuit.h"
#include "TProfile.h"
#include "TTree.h"


// Calculates the median of a TH1
double Median(const TH1D * h1) {
   int n = h1->GetXaxis()->GetNbins();
   std::vector<double>  x(n);
   h1->GetXaxis()->GetCenter( &x[0] );
   const double * y = h1->GetArray();
   // exclude underflow/overflows from bin content array y
   return TMath::Median(n, &x[0], &y[1]);
}

TH1D* getFractionHistogramFromTH2(TH2D* h2, bool invertAxis = false) {
    int xBins = h2->GetXaxis()->GetNbins();
    int yBins = h2->GetYaxis()->GetNbins();
    double xMin = h2->GetXaxis()->GetXmin();
    double xMax = h2->GetXaxis()->GetXmax();
    double yMin = h2->GetYaxis()->GetXmin();
    double yMax = h2->GetYaxis()->GetXmax();

    // double totalIntegral = h2->Integral();

    std::stringstream ss_name;
    ss_name << std::string(h2->GetName()) << "_fraction";

    TH1D* h;
    TProfile* h_profile;
    double cummulativeFraction = 0;
    if(invertAxis) {
        h_profile = h2->ProfileY();
        h = new TH1D(ss_name.str().c_str(), h2->GetTitle(), yBins, yMin, yMax);
        h->GetXaxis()->SetTitle(h2->GetYaxis()->GetTitle());
        h->GetYaxis()->SetTitle("Fraction");
        for(int bin = 0; bin < yBins; bin++) {
            double fraction = h2->Integral(0, xBins, bin, bin) * h_profile->GetBinContent(bin);
            cummulativeFraction += fraction;
            h->SetBinContent(bin, cummulativeFraction);
        }
        h->Scale(1./h->GetBinContent(yBins-1));
    } else {
        h_profile = h2->ProfileX();
        h = new TH1D(ss_name.str().c_str(), h2->GetTitle(), xBins, xMin, xMax);
        h->GetXaxis()->SetTitle(h2->GetXaxis()->GetTitle());
        h->GetYaxis()->SetTitle("Fraction");
        for(int bin = 0; bin < xBins; bin++) {
            double fraction = h2->Integral(bin, bin, 0, yBins) * h_profile->GetBinContent(bin);
            cummulativeFraction += fraction;
            h->SetBinContent(bin, cummulativeFraction);
        }
        h->Scale(1./h->GetBinContent(xBins-1));
    }


    return h;
}


// Initialization
TdcAnalysis::TdcAnalysis() {
    std::cout << std::endl;
    std::cout << "Creating TDC Analysis" << std::endl;


    int nLayers = 16;
    for(int i=0; i<nLayers; i++) {
        if(i < 12) {
            _nChannelsX[i] = 24;
            _nChannelsY[i] = 24;
        } else {
            _nChannelsX[i] = 24;
            _nChannelsY[i] = 24;
        }
    }

    _nLayers = nLayers;
}


/*
    Constructor, needs a tdcManager to initialize proper geometry and parameters
*/
TdcAnalysis::TdcAnalysis(TdcManager* tdcManager) {
    TM = tdcManager;

    // Getting parameters from manager
    _v_chipIDs = TM->getChipIDs();

    _nChannels = TM->getNChannels();
    _nChannelsX = TM->getNChannelsX();
    _nChannelsY = TM->getNChannelsY();

    _nMemoryCells = TM->getNMemoryCells();
    _nLayers = TM->getNLayers();

    TdcAnalysis();
}


/*
    Writing event maps using the HitMap class. For all events in v_eventsForHitMap
    a hitmap is created to visualize the hits in this event. The canvases are
    written to map_events.root
*/
void TdcAnalysis::writeEventMaps(std::vector<Event*> v_eventsForHitMap) {
    // Writing Hitmap for single Events
    std::cout << "Number of events for Hitmaps: " <<  v_eventsForHitMap.size() << std::endl;
    TFile* hitMap_file = new TFile("map_events.root", "recreate");
    if(hitMap_file->IsOpen()) {
        int count = 0;
        for(auto evt: v_eventsForHitMap) {
            std::vector<double> v_delay;
            if(count > 200) break;
            std::stringstream ss_name;
            ss_name << "event_" << count;

            HitMap* hitmap = new HitMap(_nChannelsX, _nChannelsY, ss_name.str());
            for(auto& hit: evt->v_hits) {

                double delay = hit.ns - evt->T0_time ;
                v_delay.push_back(delay);

                hitmap->set(hit.I, hit.J, hit.K, delay+200);
            }

            hitmap->commonZScale(150,2000);

            std::sort(v_delay.begin(), v_delay.end());
            std::cout << "Event ID: " << evt->ID << std::endl;
            std::cout << "nHits: " << evt->v_hits.size() << "\t" << "median delay: " << v_delay[v_delay.size()/2] << std::endl;

            hitmap->write(hitMap_file);

            delete hitmap;

            count++;
        }
    }
    hitMap_file->Close();
    delete hitMap_file;
}


double DoubleGaussConv(double *x, double *par) {
    double Norm1 = par[0];
    double arg1 = (x[0] - par[1] + par[8])*(x[0] - par[1] + par[8])/( 2*( par[2]*par[2] + par[6]*par[6] ) );

    double Norm2 = par[3];
    double arg2 = (x[0] - par[4] + par[8])*(x[0] - par[4] + par[8])/( 2*( par[5]*par[5] + par[6]*par[6] ) );

    return par[7] * (Norm1 * TMath::Exp( - arg1 ) + Norm2 * TMath::Exp( - arg2 )) ;
}

void TdcAnalysis::init_hitDistribution(Analysis::HitDistribution* figHits, std::string prefix) {
    figHits->m_ijk_calibratedHits.clear();
    figHits->m_ijk_notCalibratedHits.clear();

    std::stringstream ss_name;
    std::stringstream ss_title;
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "hitmap_" << prefix;
    figHits->hitmap = new HitMap(_nChannelsX, _nChannelsY, std::string(ss_name.str()));

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "hitmap_" << prefix << "_nHitsSameChip";
    figHits->hitmap_nHitsSameChip = new HitMap(_nChannelsX, _nChannelsY, std::string(ss_name.str()));

    if(prefix == std::string("hits")) {
        ss_name.str(std::string()); ss_name.clear();
        ss_name << "hitmap_" << prefix << "_calibratedMemoryCells";
        figHits->hitmap_calibratedMemoryCells = new HitMap(_nChannelsX, _nChannelsY, std::string(ss_name.str()));
    }

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "hitmap_" << prefix << "_notCalibratedHits";
    figHits->hitmap_notCalibratedHits = new HitMap(_nChannelsX, _nChannelsY, std::string(ss_name.str()));

    // CoG Z
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_hitZ";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Hit Positon Z";
    ss_title << ((prefix == std::string("pre")) ? "before data selection" : "after data selection");
    figHits->h_posZ = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 2000, -1000, 1000);
    figHits->h_posZ->GetXaxis()->SetTitle("hitPos Z [mm]");
    figHits->h_posZ->GetYaxis()->SetTitle("# Entries");

    // CoG X
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_hitX";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Hit Position X";
    ss_title << ((prefix == std::string("pre")) ? "before data selection" : "after data selection");
    figHits->h_posX = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 1000, -500, 500);
    figHits->h_posX->GetXaxis()->SetTitle("hitPos X [mm]");
    figHits->h_posX->GetYaxis()->SetTitle("# Entries");

    // CoG Y
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_hitY";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Hit Positon Y";
    ss_title << ((prefix == std::string("pre")) ? "before data selection" : "after data selection");
    figHits->h_posY = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 1000, -500, 500);
    figHits->h_posY->GetXaxis()->SetTitle("hitPos Y [mm]");
    figHits->h_posY->GetYaxis()->SetTitle("# Entries");

    // CoG Z
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_cogZ";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Center of Gravity in Z Direction ";
    ss_title << ((prefix == std::string("pre")) ? "before data selection" : "after data selection");
    figHits->h_cogZ = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 1000, 0, 1000);
    figHits->h_cogZ->GetXaxis()->SetTitle("CoG Z [mm]");
    figHits->h_cogZ->GetYaxis()->SetTitle("# Entries");

    // CoG X
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_cogX";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Center of Gravity in X Direction ";
    ss_title << ((prefix == std::string("pre")) ? "before data selection" : "after data selection");
    figHits->h_cogX = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 1000, -500, 500);
    figHits->h_cogX->GetXaxis()->SetTitle("CoG X [mm]");
    figHits->h_cogX->GetYaxis()->SetTitle("# Entries");

    // CoG Y
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_cogY";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Center of Gravity in Y Direction ";
    ss_title << ((prefix == std::string("pre")) ? "before data selection" : "after data selection");
    figHits->h_cogY = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 1000, -500, 500);
    figHits->h_cogY->GetXaxis()->SetTitle("CoG Y [mm]");
    figHits->h_cogY->GetYaxis()->SetTitle("# Entries");

    // nHits
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_nHits";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Number of Hits ";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h_nHits = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 200, 0, 200);
    figHits->h_nHits->GetXaxis()->SetTitle("# Hits");
    figHits->h_nHits->GetYaxis()->SetTitle("# Entries");

    // singleHitEnergy
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_singleHitEnergy";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Single Hit Energy ";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h_singleHitEnergy = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 2000, 0, 200);
    figHits->h_singleHitEnergy->GetXaxis()->SetTitle("Energy [MIP]");
    figHits->h_singleHitEnergy->GetYaxis()->SetTitle("# Entries");

    // Mean number of Hits per Layer
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_mean_nHitsPerLayer";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Mean Number of Hits per Layer ";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h_mean_nHitsPerLayer = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20);
    figHits->h_mean_nHitsPerLayer->GetXaxis()->SetTitle("Layer Number");
    figHits->h_mean_nHitsPerLayer->GetYaxis()->SetTitle("<# Hits per Layer>");

    // Mean energy per Layer
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_mean_energyPerLayer";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Mean Energy per Layer ";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h_mean_energyPerLayer = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20);
    figHits->h_mean_energyPerLayer->GetXaxis()->SetTitle("Layer Number");
    figHits->h_mean_energyPerLayer->GetYaxis()->SetTitle("<Energy per Layer> [MIP]");

    // Mean number of Hits per Chip
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_mean_nHitsPerChip";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Mean Number of Hits per Chip ";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h_mean_nHitsPerChip = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 100, 140, 240);
    figHits->h_mean_nHitsPerChip->GetXaxis()->SetTitle("Chip Number");
    figHits->h_mean_nHitsPerChip->GetYaxis()->SetTitle("<# Hits per Chip>");

    // number of Hits in the same chip
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_nHitsSameChip";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Number of Hits in the Same Chip ";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h_nHitsSameChip = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20);
    figHits->h_nHitsSameChip->GetXaxis()->SetTitle("# Hits in the Same Chip");
    figHits->h_nHitsSameChip->GetYaxis()->SetTitle("# Entries");

    // number of Hits in the same chip
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_memCell";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Memory Cells";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h_memCell = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20);
    figHits->h_memCell->GetXaxis()->SetTitle("MemoryCell");
    figHits->h_memCell->GetYaxis()->SetTitle("# Entries");

    // Shower Start
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_showerStart";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Shower Start";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h_showerStart = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20);
    figHits->h_showerStart->GetXaxis()->SetTitle("Shower Start Layer");
    figHits->h_showerStart->GetYaxis()->SetTitle("# Entries");

    // MC Truth Shower Start
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_MCTruthShowerStart";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "MC Truth Shower Start";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h_MCTruthShowerStart = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20);
    figHits->h_MCTruthShowerStart->GetXaxis()->SetTitle("MC Truth Shower Start Layer");
    figHits->h_MCTruthShowerStart->GetYaxis()->SetTitle("# Entries");

    // Shower Start vs MCTruth
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_" << prefix << "_showerStart_vs_MCTruth";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Shower Start vs MCTruth";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h2_showerStart_vs_MCTruth = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20, 20, 0, 20);
    figHits->h2_showerStart_vs_MCTruth->GetXaxis()->SetTitle("ShowerStart [layers]");
    figHits->h2_showerStart_vs_MCTruth->GetYaxis()->SetTitle("MC Truth [layers]");

    // Shower Start vs MCTruth
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_showerStart_vs_MCTruth";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Shower Start vs MCTruth";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h_showerStart_vs_MCTruth = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 40, -20, 20);
    figHits->h_showerStart_vs_MCTruth->GetXaxis()->SetTitle("ShowerStart - MC Truth [layers]");
    figHits->h_showerStart_vs_MCTruth->GetYaxis()->SetTitle("# Entries");

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_" << prefix << "_showerDepth_vs_layer";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Reconstructed Shower Depth vs Layer";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h2_showerDepth_vs_Layer = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 16, 0, 16, 800, 0, 800);
    figHits->h2_showerDepth_vs_Layer->GetXaxis()->SetTitle("Layer");
    figHits->h2_showerDepth_vs_Layer->GetYaxis()->SetTitle("Reco Shower Depth [mm]");

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_" << prefix << "_trueShowerStart_vs_layer";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "True Shower Depth vs Layer";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h2_trueShowerDepth_vs_Layer = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 16, 0, 16, 800, 0, 800);
    figHits->h2_trueShowerDepth_vs_Layer->GetXaxis()->SetTitle("Layer");
    figHits->h2_trueShowerDepth_vs_Layer->GetYaxis()->SetTitle("True Shower Depth [mm]");

    // Energy Fraction over Radius
    // ss_name.str(std::string()); ss_name.clear();
    // ss_name << "h_" << prefix << "_energyFraction_over_radius";
    // ss_title.str(std::string()); ss_title.clear();
    // ss_title << "Energy Fraction over Radius ";
    // ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    // figHits->h_energyFraction_over_radius = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 500, 0, 500);
    // figHits->h_energyFraction_over_radius->GetXaxis()->SetTitle("Radius [mm]");
    // figHits->h_energyFraction_over_radius->GetYaxis()->SetTitle("Energy Fraction");
    //
    // // Energy Fraction over Shower Depth
    // ss_name.str(std::string()); ss_name.clear();
    // ss_name << "h_" << prefix << "_energyFraction_over_shoerDepth";
    // ss_title.str(std::string()); ss_title.clear();
    // ss_title << "Energy Fraction over Shower Depth ";
    // ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    // figHits->h_energyFraction_over_showerDepth = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 800, 0, 800);
    // figHits->h_energyFraction_over_showerDepth->GetXaxis()->SetTitle("Shower Depth [mm]");
    // figHits->h_energyFraction_over_showerDepth->GetYaxis()->SetTitle("Energy Fraction");

    // Energy Sum
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_energySum";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Energy Sum of Event ";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h_energySum = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 1000, 0, 1000);
    figHits->h_energySum->GetXaxis()->SetTitle("Energy Sum [MIP]");
    figHits->h_energySum->GetYaxis()->SetTitle("# Entries");

    // h2 nHits CogZ
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_" << prefix << "_nHits_CoGZ";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "nHits CoGZ ";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h2_nHits_cogZ = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 1000, 0, 1000, 200, 0, 200);
    figHits->h2_nHits_cogZ->GetXaxis()->SetTitle("CoG Z [mm]");
    figHits->h2_nHits_cogZ->GetYaxis()->SetTitle("# Hits");

    // nCherenkov Hits
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_nCherenkov";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Number of Cherenkov hits";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h_nCherenkovHits = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 5, 0, 5);
    figHits->h_nCherenkovHits->GetXaxis()->SetTitle("# Cherenkov Hits");
    figHits->h_nCherenkovHits->GetYaxis()->SetTitle("# Entries");

    // nT0 Hits
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_nT0Hits";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Number of T0 hits";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h_nT0Hits = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 5, 0, 5);
    figHits->h_nT0Hits->GetXaxis()->SetTitle("# T0 Hits");
    figHits->h_nT0Hits->GetYaxis()->SetTitle("# Entries");

    // nTracks
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_nTracks";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Number of Tracks";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h_nTracks = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 5, 0, 5);
    figHits->h_nTracks->GetXaxis()->SetTitle("# Tracks");
    figHits->h_nTracks->GetYaxis()->SetTitle("# Entries");

    // Hit Radius
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_hitRadius";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Hit Radius";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h_hitRadius = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 100, 0, 800);
    figHits->h_hitRadius->GetXaxis()->SetTitle("Hit Radius [mm]");
    figHits->h_hitRadius->GetYaxis()->SetTitle("# Entries");

    // Hit Radius Small Layers
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_hitRadius_smallLayers";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Hit Radius Small Layers ";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h_hitRadius_smallLayer = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 100, 0, 800);
    figHits->h_hitRadius_smallLayer->GetXaxis()->SetTitle("Hit Radius [mm]");
    figHits->h_hitRadius_smallLayer->GetYaxis()->SetTitle("# Entries");

    // Hit Radius Big Layers
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_" << prefix << "_hitRadius_bigLayers";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Hit Radius Big Layers ";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h_hitRadius_bigLayer = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 100, 0, 800);
    figHits->h_hitRadius_bigLayer->GetXaxis()->SetTitle("Hit Radius [mm]");
    figHits->h_hitRadius_bigLayer->GetYaxis()->SetTitle("# Entries");


    // Mean Energy over Hit Radius
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_" << prefix << "_mip_over_hitRadius";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Mean Energy over Hit Radius";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h2_mip_over_hitRadius = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 100, 0, 800, 100, 0, 100);
    figHits->h2_mip_over_hitRadius->GetXaxis()->SetTitle("Hit Radius [mm]");
    figHits->h2_mip_over_hitRadius->GetYaxis()->SetTitle("Energy [MIP]");

    // Mean Energy over shower depth
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_" << prefix << "_mip_over_showerDepth";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Mean Energy over Shower Depth";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h2_mip_over_showerDepth = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 100, 0, 800, 100, 0, 100);
    figHits->h2_mip_over_showerDepth->GetXaxis()->SetTitle("Shower Depth [mm]");
    figHits->h2_mip_over_showerDepth->GetYaxis()->SetTitle("Energy [MIP]");

    // Mean Radius over shower depth
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_" << prefix << "_meanRadius_over_showerDepth";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Mean Radius over Shower Depth";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h2_radius_over_showerDepth = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 100, 0, 800, 100, 0, 800);
    figHits->h2_radius_over_showerDepth->GetXaxis()->SetTitle("Shower Depth [mm]");
    figHits->h2_radius_over_showerDepth->GetYaxis()->SetTitle("Radius [mm]");

    // Radius over nHitsSameChip
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_" << prefix << "_radius_over_nHitsSameChip";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Radius over nHitsSameChip";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h2_radius_over_nHitsSameChip = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20, 100, 0, 800);
    figHits->h2_radius_over_nHitsSameChip->GetXaxis()->SetTitle("# Hits in the Same Chip");
    figHits->h2_radius_over_nHitsSameChip->GetYaxis()->SetTitle("Radius [mm]");

    // Radius over MemoryCell
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_" << prefix << "_radius_over_memCell";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Radius over Memory Cell";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h2_radius_over_memCell = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20, 100, 0, 800);
    figHits->h2_radius_over_memCell->GetXaxis()->SetTitle("Memory Cell");
    figHits->h2_radius_over_memCell->GetYaxis()->SetTitle("Radius [mm]");

    // Radius over MemoryCell
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_" << prefix << "_nHitsSameChip_over_memCell";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "nHitsSameChip over Memory Cell";
    ss_title << ((prefix == std::string("pre")) ? "Before Data Selection" : "After Data Selection");
    figHits->h2_nHitsSameChip_over_memCell = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20, 20, 0, 20);
    figHits->h2_nHitsSameChip_over_memCell->GetXaxis()->SetTitle("Memory Cell");
    figHits->h2_nHitsSameChip_over_memCell->GetYaxis()->SetTitle("nHitsSameChip");

    if(prefix == std::string("hits")) {
        for(int k=0; k < _nLayers; k++) {
            ss_name.str(std::string()); ss_name.clear();
            ss_name << "h_" << prefix << "_nHits_layer_" << k;
            figHits->mh_layer_nHits[k] = new TH1D(ss_name.str().c_str(), "Number of Hits per Event after data selection", 100, 0, 100);
        }
    }
}

/*
    Analyzing raw data without any cuts.
*/
void TdcAnalysis::analyze_hitDistribution(std::string prefix = std::string("pre") ) {

    Analysis::HitDistribution* figHits;

    std::map<int, std::vector<int> > mv_nHitsPerChip;

    std::map<int, std::vector<int> > m_layer_nHits;
    std::map<int, std::vector<int> > m_layer_energy;
    std::map<int, std::vector<int> > m_chip_nHits;


    // selecting Prefix
    // this is done to use this function several times on different data sets
    if(prefix == std::string("pre") ) {
        std::cout << std::endl;
        std::cout << "Analyze hits before processing..." << std::endl;
        _fig_hitsPre = new Analysis::HitDistribution;
        figHits = _fig_hitsPre;
        fHasAnalyseHitDistributionPre = true;
    } else if(prefix == std::string("hits") ) {
        std::cout << std::endl;
        std::cout << "Analyze hits after processing..." << std::endl;
        _fig_hits = new Analysis::HitDistribution;
        figHits = _fig_hits;
        fHasAnalyseHitDistribution = true;
    } else {
        std::cout << "[analyze_hitDistribution]: Wrong Prefix. Skipping.";
        return;
    }

    init_hitDistribution(figHits, prefix);

    for(auto& evt: TM->_v_events) {

        if(prefix == std::string("hits")) {
            evt.energySum = 0;
        }

        // if(prefix == std::string("pre")) {
            evt.showerStart = 0;
        // }

        std::map<int, int> m_count_hits_layer; // counting hits per layer
        std::map<int, int> m_energy_layer; // counting hits per layer
        std::map<int, int> m_count_hits_chip; // counting hits per chip
        std::map<int, double> m_layer_position = TM->getLayerPositions();
        for(auto& hit: evt.v_hits) {
            figHits->hitmap->fill(hit.I, hit.J, hit.K);
            figHits->h_posX->Fill(hit.posX);
            figHits->h_posY->Fill(hit.posY);
            figHits->h_posZ->Fill(hit.posZ);

            mv_nHitsPerChip[hit.chipID].push_back(hit.nHitsSameChip);
            m_count_hits_layer[hit.K]++;
            m_count_hits_chip[hit.chipID]++;

            m_energy_layer[hit.K] += hit.mip;
            if(prefix == std::string("hits")) {
                evt.energySum += hit.mip;
            }


            figHits->h_singleHitEnergy->Fill(hit.mip);
            if(TM->isCalibratedMemoryCell(std::make_pair(hit.chipID, hit.channel), hit.memoryCell)) {
                figHits->m_ijk_calibratedHits[std::make_tuple(hit.I, hit.J, hit.K)]++;
            } else{
                figHits->m_ijk_notCalibratedHits[std::make_tuple(hit.I, hit.J, hit.K)]++;
            }

            double radius = sqrt(((hit.I-12) - evt.cogX/30.)*((hit.I-12) - evt.cogX/30.) + ((hit.J-12) - evt.cogY/30.) * ((hit.J-12) - evt.cogY/30.));
            hit.radius = radius*30.;
            double showerDepth = m_layer_position[hit.K] - m_layer_position[evt.showerStartAllLayers];
            // if(evt.showerStartAllLayers - evt.showerStartMCTruth == 11) {
            //     _v_eventsForHitMap.push_back(&evt);
            // }

            if(showerDepth > 0) hit.showerDepth = showerDepth;
            else hit.showerDepth = -1;

            figHits->h_hitRadius->Fill(radius*30.);
            figHits->h_nHitsSameChip->Fill(hit.nHitsSameChip);
            figHits->h_memCell->Fill(hit.memoryCell);

            if(TM->isHitSmallLayer(hit)) figHits->h_hitRadius_smallLayer->Fill(radius*30.);
            else if(TM->isHitBigLayer(hit)) figHits->h_hitRadius_bigLayer->Fill(radius*30.);

            if(prefix == std::string("hits")) {
                figHits->h2_mip_over_hitRadius->Fill(hit.radius, hit.mip);
                figHits->h2_radius_over_memCell->Fill(hit.memoryCell, hit.radius);
                figHits->h2_radius_over_nHitsSameChip->Fill(hit.nHitsSameChip, hit.radius);
                figHits->h2_nHitsSameChip_over_memCell->Fill(hit.memoryCell, hit.nHitsSameChip);

                if(hit.showerDepth > 0) {
                    figHits->h2_mip_over_showerDepth->Fill(hit.showerDepth, hit.mip);
                    figHits->h2_radius_over_showerDepth->Fill(hit.showerDepth, hit.radius);
                    figHits->h2_showerDepth_vs_Layer->Fill(hit.K, hit.showerDepth);
                    if(TM->isSimulatedData()) {
                        double trueShowerDepth = m_layer_position[hit.K] - m_layer_position[evt.showerStartMCTruth];
                        figHits->h2_trueShowerDepth_vs_Layer->Fill(hit.K, trueShowerDepth);
                    }
                }
            }
        }

        if(prefix == std::string("pre")) {
            for(auto& t0: evt.T0s) {
                int t0ID = TM->getT0ID(std::make_pair(t0.second.chipID, t0.second.channel));
                if(t0ID >= 0) {
                    _m_nHits_inT0s[t0ID] += 1;
                }
            }
        }

        figHits->h_energySum->Fill(evt.energySum);

        // save number of hits per layer to calculate the mean later
        for(int k=0; k < _nLayers; k++) {
            m_layer_nHits[k].push_back(m_count_hits_layer[k]);
            m_layer_energy[k].push_back(m_energy_layer[k]);
            // if(evt.showerStart == 0 && m_count_hits_layer[k] > 5) {
            // if(prefix == std::string("pre") && evt.showerStart == 0 && m_count_hits_layer[k] > 3) {
                    // evt.showerStart = k;
            // }
        }

        if(prefix == std::string("hits")) {
            //showerStartAllLayers is after event seleciton but using all layers.
            figHits->h_showerStart->Fill(evt.showerStartAllLayers);
            if(TM->isSimulatedData()) {
                figHits->h_MCTruthShowerStart->Fill(evt.showerStartMCTruth);
                figHits->h2_showerStart_vs_MCTruth->Fill(evt.showerStartAllLayers, evt.showerStartMCTruth);
            }
            if(not TM->isMuon() && TM->isSimulatedData()) {
                figHits->h_showerStart_vs_MCTruth->Fill(evt.showerStartAllLayers - evt.showerStartMCTruth);
            }
        } else {
            figHits->h_showerStart->Fill(evt.showerStart);
        }

        for(int chipID: _v_chipIDs) {
            m_chip_nHits[chipID].push_back(m_count_hits_chip[chipID]);
        }

        figHits->h_cogZ->Fill(evt.cogZ);
        figHits->h_cogY->Fill(evt.cogY);
        figHits->h_cogX->Fill(evt.cogX);
        figHits->h_nHits->Fill(evt.v_hits.size());
        figHits->h_nT0Hits->Fill(evt.T0s.size());
        figHits->h_nCherenkovHits->Fill(evt.nCherenkovs);
        figHits->h_nTracks->Fill(evt.nTracks);
        figHits->h2_nHits_cogZ->Fill(evt.cogZ, evt.v_hits.size());




        if(prefix == std::string("hits")) {
            for(int k=0; k < _nLayers; k++) {
                figHits->mh_layer_nHits[k]->Fill(m_count_hits_layer[k]);
            }
        }
    }

    if(prefix == std::string("hits")) {
        figHits->h_energyFraction_over_radius = getFractionHistogramFromTH2(figHits->h2_mip_over_hitRadius);
        figHits->h_energyFraction_over_showerDepth = getFractionHistogramFromTH2(figHits->h2_mip_over_showerDepth);
    }

    // calculate mean number of hits per layer
    for(int k=0; k < _nLayers; k++) {
        int s = 0; for(auto& e: m_layer_nHits[k]) s += e;
        figHits->h_mean_nHitsPerLayer->SetBinContent(k, s/(double)m_layer_nHits[k].size());

        s = 0; for(auto& e: m_layer_energy[k]) s += e;
        figHits->h_mean_energyPerLayer->SetBinContent(k, s/(double)m_layer_energy[k].size());
    }

    for(int chipID: _v_chipIDs) {
        int s = 0; for(auto& e: m_chip_nHits[chipID]) s += e;
        // std::cout << s/(double)m_chip_nHits[chipID].size() << std::endl;
        figHits->h_mean_nHitsPerChip->SetBinContent(chipID-_v_chipIDs[0], s/(double)m_chip_nHits[chipID].size());
    }

    // calculate mean number of hits per chip
    std::map<int, double> m_meanHitsPerChip;
    for(auto& v_nHitsPerChip: mv_nHitsPerChip) {
        std::vector<int> v = v_nHitsPerChip.second;
        if(v.size() > 100) {
            double mean = 0;
            for(auto& n: v) {
                mean += n;
            }
            mean = mean / (double)v.size();
            m_meanHitsPerChip[v_nHitsPerChip.first] = mean;
        }
    }

    for(int k=1; k < _nLayers; k++) {
        for(int i=1; i <= _nChannelsX[k]; i++) {
            for(int j=1; j <= _nChannelsY[k]; j++) {
                int chip = TM->convert_ijk_to_chipChan(i,j,k).first;
                int channel = TM->convert_ijk_to_chipChan(i,j,k).second;
                figHits->hitmap_nHitsSameChip->set(i-1,j-1,k,m_meanHitsPerChip[chip]);
                std::tuple<int, int, int> t_ijk = std::make_tuple(i, j, k);
                if(figHits->m_ijk_calibratedHits[t_ijk] > 0) {
                    figHits->hitmap_notCalibratedHits->set(i, j, k, figHits->m_ijk_notCalibratedHits[t_ijk] / (double)figHits->m_ijk_calibratedHits[t_ijk]);
                }

                if(prefix == std::string("hits")) {
                    int memCell = TM->getCalibratedMemoryCell(std::make_pair(chip, channel));
                    figHits->hitmap_calibratedMemoryCells->set(i-1,j-1,k, memCell);
                }
            }
        }
    }

}


/*
    Subfunction for figures of data before preprocessing
*/
void TdcAnalysis::write_hitDistribution(std::string prefix, TFile* ofile) {
    Analysis::HitDistribution* figHits;

    if( prefix == std::string("pre") ) {
        std::cout << "pre hits, ";
        figHits = _fig_hitsPre;
    } else if(prefix == std::string("hits") )  {
        std::cout << "hits, ";
        figHits = _fig_hits;
    } else {
        std::cout << "did not use correct prefix!" << std::endl;
        return;
    }

    figHits->hitmap->setLogZ();
    figHits->hitmap->commonZScale();
    figHits->hitmap->write(ofile);

    figHits->hitmap_nHitsSameChip->commonZScale(0,10);
    figHits->hitmap_nHitsSameChip->write(ofile);

    if(prefix == std::string("hits")) {
        figHits->hitmap_calibratedMemoryCells->commonZScale(0,16);
        figHits->hitmap_calibratedMemoryCells->write(ofile);
    }

    figHits->hitmap_notCalibratedHits->commonZScale();
    figHits->hitmap_notCalibratedHits->write(ofile);

    if(ofile->IsOpen()) {
        figHits->h_cogZ->Write();
        figHits->h_cogX->Write();
        figHits->h_cogY->Write();

        figHits->h_posX->Write();
        figHits->h_posY->Write();
        figHits->h_posZ->Write();

        figHits->h_nHits->Write();
        figHits->h_singleHitEnergy->Write();
        figHits->h_mean_nHitsPerLayer->Write();
        figHits->h_mean_energyPerLayer->Write();
        figHits->h_mean_nHitsPerChip->Write();
        figHits->h_nHitsSameChip->Write();
        figHits->h_memCell->Write();
        figHits->h_energySum->Write();
        figHits->h2_nHits_cogZ->Write();

        figHits->h_nT0Hits->Write();
        figHits->h_nCherenkovHits->Write();
        figHits->h_nTracks->Write();

        figHits->h_hitRadius->Write();
        figHits->h_hitRadius_smallLayer->Write();
        figHits->h_hitRadius_bigLayer->Write();

        if(prefix == std::string("hits")) {
            figHits->h2_mip_over_hitRadius->Write();
            figHits->h_energyFraction_over_radius->Write();
            figHits->h2_mip_over_showerDepth->Write();
            figHits->h_energyFraction_over_showerDepth->Write();
            figHits->h2_showerDepth_vs_Layer->Write();
            figHits->h2_radius_over_showerDepth->Write();
            figHits->h2_radius_over_memCell->Write();
            figHits->h2_radius_over_nHitsSameChip->Write();
            figHits->h2_nHitsSameChip_over_memCell->Write();
        }

        // if(prefix == std::string("pre")) {
        figHits->h_showerStart->Write();
        if(prefix == std::string("hits")) {
            if(not TM->isMuon() && TM->isSimulatedData()) {
                figHits->h_MCTruthShowerStart->Write();
                figHits->h_showerStart_vs_MCTruth->Write();
                figHits->h2_trueShowerDepth_vs_Layer->Write();
                figHits->h2_showerStart_vs_MCTruth->Write();
            }
        }


        // Write nHits per Layer
        if(prefix == std::string("hits") )  {
            for(auto& ph: figHits->mh_layer_nHits) {
                TH1D* h = ph.second;
                if(h->GetEntries() > 0) {
                    h->Write();
                }
            }
        }
    } else {
        std::cout << "Could not write prepocessed figures - file not open." << std::endl;
    }
}


void TdcAnalysis::init_t0s() {
    // Creating mip figures
    for(int i=0; i < TM->getNT0s(); i++) {
        std::stringstream ss_name;
        ss_name << "h_t0_mip_" << i;

        _fig_t0s->mh_mip[i] = new TH1D(ss_name.str().c_str(), "T0 Mip", 10000, 0, 100);
        _fig_t0s->mh_mip[i]->GetXaxis()->SetTitle("Mip");
        _fig_t0s->mh_mip[i]->GetYaxis()->SetTitle("#");
    }

    for(int i=0; i < TM->getNT0s(); i++) {
        std::stringstream ss_name;
        ss_name << "h_t0_timeDifference_" << TM->getT0s()[i].first << "_even";

        _fig_t0s->mh_ns_diff_preCorrection[i][0] = new TH1D(ss_name.str().c_str(), "T0 time difference to mean of all other T0s, even bxID", 2000, -100, 100);
        _fig_t0s->mh_ns_diff_preCorrection[i][0]->GetXaxis()->SetTitle("Time Difference [ns]");
        _fig_t0s->mh_ns_diff_preCorrection[i][0]->GetYaxis()->SetTitle("#");

        ss_name.clear();
        ss_name.str(std::string());
        ss_name << "h_t0_timeDifference_" << TM->getT0s()[i].first << "_odd";

        _fig_t0s->mh_ns_diff_preCorrection[i][1] = new TH1D(ss_name.str().c_str(), "T0 time difference to mean of all other T0s, odd bxID", 2000, -100, 100);
        _fig_t0s->mh_ns_diff_preCorrection[i][1]->GetXaxis()->SetTitle("Time Difference [ns]");
        _fig_t0s->mh_ns_diff_preCorrection[i][1]->GetYaxis()->SetTitle("#");
    }
}

/*
    Analyzing T0 data.
*/
void TdcAnalysis::analyze_t0s() {
    std::cout << "\nAnalyze T0s..." << std::endl;

    std::cout << "\nWe have " << TM->getNT0s() << " T0s." << std::endl;
    std::cout << "ChipID\tChannel\tI\tJ\tK\tnHits" << std::endl;


    int i = 0;
    for(auto& p_t0: TM->getT0s()) {
        auto t = TM->convert_chipChan_to_ijk(p_t0.first, p_t0.second);
        std::cout << p_t0.first << "\t" << p_t0.second << "\t" << std::get<0>(t) << "\t" << std::get<1>(t) << "\t" << std::get<2>(t) << "\t" << _m_nHits_inT0s[i] << std::endl;
        i++;
    }


    _fig_t0s = new Analysis::T0s;

    init_t0s();


    // Mip Plots
    for(const auto& evt: TM->_v_events) {
        for(const auto& p_t0: evt.T0s) {
            Hit t0 = p_t0.second;
            _fig_t0s->mh_mip[p_t0.first]->Fill(t0.mip);
        }

        // Fill time difference between T0 and mean of all other T0s
        assert((unsigned int)evt.nT0s == evt.T0s.size());
        for(auto& p_t0_1: evt.T0s) {
            int i = p_t0_1.first;
            double t = 0.;
            for(auto& p_t0_2: evt.T0s) {
                int j = p_t0_2.first;
                if(i == j) continue;
                t += evt.T0s.at(j).ns;
            }
            if(evt.nT0s -1 > 0) {
                t = t / (double) (evt.nT0s-1.);
            } else {
                t = -1;
            }

            if(t > 0 && evt.T0s.at(i).ns > 0) {
                _fig_t0s->mh_ns_diff_preCorrection[i][evt.T0s.at(i).bxID]->Fill(evt.T0s.at(i).ns - t);
            }
        }
    }

    fHasAnalyseT0 = true;

    for(auto& evt: TM->_v_events) {
        for(auto& p_t0: evt.T0s) {
            if(p_t0.second.chipID < 0) std::cout << GREEN << evt.nT0s << " " << evt.T0s.size() << " Report" << NORMAL << std::endl;
        }
    }


}


/*
    Subfunction for figures of T0 data
*/
void TdcAnalysis::write_t0s(TFile* ofile) {
    std::cout << "T0s, ";
    if(ofile->IsOpen()) {
        for(auto& h: _fig_t0s->mh_mip) {
            h.second->Write();
        }

        // write time difference between T0s
        for(int i=0; i < TM->getNT0s(); i++) {
            _fig_t0s->mh_ns_diff_preCorrection[i][0]->Write();
            _fig_t0s->mh_ns_diff_preCorrection[i][1]->Write();
        }

        for(auto& gr: TM->_v_t0_dt_plots) {
            if(gr->GetN() > 0) {
                gr->Write();
            }
        }

        // XXX switch this back on. There is a segfault for simulated data...
        if(TM->_h_t0s->GetEntries() > 0) TM->_h_t0s->Write();

    } else {
        std::cout << "Could not write T0 figures, file is not open." << std::endl;
    }
}

void TdcAnalysis::init_general() {
    _fig_general->hitmap_chips = new HitMap(_nChannelsX, _nChannelsY, std::string("hitmap_chips"));
    _fig_general->hitmap_brokenChannels = new HitMap(_nChannelsX, _nChannelsY, std::string("hitmap_brokenChannels"));
}

void TdcAnalysis::analyze_general() {
    std::cout << "Analyze general..." << std::endl;

    _fig_general = new Analysis::General;

    init_general();

    for(int k=1; k < _nLayers; k++) {
        for(int i=1; i <= _nChannelsX[k]; i++) {
            for(int j=1; j <= _nChannelsY[k]; j++) {
                int chip = TM->convert_ijk_to_chipChan(i,j,k).first;
                _fig_general->hitmap_chips->set(i-1,j-1,k,chip);
                if(TM->isBrokenChip(chip)) {
                    _fig_general->hitmap_brokenChannels->set(i-1,j-1,k,1);
                } else {
                    if(k < 4) {
                        _fig_general->hitmap_brokenChannels->set(i-1,j-1,k,-1);
                    } else if(k < 12 && (i <= 6 || j <=6 || i > 18 || j > 18 )) {
                       _fig_general->hitmap_brokenChannels->set(i-1,j-1,k,-1);
                    } else {
                        _fig_general->hitmap_brokenChannels->set(i-1,j-1,k,0);
                    }
                }
            }
        }
    }

    fHasAnalyseGeneral = true;
}


void TdcAnalysis::write_general(TFile* ofile) {
    std::cout << "general, ";

    _fig_general->hitmap_chips->commonZScale();
    _fig_general->hitmap_chips->write(ofile);
    _fig_general->hitmap_chips->commonZScale();
    _fig_general->hitmap_brokenChannels->write(ofile);

    if(ofile->IsOpen()) {
    } else {
        std::cout << "Could not write general figures - file not open." << std::endl;
    }
}


// reject double particle events
bool TdcAnalysis::isDoubleParticle(std::map<double, Hit*>& m_dT_hits,
                                   Event& evt,
                                   double pStartTime = 100,
                                   double pWindowSize = 30,
                                   double pMinClusterSize = 5,
                                   double pClusterSizeCut = 8,
                                   double pCoGCorrelationCut = 1.25) {

    bool result = false;

    std::map<double, Hit*>::iterator it_low = m_dT_hits.upper_bound(-50);
    std::map<double, Hit*>::iterator it_up = m_dT_hits.lower_bound(50);

    // int nEarlyHits = 0;
    // if(std::distance(m_dT_hits.begin(), it_low) < std::distance(m_dT_hits.begin(), it_up)) {
    //     nEarlyHits = std::distance(it_low, it_up);
    // }
    // int stepsize = 10; // scanning step size [ns]
    double range = pWindowSize; //window size [ns]
    double start = pStartTime;
    int i = 0;
    double maxLayerDifference = 0;
    double clusterTime = 0;
    double clusterHits = 0;
    int nDifferentLayers = 0;
    int clusterEnergy = 0;
    // int clusterCoGCorrelation = 0;
    double clusterCoGCorrelationI = 99.;
    double clusterCoGCorrelationJ = 99.;
    // holds the iterators of of the cluster window
    std::map<double, Hit*>::iterator it_low_cluster = m_dT_hits.upper_bound(-50);
    std::map<double, Hit*>::iterator it_up_cluster = m_dT_hits.lower_bound(50);
    // while(std::distance(it_up, m_dT_hits.end()) > 0) {
    // for(std::map<double, Hit*>::iterator it_low : m_dT_hits.end()) {

    for(it_low = m_dT_hits.upper_bound(start); it_low != m_dT_hits.end(); ++it_low) {
        // std::cout << i << " " << std::endl;
        // it_low = m_dT_hits.upper_bound(start+i*stepsize);
        it_up = std::prev(m_dT_hits.lower_bound(it_low->first + range));

        // just to be sure that nothing weird happens if the window is empty
        if(std::distance(m_dT_hits.begin(), it_low) >= std::distance(m_dT_hits.begin(), it_up)) {
            continue;
        }


        int nHitsInWindow = std::distance(it_low, it_up);

        if(nHitsInWindow > 3) {
            // find maximum window
            int maxHitsInWindow = nHitsInWindow;
            double meanTime = 0;

            std::vector<int> v_k;
            // int median_k = 0;
            for(auto it = it_low; it != it_up; it++) {
                v_k.push_back(it->second->K);
                meanTime += it->second->ns - evt.T0_time;
            }
            meanTime = meanTime / nHitsInWindow;
            if(v_k.size() > 0) {
                std::sort(v_k.begin(), v_k.end());
                // median_k = v_k[v_k.size()/2];
            }
            double sum_layers = 0;
            double energySum = 0;
            std::set<int> s_layers;
            std::map<int, std::vector<Hit*> > m_layers_hits; // for calculating the cog per layer
            for(auto it = it_low; it != it_up; it++) {
                s_layers.insert(it->second->K);
                energySum += it->second->mip;
                m_layers_hits[it->second->K].push_back( &(*it->second) ); // converting iterator to pointer
            }
            std::map<int, std::pair<double, double> > m_cogIJ;
            std::map<int, double> m_cog;
            std::map<int, int> m_nHitsPerLayer;
            // calculate cog per layer
            for(auto p: m_layers_hits) {
                int k = p.first;
                double cogI = 0.;
                double cogJ = 0.;
                double energySumInLayer = 0.;
                for(auto h: p.second) {
                    cogI += (double)h->mip * (h->I - 12.5);
                    cogJ += (double)h->mip * (h->J - 12.5);
                    energySumInLayer += h->mip;
                    m_nHitsPerLayer[k]++;
                }
                cogI = cogI / (double)energySumInLayer;
                cogJ = cogJ / (double)energySumInLayer;
                m_cogIJ[k] = std::make_pair(cogI, cogJ);
            }

            // calculate correlation in cog;
            double cog_correlationI = 0.;
            double cog_correlationJ = 0.;

            //get mean COG
            double mean_cogI = 0., mean_cogJ = 0.;
            for(auto p: m_cogIJ) {
                mean_cogI += p.second.first*m_nHitsPerLayer[p.first];
                mean_cogJ += p.second.second*m_nHitsPerLayer[p.first];
            }
            mean_cogI = mean_cogI / (double)m_cogIJ.size() / (double)nHitsInWindow;
            mean_cogJ = mean_cogJ / (double)m_cogIJ.size() / (double)nHitsInWindow;

            for(auto p: m_cogIJ) {
                cog_correlationI += std::fabs(p.second.first - mean_cogI)*std::fabs(p.second.first - mean_cogI);
                cog_correlationJ += std::fabs(p.second.second - mean_cogJ)*std::fabs(p.second.second - mean_cogJ);
            }

            if(s_layers.size() > 1) {
                cog_correlationI = sqrt(cog_correlationI) / (double)s_layers.size();
                cog_correlationJ = sqrt(cog_correlationJ) / (double)s_layers.size();
            } else {
                cog_correlationI = 99;
                cog_correlationJ = 99;
            }

            if(maxHitsInWindow > clusterHits) {
                maxLayerDifference = sum_layers;
                clusterTime = meanTime; // mean time of time cluster
                clusterHits = maxHitsInWindow; // size of time cluster
                clusterEnergy = energySum;
                clusterCoGCorrelationI = cog_correlationI;
                clusterCoGCorrelationJ = cog_correlationJ;
                nDifferentLayers = s_layers.size(); // number of different layers in time cluster
                it_low_cluster = it_low;
                it_up_cluster = it_up;
            }
        }

        i++;
    }


    if(clusterCoGCorrelationI != 99 && clusterCoGCorrelationJ != 99 && nDifferentLayers > 2) {
        _fig_timing->h2_cogCorrelationIJ->Fill(clusterCoGCorrelationI, clusterCoGCorrelationJ);
        _fig_timing->h2_cogCorrelation_clusterEnergy->Fill(clusterCoGCorrelationI+clusterCoGCorrelationJ, clusterHits);
    }

    _fig_timing->h2_clusterSize_clusterTime->Fill(clusterTime, clusterEnergy);
    _fig_timing->h2_clusterEnergy_nDifferentLayers->Fill(clusterEnergy, nDifferentLayers);
    _fig_timing->h_weightedCorrelatedLayerDifference->Fill(maxLayerDifference);

    if(clusterHits > 10) {
        _fig_timing->h_doubleParticleTime->Fill(clusterTime);
    }
    if(((clusterHits > pClusterSizeCut) || (clusterCoGCorrelationI + clusterCoGCorrelationJ < pCoGCorrelationCut && clusterHits > pMinClusterSize))) {
         result = true;
    }

    return result;
}


void TdcAnalysis::init_timing() {
    std::stringstream ss_name;
    std::stringstream ss_title;
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "hitmap_fwhm";
    _fig_timing->hitmap_fwhm = new HitMap(_nChannelsX, _nChannelsY, std::string(ss_name.str()));

    _fig_timing->h_resolution_evtTime = new TH1D("h_resolution_evtTime", "Hittime - Event Time", 2000, -500, 1500);
    _fig_timing->h_resolution_evtTime->GetXaxis()->SetTitle("Hit time [ns]");
    _fig_timing->h_resolution_evtTime->GetYaxis()->SetTitle("# Entries");
    _fig_timing->h_resolution_t0Time = new TH1D("h_resolution_t0Time", "Hit Time with Respect to T0 Time", 2000, -500, 1500);
    _fig_timing->h_resolution_t0Time->GetXaxis()->SetTitle("Hit time [ns]");
    _fig_timing->h_resolution_t0Time->GetYaxis()->SetTitle("# Entries");
    _fig_timing->h_resolution_t0Time->Sumw2();
    _fig_timing->h_resolution_t0Time_withFit = new TH1D("h_resolution_t0Time_withFit", "Hit Time with Respect to T0 Time", 2000, -500, 1500);
    _fig_timing->h_resolution_t0Time_withFit->GetXaxis()->SetTitle("Hit time [ns]");
    _fig_timing->h_resolution_t0Time_withFit->GetYaxis()->SetTitle("# Entries");
    _fig_timing->h_resolution_t0Time_normalized = new TH1D("h_resolution_t0Time_normalized", "Hittime - T0 time, normalized to event", 2000, -500, 1500);
    _fig_timing->h_resolution_t0Time_normalized->GetXaxis()->SetTitle("Hit time [ns]");
    _fig_timing->h_resolution_t0Time_normalized->GetYaxis()->SetTitle("# Entries");
    _fig_timing->h_resolution_t0Time_normalized->Sumw2();


    _fig_timing->h_T3B_resolution = new TH1D("h_T3B_resolution", "Hit Time with Respect to T0 Time - weighted for T3B Geometry", 425, -200, 1500);
    _fig_timing->h_T3B_resolution->GetXaxis()->SetTitle("Hit time [ns]");
    _fig_timing->h_T3B_resolution->GetYaxis()->SetTitle("# Entries");

    for(int k=0; k < 10; k++) {
        ss_name.str(std::string()); ss_name.clear();
        ss_name << "h_resolution_showerDepth_" << k;
        ss_title.str(std::string()); ss_title.clear();
        ss_title << "Time Resolution with Respect to T0 time for Shower Depth " << k;

        _fig_timing->mh_showerDepth_resolution[k] = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 1700, -200, 1500);
    }

    for(int k=0; k < 16; k++) {
        ss_name.str(std::string()); ss_name.clear();
        ss_name << "h_resolution_radius_smallLayers" << k;
        ss_title.str(std::string()); ss_title.clear();
        ss_title << "Time Resolution with Respect to T0 time for Small Layers for Radius (IJ) " << k;

        _fig_timing->mh_radius_resolution_smallLayers[k] = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 1700, -200, 1500);
    }

    for(int k=0; k < 16; k++) {
        ss_name.str(std::string()); ss_name.clear();
        ss_name << "h_resolution_radius_bigLayers" << k;
        ss_title.str(std::string()); ss_title.clear();
        ss_title << "Time Resolution with Respect to T0 time for Big Layers for Radius (IJ) " << k;

        _fig_timing->mh_radius_resolution_bigLayers[k] = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 1700, -200, 1500);
    }

    for(int k=0; k < 16; k++) {
        ss_name.str(std::string()); ss_name.clear();
        ss_name << "h_resolution_radius_" << k;
        ss_title.str(std::string()); ss_title.clear();
        ss_title << "Time Resolution with Respect to T0 time for Radius (IJ) " << k;

        _fig_timing->mh_radius_resolution[k] = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 1700, -200, 1500);
    }

    for(int k=0; k < 16; k++) {
        ss_name.str(std::string()); ss_name.clear();
        ss_name << "h_resolution_radius_lastLayer_" << k;
        ss_title.str(std::string()); ss_title.clear();
        ss_title << "Time Resolution with Respect to T0 time for Radius (IJ) Last Layer " << k;

        _fig_timing->mh_radius_resolution_lastLayer[k] = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 425, -200, 1500);
    }

    for(int chip: _v_chipIDs) {
        ss_name.str(std::string()); ss_name.clear();
        ss_name << "h_resolution_chip_" << chip;
        ss_title.str(std::string()); ss_title.clear();
        ss_title << "Time Resolution with Respect to T0 time for Chip " << chip;

        _fig_timing->mh_chip_resolution[chip] = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 400, -200, 1500);
    }

    // time resolution for ijk
    for(int k=0; k < _nLayers; k++) {
        for(int i=0; i < _nChannelsX[k]; i++) {
            for(int j=0; j < _nChannelsY[k]; j++) {
                ss_name.str(std::string()); ss_name.clear();
                ss_name << "h_resolution_kij_" << k << "-" << i << "-" << j;
                ss_title.str(std::string()); ss_title.clear();
                ss_title << "Time Resolution with Respect to T0 time for Channel " << k << "-" << i << "-" << j;

                _fig_timing->mh_ijk_resolution[std::make_tuple(i,j,k)] = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 300, -100, 200);
            }
        }
    }

    for(int k=0; k < _nChannels; k++) {
        ss_name.str(std::string()); ss_name.clear();
        ss_name << "h_resolution_nHitsSameChip_" << k;
        ss_title.str(std::string()); ss_title.clear();
        ss_title << "Time Resolution with Respect to T0 time for " << k << " hits in the same chip" << k;

        _fig_timing->mh_nHitsSameChip_resolution[k] = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 400, -200, 200);
    }

    _fig_timing->h_shiftPerChip = new TH1D("h_shiftPerChip", "Shift for each chip", 1000, -5, 5);

    for(int k=0; k < _nLayers; k++) {
        ss_name.str(std::string()); ss_name.clear();
        ss_name << "h2_layer_radius_resolution_" << k;
        ss_title.str(std::string()); ss_title.clear();
        ss_title << "Time Resolution with Respect to T0 time for Layer " << k;

        _fig_timing->mh2_layer_radius_resolution[k] = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 1700, -200, 1500, 20, 0, 600);
    }

    for(int k=0; k < _nLayers; k++) {
        ss_name.str(std::string()); ss_name.clear();
        ss_name << "h2_layer_showerDepth_resolution_" << k;
        ss_title.str(std::string()); ss_title.clear();
        ss_title << "Time Resolution with Respect to T0 time for Layer " << k;

        _fig_timing->mh2_layer_showerDepth_resolution[k] = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 1700, -200, 1500, 30, 0, 900);
    }

    for(int k=0; k < _nMemoryCells; k++) {
        ss_name.str(std::string()); ss_name.clear();
        ss_name << "h_memCell_resolution_" << k;
        ss_title.str(std::string()); ss_title.clear();
        ss_title << "Time Resolution with Respect to T0 time for Memory Cell " << k;

        _fig_timing->mh_memCell_resolution[k] = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 1700, -200, 1500);
    }


    // h2 time radius
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_time_over_radius";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Time over Radius (i,j) ";
    _fig_timing->h2_time_over_radius = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 300, 0, 300, 1700, -200, 1500);
    _fig_timing->h2_time_over_radius->GetXaxis()->SetTitle("Radius [mm]");
    _fig_timing->h2_time_over_radius->GetYaxis()->SetTitle("dT [ns]");

    // Mean Time over radius
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_meanTime_radius";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Mean time between -50 and 200ns";
    _fig_timing->h_meanTime_radius = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 100, 0, 100);
    _fig_timing->h_meanTime_radius->GetXaxis()->SetTitle("Radius [mm]");
    _fig_timing->h_meanTime_radius->GetYaxis()->SetTitle("Mean Time (-50, 200ns) [ns]");

    // Mean Time over mip
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_meanTime_mip";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Mean time between -50 and 200ns";
    _fig_timing->h_meanTime_mip = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 500, 0, 100);
    _fig_timing->h_meanTime_mip->GetXaxis()->SetTitle("Hit Energy [MIP]");
    _fig_timing->h_meanTime_mip->GetYaxis()->SetTitle("Mean Time (-50, 200ns) [ns]");

    // h2 time radius 7 4
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_radius_resolution_layer8_showerStart5";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Time over Radius vor Layer 8 and Shower Start in Layer 5 ";
    _fig_timing->h2_radius_resolution_layer8_showerStart5 = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 10, 0, 300, 1700, -200, 1500);
    _fig_timing->h2_radius_resolution_layer8_showerStart5->GetXaxis()->SetTitle("Radius [mm]");
    _fig_timing->h2_radius_resolution_layer8_showerStart5->GetYaxis()->SetTitle("dT [ns]");

    // h2 time radius 8 4
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_radius_resolution_layer9_showerStart5";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Time over Radius vor Layer 9 and Shower Start in Layer 5 ";
    _fig_timing->h2_radius_resolution_layer9_showerStart5 = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 10, 0, 300, 1700, -200, 1500);
    _fig_timing->h2_radius_resolution_layer9_showerStart5->GetXaxis()->SetTitle("Radius [mm]");
    _fig_timing->h2_radius_resolution_layer9_showerStart5->GetYaxis()->SetTitle("dT [ns]");

    // h2 time radius 10 7
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_radius_resolution_layer10_showerStart7";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Time over Radius vor Layer 10 and Shower Start in Layer 7 ";
    _fig_timing->h2_radius_resolution_layer10_showerStart7 = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 10, 0, 300, 1700, -200, 1500);
    _fig_timing->h2_radius_resolution_layer10_showerStart7->GetXaxis()->SetTitle("Radius [mm]");
    _fig_timing->h2_radius_resolution_layer10_showerStart7->GetYaxis()->SetTitle("dT [ns]");

    // h2 time radius 11 7
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_radius_resolution_layer11_showerStart7";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Time over Radius vor Layer 11 and Shower Start in Layer 7 ";
    _fig_timing->h2_radius_resolution_layer11_showerStart7 = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 10, 0, 300, 1700, -200, 1500);
    _fig_timing->h2_radius_resolution_layer11_showerStart7->GetXaxis()->SetTitle("Radius [mm]");
    _fig_timing->h2_radius_resolution_layer11_showerStart7->GetYaxis()->SetTitle("dT [ns]");

    // h2 time radius 13 10
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_radius_resolution_layer13_showerStart10";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Time over Radius vor Layer 13 and Shower Start in Layer 10 ";
    _fig_timing->h2_radius_resolution_layer13_showerStart10 = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 10, 0, 300, 1700, -200, 1500);
    _fig_timing->h2_radius_resolution_layer13_showerStart10->GetXaxis()->SetTitle("Radius [mm]");
    _fig_timing->h2_radius_resolution_layer13_showerStart10->GetYaxis()->SetTitle("dT [ns]");

    // h2 time mip
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_time_over_mip";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Time over MIP";
    _fig_timing->h2_time_over_mip = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 100, 0, 20, 1700, -200, 1500);
    _fig_timing->h2_time_over_mip->GetXaxis()->SetTitle("MIP");
    _fig_timing->h2_time_over_mip->GetYaxis()->SetTitle("dT [ns]");

    // h2 time layer
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_time_over_layer";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Time over Layer";
    _fig_timing->h2_time_over_layer = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20, 1700, -200, 1500);
    _fig_timing->h2_time_over_layer->GetXaxis()->SetTitle("Layer");
    _fig_timing->h2_time_over_layer->GetYaxis()->SetTitle("dT [ns]");

    // h2 time showerDepth
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_time_over_showerDepth";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Time over Shower Depth";
    _fig_timing->h2_time_over_showerDepth = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 800, 0, 800, 1700, -200, 1500);
    _fig_timing->h2_time_over_showerDepth->GetXaxis()->SetTitle("Shower Depth [mm]");
    _fig_timing->h2_time_over_showerDepth->GetYaxis()->SetTitle("dT [ns]");

    // h2 time memCell
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_time_over_memCell";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Time over MemoryCell";
    _fig_timing->h2_time_over_memCell = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 16, 0, 16, 1700, -200, 1500);
    _fig_timing->h2_time_over_memCell->GetXaxis()->SetTitle("Memory Cell");
    _fig_timing->h2_time_over_memCell->GetYaxis()->SetTitle("dT [ns]");

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fLateHits_radius_smallLayers";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of Late Hits over Radius in Small Layers";
    _fig_timing->h_fLateHits_over_radius_smallLayers = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 500, 0, 500);
    _fig_timing->h_fLateHits_over_radius_smallLayers->GetXaxis()->SetTitle("Hit radius [mm]");
    _fig_timing->h_fLateHits_over_radius_smallLayers->GetYaxis()->SetTitle("Fraction of late Hits > 50ns");

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fLateHits_radius_bigLayers";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of Late Hits over Radius in Big Layers";
    _fig_timing->h_fLateHits_over_radius_bigLayers = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 500, 0, 500);
    _fig_timing->h_fLateHits_over_radius_bigLayers->GetXaxis()->SetTitle("Hit radius [mm]");
    _fig_timing->h_fLateHits_over_radius_bigLayers->GetYaxis()->SetTitle("Fraction of late Hits > 50ns");

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fLateHits_radius";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of late hits over radius";
    _fig_timing->h_fLateHits_over_radius = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 500, 0, 500);
    _fig_timing->h_fLateHits_over_radius->GetXaxis()->SetTitle("Hit radius [mm]");
    _fig_timing->h_fLateHits_over_radius->GetYaxis()->SetTitle("Fraction of late Hits > 50ns");

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fLateHits_mip";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of late hits over mip";
    _fig_timing->h_fLateHits_over_mip = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20);
    _fig_timing->h_fLateHits_over_mip->GetXaxis()->SetTitle("Hit energy [mip]");
    _fig_timing->h_fLateHits_over_mip->GetYaxis()->SetTitle("Fraction of late Hits > 50ns");

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fLateHits_layer";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of late hits over layer";
    _fig_timing->h_fLateHits_over_layer = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20);
    _fig_timing->h_fLateHits_over_layer->GetXaxis()->SetTitle("Hit layer");
    _fig_timing->h_fLateHits_over_layer->GetYaxis()->SetTitle("Fraction of late Hits > 50ns");

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fLateHits_over_showerDepth";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of late hits over layer";
    _fig_timing->h_fLateHits_over_showerDepth = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 800, 0, 800);
    _fig_timing->h_fLateHits_over_showerDepth->GetXaxis()->SetTitle("Shower Depth [mm]");
    _fig_timing->h_fLateHits_over_showerDepth->GetYaxis()->SetTitle("Fraction of late Hits > 50ns");

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fLateHits_radius25";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of late hits over radius25";
    _fig_timing->h_fLateHits_over_radius25 = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 500, 0, 500);
    _fig_timing->h_fLateHits_over_radius25->GetXaxis()->SetTitle("Hit radius [mm]");
    _fig_timing->h_fLateHits_over_radius25->GetYaxis()->SetTitle("Fraction of late Hits > 25ns");

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fLateHits_mip25";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of late hits over mip25";
    _fig_timing->h_fLateHits_over_mip25 = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20);
    _fig_timing->h_fLateHits_over_mip25->GetXaxis()->SetTitle("Hit energy [mip]");
    _fig_timing->h_fLateHits_over_mip25->GetYaxis()->SetTitle("Fraction of late Hits > 25ns");

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fLateHits_layer25";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of late hits over layer25";
    _fig_timing->h_fLateHits_over_layer25 = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20);
    _fig_timing->h_fLateHits_over_layer25->GetXaxis()->SetTitle("Hit layer");
    _fig_timing->h_fLateHits_over_layer25->GetYaxis()->SetTitle("Fraction of late Hits > 25ns");

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fLateHits_radius50";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of late hits over radius";
    _fig_timing->h_fLateHits_over_radius50 = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 500, 0, 500);
    _fig_timing->h_fLateHits_over_radius50->GetXaxis()->SetTitle("Hit radius [mm]");
    _fig_timing->h_fLateHits_over_radius50->GetYaxis()->SetTitle("Fraction of late Hits > 50ns");

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fLateHits_mip50";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of late hits over mip";
    _fig_timing->h_fLateHits_over_mip50 = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20);
    _fig_timing->h_fLateHits_over_mip50->GetXaxis()->SetTitle("Hit energy [mip]");
    _fig_timing->h_fLateHits_over_mip50->GetYaxis()->SetTitle("Fraction of late Hits > 50ns");

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fLateHits_layer50";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of late hits over layer";
    _fig_timing->h_fLateHits_over_layer50 = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20);
    _fig_timing->h_fLateHits_over_layer50->GetXaxis()->SetTitle("Hit layer");
    _fig_timing->h_fLateHits_over_layer50->GetYaxis()->SetTitle("Fraction of late Hits > 50ns");

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fLateHits_radius100";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of late hits over radius";
    _fig_timing->h_fLateHits_over_radius100 = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 500, 0, 500);
    _fig_timing->h_fLateHits_over_radius100->GetXaxis()->SetTitle("Hit radius [mm]");
    _fig_timing->h_fLateHits_over_radius100->GetYaxis()->SetTitle("Fraction of late Hits > 100ns");

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fLateHits_over_radius100_smallLayers";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of late hits over radius for small Layers";
    _fig_timing->h_fLateHits_over_radius100_smallLayers = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 500, 0, 500);
    _fig_timing->h_fLateHits_over_radius100_smallLayers->GetXaxis()->SetTitle("Hit radius [mm]");
    _fig_timing->h_fLateHits_over_radius100_smallLayers->GetYaxis()->SetTitle("Fraction of late Hits > 100ns");

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fLateHits_over_radius100_bigLayers";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of late hits over radius for big Layers";
    _fig_timing->h_fLateHits_over_radius100_bigLayers = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 500, 0, 500);
    _fig_timing->h_fLateHits_over_radius100_bigLayers->GetXaxis()->SetTitle("Hit radius [mm]");
    _fig_timing->h_fLateHits_over_radius100_bigLayers->GetYaxis()->SetTitle("Fraction of late Hits > 100ns");

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fLateHits_mip100";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of late hits over mip";
    _fig_timing->h_fLateHits_over_mip100 = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20);
    _fig_timing->h_fLateHits_over_mip100->GetXaxis()->SetTitle("Hit energy [mip]");
    _fig_timing->h_fLateHits_over_mip100->GetYaxis()->SetTitle("Fraction of late Hits > 100ns");

    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fLateHits_layer100";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of late hits over layer";
    _fig_timing->h_fLateHits_over_layer100 = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20);
    _fig_timing->h_fLateHits_over_layer100->GetXaxis()->SetTitle("Hit layer");
    _fig_timing->h_fLateHits_over_layer100->GetYaxis()->SetTitle("Fraction of late Hits > 100ns");

    // Number of late hits
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_number_of_late_hits";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Number of late hits per Event";
    _fig_timing->h_number_of_late_hits = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 200, 0, 200);
    _fig_timing->h_number_of_late_hits->GetXaxis()->SetTitle("# Late hits > 50ns");
    _fig_timing->h_number_of_late_hits->GetYaxis()->SetTitle("# Entries");

    // Fraction of late hits
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fraction_of_late_hits";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of late hits per Event";
    _fig_timing->h_fraction_of_late_hits = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 100, 0, 1);
    _fig_timing->h_fraction_of_late_hits->GetXaxis()->SetTitle("# Fraction of Late hits > 50ns");
    _fig_timing->h_fraction_of_late_hits->GetYaxis()->SetTitle("# Entries");

    // Fraction of late hits
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fraction_of_late_hits25";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of late hits per Event";
    _fig_timing->h_fraction_of_late_hits25 = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 100, 0, 1);
    _fig_timing->h_fraction_of_late_hits25->GetXaxis()->SetTitle("# Fraction of Late hits > 25ns");
    _fig_timing->h_fraction_of_late_hits25->GetYaxis()->SetTitle("# Entries");

    // Fraction of late hits
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fraction_of_late_hits50";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of late hits per Event";
    _fig_timing->h_fraction_of_late_hits50 = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 100, 0, 1);
    _fig_timing->h_fraction_of_late_hits50->GetXaxis()->SetTitle("# Fraction of Late hits > 50ns");
    _fig_timing->h_fraction_of_late_hits50->GetYaxis()->SetTitle("# Entries");

    // Fraction of late hits
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_fraction_of_late_hits100";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Fraction of late hits per Event";
    _fig_timing->h_fraction_of_late_hits100 = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 100, 0, 1);
    _fig_timing->h_fraction_of_late_hits100->GetXaxis()->SetTitle("# Fraction of Late hits > 100ns");
    _fig_timing->h_fraction_of_late_hits100->GetYaxis()->SetTitle("# Entries");

    // Fraction of late hits
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_numberOfTimeGaps";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Number of Time Gaps bigger than 50ns";
    _fig_timing->h_numberOfTimeGaps = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 100, 0, 100);
    _fig_timing->h_numberOfTimeGaps->GetXaxis()->SetTitle("# Time Gaps > 50ns");
    _fig_timing->h_numberOfTimeGaps->GetYaxis()->SetTitle("# Entries");

    // h2 time correlation layers 7 and 8
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_timeCorrelation_7_8";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Time Correlation layers 7 and 8 ";
    _fig_timing->h2_timeCorrelation_7_8 = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 2050, -50, 2000, 2050, -50, 2000);
    _fig_timing->h2_timeCorrelation_7_8->GetXaxis()->SetTitle("Hit time (Layer 7) [ns]");
    _fig_timing->h2_timeCorrelation_7_8->GetYaxis()->SetTitle("Hit time of closest hit (Layer 8) [ns]");

    // h2 time correlation layers 14 and 15
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_timeCorrelation_14_15";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Time Correlation layers 14 and 15 ";
    _fig_timing->h2_timeCorrelation_14_15 = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 2050, -50, 2000, 2050, -50, 2000);
    _fig_timing->h2_timeCorrelation_14_15->GetXaxis()->SetTitle("Hit time (Layer 14) [ns]");
    _fig_timing->h2_timeCorrelation_14_15->GetYaxis()->SetTitle("Hit time of closest hit (Layer 15) [ns]");


    // h2 energy vs timing
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_energy_vs_timing";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Energy vs Timing";
    _fig_timing->h2_energy_vs_timing = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 30, 0, 30, 2050, -50, 2000);
    _fig_timing->h2_energy_vs_timing->GetXaxis()->SetTitle("Hit Energy [MIP]");
    _fig_timing->h2_energy_vs_timing->GetYaxis()->SetTitle("Hit Time [ns]");


    // EnergyFraction in late hits
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_energy_inLateHits";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Energy Fraction in Hits later than 50ns";
    _fig_timing->h_energy_inLateHits = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 100, 0, 1);
    _fig_timing->h_energy_inLateHits->GetXaxis()->SetTitle("Energy Fraction");
    _fig_timing->h_energy_inLateHits->GetYaxis()->SetTitle("# Entries");


    // EnergyFraction in late hits
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_weightedCorrelatedLayerDifference";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Energy Weighted Correlated Layer Difference";
    _fig_timing->h_weightedCorrelatedLayerDifference = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 1000, 0, 1000);
    _fig_timing->h_weightedCorrelatedLayerDifference->GetXaxis()->SetTitle("EWCLD");
    _fig_timing->h_weightedCorrelatedLayerDifference->GetYaxis()->SetTitle("# Entries");


    // h2 clusterSize vs cluster Time
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_clusterSize_clusterTime";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Cluster Size vs Cluster Time";
    _fig_timing->h2_clusterSize_clusterTime = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 2050, -50, 2000, 100, 0, 100);
    _fig_timing->h2_clusterSize_clusterTime->GetXaxis()->SetTitle("Cluster Time [ns]");
    _fig_timing->h2_clusterSize_clusterTime->GetYaxis()->SetTitle("Cluster Energy [MIP]");


    // h2 clusterEnergy vs nDifferentLayers
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_clusterEnergy_nDifferentLayers";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Cluster Energy vs Number of Different Layers in Cluster";
    _fig_timing->h2_clusterEnergy_nDifferentLayers = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 100, 0, 100, 16, 0, 16);
    _fig_timing->h2_clusterEnergy_nDifferentLayers->GetXaxis()->SetTitle("Cluster Energy [MIP]");
    _fig_timing->h2_clusterEnergy_nDifferentLayers->GetYaxis()->SetTitle("# Different Layers");

    // h2 cogCorrelation vs clusterEnergy
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_cogCorrelation_clusterEnergy";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "COG Correlation between layers vs clusterEnergy";
    _fig_timing->h2_cogCorrelation_clusterEnergy = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 1200, 0, 12, 100, 0, 100);
    _fig_timing->h2_cogCorrelation_clusterEnergy->GetXaxis()->SetTitle("COG Correlation");
    _fig_timing->h2_cogCorrelation_clusterEnergy->GetYaxis()->SetTitle("Cluster Energy [MIP]");

    // h2 cogCorrelationI vs cogCorrelationJ
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h2_cogCorrelationIJ";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "COG Correlation between layers  I vs J";
    _fig_timing->h2_cogCorrelationIJ = new TH2D(ss_name.str().c_str(), ss_title.str().c_str(), 1200, 0, 12, 1200, 0, 12);
    _fig_timing->h2_cogCorrelationIJ->GetXaxis()->SetTitle("COG Correlation I");
    _fig_timing->h2_cogCorrelationIJ->GetYaxis()->SetTitle("COG Correlation J");

    // Double particle time
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_doubleParticleTime";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Timing of late clusters with > 10 hits";
    _fig_timing->h_doubleParticleTime = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 4000, 0, 4000);
    _fig_timing->h_doubleParticleTime->GetXaxis()->SetTitle("Cluster Time [ns]");
    _fig_timing->h_doubleParticleTime->GetYaxis()->SetTitle("# Entries");

    // Last Layer geometrical radius
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_lastLayerHitGeoRadius";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Geometric Radius in Last Layer";
    _fig_timing->h_lastLayerHitGeoRadius = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20);
    _fig_timing->h_lastLayerHitGeoRadius->GetXaxis()->SetTitle("Geometric Radius [mm]");
    _fig_timing->h_lastLayerHitGeoRadius->GetYaxis()->SetTitle("# Entries");

    // strip geometrical radius
    ss_name.str(std::string()); ss_name.clear();
    ss_name << "h_stripHitGeoRadius";
    ss_title.str(std::string()); ss_title.clear();
    ss_title << "Geometric Radius in T3B Like Strip in Last Layer";
    _fig_timing->h_stripHitGeoRadius = new TH1D(ss_name.str().c_str(), ss_title.str().c_str(), 20, 0, 20);
    _fig_timing->h_stripHitGeoRadius->GetXaxis()->SetTitle("Geometric Radius [mm]");
    _fig_timing->h_stripHitGeoRadius->GetYaxis()->SetTitle("# Entries");
}


/*
    Do timing analysis
*/
void TdcAnalysis::analyze_timing() {
    std::cout << std::endl;
    std::cout << "Analyse timing..." << std::endl;

    _fig_timing = new Analysis::Timing;

    init_timing();

    int count_hitNotCalibrated = 0;
    int count_hitTimeOutOfRange = 0;
    int count_brokenChip = 0;
    int count_accepted = 0;
    int count_noEventTime = 0;
    int count_noT0Time = 0;
    int count_tooMuchLateHits = 0;
    int count_notEnoughValidHits = 0;

    std::map<int, std::vector<double> > mv_layer_timing; // for shifting layers to 0
    std::map<int, std::vector<double> > mv_chip_timing; // for shifting chips to 0

    // Calculate event time and reject bad hits / events
    for(auto& evt: TM->_v_events) {

        if(TM->isSimulatedData()) evt.T0_time = 0;

        // calculate event time
        for(auto& hit: evt.v_hits) {
            bool pass = true; // Only take data in calibrated channels that has propper time constraints
            if(TM->isBrokenChip(hit.chipID) || not TM->isCalibratedChannel(std::make_pair(hit.channel, hit.chipID)) ) {
                pass = false;
            }
            // XXX: What is about this?
            //if(hit.nHitsSameChip > 16) {
                //pass = false;
            //}

            if(not TM->isSimulatedData()) {
                if(hit.ns < 0) {
                    pass = false;
                } else if(hit.ns < 500 || hit.ns > 3500) {
                    pass = false;
                }
            }

            hit.passTiming = pass;
            if(not pass) continue;
            else evt.v_hits_time.push_back(&hit);

            // calculate event time
            int count = 0;
            hit.event_time = 0;
            for(auto& hit2: evt.v_hits) {
                if(&hit == &hit2) continue;
                if((hit2.ns > 500 && hit2.ns < 3500 && not TM->isBrokenChip(hit2.chipID)) || TM->isSimulatedData()){
                    hit.event_time += hit2.ns;
                    count++;
                }
            }

            // only take event time if there are at least 4 hits
            if(count > 4) {
                hit.event_time = hit.event_time / (double) count;
            } else {
                hit.event_time = -1;
            }

            if(evt.T0_time > 0 || TM->isSimulatedData()) {
                double dT = hit.ns - evt.T0_time;

                if(fabs(dT) < 20 || TM->isSimulatedData()) {
                    mv_layer_timing[hit.K].push_back(dT);
                    mv_chip_timing[hit.chipID*(2*hit.bxID-1)*10000+hit.channel*100+hit.memoryCell].push_back(dT);
                }
            }
        } // end loop over hits

    } // end loop over events

    // using median to determine shift of the distribution
    std::map<int, double> m_shiftLayersTo0;
    std::cout << std::endl;
    std::cout << "[Timing Analysis]: Shift per Layer" << std::endl;
    std::cout << "Layer\tShift" << std::endl;
    for(auto& mv: mv_layer_timing) {
        int k = mv.first;
        std::vector<double> v = mv.second;
        std::sort(v.begin(), v.end());
        m_shiftLayersTo0[k] = v[(int)v.size()/2];
        if(TM->isSimulatedData()) {
            double m = 0; int c = 0;
            for(double d: v) {
                if(fabs(d - m_shiftLayersTo0[k]) < 20) {m += d; c++;}
            }
            m_shiftLayersTo0[k] = m / (double)c;
        }
        std::cout << k << "\t" << m_shiftLayersTo0[k] << std::endl;
    }

    // using median to determine shift of the distribution
    std::map<int, double> m_shiftChipsTo0;
    // std::cout << std::endl;
    // std::cout << "[Timing Analysis]: Shift per Chip" << std::endl;
    // std::cout << "ChipID\tShift" << std::endl;
    for(auto& mv: mv_chip_timing) {
        int chipID = mv.first;
        std::vector<double> v = mv.second;

        if(v.size() > 100) {
            // std::sort(v.begin(), v.end());
            // m_shiftChipsTo0[chipID] = v[(int)v.size()/2];
            double m = 0;
            for(double e: v) {m += e;}
            m_shiftChipsTo0[chipID] = m/(double)v.size();

            // std::cout << chipID << "\t" << m_shiftChipsTo0[chipID] << std::endl;

            _fig_timing->h_shiftPerChip->Fill(m_shiftChipsTo0[chipID]);
        }
    }

    std::map<int, int> m_count_earlyHits_radius;
    std::map<int, int> m_count_earlyHits_mip;
    std::map<int, int> m_count_earlyHits_layer;
    std::map<int, int> m_count_earlyHits_showerDepth;

    std::map<int, int> m_count_lateHits_radius;
    std::map<int, int> m_count_lateHits_mip;
    std::map<int, int> m_count_lateHits_layer;
    std::map<int, int> m_count_lateHits_showerDepth;

    std::map<int, int> m_count_earlyHits_radius25;
    std::map<int, int> m_count_earlyHits_mip25;
    std::map<int, int> m_count_earlyHits_layer25;
    std::map<int, int> m_count_lateHits_radius25;
    std::map<int, int> m_count_lateHits_mip25;
    std::map<int, int> m_count_lateHits_layer25;

    std::map<int, int> m_count_earlyHits_radius50;
    std::map<int, int> m_count_earlyHits_mip50;
    std::map<int, int> m_count_earlyHits_layer50;
    std::map<int, int> m_count_lateHits_radius50;
    std::map<int, int> m_count_lateHits_mip50;
    std::map<int, int> m_count_lateHits_layer50;

    std::map<int, int> m_count_earlyHits_radius100;
    std::map<int, int> m_count_earlyHits_mip100;
    std::map<int, int> m_count_earlyHits_layer100;
    std::map<int, int> m_count_lateHits_radius100;
    std::map<int, int> m_count_lateHits_mip100;
    std::map<int, int> m_count_lateHits_layer100;

    std::map<int, int> m_count_earlyHits_radius100_smallLayers;
    std::map<int, int> m_count_earlyHits_radius100_bigLayers;
    std::map<int, int> m_count_lateHits_radius100_smallLayers;
    std::map<int, int> m_count_lateHits_radius100_bigLayers;

    int count_events = 0;
    int count_events_forT3B = 0;
    int count_events_forStripT3B = 0;
    int T3BMultiplicity = 0;

    int count_rejected = 0;

    for(auto& evt: TM->_v_events) {

        bool countedThisEvent_forT3B = false;
        bool countedThisEvent_forStripT3B = false;

        if(TM->isSimulatedData()) evt.T0_time = 0;

        int count_hits = 0;
        int count_late_hits = 0;
        int count_late_hits25 = 0;
        int count_late_hits50 = 0;
        int count_late_hits100 = 0;
        std::map<double, Hit*> m_dT_hits;
        // Correct for layer shift and count late hits
        for(auto hit_ref: evt.v_hits_time) {
            Hit& hit = *hit_ref;
            hit.ns = hit.ns - m_shiftLayersTo0[hit.K];
            if(hit.ns - evt.T0_time < 1000)
            count_hits++;
            if(hit.ns - evt.T0_time > 50) count_late_hits++;
            if(hit.ns - evt.T0_time > 25) count_late_hits25++;
            if(hit.ns - evt.T0_time > 50) count_late_hits50++;
            if(hit.ns - evt.T0_time > 100) count_late_hits100++;
            m_dT_hits[hit.ns - evt.T0_time] = &hit;
        }


        // reject double particles
        if(TM->isPion()) {
            // start, windowSize, minClusterSize, clusterSize, max CogDiff
            if(isDoubleParticle(m_dT_hits, evt, 100, 30, 6, 10, 1.5)) {
                count_rejected++;
                continue;
            }
        }



        if(count_hits > 10) {
            _fig_timing->h_number_of_late_hits->Fill(count_late_hits);
            _fig_timing->h_fraction_of_late_hits->Fill(count_late_hits / (double) count_hits);
            _fig_timing->h_fraction_of_late_hits25->Fill(count_late_hits25 / (double) count_hits);
            _fig_timing->h_fraction_of_late_hits50->Fill(count_late_hits50 / (double) count_hits);
            _fig_timing->h_fraction_of_late_hits100->Fill(count_late_hits100 / (double) count_hits);
            evt.fractionOfLateHits = count_late_hits / (double) count_hits;
        } else {
            evt.fractionOfLateHits = -1;
        }

        bool fIsNotInAlready = true;
        double lateEnergyInEvent = 0;
        double totalEnergyInEvent = 0;
        for(auto hit_ref: evt.v_hits_time) {


            Hit& hit = *hit_ref;

            if((hit.J > 22) || (hit.J < 3))  {
                std::cout << hit.I << "\t" << hit.J << "\t" << hit.K << std::endl;

            }

            bool pass = true;
            if(TM->isBrokenChip(hit.chipID)) {
                count_brokenChip++;
                pass = false;
            }
            if(not TM->isSimulatedData()) {
                if(hit.ns < 0) {
                    count_hitNotCalibrated++;
                    pass = false;
                } else if(hit.ns < 500 || hit.ns > 3500) {
                    count_hitTimeOutOfRange++;
                    pass = false;
                }
            }
            if(evt.fractionOfLateHits < 0 && TM->isPion()) {
                count_notEnoughValidHits++;
                pass = false;
            }

            if((hit.ns > 500 && hit.ns < 3500 && evt.T0_time > 0) || TM->isSimulatedData()) {
                _fig_timing->mh_chip_resolution[hit.chipID]->Fill(hit.ns - evt.T0_time);
                _fig_timing->mh_showerDepth_resolution[(int)hit.showerDepth/80]->Fill(hit.ns - evt.T0_time);

                assert(hit.I <= _nChannelsX[hit.K] && hit.J <= _nChannelsY[hit.K] && hit.K < _nLayers);
                _fig_timing->mh_ijk_resolution[std::make_tuple(hit.I-1, hit.J-1, hit.K)]->Fill(hit.ns - evt.T0_time);
            }

            if(not pass) continue;

            count_accepted++;


            if(hit.event_time <= 0) {
                count_noEventTime++;
            } else {
                _fig_timing->h_resolution_evtTime->Fill(hit.ns - hit.event_time);
            }


            if(evt.T0_time > 0 || TM->isSimulatedData()) {

                double dT = hit.ns - evt.T0_time;

                _fig_timing->h_resolution_t0Time->Fill(dT);
                _fig_timing->h_resolution_t0Time_withFit->Fill(dT);
                _fig_timing->h_resolution_t0Time_normalized->Fill(dT);


                if(dT > 50) lateEnergyInEvent += hit.mip;
                else totalEnergyInEvent += hit.mip;

                _fig_timing->h2_energy_vs_timing->Fill(hit.mip, dT);

                _fig_timing->mh_nHitsSameChip_resolution[hit.nHitsSameChip]->Fill(dT);

                std::map<int, double> m_layer_position = TM->getLayerPositions();

                double radius = sqrt(((hit.I-12.5) - evt.cogX/30.)*((hit.I-12.5) - evt.cogX/30.) + ((hit.J-12.5) - evt.cogY/30.) * ((hit.J-12.5) - evt.cogY/30.));
                double geomRadius = sqrt(((double)hit.I-12.5)*((double)hit.I-12.5) + ((double)hit.J-12.5)*((double)hit.J-12.5));
                double showerDepth = m_layer_position[hit.K] - m_layer_position[evt.showerStartAllLayers];
                hit.showerDepth = showerDepth;
                // std::cout << radius << std::endl;
                _fig_timing->h2_time_over_radius->Fill(radius*30., dT);
                _fig_timing->h2_time_over_mip->Fill(hit.mip, dT);
                _fig_timing->h2_time_over_layer->Fill(hit.K, dT);
                _fig_timing->h2_time_over_showerDepth->Fill(hit.showerDepth, dT);
                _fig_timing->h2_time_over_memCell->Fill(hit.memoryCell, dT);
                _fig_timing->mh_memCell_resolution[hit.memoryCell]->Fill(dT);


                if(evt.showerStartAllLayers == 5 && hit.K == 8) _fig_timing->h2_radius_resolution_layer8_showerStart5->Fill(hit.radius, dT);
                if(evt.showerStartAllLayers == 5 && hit.K == 9) _fig_timing->h2_radius_resolution_layer9_showerStart5->Fill(hit.radius, dT);
                if(evt.showerStartAllLayers == 7 && hit.K == 10) _fig_timing->h2_radius_resolution_layer10_showerStart7->Fill(hit.radius, dT);
                if(evt.showerStartAllLayers == 7 && hit.K == 11) _fig_timing->h2_radius_resolution_layer11_showerStart7->Fill(hit.radius, dT);
                if(evt.showerStartAllLayers == 10 && hit.K == 13) _fig_timing->h2_radius_resolution_layer13_showerStart10->Fill(hit.radius, dT);

                if(hit.K != 11 && hit.K !=12) {
                    if((int)radius > 0 && (int)radius < 16) _fig_timing->mh_radius_resolution[(int)radius]->Fill(dT);

                    _fig_timing->mh2_layer_radius_resolution[(int)hit.K]->Fill(dT, radius*30.);
                    _fig_timing->mh2_layer_showerDepth_resolution[(int)hit.K]->Fill(dT, hit.showerDepth);

                    if((int)geomRadius >= 0 && (int)geomRadius < 16 && hit.K == 15) {
                        _fig_timing->mh_radius_resolution_lastLayer[(int)geomRadius]->Fill(dT);
                        _fig_timing->h_lastLayerHitGeoRadius->Fill((int)geomRadius);
                        if(not countedThisEvent_forT3B) {
                            count_events_forT3B++;
                            countedThisEvent_forT3B = true;
                        }
                        if(hit.I == 12 && hit.J > 12) {
                            _fig_timing->h_stripHitGeoRadius->Fill((int)geomRadius);
                            T3BMultiplicity++;
                            if(not countedThisEvent_forStripT3B) {
                                count_events_forStripT3B++;
                                countedThisEvent_forStripT3B = true;
                            }

                        }
                    }
                    if((int)radius > 0 && (int)radius < 16 && hit.K > 11) _fig_timing->mh_radius_resolution_bigLayers[(int)radius]->Fill(dT);
                    else if((int)radius > 0 && (int)radius < 16 && hit.K < 12) _fig_timing->mh_radius_resolution_smallLayers[(int)radius]->Fill(dT);
                }

                if(dT < 1000 && hit.K != 12) {

                    if(dT < 1000 && dT > -50) {
                        m_count_earlyHits_radius[(int)(radius*30.)]++;
                        m_count_earlyHits_mip[hit.mip]++;
                        m_count_earlyHits_layer[hit.K]++;
                        m_count_earlyHits_showerDepth[hit.showerDepth]++;
                    }
                    if(dT > 50 && dT < 1000) {
                        m_count_lateHits_radius[(int)(radius*30.)]++;
                        m_count_lateHits_mip[hit.mip]++;
                        m_count_lateHits_layer[hit.K]++;
                        m_count_lateHits_showerDepth[hit.showerDepth]++;
                    }

                    if(dT < 1000 && dT > -50) {
                        m_count_earlyHits_radius25[(int)(radius*30.)]++;
                        m_count_earlyHits_mip25[hit.mip]++;
                        m_count_earlyHits_layer25[hit.K]++;
                    }
                    if(dT > 25 && dT < 1000) {
                        m_count_lateHits_radius25[(int)(radius*30.)]++;
                        m_count_lateHits_mip25[hit.mip]++;
                        m_count_lateHits_layer25[hit.K]++;
                    }

                    if(dT < 1000 && dT > -50) {
                        m_count_earlyHits_radius50[(int)(radius*30.)]++;
                        m_count_earlyHits_mip50[hit.mip]++;
                        m_count_earlyHits_layer50[hit.K]++;
                    }
                    if(dT > 50 && dT < 1000) {
                        m_count_lateHits_radius50[(int)(radius*30.)]++;
                        m_count_lateHits_mip50[hit.mip]++;
                        m_count_lateHits_layer50[hit.K]++;
                    }

                    if(dT < 1000 && dT > -50) {
                        m_count_earlyHits_radius100[(int)(radius*30.)]++;
                        m_count_earlyHits_mip100[hit.mip]++;
                        m_count_earlyHits_layer100[hit.K]++;
                        if(TM->isHitSmallLayer(hit)) m_count_earlyHits_radius100_smallLayers[(int)(radius*30.)]++;
                        else if(TM->isHitBigLayer(hit)) m_count_earlyHits_radius100_bigLayers[(int)(radius*30.)]++;
                    }
                    if(dT > 100 && dT < 1000) {
                        m_count_lateHits_radius100[(int)(radius*30.)]++;
                        m_count_lateHits_mip100[hit.mip]++;
                        m_count_lateHits_layer100[hit.K]++;
                        if(TM->isHitSmallLayer(hit)) m_count_lateHits_radius100_smallLayers[(int)(radius*30.)]++;
                        else if(TM->isHitBigLayer(hit)) m_count_lateHits_radius100_bigLayers[(int)(radius*30.)]++;
                    }
                }
                // For time correlation plot
                if(hit.K == 7) {
                    std::vector<std::pair<double, double> > v_layer8Dist;
                    for(auto hit2_ref: evt.v_hits_time) {
                        Hit& hit2 = *hit2_ref;
                        if(hit2.K == 8) {
                            v_layer8Dist.push_back(std::make_pair(hit.ns - evt.T0_time, hit2.ns - evt.T0_time));
                            _fig_timing->h2_timeCorrelation_7_8->Fill(hit.ns - evt.T0_time, hit2.ns - evt.T0_time);

                            if(hit.ns - evt.T0_time > 1000 && fabs(hit.ns - hit2.ns) < 100 && fIsNotInAlready) {
                                //_v_eventsForHitMap.push_back(&evt);
                                fIsNotInAlready = false;
                            }
                        }
                    }
                }

                if(hit.K == 14) {
                    for(auto hit2_ref: evt.v_hits_time) {
                        Hit& hit2 = *hit2_ref;
                        if(hit2.K == 15) {
                            _fig_timing->h2_timeCorrelation_14_15->Fill(hit.ns - evt.T0_time, hit2.ns - evt.T0_time);
                            if(hit.ns - evt.T0_time > 200 && fabs(hit.ns - hit2.ns) < 50 && fIsNotInAlready) {
                                //  _v_eventsForHitMap.push_back(&evt);
                                fIsNotInAlready = false;
                            }
                        }
                    }
                }

            } else {
                count_noT0Time++;
            }
            // }
        } // end loop over hits
        if(count_hits > 0) count_events++;
        _fig_timing->h_energy_inLateHits->Fill(lateEnergyInEvent / totalEnergyInEvent);


    } // end loop over events


    // Add systematic Error to MIP and radius distributions
    double absoluteSystematicError = 1.; // value taken from Eldwan; 1ns
    _fig_timing->h_meanTime_radius = _fig_timing->h2_time_over_radius->ProfileX("",150,350)->ProjectionX();
    int nBins =_fig_timing->h_meanTime_radius->GetNbinsX();
    for(int i=0; i < nBins; i++) {
        double statError = _fig_timing->h_meanTime_radius->GetBinError(i);
        double systError = absoluteSystematicError;
        double error = sqrt(statError*statError + systError*systError);
        _fig_timing->h_meanTime_radius->SetBinError(i, error);
    }

    _fig_timing->h_meanTime_mip = _fig_timing->h2_time_over_mip->ProfileX("",150,350)->ProjectionX();
    nBins =_fig_timing->h_meanTime_mip->GetNbinsX();
    for(int i=0; i < nBins; i++) {
        double statError = _fig_timing->h_meanTime_mip->GetBinError(i);
        double systError = absoluteSystematicError;
        double error = sqrt(statError*statError + systError*systError);
        _fig_timing->h_meanTime_mip->SetBinError(i, error);
    }

    // std::stringstream ss_name, ss_title;
    // ss_name.str(std::string()); ss_name.clear();
    // ss_name << "h_meanTime_radius";
    // ss_title.str(std::string()); ss_title.clear();
    // ss_title << "Mean time between -50 and 200ns";
    // _fig_timing->h_meanTime_radius->SetName(ss_name.str().c_str());
    // _fig_timing->h_meanTime_radius->GetXaxis()->SetTitle("Radius [mm]");
    // _fig_timing->h_meanTime_radius->GetYaxis()->SetTitle("Mean Time (-50, 200ns) [ns]");
    //
    // ss_name.str(std::string()); ss_name.clear();
    // ss_name << "h_meanTime_mip";
    // ss_title.str(std::string()); ss_title.clear();
    // ss_title << "Mean time between -50 and 200ns";
    // _fig_timing->h_meanTime_mip->SetName(ss_name.str().c_str());
    // _fig_timing->h_meanTime_mip->GetXaxis()->SetTitle("MIP [mm]");
    // _fig_timing->h_meanTime_mip->GetYaxis()->SetTitle("Mean Time (-50, 200ns) [ns]");


    // Add systematic error on time distribution

    double relativeSystematicError = 0.3; // value taken from Eldwan
    nBins =_fig_timing->h_resolution_t0Time_normalized->GetNbinsX();
    for(int i=0; i < nBins; i++) {
        double statError = _fig_timing->h_resolution_t0Time_normalized->GetBinError(i);
        double systError = _fig_timing->h_resolution_t0Time_normalized->GetBinContent(i) * relativeSystematicError;
        double error = sqrt(statError*statError + systError*systError);
        _fig_timing->h_resolution_t0Time_normalized->SetBinError(i, error);
    }

    nBins =_fig_timing->h_resolution_t0Time->GetNbinsX();
    for(int i=0; i < nBins; i++) {
        double statError = _fig_timing->h_resolution_t0Time->GetBinError(i);
        double systError = _fig_timing->h_resolution_t0Time->GetBinContent(i) * relativeSystematicError;
        double error = sqrt(statError*statError + systError*systError);
        _fig_timing->h_resolution_t0Time->SetBinError(i, error);
    }

    _fig_timing->h_energyFraction_over_time = getFractionHistogramFromTH2(_fig_timing->h2_time_over_mip, true);

    std::cout << "There are " << count_events_forT3B << " events with a hit in the last Layer" << std::endl;
    std::cout << "There are " << count_events_forStripT3B << " events with a hit in the strip in the last Layer" << std::endl;
    std::cout << "T3B Multiplicity: "  << T3BMultiplicity/(double)count_events_forStripT3B << std::endl;


    // std::cout << count_clusterSize_10 << " (10)\t" << count_clusterSize_5 << " (5)\t" << count_clusterSize_3 << " (3)\t" << std::endl;
    std::cout << "I rejected " << count_rejected << " events because of double particles. " << std::endl;

    _fig_timing->h_resolution_t0Time_normalized->Scale(1./(double)count_events);

    if(TM->isPion()) {
        // Fit time distribution
        TF1* f_singleExpo = new TF1("f_singleExpo", "[0]+[1]*exp(-x/[2])", 1, 2000);
        TF1* f_doubleExpo = new TF1("f_doubleExpo", "[0]+[1]*exp(-x/[3])+[2]*exp(-x/[4])", 1, 2000);
        f_doubleExpo->SetParameter(0, 10);
        f_doubleExpo->SetParameter(1, 10e4);
        f_doubleExpo->SetParameter(2, 20);
        f_doubleExpo->SetParameter(3, 7);
        f_doubleExpo->SetParameter(4, 200);
        _fig_timing->h_resolution_t0Time_withFit->Fit("f_doubleExpo", "L", "", 25, 1000);

        // -------------
        // LAYER timing
        // -------------
        std::vector<double> v_showerDepth_fast;
        std::vector<double> v_showerDepth_fast_slow;
        std::vector<double> v_showerDepth_slow;
        std::vector<double> v_showerDepth_forMean;
        std::vector<double> v_timeFit_showerDepth_constFast;
        std::vector<double> v_timeFit_showerDepth_constSlow;
        std::vector<double> v_timeFit_showerDepth_Afast;
        std::vector<double> v_timeFit_showerDepth_Aslow;
        std::vector<double> v_timeFit_showerDepth_Tfast;
        std::vector<double> v_timeFit_showerDepth_Tslow;
        std::vector<double> v_timeMean_overShowerDepth;
        std::vector<double> v_timeMean_overShowerDepth_50To200;
        std::vector<double> v_timeMedian_overShowerDepth;

        std::vector<double> v_timeFit_showerDepth_constFast_err;
        std::vector<double> v_timeFit_showerDepth_constSlow_err;
        std::vector<double> v_timeFit_showerDepth_Afast_err;
        std::vector<double> v_timeFit_showerDepth_Aslow_err;
        std::vector<double> v_timeFit_showerDepth_Tfast_err;
        std::vector<double> v_timeFit_showerDepth_Tslow_err;
        std::vector<double> v_timeMean_overShowerDepth_err;
        std::vector<double> v_timeMean_overShowerDepth_50To200_err;

        std::vector<double> v_timeFit_showerDepth_Afast_over_Aslow;
        std::vector<double> v_timeFit_showerDepth_Afast_over_Aslow_err;

        std::vector<double> v_xErrors;
        std::vector<double> v_xErrors_mean;

        // for(auto& ph: _fig_timing->mh_showerDepth_resolution) {
        for(auto& ph: _fig_timing->mh_showerDepth_resolution) {
            TH1D* h = ph.second;
            if(ph.first == 11 || ph.first == 12) continue;
            if(h->GetEntries() > 1000) {
                bool fastFitConverged = false;
                h->GetXaxis()->SetRangeUser(-50,200);
                v_timeMean_overShowerDepth_50To200.push_back(h->GetMean());
                v_timeMean_overShowerDepth_50To200_err.push_back(h->GetMeanError());

                h->GetXaxis()->SetRangeUser(-50,1000);

                // f_doubleExpo->SetParameter(0, 10);
                // f_doubleExpo->SetParameter(1, 10e4);
                // f_doubleExpo->SetParameter(2, 20);
                // f_doubleExpo->SetParameter(3, 7);
                // f_doubleExpo->SetParameter(4, 200);
                // h->Fit("f_doubleExpo", "L", "", 25, 600);

                f_singleExpo->SetParameter(0, 10);
                f_singleExpo->SetParameter(1, 10e4);
                f_singleExpo->SetParameter(2, 7);
                h->Fit("f_singleExpo", "L", "", 25, 50);
                if(strcmp(gMinuit->fCstatu.Data(), "CONVERGED ") == 0) {
                    std::cout << GREEN << "Fit converged!" << NORMAL << std::endl;
                    v_showerDepth_slow.push_back(ph.first*80);
                    v_timeFit_showerDepth_constSlow.push_back(f_singleExpo->GetParameter(0));
                    v_timeFit_showerDepth_Afast.push_back(f_singleExpo->GetParameter(1));
                    v_timeFit_showerDepth_Tfast.push_back(f_singleExpo->GetParameter(2));

                    v_timeFit_showerDepth_constSlow_err.push_back(f_singleExpo->GetParError(0));
                    v_timeFit_showerDepth_Afast_err.push_back(f_singleExpo->GetParError(1));
                    v_timeFit_showerDepth_Tfast_err.push_back(f_singleExpo->GetParError(2));
                    fastFitConverged = true;
                } else {
                    fastFitConverged = false;
                    std::cout << RED << "Fit Failed!" << NORMAL << std::endl;
                }

                f_singleExpo->SetParameter(0, 10);
                f_singleExpo->SetParameter(1, 20);
                f_singleExpo->SetParameter(2, 200);
                h->Fit("f_singleExpo", "L", "", 200, 800);
                if(strcmp(gMinuit->fCstatu.Data(), "CONVERGED ") == 0) {
                    std::cout << GREEN << "Fit converged!" << NORMAL << std::endl;
                    v_showerDepth_fast.push_back(ph.first*80);
                    v_timeFit_showerDepth_constFast.push_back(f_singleExpo->GetParameter(0));
                    v_timeFit_showerDepth_Aslow.push_back(f_singleExpo->GetParameter(1));
                    v_timeFit_showerDepth_Tslow.push_back(f_singleExpo->GetParameter(2));

                    v_timeFit_showerDepth_constFast_err.push_back(f_singleExpo->GetParError(0));
                    v_timeFit_showerDepth_Aslow_err.push_back(f_singleExpo->GetParError(1));
                    v_timeFit_showerDepth_Tslow_err.push_back(f_singleExpo->GetParError(2));

                    if(fastFitConverged) {
                        v_showerDepth_fast_slow.push_back(ph.first*80);
                        double Afast = v_timeFit_showerDepth_Afast.back();
                        double Afast_err = v_timeFit_showerDepth_Afast_err.back();
                        double Aslow = v_timeFit_showerDepth_Aslow.back();
                        double Aslow_err = v_timeFit_showerDepth_Aslow_err.back();
                        v_timeFit_showerDepth_Afast_over_Aslow.push_back( Afast / Aslow);
                        v_timeFit_showerDepth_Afast_over_Aslow_err.push_back( sqrt(pow(Afast_err/Aslow,2) + pow(Afast*Aslow_err/Aslow/Aslow,2)) );
                    }

                } else {

                    std::cout << RED << "Fit Failed!" << NORMAL << std::endl;
                }

                v_showerDepth_forMean.push_back(ph.first*80);
                v_timeMean_overShowerDepth.push_back(h->GetMean());
                v_timeMean_overShowerDepth_err.push_back(h->GetMeanError());
                v_timeMedian_overShowerDepth.push_back(Median(h));
            }
        }

        _fig_timing->gr_timeFit_showerDepth_const = new TGraphErrors(v_timeFit_showerDepth_constFast.size(), &(v_showerDepth_fast[0]), &(v_timeFit_showerDepth_constFast[0]), &(v_xErrors[0]), &(v_timeFit_showerDepth_constFast_err[0]));
        _fig_timing->gr_timeFit_showerDepth_const->SetName("gr_timeFit_showerDepth_const");
        _fig_timing->gr_timeFit_showerDepth_Tfast = new TGraphErrors(v_showerDepth_fast.size(), &(v_showerDepth_fast[0]), &(v_timeFit_showerDepth_Tfast[0]), &(v_xErrors[0]), &(v_timeFit_showerDepth_Tfast_err[0]));
        _fig_timing->gr_timeFit_showerDepth_Tfast->SetName("gr_timeFit_showerDepth_Tfast");
        _fig_timing->gr_timeFit_showerDepth_Tslow = new TGraphErrors(v_showerDepth_slow.size(), &(v_showerDepth_slow[0]), &(v_timeFit_showerDepth_Tslow[0]) , &(v_xErrors[0]), &(v_timeFit_showerDepth_Tslow_err[0]));
        _fig_timing->gr_timeFit_showerDepth_Tslow->SetName("gr_timeFit_showerDepth_Tslow");
        _fig_timing->gr_timeFit_showerDepth_Afast = new TGraphErrors(v_showerDepth_fast.size(), &(v_showerDepth_fast[0]), &(v_timeFit_showerDepth_Afast[0]) , &(v_xErrors[0]), &(v_timeFit_showerDepth_Afast_err[0]));
        _fig_timing->gr_timeFit_showerDepth_Afast->SetName("gr_timeFit_showerDepth_Afast");
        _fig_timing->gr_timeFit_showerDepth_Aslow = new TGraphErrors(v_showerDepth_slow.size(), &(v_showerDepth_slow[0]), &(v_timeFit_showerDepth_Aslow[0]) , &(v_xErrors[0]), &(v_timeFit_showerDepth_Aslow_err[0]));
        _fig_timing->gr_timeFit_showerDepth_Aslow->SetName("gr_timeFit_showerDepth_Aslow");

        _fig_timing->gr_timeFit_showerDepth_Afast_over_Aslow = new TGraphErrors(v_showerDepth_fast_slow.size(), &(v_showerDepth_fast_slow[0]), &(v_timeFit_showerDepth_Afast_over_Aslow[0]) , &(v_xErrors[0]), &(v_timeFit_showerDepth_Afast_over_Aslow_err[0]));
        _fig_timing->gr_timeFit_showerDepth_Afast_over_Aslow->SetName("gr_timeFit_showerDepth_Afast_over_Aslow");

        _fig_timing->gr_timeMean_overShowerDepth = new TGraphErrors(v_timeMean_overShowerDepth.size(), &(v_showerDepth_forMean[0]), &(v_timeMean_overShowerDepth[0]) , &(v_xErrors_mean[0]), &(v_timeMean_overShowerDepth_err[0]));
        _fig_timing->gr_timeMean_overShowerDepth->SetName("gr_timeMean_overShowerDepth");
        _fig_timing->gr_timeMean_overShowerDepth_50To200 = new TGraphErrors(v_timeMean_overShowerDepth_50To200.size(), &(v_showerDepth_forMean[0]), &(v_timeMean_overShowerDepth_50To200[0]) , &(v_xErrors_mean[0]), &(v_timeMean_overShowerDepth_50To200_err[0]));
        _fig_timing->gr_timeMean_overShowerDepth_50To200->SetName("gr_timeMean_overShowerDepth_50To200");


        _fig_timing->gr_timeMedian_overShowerDepth = new TGraph(v_timeMedian_overShowerDepth.size(), &(v_showerDepth_forMean[0]), &(v_timeMedian_overShowerDepth[0]) );
        _fig_timing->gr_timeMedian_overShowerDepth->SetName("gr_timeMedian_overShowerDepth");

        // -------------
        // RADIUS timing
        // -------------
        std::vector<double> v_radius_slow;
        std::vector<double> v_radius_fast;
        std::vector<double> v_radius_fast_slow;
        std::vector<double> v_radius_forMean;
        std::vector<double> v_timeFit_radius_constSlow;
        std::vector<double> v_timeFit_radius_constFast;
        std::vector<double> v_timeFit_radius_Afast;
        std::vector<double> v_timeFit_radius_Aslow;
        std::vector<double> v_timeFit_radius_Tfast;
        std::vector<double> v_timeFit_radius_Tslow;
        std::vector<double> v_timeMean_overRadius;
        std::vector<double> v_timeMean_overRadius_50To200;
        std::vector<double> v_timeMean_overRadius_50To200_smallLayers;
        std::vector<double> v_timeMean_overRadius_50To200_bigLayers;
        std::vector<double> v_timeMean_overRadius_50To200_lastLayer;

        std::vector<double> v_timeFit_radius_constSlow_err;
        std::vector<double> v_timeFit_radius_constFast_err;
        std::vector<double> v_timeFit_radius_Afast_err;
        std::vector<double> v_timeFit_radius_Aslow_err;
        std::vector<double> v_timeFit_radius_Tfast_err;
        std::vector<double> v_timeFit_radius_Tslow_err;
        std::vector<double> v_timeMean_overRadius_err;
        std::vector<double> v_timeMean_overRadius_50To200_err;
        std::vector<double> v_timeMean_overRadius_50To200_smallLayers_err;
        std::vector<double> v_timeMean_overRadius_50To200_bigLayers_err;
        std::vector<double> v_timeMean_overRadius_50To200_lastLayer_err;

        std::vector<double> v_timeFit_radius_Afast_over_Aslow;
        std::vector<double> v_timeFit_radius_Afast_over_Aslow_err;

        v_xErrors.clear();
        v_xErrors_mean.clear();


        std::vector<double> v_timeMedian_overRadius;

        for(auto& ph: _fig_timing->mh_radius_resolution) {
            TH1D* h = ph.second;
            if(h->GetEntries() > 1000) {
                bool fastFitConverged = false;
                h->GetXaxis()->SetRangeUser(-50,200);
                v_timeMean_overRadius_50To200.push_back(h->GetMean());
                v_timeMean_overRadius_50To200_err.push_back(h->GetMeanError());
                v_xErrors_mean.push_back(0);

                h->GetXaxis()->SetRangeUser(-50,1000);

                f_singleExpo->SetParameter(0, 10);
                f_singleExpo->SetParameter(1, 10e4);
                f_singleExpo->SetParameter(2, 7);
                h->Fit("f_singleExpo", "L", "", 25, 50);
                if(strcmp(gMinuit->fCstatu.Data(), "CONVERGED ") == 0) {
                    std::cout << GREEN << "Fit converged!" << NORMAL << std::endl;
                    v_radius_slow.push_back(ph.first*30);
                    v_timeFit_radius_constSlow.push_back(f_singleExpo->GetParameter(0));
                    v_timeFit_radius_Afast.push_back(f_singleExpo->GetParameter(1));
                    v_timeFit_radius_Tfast.push_back(f_singleExpo->GetParameter(2));

                    v_timeFit_radius_constSlow_err.push_back(f_singleExpo->GetParError(0));
                    v_timeFit_radius_Afast_err.push_back(f_singleExpo->GetParError(1));
                    v_timeFit_radius_Tfast_err.push_back(f_singleExpo->GetParError(2));
                    fastFitConverged = true;
                } else {
                    fastFitConverged = false;
                    std::cout << RED << "Fit Failed!" << NORMAL << std::endl;
                }

                f_singleExpo->SetParameter(0, 10);
                f_singleExpo->SetParameter(1, 20);
                f_singleExpo->SetParameter(2, 200);
                h->Fit("f_singleExpo", "L", "", 200, 800);
                if(strcmp(gMinuit->fCstatu.Data(), "CONVERGED ") == 0) {
                    std::cout << GREEN << "Fit converged!" << NORMAL << std::endl;
                    v_radius_fast.push_back(ph.first*30);
                    v_timeFit_radius_constFast.push_back(f_singleExpo->GetParameter(0));
                    v_timeFit_radius_Aslow.push_back(f_singleExpo->GetParameter(1));
                    v_timeFit_radius_Tslow.push_back(f_singleExpo->GetParameter(2));

                    v_timeFit_radius_constFast_err.push_back(f_singleExpo->GetParError(0));
                    v_timeFit_radius_Aslow_err.push_back(f_singleExpo->GetParError(1));
                    v_timeFit_radius_Tslow_err.push_back(f_singleExpo->GetParError(2));

                    if(fastFitConverged) {
                        v_radius_fast_slow.push_back(ph.first*80);
                        double Afast = v_timeFit_radius_Afast.back();
                        double Afast_err = v_timeFit_radius_Afast_err.back();
                        double Aslow = v_timeFit_radius_Aslow.back();
                        double Aslow_err = v_timeFit_radius_Aslow_err.back();
                        v_timeFit_radius_Afast_over_Aslow.push_back( Afast / Aslow);
                        v_timeFit_radius_Afast_over_Aslow_err.push_back( sqrt(pow(Afast_err/Aslow,2) + pow(Afast*Aslow_err/Aslow/Aslow,2)) );
                    }
                } else {

                    std::cout << RED << "Fit Failed!" << NORMAL << std::endl;
                }

                v_radius_forMean.push_back(ph.first*30);
                v_timeMean_overRadius.push_back(h->GetMean());
                v_timeMean_overRadius_err.push_back(h->GetMeanError());
                v_timeMedian_overRadius.push_back(Median(h));
            }
        }

        _fig_timing->gr_timeFit_radius_const = new TGraphErrors(v_timeFit_radius_constFast.size(), &(v_radius_fast[0]), &(v_timeFit_radius_constFast[0]), &(v_xErrors[0]), &(v_timeFit_radius_constFast_err[0]));
        _fig_timing->gr_timeFit_radius_const->SetName("gr_timeFit_radius_const");
        _fig_timing->gr_timeFit_radius_Tfast = new TGraphErrors(v_radius_fast.size(), &(v_radius_fast[0]), &(v_timeFit_radius_Tfast[0]), &(v_xErrors[0]), &(v_timeFit_radius_Tfast_err[0]));
        _fig_timing->gr_timeFit_radius_Tfast->SetName("gr_timeFit_radius_Tfast");
        _fig_timing->gr_timeFit_radius_Tslow = new TGraphErrors(v_radius_slow.size(), &(v_radius_slow[0]), &(v_timeFit_radius_Tslow[0]) , &(v_xErrors[0]), &(v_timeFit_radius_Tslow_err[0]));
        _fig_timing->gr_timeFit_radius_Tslow->SetName("gr_timeFit_radius_Tslow");
        _fig_timing->gr_timeFit_radius_Afast = new TGraphErrors(v_radius_fast.size(), &(v_radius_fast[0]), &(v_timeFit_radius_Afast[0]) , &(v_xErrors[0]), &(v_timeFit_radius_Afast_err[0]));
        _fig_timing->gr_timeFit_radius_Afast->SetName("gr_timeFit_radius_Afast");
        _fig_timing->gr_timeFit_radius_Aslow = new TGraphErrors(v_radius_slow.size(), &(v_radius_slow[0]), &(v_timeFit_radius_Aslow[0]) , &(v_xErrors[0]), &(v_timeFit_radius_Aslow_err[0]));
        _fig_timing->gr_timeFit_radius_Aslow->SetName("gr_timeFit_radius_Aslow");

        _fig_timing->gr_timeFit_radius_Afast_over_Aslow = new TGraphErrors(v_radius_fast_slow.size(), &(v_radius_fast_slow[0]), &(v_timeFit_radius_Afast_over_Aslow[0]) , &(v_xErrors[0]), &(v_timeFit_radius_Afast_over_Aslow_err[0]));
        _fig_timing->gr_timeFit_radius_Afast_over_Aslow->SetName("gr_timeFit_radius_Afast_over_Aslow");

        _fig_timing->gr_timeMean_overRadius = new TGraphErrors(v_timeMean_overRadius.size(), &(v_radius_forMean[0]), &(v_timeMean_overRadius[0]) , &(v_xErrors_mean[0]), &(v_timeMean_overRadius_err[0]));
        _fig_timing->gr_timeMean_overRadius->SetName("gr_timeMean_overRadius");
        _fig_timing->gr_timeMean_overRadius_50To200 = new TGraphErrors(v_timeMean_overRadius_50To200.size(), &(v_radius_forMean[0]), &(v_timeMean_overRadius_50To200[0]) , &(v_xErrors_mean[0]), &(v_timeMean_overRadius_50To200_err[0]));
        _fig_timing->gr_timeMean_overRadius_50To200->SetName("gr_timeMean_overRadius_50To200");

        _fig_timing->gr_timeMedian_overRadius = new TGraph(v_timeMedian_overRadius.size(), &(v_radius_forMean[0]), &(v_timeMedian_overRadius[0]) );
        _fig_timing->gr_timeMedian_overRadius->SetName("gr_timeMedian_overRadius");


        v_xErrors_mean.clear();
        v_radius_forMean.clear();

        for(auto& ph: _fig_timing->mh_radius_resolution_smallLayers) {
            TH1D* h = ph.second;
            // std::cout << ph.first << " " <<  h->GetEntries() << std::endl;
            if(h->GetEntries() > 100) {
                h->GetXaxis()->SetRangeUser(-50,200);
                v_timeMean_overRadius_50To200_smallLayers.push_back(h->GetMean());
                v_timeMean_overRadius_50To200_smallLayers_err.push_back(h->GetMeanError());
                v_radius_forMean.push_back(ph.first);
                v_xErrors_mean.push_back(0);
            }
        }

        _fig_timing->gr_timeMean_overRadius_50To200_smallLayers = new TGraphErrors(v_timeMean_overRadius_50To200_smallLayers.size(), &(v_radius_forMean[0]), &(v_timeMean_overRadius_50To200_smallLayers[0]) , &(v_xErrors_mean[0]), &(v_timeMean_overRadius_50To200_smallLayers_err[0]));
        _fig_timing->gr_timeMean_overRadius_50To200_smallLayers->SetName("gr_timeMean_overRadius_50To200_smallLayers");

        v_xErrors_mean.clear();
        v_radius_forMean.clear();

        for(auto& ph: _fig_timing->mh_radius_resolution_bigLayers) {
            TH1D* h = ph.second;
            // std::cout << ph.first << " " <<  h->GetEntries() << std::endl;
            if(h->GetEntries() > 100) {
                h->GetXaxis()->SetRangeUser(-50,200);
                v_timeMean_overRadius_50To200_bigLayers.push_back(h->GetMean());
                v_timeMean_overRadius_50To200_bigLayers_err.push_back(h->GetMeanError());
                v_radius_forMean.push_back(ph.first);
                v_xErrors_mean.push_back(0);
            }
        }
        _fig_timing->gr_timeMean_overRadius_50To200_bigLayers = new TGraphErrors(v_timeMean_overRadius_50To200_bigLayers.size(), &(v_radius_forMean[0]), &(v_timeMean_overRadius_50To200_bigLayers[0]) , &(v_xErrors_mean[0]), &(v_timeMean_overRadius_50To200_bigLayers_err[0]));
        _fig_timing->gr_timeMean_overRadius_50To200_bigLayers->SetName("gr_timeMean_overRadius_50To200_bigLayers");

        v_xErrors_mean.clear();
        v_radius_forMean.clear();

        for(auto& ph: _fig_timing->mh_radius_resolution_lastLayer) {
            TH1D* h = ph.second;
            // std::cout << ph.first << " " <<  h->GetEntries() << std::endl;
            if(h->GetEntries() > 100) {
                h->GetXaxis()->SetRangeUser(-50,200);
                v_timeMean_overRadius_50To200_lastLayer.push_back(h->GetMean());
                v_timeMean_overRadius_50To200_lastLayer_err.push_back(h->GetMeanError());
                v_radius_forMean.push_back(ph.first);
                v_xErrors_mean.push_back(0);
            }
        }
        _fig_timing->gr_timeMean_overRadius_50To200_lastLayer = new TGraphErrors(v_timeMean_overRadius_50To200_lastLayer.size(), &(v_radius_forMean[0]), &(v_timeMean_overRadius_50To200_lastLayer[0]) , &(v_xErrors_mean[0]), &(v_timeMean_overRadius_50To200_lastLayer_err[0]));
        _fig_timing->gr_timeMean_overRadius_50To200_lastLayer->SetName("gr_timeMean_overRadius_50To200_lastLayer");

        v_xErrors_mean.clear();
        v_radius_forMean.clear();

        for(auto& ph: _fig_timing->mh2_layer_radius_resolution) {
            TH2D* h2 = ph.second;
            // std::cout << ph.first << " " <<  h->GetEntries() << std::endl;
            if(h2->GetEntries() > 100) {
                // h2->GetXaxis()->SetRangeUser(-50,200);
                std::stringstream ss_name;
                ss_name << "pr_layers_timeMean_overRadius_50To200_" << ph.first;
                _fig_timing->mpr_layers_timeMean_overRadius_50To200[ph.first] = h2->ProfileY(ss_name.str().c_str(), -50+200, 200+200);
            }
        }


    }


    for(auto& p: m_count_earlyHits_radius) {
        if(p.second > 0) {
            _fig_timing->h_fLateHits_over_radius->SetBinContent(p.first+1, m_count_lateHits_radius[p.first] / (double)p.second);
            _fig_timing->h_fLateHits_over_radius25->SetBinContent(p.first+1, m_count_lateHits_radius25[p.first] / (double)m_count_earlyHits_radius25[p.first]);
            _fig_timing->h_fLateHits_over_radius50->SetBinContent(p.first+1, m_count_lateHits_radius50[p.first] / (double)m_count_earlyHits_radius50[p.first]);
            _fig_timing->h_fLateHits_over_radius100->SetBinContent(p.first+1, m_count_lateHits_radius100[p.first] / (double)m_count_earlyHits_radius100[p.first]);
        }
    }
    for(auto& p: m_count_earlyHits_radius100_smallLayers) {
        if(p.second > 0) _fig_timing->h_fLateHits_over_radius100_smallLayers->SetBinContent(p.first+1, m_count_lateHits_radius100_smallLayers[p.first] / (double)p.second);
    }
    for(auto& p: m_count_earlyHits_radius100_bigLayers) {
        if(p.second > 0) _fig_timing->h_fLateHits_over_radius100_bigLayers->SetBinContent(p.first+1, m_count_lateHits_radius100_bigLayers[p.first] / (double)p.second);
    }
    for(auto& p: m_count_earlyHits_mip) {
        if(p.second > 0) {
            _fig_timing->h_fLateHits_over_mip->SetBinContent(p.first+1, m_count_lateHits_mip[p.first] / (double)p.second);
            _fig_timing->h_fLateHits_over_mip25->SetBinContent(p.first+1, m_count_lateHits_mip25[p.first] / (double)m_count_earlyHits_mip25[p.first]);
            _fig_timing->h_fLateHits_over_mip50->SetBinContent(p.first+1, m_count_lateHits_mip50[p.first] / (double)m_count_earlyHits_mip50[p.first]);
            _fig_timing->h_fLateHits_over_mip100->SetBinContent(p.first+1, m_count_lateHits_mip100[p.first] / (double)m_count_earlyHits_mip100[p.first]);
        }
    }
    for(auto& p: m_count_earlyHits_layer) {
        if(p.second > 0) {
            if(p.first == 15) std::cout << ORANGE << "\nLate hits in last layer: " << m_count_lateHits_layer[p.first] / (double)p.second << "\n" << NORMAL <<  std::endl;
            _fig_timing->h_fLateHits_over_layer->SetBinContent(p.first+1, m_count_lateHits_layer[p.first] / (double)p.second);
            _fig_timing->h_fLateHits_over_layer25->SetBinContent(p.first+1, m_count_lateHits_layer25[p.first] / (double)m_count_earlyHits_layer25[p.first]);
            _fig_timing->h_fLateHits_over_layer50->SetBinContent(p.first+1, m_count_lateHits_layer50[p.first] / (double)m_count_earlyHits_layer50[p.first]);
            _fig_timing->h_fLateHits_over_layer100->SetBinContent(p.first+1, m_count_lateHits_layer100[p.first] / (double)m_count_earlyHits_layer100[p.first]);
        }
    }

    for(auto& p: m_count_earlyHits_showerDepth) {
        if(p.second > 0) {
            _fig_timing->h_fLateHits_over_showerDepth->SetBinContent(p.first+1, m_count_lateHits_showerDepth[p.first] / (double)p.second);

        }
    }

    // Produce T3B Timing plot
    TList * list = new TList;
    // Weights are from Lars Weuste PhD Thesis
    // To account for different geometry between AHCAL and T3B
    // std::vector<double> weights = {0.786, 6.5, 13.0, 19.5, 26.0, 32.5, 39.0, 45.5, 52.0, 58.5, 65.0, 71.4, 77.9, 84.4, 90.9, 0., 0., 0., 0.};
    std::map<int, double> weights;

    std::cout << "Weights for T3B Geometry:" << std::endl;
    // calculate weights
    double sumOfWeights = 0;
    _fig_timing->h_stripHitGeoRadius->Scale(1./count_events_forStripT3B);
    for(int i=1; i < _fig_timing->h_stripHitGeoRadius->GetNbinsX()+1; i++) {
        if(_fig_timing->h_stripHitGeoRadius->GetBinContent(i) > 0) {
            // weights[i-1] = _fig_timing->h_lastLayerHitGooRadius->GetBinContent(i) / (double)_fig_timing->h_stripHitGeoRadius->GetBinContent(i);
            weights[i-1] = (double)_fig_timing->h_stripHitGeoRadius->GetBinContent(i);
            // weights[i-1] = 0.005 + exp(-0.1273-0.6567*((double)i-1.));
            sumOfWeights += weights[i-1];
        } else {
            weights[i-1] = 0;
        }
        std::cout << i-1 << ":\t" << weights[i-1] << "\n";
    }
    std::cout << std::endl;

    // weights[0] = 0.6312;
    // weights[1] = 0.3775;
    // weights[2] = 0.1991;
    // weights[3] = 0.1143;
    // weights[4] = 0.06754;
    // weights[5] = 0.04523;
    // weights[6] = 0.03101;
    // weights[7] = 0.02242;
    // weights[8] = 0.01611;
    // weights[9] = 0.00606;

    std::cout << "Sum of Weights: " << sumOfWeights << std::endl;

    double i = 0;
    for(auto& ph: _fig_timing->mh_radius_resolution_lastLayer) {
        TH1D* h = ph.second;

        assert(i < weights.size());

        // h->Scale(1./(double)h->Integral(-50,1000));
        h->Scale(1./(double)h->GetEntries()); // What to choose here?

        if(weights[i] > 0)  {
            // h->Scale(1./(double)(weights[i]));
            // h->Scale(1./(double)count_events_forT3B);
            h->Scale((double)(weights[i]));
            // h->Scale((double)(1.5/sumOfWeights));
            h->Scale(1./h->GetXaxis()->GetBinWidth(1)*0.8); // *0.8. If I want nHits per 2ns the number must be bigger
                                                            // Divided by the bin width. If I have bigger bins, the number has to get smaller
            list->Add(h);
        }

        i++;
    }

    _fig_timing->h_T3B_resolution->Merge(list); // merge list of weighted distributions
    // Overall normalization
    // _fig_timing->h_T3B_resolution->Scale(count_events_forT3B / count_events_forStripT3B); // merge list of weighted distributions
    // _fig_timing->h_T3B_resolution->Scale(1./(double)count_events_forT3B); // scale to the number of events
    // _fig_timing->h_T3B_resolution->Scale(1./(double)5.); // because of binning

    // go through timing distributions for each channel
    for(auto& ph: _fig_timing->mh_ijk_resolution) {
        TH1D* h = ph.second;
        if(h->GetEntries() < 1000) continue;

        int i = std::get<0>(ph.first);
        int j = std::get<1>(ph.first);
        int k = std::get<2>(ph.first);

        // get FWHM
        double thr = h->GetMaximum() / 2.;
        int bin_min = h->FindFirstBinAbove(thr);
        int bin_max = h->FindLastBinAbove(thr);
        double xmin = h->GetXaxis()->GetBinCenter(bin_min);
        double xmax = h->GetXaxis()->GetBinCenter(bin_max);

        double FWHM = xmax - xmin;

        _fig_timing->hitmap_fwhm->set(i, j, k, FWHM);
    }

    // Do Double Gaus fit to nHitsSameChip distributions

    std::cout << std::endl;
    std::cout << "nHitsSameChipFit" << std::endl;

    for(auto& ph: _fig_timing->mh_nHitsSameChip_resolution) {
        TH1D* h = ph.second;
        if(h->GetEntries() > 1000) {
            TF1* f1 = new TF1("f1", DoubleGaussConv, -20, 20, 9);
            f1->FixParameter(0, 4745);
            f1->FixParameter(1, 0.048);
            f1->FixParameter(2, 3.153);
            f1->FixParameter(3, 4164);
            f1->FixParameter(4, -0.198);
            f1->FixParameter(5, 6.459);

            f1->SetParameter(6, 3.);
            f1->SetParLimits(6, 0., 50);
            f1->SetParameter(7, 0.5);
            f1->SetParameter(8, 0.);

            h->Fit("f1", "Q", "", -20, 20);

            std::cout << ph.first << "\t" << f1->GetParameter(6) << "\t" << h->GetEntries() << std::endl; //<< "\t" << f1->GetParameter(4) << std::endl;
        }
    }



    std::cout << std::endl;
    std::cout << count_hitNotCalibrated << "\tnot calibrated hits." << std::endl;
    std::cout << count_hitTimeOutOfRange << "\thits out of time range 500-3500." << std::endl;
    std::cout << count_brokenChip << "\thits in broken chips." << std::endl;
    std::cout << count_accepted << "\taccepted hits (";
    std::cout << count_accepted / (double)(count_accepted+count_brokenChip+count_hitTimeOutOfRange+count_hitNotCalibrated) * 100 << "%)" << std::endl;
    std::cout << std::endl;
    std::cout << count_noEventTime << "\thits have no event time." << std::endl;
    std::cout << count_noT0Time << "\thits have no T0 time." << std::endl;
    std::cout << count_tooMuchLateHits << "\thave to many (> 50%) late hits." << std::endl;
    std::cout << count_notEnoughValidHits << "\thave not enough valid hits." << std::endl;

    // get FWHM
    double thr = _fig_timing->h_resolution_t0Time->GetMaximum() / 2.;
    int bin_min = _fig_timing->h_resolution_t0Time->FindFirstBinAbove(thr);
    int bin_max = _fig_timing->h_resolution_t0Time->FindLastBinAbove(thr);
    double xmin = _fig_timing->h_resolution_t0Time->GetXaxis()->GetBinCenter(bin_min);
    double xmax = _fig_timing->h_resolution_t0Time->GetXaxis()->GetBinCenter(bin_max);
    double FWHM = xmax - xmin;

    std::cout << std::endl;
    std::cout << CYAN << "FWHM: " << FWHM << NORMAL << std::endl;
    _fig_timing->h_resolution_t0Time->GetXaxis()->SetRangeUser(-50, 50);
    std::cout << CYAN << "RMS: " << _fig_timing->h_resolution_t0Time->GetRMS() << NORMAL << std::endl;

    fHasAnalyseTiming = true;
}


void TdcAnalysis::write_timing(TFile* ofile) {
    std::cout << "timing, ";

    _fig_timing->hitmap_fwhm->commonZScale(5,20);
    _fig_timing->hitmap_fwhm->write(ofile);

    if(ofile->IsOpen()) {
        // _fig_timing->h_resolution_t0Time->Scale(1./_fig_timing->h_resolution_t0Time->GetEntries());
        _fig_timing->h_resolution_evtTime->Write(); _fig_timing->h_resolution_t0Time->Write();
        _fig_timing->h_resolution_t0Time_normalized->Write();
        _fig_timing->h_resolution_t0Time_withFit->Write();

        for(auto& ph: _fig_timing->mh_showerDepth_resolution) {
            TH1D* h = ph.second;
            if(h->GetEntries() > 0) {
                h->Write();
            }
        }

        for(auto& ph: _fig_timing->mh2_layer_showerDepth_resolution) {
            TH2D* h2 = ph.second;
            // std::cout << ph.first << " " <<  h->GetEntries() << std::endl;
            if(h2->GetEntries() > 0) {
                // h2->GetXaxis()->SetRangeUser(-50,200);
                h2->Write();
            }
        }

        for(auto& ph: _fig_timing->mh_radius_resolution) {
            TH1D* h = ph.second;
            if(h->GetEntries() > 0) {
                h->Write();
            }
        }

        for(auto& ph: _fig_timing->mh_chip_resolution) {
            TH1D* h = ph.second;
            if(h->GetEntries() > 0) {
                h->Write();
            }
        }

        bool plot_ijk_resolution = false;
        if(plot_ijk_resolution) {
            for(auto& ph: _fig_timing->mh_ijk_resolution) {
                TH1D* h = ph.second;
                if(h->GetEntries() > 100) {
                    h->Write();
                }
            }
        }

        for(auto& ph: _fig_timing->mh_nHitsSameChip_resolution) {
            TH1D* h = ph.second;
            if(h->GetEntries() > 0) {
                h->Scale(1./(double)h->GetEntries());
                h->Write();
            }
        }

        for(auto& ph: _fig_timing->mh_memCell_resolution) {
            TH1D* h = ph.second;
            if(h->GetEntries() > 0) {
                h->Scale(1./(double)h->GetEntries());
                h->Write();
            }
        }

        for(auto& ph: _fig_timing->mh_radius_resolution_lastLayer) {
            TH1D* h = ph.second;
            h->Write();
        }

        _fig_timing->h_T3B_resolution->Write();
        _fig_timing->h_lastLayerHitGeoRadius->Write();
        _fig_timing->h_stripHitGeoRadius->Write();

        _fig_timing->h_shiftPerChip->Write();

        _fig_timing->h2_time_over_radius->Write();
        _fig_timing->h2_time_over_mip->Write();
        _fig_timing->h2_time_over_layer->Write();
        _fig_timing->h2_time_over_showerDepth->Write();
        _fig_timing->h2_time_over_memCell->Write();
        _fig_timing->h_energyFraction_over_time->Write();

        _fig_timing->h_meanTime_radius->Write();
        _fig_timing->h_meanTime_mip->Write();

        _fig_timing->h_fLateHits_over_radius->Write();
        _fig_timing->h_fLateHits_over_mip->Write();
        _fig_timing->h_fLateHits_over_layer->Write();
        _fig_timing->h_fLateHits_over_showerDepth->Write();
        _fig_timing->h_fLateHits_over_radius25->Write();
        _fig_timing->h_fLateHits_over_mip25->Write();
        _fig_timing->h_fLateHits_over_layer25->Write();
        _fig_timing->h_fLateHits_over_radius50->Write();
        _fig_timing->h_fLateHits_over_mip50->Write();
        _fig_timing->h_fLateHits_over_layer50->Write();
        _fig_timing->h_fLateHits_over_radius100->Write();
        _fig_timing->h_fLateHits_over_mip100->Write();
        _fig_timing->h_fLateHits_over_layer100->Write();

        _fig_timing->h_fLateHits_over_radius100_smallLayers->Write();
        _fig_timing->h_fLateHits_over_radius100_bigLayers->Write();

        _fig_timing->h2_timeCorrelation_7_8->Write();
        _fig_timing->h2_timeCorrelation_14_15->Write();

        _fig_timing->h_number_of_late_hits->Write();
        _fig_timing->h_fraction_of_late_hits->Write();
        _fig_timing->h_fraction_of_late_hits25->Write();
        _fig_timing->h_fraction_of_late_hits50->Write();
        _fig_timing->h_fraction_of_late_hits100->Write();

        _fig_timing->h_numberOfTimeGaps->Write();

        _fig_timing->h2_energy_vs_timing->Write();
        _fig_timing->h_energy_inLateHits->Write();
        _fig_timing->h_weightedCorrelatedLayerDifference->Write();

        _fig_timing->h2_clusterSize_clusterTime->Write();
        _fig_timing->h2_clusterEnergy_nDifferentLayers->Write();

        _fig_timing->h2_cogCorrelationIJ->Write();
        _fig_timing->h2_cogCorrelation_clusterEnergy->Write();

        _fig_timing->h_doubleParticleTime->Write();

        _fig_timing->h2_radius_resolution_layer8_showerStart5->Write();
        _fig_timing->h2_radius_resolution_layer9_showerStart5->Write();
        _fig_timing->h2_radius_resolution_layer10_showerStart7->Write();
        _fig_timing->h2_radius_resolution_layer11_showerStart7->Write();
        _fig_timing->h2_radius_resolution_layer13_showerStart10->Write();


        if(TM->isPion()) {
            _fig_timing->gr_timeFit_showerDepth_const->Write();
            _fig_timing->gr_timeFit_showerDepth_Afast->Write();
            _fig_timing->gr_timeFit_showerDepth_Aslow->Write();
            _fig_timing->gr_timeFit_showerDepth_Afast_over_Aslow->Write();
            _fig_timing->gr_timeFit_showerDepth_Tfast->Write();
            _fig_timing->gr_timeFit_showerDepth_Tslow->Write();
            _fig_timing->gr_timeMean_overShowerDepth->Write();
            _fig_timing->gr_timeMean_overShowerDepth_50To200->Write();
            _fig_timing->gr_timeMedian_overShowerDepth->Write();


            _fig_timing->gr_timeFit_radius_const->Write();
            _fig_timing->gr_timeFit_radius_Afast->Write();
            _fig_timing->gr_timeFit_radius_Aslow->Write();
            _fig_timing->gr_timeFit_radius_Afast_over_Aslow->Write();
            _fig_timing->gr_timeFit_radius_Tfast->Write();
            _fig_timing->gr_timeFit_radius_Tslow->Write();
            _fig_timing->gr_timeMean_overRadius->Write();
            _fig_timing->gr_timeMean_overRadius_50To200->Write();
            _fig_timing->gr_timeMedian_overRadius->Write();

            _fig_timing->gr_timeMean_overRadius_50To200_smallLayers->Write();
            _fig_timing->gr_timeMean_overRadius_50To200_bigLayers->Write();
            _fig_timing->gr_timeMean_overRadius_50To200_lastLayer->Write();

            for(auto& pr: _fig_timing->mpr_layers_timeMean_overRadius_50To200) {
                pr.second->Write();
            }
        }
    }

    writeEventMaps(_v_eventsForHitMap);
}


/*
    initialize plots for analysing calibration
*/
void TdcAnalysis::init_calibration() {


}


/*
    analysing calibration
*/
void TdcAnalysis::analyze_calibration(std::string prefix) {

    if(not fHasAnalyseCalibration) {
        _fig_calibration = new Analysis::Calibration;
    }

    init_calibration();

    if(prefix == std::string("timewalk")) {

        std::map<int, std::vector<double>> mv_mip_timing;

        double mip_resolution = 0.1; // mips

        for(auto& evt: TM->_v_events) {
            for(auto& hit: evt.v_hits) {
                if(not TM->isBrokenChip(hit.chipID) && hit.ns > 500 && hit.ns < 3500) {
                    double dt = hit.ns - evt.T0_time;
                    if(dt < 50 && dt > -50) {
                        mv_mip_timing[(int)((hit.mip) / mip_resolution + 0.5)].push_back(dt);
                    }
                }
            }
        }

        std::vector<double> v_mips;
        std::vector<double> v_dt_mean;
        std::vector<double> v_dt_median;

        for(auto& pv_mip_timing: mv_mip_timing) {
            int mip_idx = pv_mip_timing.first;
            double mip = mip_idx * (double) mip_resolution;

            std::vector<double> v_mip_timing = pv_mip_timing.second;

            if(v_mip_timing.size() > 100) {
                double mean_dt = 0;
                for(auto& dt: v_mip_timing) {
                    mean_dt += dt;
                }

                mean_dt = mean_dt / (double)v_mip_timing.size();
                std::sort(v_mip_timing.begin(), v_mip_timing.end());
                double median_dt = v_mip_timing[(int)v_mip_timing.size()/2];

                v_mips.push_back(mip);
                v_dt_mean.push_back(mean_dt);
                v_dt_median.push_back(median_dt);
            }
        }

        // TF1* f_tw = new TF1("f_tw", "[0]+[1]*exp([2])", (v_mips[0]/mip_resolution), (v_mips[v_mips.size()-1]/mip_resolution))
        TF1* f_tw = new TF1("f_tw", "[0]+[1]*exp(x*[2])", 0, 100);
        f_tw->SetParameter(0,-2.);
        f_tw->SetParameter(1, 8.);
        f_tw->SetParameter(2,-1.);
        _fig_calibration->gr_timewalk_correction_mean = new TGraph(v_mips.size(), &(v_mips[0]), &(v_dt_mean[0]));
        _fig_calibration->gr_timewalk_correction_mean->SetName("gr_timewalk_correction_mean");
        _fig_calibration->gr_timewalk_correction_median = new TGraph(v_mips.size(), &(v_mips[0]), &(v_dt_median[0]));
        _fig_calibration->gr_timewalk_correction_median->SetName("gr_timewalk_correction_median");

        _fig_calibration->gr_timewalk_correction_median->Fit("f_tw", "Q", "", 0.5, 8);

        std::cout << std::endl;
        std::cout << "Time Walk Correction (new / old):" << std::endl;
        std::cout << "\t[0]: " <<  f_tw->GetParameter(0) << "\t" << std::get<0>(TM->getTwCoeff()) << std::endl;
        std::cout << "\t[1]: " <<  f_tw->GetParameter(1) << "\t" << std::get<1>(TM->getTwCoeff()) << std::endl;
        std::cout << "\t[2]: " <<  f_tw->GetParameter(2) << "\t" << std::get<2>(TM->getTwCoeff()) << std::endl;


    } else if(prefix == std::string("nHitsSameChip")) {


        std::map<int, std::vector<double> > mv_nHitsSameChip_dt;

        for(auto& evt: TM->_v_events) {
            for(auto& hit: evt.v_hits) {
                if(not TM->isBrokenChip(hit.chipID) && hit.ns > 500 && hit.ns < 3500) {
                    double dt = hit.ns - evt.T0_time;
                    if(dt < 100 && dt > -100) {
                        mv_nHitsSameChip_dt[hit.nHitsSameChip].push_back(dt);
                    }
                }
            }
        }

        std::vector<double> v_nHitsSameChip;
        std::vector<double> v_rms;
        std::vector<double> v_shift_mean;
        std::vector<double> v_shift_median;

        std::cout << std::endl;
        std::cout << "nHits\tMean\tMedian\tCurrent\tRMS\tEntries";
        std::cout << std::setprecision(4) << std::right << std::endl;
        std::map<int, double> nHitsSameChipCorrection = TM->getNHitsSameChipShift();
        for(auto& p: mv_nHitsSameChip_dt) {
            int nHitsSameChip = p.first;
            std::vector<double> v_dt = p.second;

            std::sort(v_dt.begin(), v_dt.end());
            double median = v_dt[(int)v_dt.size()/2];

            // calculate Mean and Median
            double mean = 0, rms =0;
            int c =0;
            // double rms = 0;
            for(double dt: v_dt) {
                if(fabs(median - dt) < 25) {mean += dt; c++;}
            }
            mean = mean / (double) c;

            // calculate rms
            c = 0;
            for(double dt: v_dt) {
                if(fabs(mean - dt) < 50) {
                    rms += (dt - mean)*(dt - mean);
                    c++;
                }
            }
            rms = sqrt(rms / c);

            if(v_dt.size() > 1000) {
                v_nHitsSameChip.push_back(nHitsSameChip);
                v_shift_mean.push_back(mean);
                v_shift_median.push_back(median);
                v_rms.push_back(rms);
            }

            std::cout << nHitsSameChip << "\t" << mean << "\t" << median << "\t";
            std::cout << nHitsSameChipCorrection[nHitsSameChip] << "\t" << rms << "\t"<< v_dt.size() <<std::endl;
        }

        _fig_calibration->gr_nHitsSameChip_shift_mean = new TGraph(v_nHitsSameChip.size(), &(v_nHitsSameChip[0]), &(v_shift_mean[0]));
        _fig_calibration->gr_nHitsSameChip_shift_mean->SetName("gr_nHitsSameChip_shift_mean");
        _fig_calibration->gr_nHitsSameChip_shift_median = new TGraph(v_nHitsSameChip.size(), &(v_nHitsSameChip[0]), &(v_shift_median[0]));
        _fig_calibration->gr_nHitsSameChip_shift_median->SetName("gr_nHitsSameChip_shift_median");
        _fig_calibration->gr_nHitsSameChip_rms = new TGraph(v_nHitsSameChip.size(), &(v_nHitsSameChip[0]), &(v_rms[0]));
        _fig_calibration->gr_nHitsSameChip_rms->SetName("gr_nHitsSameChip_rms");

    }


    fHasAnalyseCalibration = true;
}


/*
    initialize plots for analysing calibration
*/
void TdcAnalysis::write_calibration(std::string prefix, TFile* ofile) {
    if(ofile->IsOpen()) {
        if(prefix == std::string("timewalk")) {
            _fig_calibration->gr_timewalk_correction_mean->Write();
            _fig_calibration->gr_timewalk_correction_median->Write();
        } else if(prefix == std::string("nHitsSameChip")) {
            _fig_calibration->gr_nHitsSameChip_shift_mean->Write();
            _fig_calibration->gr_nHitsSameChip_shift_median->Write();
            _fig_calibration->gr_nHitsSameChip_rms->Write();
        }
    }
}

/*
    Writer function for figures
*/
void TdcAnalysis::writeFigures(const char* fname) {
    std::cout << std::endl;
    std::cout << "Writing Figures... " << std::endl;
    TFile* ofile = new TFile(fname, "recreate");

    if(ofile->IsOpen()) {
        if(fHasAnalyseHitDistributionPre) write_hitDistribution(std::string("pre"), ofile);
        if(fHasAnalyseHitDistribution) write_hitDistribution(std::string("hits"), ofile);
        if(fHasAnalyseT0) write_t0s(ofile);
        if(fHasAnalyseGeneral) write_general(ofile);
        if(fHasAnalyseCalibration) write_calibration(std::string("timewalk"), ofile);
        if(fHasAnalyseCalibration) write_calibration(std::string("nHitsSameChip"), ofile);
        if(fHasAnalyseTiming) write_timing(ofile); // has to come last?
    } else {
        std::cout << "Could not open root file." << std::endl;
    }
    std::cout << std::endl;
    ofile->Close();

}
