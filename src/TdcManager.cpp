#include "TdcManager.h"


#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

#include "TF1.h"
#include "TFile.h"
#include "TGraph.h"
#include "TH1.h"
#include "TH2.h"
#include "TMath.h"
#include "TTree.h"
#include "TVector.h"

// Initialization
TdcManager::TdcManager() {
    std::cout << std::endl;
    std::cout << "Creating TDC Manager" << std::endl;
    _nChannels = 36;
    _nMemoryCells = 16;

    _bxPeriod = 4000.; //[ns]
    _rampDeadtime = 0.02;

    for(int i=140; i < 237; i++) {
        _v_chipIDs.push_back(i);
    }

    // TODO: This should not be hardcoded!
    _m_nHitsSameChipCorrection[1] = -9.2;
    _m_nHitsSameChipCorrection[2] = -7.6;
    _m_nHitsSameChipCorrection[3] = -6.1;
    _m_nHitsSameChipCorrection[4] = -4.5;
    _m_nHitsSameChipCorrection[5] = -2.9;
    _m_nHitsSameChipCorrection[6] = -1.0;
    _m_nHitsSameChipCorrection[7] = 0.6;
    _m_nHitsSameChipCorrection[8] = 2.5;
    _m_nHitsSameChipCorrection[9] = 4.5;
    _m_nHitsSameChipCorrection[10] = 6.3;
    _m_nHitsSameChipCorrection[11] = 8.8;
    _m_nHitsSameChipCorrection[12] = 11.3;
    _m_nHitsSameChipCorrection[13] = 14.0;
    _m_nHitsSameChipCorrection[14] = 16.0;
    _m_nHitsSameChipCorrection[15] = 19.0;
    _m_nHitsSameChipCorrection[16] = 22.0;
    _m_nHitsSameChipCorrection[17] = 22.0;
    _m_nHitsSameChipCorrection[18] = 22.0;
    _m_nHitsSameChipCorrection[19] = 22.0;
    _m_nHitsSameChipCorrection[20] = 22.0;
    _m_nHitsSameChipCorrection[21] = 22.0;
    _m_nHitsSameChipCorrection[22] = 22.0;
    _m_nHitsSameChipCorrection[23] = 22.0;
    _m_nHitsSameChipCorrection[24] = 22.0;
    _m_nHitsSameChipCorrection[25] = 22.0;
    _m_nHitsSameChipCorrection[26] = 22.0;
    _m_nHitsSameChipCorrection[27] = 22.0;
    _m_nHitsSameChipCorrection[28] = 22.0;
    _m_nHitsSameChipCorrection[29] = 22.0;
    _m_nHitsSameChipCorrection[30] = 22.0;
    _m_nHitsSameChipCorrection[31] = 22.0;
    _m_nHitsSameChipCorrection[32] = 22.0;
    _m_nHitsSameChipCorrection[33] = 22.0;
    _m_nHitsSameChipCorrection[34] = 22.0;
    _m_nHitsSameChipCorrection[35] = 22.0;


    int nLayers = 16;
    for(int i=0; i<nLayers; i++) {
        if(i < 12) {
            _nChannelsX[i] = 24;
            _nChannelsY[i] = 24;
        } else {
            _nChannelsX[i] = 24;
            _nChannelsY[i] = 24;
        }
    }

    _nLayers = nLayers;

    _h_t0s = new TH1D("h_t0s", "T0s minus T0-time", 2000, -100, 100);
    // _hitmap_rawHits = new HitMap(_nChannelsX, _nChannelsY, std::string("hitmap_rawHits"));
}

/*
    Read Root file for simulated data
    * T0s are preselected
    * Cherenkov hits and broken T0s are ignored
*/
void TdcManager::readRootFileNewFormat(const char * fn_root_hits, int limit) {
    std::cout << std::endl;
    std::cout << "Reading from Root file with new Format: " << GREEN << fn_root_hits << NORMAL << " ..." << std::endl;
    if(limit > 0) {
        std::cout << ORANGE << "Limited to reading " << limit << " events." << NORMAL << std::endl;
    }

    std::cout << "External T0 Correction is switched ";
    if(fT0Correction) std::cout << GREEN << "on";
    else std::cout << RED << "off";
    std::cout << NORMAL << std::endl;

    assert(fHasReadT0Correction || not fT0Correction);

    if(not fHasReadHardwareConnection) {
        std::cout << std::endl;
        std::cout << "Apparently the Hardware Connection was not read in. Do this first, it is needed. ";
        std::cout << "It needs to have the format:\nI\tJ\tK\tchipID\tchannel." << std::endl;
        std::cout << "I will abort here and do not read in the root file..." << std::endl;
    }
    assert(fHasReadHardwareConnection);

    TFile* f = new TFile(fn_root_hits, "read");

    int count = 0;

    if(f->IsOpen()) {
        // Workaround to read from the simulation tree
        RootHit simHit;

        TTree* T = (TTree*)f->Get("bigtree");
        T->SetBranchAddress("eventNumber", &simHit.eventID);
        //XXX: currently not in the simulated trees
        if(not _isSimulatedData) {
            T->SetBranchAddress("event_BXID", &simHit.BxID);
        }
        T->SetBranchAddress("ahc_nHits", &simHit.nHits);
        T->SetBranchAddress("ahc_energySum", &simHit.energySum);
        T->SetBranchAddress("ahc_cogZ", &simHit.cogZ);
        T->SetBranchAddress("ahc_hitTime", &simHit.hitTime);
        T->SetBranchAddress("ahc_hitEnergy", &simHit.hitEnergy);
        T->SetBranchAddress("ahc_hitI", &simHit.I);
        T->SetBranchAddress("ahc_hitJ", &simHit.J);
        T->SetBranchAddress("ahc_hitK", &simHit.K);
        T->SetBranchAddress("ahc_hitType", &simHit.type);
        T->SetBranchAddress("ahc_hitPos", &simHit.hitPos);

        if(isMuon()) {
            T->SetBranchAddress("event_nTracks", &simHit.nTracks);
        }

        if(not isMuon() && isSimulatedData()) {
            T->SetBranchAddress("event_MCTruth_ShowerStartLayer", &simHit.MCTruth_showerStart);
        }

        int ID = 0;
        // Going through all events
        for(int i=0; i < T->GetEntries(); i++) {
            if(_v_events.size() > limit && limit > 0) break;

            T->GetEntry(i);
            if(simHit.nHits == 0) continue;

            Hit hit;
            Event event;

            event.T0_time = 0;
            event.ID = ID++;

            double cogX = 0;
            double cogY = 0;

            // Going through all hits in the event
            // and saving in Hit data structure
            for(int k=0; k < simHit.nHits; k++) {
                count++;

                hit.bxID = 0;
                // this looks wrong, but is actually correct... "type" is defined in the
                // calibration processor as 100*gainbit + memCell.. don't ask me why...
                if(_isSimulatedData) {
                    hit.memoryCell = 1;
                } else {
                    hit.memoryCell = simHit.type[k] % 100;
                }

                hit.mip = simHit.hitEnergy[k];
                hit.I = simHit.I[k];
                hit.J = simHit.J[k];
                hit.K = simHit.K[k];
                hit.module = hit.K;
                hit.bxID = simHit.BxID % 2;

                hit.posX = simHit.hitPos[k*3];
                hit.posY = simHit.hitPos[k*3+1];
                hit.posZ = simHit.hitPos[k*3+2];

                hit.tdc = simHit.hitTime[k];

                if(_isSimulatedData) {
                    hit.ns = simHit.hitTime[k];
                    hit.ns = hit.ns - _m_simulationShiftByLayer[hit.K];
                    hit.ns = hit.ns - 0.5;
                }

                // Comming from Hardware Connection
                hit.chipID = _m_ijk_to_ChipChan[std::make_tuple(hit.I, hit.J, hit.K)].first;
                hit.channel = _m_ijk_to_ChipChan[std::make_tuple(hit.I, hit.J, hit.K)].second;

                // Event information is stored in the first hit...
                if(k==0) {
                    event.cogZ = simHit.cogZ;
                    event.nTracks = simHit.nTracks;
                    if(not isMuon() && isSimulatedData()) {
                        event.showerStartMCTruth = simHit.MCTruth_showerStart + 2;
                    }
                }


                // Preselecting T0s
                if(isT0(std::make_pair(hit.chipID, hit.channel)) && not _isSimulatedData) {
                    hit.isT0 = true;

                    // T0 calibration
                    double pedestal = _m_T0_calibration[hit.chipID][hit.channel][hit.memoryCell][hit.bxID].first;
                    double slope = _m_T0_calibration[hit.chipID][hit.channel][1][hit.bxID].second;

                    if(pedestal < 0 || slope < 0) {
                        hit.ns = -1;
                        continue;
                    } else {
                        double c = std::get<0>(_m_T0_correction[hit.module][hit.bxID]);
                        double b = std::get<1>(_m_T0_correction[hit.module][hit.bxID]);
                        double a = std::get<2>(_m_T0_correction[hit.module][hit.bxID]);


                        hit.ns = (double)(hit.tdc - pedestal) * (slope);

                        // Do t0 correction
                        // module 14 is the one that we calibrate relative to
                        if(fT0Correction && hit.module != 14) {
                            // std::cout << "Correction" << std::endl;
                            if(b == 0) std::cout << "Attention: T0_correction is not read in!?" << std::endl;
                            hit.ns += c + hit.ns*b + hit.ns*hit.ns*a;
                        }
                    }

                    for(unsigned int i=0; i < _v_T0.size(); i++) {
                        if(hit.chipID == _v_T0[i].first && hit.channel == _v_T0[i].second) {
                            event.T0s[i] = hit;
                            break;
                        }
                    }

                } else if((not isCherenkov(std::make_pair(hit.chipID, hit.channel)) && not isBrokenT0(std::make_pair(hit.chipID, hit.channel))) || _isSimulatedData) {
                    // do not save Cherenkov hits and broken T0s
                    event.energySum += hit.mip;
                    event.v_hits.push_back(hit);
                    cogX += hit.mip * (hit.I - 12.)*30.;
                    cogY += hit.mip * (hit.J - 12.)*30.;
                } else if(isCherenkov(std::make_pair(hit.chipID, hit.channel))) {
                    event.nCherenkovs++;
                }
            }

            event.cogZ = calculateCogZ(event);

            event.nT0s = event.T0s.size();

            event.cogX = cogX / event.energySum;
            event.cogY = cogY / event.energySum;

            _v_events.push_back(event);
        }

        std::cout << "I read in total " << count << " hits, and saved "  << _v_events.size() << " events." << std::endl;
        fHasReadRootFile = true;
    } else {
        std::cout << "Could not read root file: " << fn_root_hits << std::endl;
    }


}


/*
    Performs Cuts, handles T0s
*/
void TdcManager::selectEvents() {
    std::cout << std::endl;
    std::cout << "Event selection... " << std::endl;

    if(not fHasReadRootFile) {
        std::cout << "Cannot select events: Root file is not read in." << std::endl;
    }
    assert(fHasReadRootFile);

    int count_memCellZero = 0;
    int count_notEnoughEnergy = 0;
    int count_T0sOutOfTimeRange = 0;
    int count_T0sNotCalibrated = 0;
    int count_notEnoughT0s = 0;
    int count_notPassingSelection = 0;

    int count_notPassingSelection_nTracks = 0;
    int count_notPassingSelection_minHits = 0;
    int count_notPassingSelection_maxHits = 0;
    int count_notPassingSelection_minCogZ = 0;
    int count_notPassingSelection_maxCogZ = 0;
    int count_notPassingSelection_cherenkov = 0;
    int count_notPassingSelection_nHitsSameChip = 0;

    std::vector<Event> new_events;
    for(auto& evt: _v_events) {

        bool pass_selection = true;

        if(evt.nCherenkovs == _cuts.nCherenkovs && _cuts.nCherenkovs != -1) {
            pass_selection = false;
            count_notPassingSelection_cherenkov++;
        } else if(evt.nTracks != _cuts.nTracks_inEvent && _cuts.nTracks_inEvent != -1) {
            pass_selection = false;
            count_notPassingSelection_nTracks++;
        } else if((int)evt.v_hits.size() < _cuts.minHits_inEvent) {
            pass_selection = false;
            count_notPassingSelection_minHits++;
        } else if((int)evt.v_hits.size() > _cuts.maxHits_inEvent && _cuts.maxHits_inEvent != -1) {
            pass_selection = false;
            count_notPassingSelection_maxHits++;
        } else if(evt.cogZ < _cuts.minCogZ_inEvent && ((int)evt.v_hits.size() < _cuts.minHits_beatingCogZ || _cuts.minHits_beatingCogZ == -1)) {
            pass_selection = false;
            count_notPassingSelection_minCogZ++;
        } else if(evt.cogZ < _cuts.maxCogZ_inEvent && _cuts.maxHits_inEvent != -1) {
            pass_selection = false;
            count_notPassingSelection_maxCogZ++;
        }

        if(not pass_selection) {
            count_notPassingSelection++;
            continue;
        }

        // Calculate nHitsSameChip
        for(auto& h1: evt.v_hits) {
            for(auto& h2: evt.v_hits) {
                if(h1.chipID == h2.chipID) {
                    h1.nHitsSameChip++;
                }
            }
        }

        std::map<int, int> m_count_hits_layer; // counting hits per layer

        // Cuts on individual hits
        std::vector<Hit> new_hits;
        for(auto& hit: evt.v_hits) {
            if(hit.memoryCell == 0 && _cuts.excludeMemoryCellZero) {
                count_memCellZero++;
                continue;
            }

            if(hit.mip < _cuts.minEnergy_hits) {
                count_notEnoughEnergy++;
                continue;
            }

            if(hit.nHitsSameChip > _cuts.maxNHitsSameChip || _cuts.maxNHitsSameChip < 0) {
                pass_selection = false;
                count_notPassingSelection_nHitsSameChip++;
                continue;
            }

            // for shower start. This has to be done before exclusion of layers.
            m_count_hits_layer[hit.K]++;

            // excluding already layers 5, 6
            if( ((isElectron() || isPion()) && (hit.K == 5 || hit.K == 6))) continue;
            // layer 12 is only excluded for the time analysis later
            new_hits.push_back(hit);
        } // End loop through hits

        if(not pass_selection) {
            count_notPassingSelection++;
            continue;
        }

        evt.v_hits = new_hits;

        // calculate shower start
        for(int k=0; k < _nLayers; k++) {
            // std::cout << k << ":\t" << m_count_hits_layer[k] << std::endl;
            if(m_count_hits_layer[4] > 4 && m_count_hits_layer[5] > 4) {
                evt.showerStartAllLayers = -100;
                break;
            }
            if(k > 11 && evt.showerStartAllLayers < 0) {
                evt.showerStartAllLayers = 100;
                break;
            } // consider only small layers

            if(m_count_hits_layer[k] >  5) {
                if(evt.showerStartAllLayers < 0) {
                    if(m_count_hits_layer[k+1] > 5) {
                        evt.showerStartAllLayers = k;
                    }
                }
            }
        }

        // if(evt.showerStartAllLayers - evt.showerStartMCTruth == 11) {
        //     std::cout << "\n------------------" << std::endl;
        //     for(int k=0; k < _nLayers; k++) {
        //         std::cout << k << ":\t" << m_count_hits_layer[k] << std::endl;
        //     }
        //     std::cout << "MC Truth: " << evt.showerStartMCTruth << std::endl;
        //     std::cout << "Data: " << evt.showerStartAllLayers << std::endl;
        //     std::cout << "\n" << evt.showerStartAllLayers - evt.showerStartMCTruth << std::endl;
        // }

        //
        //Handle T0s
        std::map<int, Hit> new_T0s;
        // XXX happens already


        for(auto& p_t0: evt.T0s) {
            Hit t0 = p_t0.second;
            if(isT0(std::make_pair(t0.chipID, t0.channel)) && not _isSimulatedData) {

                if(t0.ns < _cuts.minTime_T0 || t0.ns > _cuts.maxTime_T0) {
                     count_T0sOutOfTimeRange++; continue;
                }

                t0.isT0 = true;
                for(unsigned int i=0; i < _v_T0.size(); i++) {
                    if(t0.chipID == _v_T0[i].first && t0.channel == _v_T0[i].second) {
                        new_T0s[i] = t0;
                        break;
                    }
                }
            } else {
                std::cout << RED << "wrongly assigned T0s here? Should not happen!" << NORMAL << std::endl;
                std::cout << "Found a chip in t0s with chipID: " << t0.chipID << "\tchannel: " << t0.channel << std::endl;
                std::cout << "Available T0s are:\n";
                for(auto& pT0: _v_T0) {
                    std::cout << pT0.first << "\t" << pT0.second << "\n";
                }
                std::cout << "All other T0s in this event:\n";
                for(auto& pT0: evt.T0s) {
                    std::cout << pT0.second.chipID << "\t" << pT0.second.channel << "\n";
                }

                std::cout << std::endl;

            }
        }

        evt.T0s = new_T0s;
        evt.nT0s = evt.T0s.size();


        // T0 selection for non simulated Data
        if(not _isSimulatedData) {
            if(evt.nT0s == _cuts.minT0s) {
                pass_selection = true;
            } else {
                count_notEnoughT0s++;
                pass_selection = false;
                continue;
            }
        }

        if(pass_selection) {
            new_events.push_back(evt);
        }
    }


    if(_cuts.excludeMemoryCellZero) std::cout << count_memCellZero << "\thits are in the broken memoryCell 0." << std::endl;
    std::cout << count_notEnoughEnergy << "\thits with less than (" << _cuts.minEnergy_hits << ") MIP." << std::endl;
    std::cout << count_T0sOutOfTimeRange << "\tT0s are < " << _cuts.minTime_T0 << "ns or > " << _cuts.maxTime_T0 << " which are thrown away." << std::endl;
    std::cout << count_T0sNotCalibrated << "\tT0s could not be calibrated." << std::endl;
    std::cout << count_notEnoughT0s << "\tEvents did not have enough T0 hits. (" << _cuts.minT0s << " needed)" << std::endl;
    std::cout << "\n";
    std::cout << count_notPassingSelection << "\tEvents did not passed selection." << std::endl;

    std::cout << "\t" << count_notPassingSelection_cherenkov << "\tWrong number of cherenkov hits. (Required "<< _cuts.nCherenkovs << ")" << std::endl;
    std::cout << "\t" << count_notPassingSelection_nTracks << "\tWrong number of Tracks. (Required "<< _cuts.nTracks_inEvent << ")" << std::endl;
    std::cout << "\t" << count_notPassingSelection_minHits << "\tNot enough Hits. (Required "<< _cuts.minHits_inEvent << ")" << std::endl;
    std::cout << "\t" << count_notPassingSelection_maxHits << "\tToo much Hits. (Maximum number of hits: "<< _cuts.maxHits_inEvent << ")" << std::endl;
    std::cout << "\t" << count_notPassingSelection_minCogZ << "\tToo low CoGZ. (Min: "<< _cuts.minCogZ_inEvent << ")" << std::endl;
    std::cout << "\t" << count_notPassingSelection_maxCogZ << "\tToo high CoGZ. (Max: "<< _cuts.maxCogZ_inEvent << ")" << std::endl;
    std::cout << "\t" << count_notPassingSelection_nHitsSameChip << "\tToo many hits in one chip. (Max: "<< _cuts.maxNHitsSameChip << ")" << std::endl;

    std::cout << "Keeping " << new_events.size() << " / " << _v_events.size();
    std::cout << " (" << new_events.size() / (double)_v_events.size() * 100. << " %) Events." << std::endl;

    _v_events = new_events;
}

/*
    write a root file where noise hits are already excluded and only events are
    in that will pass the selection, to make the analysis faster
*/
void TdcManager::writeProcessedRootFile(const char* fname) {
    TFile* o_file = new TFile(fname, "recreate");
    if(o_file->IsOpen()) {
        TTree* outTree = new TTree("bigtree", "outTree");

        RootHit hit_t;
        // TVector* tv_tdcs = new TVector(0);
        Int_t nT0s;
        Int_t nTotalHits;
        Int_t nTracks;
        Float_t ahc_hitTime[10000];
        Float_t ahc_hitEnergy[10000];
        Int_t ahc_hitI[10000];
        Int_t ahc_hitJ[10000];
        Int_t ahc_hitK[10000];
        Int_t ahc_hitType[10000];

        outTree->Branch("eventNumber", &hit_t.eventID, "eventNumber/I");
        outTree->Branch("event_BXID", &hit_t.BxID, "event_BXID/I");
        outTree->Branch("event_nT0s", &nT0s, "event_nT0s/I");

        outTree->Branch("ahc_nHits", &nTotalHits, "ahc_nHits/I");
        outTree->Branch("ahc_energySum", &hit_t.energySum, "ahc_energySum/F");
        outTree->Branch("ahc_cogZ", &hit_t.cogZ, "ahc_cogZ/F");
        outTree->Branch("ahc_nHits", &hit_t.nHits, "ahc_nHits/I");
        // outTree->Branch("nTotalHits", &nTotalHits, "nTotalHits/I");
        //
        outTree->Branch("ahc_hitTime", ahc_hitTime, "ahc_hitTime[ahc_nHits]/I");
        outTree->Branch("ahc_hitEnergy", ahc_hitEnergy, "ahc_hitEnergy[ahc_nHits]/F");
        outTree->Branch("ahc_hitI", ahc_hitI, "ahc_hitI[ahc_nHits]/I");
        outTree->Branch("ahc_hitJ", ahc_hitJ, "ahc_hitJ[ahc_nHits]/I");
        outTree->Branch("ahc_hitK", ahc_hitK, "ahc_hitK[ahc_nHits]/I");
        outTree->Branch("ahc_hitType", ahc_hitType, "ahc_hitType[ahc_nHits]/I");

        if(isMuon()) {
            outTree->Branch("event_nTracks", &nTracks, "event_nTracks/I");
        }

        std::cout << "Writing " << _v_events.size() << " events to " << fname << std::endl;

        for(auto& evt: _v_events) {
            Hit h = evt.v_hits[0];
            hit_t.eventID = h.eventID;
            hit_t.BxID = h.bxID;
            hit_t.nHits = evt.v_hits.size();
            hit_t.energySum = evt.energySum;
            hit_t.cogZ = evt.cogZ;

            if(isMuon()) {
                nTracks = evt.nTracks;
            }

            nT0s = evt.nT0s;
            nTotalHits = hit_t.nHits + nT0s;

            int c = 0;
            for(auto& hit: evt.v_hits) {
                ahc_hitTime[c] = hit.tdc;
                ahc_hitEnergy[c] = hit.mip;
                ahc_hitI[c] = hit.I;
                ahc_hitJ[c] = hit.J;
                ahc_hitK[c] = hit.K;
                ahc_hitType[c] = hit.memoryCell;
                c++;
            }

            for(auto& p_t0: evt.T0s) {
                Hit t0 = p_t0.second;

                ahc_hitTime[c] = t0.tdc;
                ahc_hitEnergy[c] = t0.mip;
                ahc_hitI[c] = t0.I;
                ahc_hitJ[c] = t0.J;
                ahc_hitK[c] = t0.K;
                ahc_hitType[c] = t0.memoryCell;
                c++;
            }

            outTree->Fill();
            // delete tv_tdcs;
        }
        outTree->Write();
        outTree->Print();
    } else {
        std::cout << "!!! Could not open root file. Histograms are not saved!" << std::endl;
    }

    o_file->Close();
    delete o_file;
}


/*
    Correct offset between T0s
    FIXME: This should be done in the T0 calibration!!!
*/
void TdcManager::processT0s() {

    if(_isSimulatedData) {
        std::cout << "For simulated data there are no T0s.\n-> Skipping T0 processing." << std::endl;
        return;
    }

    // T0ID memoryCell 1, memCell 2, bxID
    std::map<int, std::map<int, std::map<int, std::map<int, std::vector<double> > > > > v_t0_ref;
    // T0ID, memoryCell memCell 2, bxID
    std::map<int, std::map<int, std::map<int, std::map<int, std::vector<double> > > > > v_t0_i;

    // Use first T0 as a reference and then save difference
    for(const auto& event: _v_events) {
        Hit t01 = event.T0s.at(0);
        for(int i=1; i < (int)event.T0s.size(); i++) {
            Hit t02 = event.T0s.at(i);
            if(t01.bxID != t02.bxID) std::cout << "Hmm.. bxIDs between T0s are not the same..." << std::endl;

            v_t0_ref[i][t01.memoryCell][t02.memoryCell][t01.bxID].push_back(t01.ns);
            v_t0_i[i][t01.memoryCell][t02.memoryCell][t01.bxID].push_back(t01.ns - t02.ns);
        }
    }

    // Calculate T0 correction
    // This gets the difference between all T0 channel, memCell and BxID combinations
    // in order to account for differences between them.

    // TODO: This needs to be done once, and then saved and read in!

    // T0ID, memCell 1, memCell 2, bxID
    std::map<int, std::map<int, std::map<int, std::map<int, std::tuple<double, double, double> > > > > t0_correction;
    for(int iT0=1; iT0 < (int)_v_T0.size(); iT0++) {
        for(int i=1; i < _nMemoryCells; i++) {
            for(int j=1; j < _nMemoryCells; j++) {
                // BxiDs
                for(int k=0; k<2; k++) {
                    if(v_t0_ref[iT0][i][j][k].size() < 10) continue;

                    std::stringstream ss_name, ss_title;
                    ss_name << "gr_t0_" << iT0 << "_" << i << "_" << j << "_" << k;
                    ss_title << "T0 1 over T0 2, Memory Cells: " << iT0 << " ID, " << i << " " << j << " " << k;

                    TGraph* gr_T0 = new TGraph(v_t0_ref[iT0][i][j][k].size(), &(v_t0_ref[iT0][i][j][k][0]), &(v_t0_i[iT0][i][j][k][0]));
                    gr_T0->SetName(ss_name.str().c_str());
                    gr_T0->SetTitle(ss_title.str().c_str());
                    gr_T0->GetXaxis()->SetTitle("T0 1 [ns]");
                    gr_T0->GetYaxis()->SetTitle("T0 1 - T0 2 [ns]");

                    // quadratic fit
                    // TF1* f_quad = new TF1("f_quad", "pol2", 0, 4000);
                    // gr_T0->Fit("f_quad", "Qrob=0.75", 0, 4000);

                    // linear fit
                    TF1* f_lin = new TF1("f_lin", "pol0", 0, 4000);
                    f_lin->SetParameter(0, 10.);
                    gr_T0->Fit("f_lin", "Qrob=0.75", 0, 4000);
                    //
                    // double t0_c = f_quad->GetParameter(0);
                    // double t0_b = f_quad->GetParameter(1);
                    // double t0_a = f_quad->GetParameter(2);

                    double t0_lin = f_lin->GetParameter(0);

                    // std::cout << t0_lin << "\t" << t0_c << "\t" << t0_b << "\t" << t0_a << std::endl;

                    // quadratic fit seems to make things worse
                    // t0_correction[iT0][i][j][k] = std::make_tuple(t0_c, t0_b, t0_a);
                    t0_correction[iT0][i][j][k] = std::make_tuple(t0_lin, 0, 0);

                    if(i==1 && j == 1) _v_t0_dt_plots.push_back(gr_T0);
                    else delete gr_T0;

                    // delete f_quad;
                    delete f_lin;

                }
            }
        }
    }

    // apply T0 correction
    for(auto& evt: _v_events) {
        Hit t0_Ref = evt.T0s.at(0);
        for(auto& p_t0: evt.T0s) {
            if(p_t0.first == 0) continue;
            Hit& t0 = p_t0.second;
            std::tuple<double, double, double> tup_correction = t0_correction[p_t0.first][t0_Ref.memoryCell][t0.memoryCell][t0.bxID];
            // may only be a linear correction
            t0.ns += std::get<0>(tup_correction) + t0.ns*std::get<1>(tup_correction) + t0.ns*t0.ns*std::get<2>(tup_correction);
        }
    }

    std::cout << std::endl;
    std::cout << "Before T0 processing, there are " << _v_events.size() << " events" << std::endl;

    int count_T0TooBigTimeDifference = 0;
    int count_T0WrongEnergy = 0;


    std::vector<Event> v_newEvents;
    for(auto event: _v_events) {
        // Mip cuts
        for(int i=0; i < (int)event.T0s.size(); i++) {
            Hit& t0 = event.T0s[i];
            if(t0.mip < _cuts.t0_mip_cuts[i]) {
                event.nT0s--;
                count_T0WrongEnergy++;
            }
        }

        if(event.nT0s < _cuts.minT0s) {
            continue;
        }

        // Calculate T0-time
        double t = 0.;
        for(const auto& t0: event.T0s) {
            t += t0.second.ns;
        }
        event.T0_time = t / event.nT0s;

        for(auto& t0: event.T0s) {
            _h_t0s->Fill(t0.second.ns - event.T0_time);
            if(fabs(t0.second.ns - event.T0_time) > _cuts.maxTimeDifference_T0) {
                event.nT0s--;
                count_T0TooBigTimeDifference++;
            }
        }

        // only take events if there are enough T0s
        if(event.nT0s < _cuts.minT0s) {
            continue;
        } else {
            v_newEvents.push_back(event);
            // for(auto& t0: v_newEvents[v_newEvents.size()].T0s) {
            //     if(t0.second.chipID < 0) std::cout << GREEN << "I report" << NORMAL << std::endl;
            // }
        }


    }



    std::cout << std::endl;
    std::cout << CYAN << "T0 resolution: " << _h_t0s->GetRMS() << " (RMS)"<< NORMAL << std::endl;
    std::cout << std::endl;

    _v_events.clear();
    _v_events = v_newEvents;

    // for(auto& evt: _v_events) {
    //     for(auto& t0: evt.T0s) {
    //         if(t0.second.chipID < 0) std::cout << GREEN << "I report" << NORMAL << std::endl;
    //     }
    // }

    std::cout << "After T0 processing, there are " << _v_events.size() << " events" << std::endl;
    std::cout << count_T0WrongEnergy << "\tT0s are thrown away because they did not survived the energy cut." << std::endl;
    std::cout << count_T0TooBigTimeDifference << "\tT0s are thrown away because they are too different in time." << std::endl;
}


/*
    Read T0 calibration from file
    The format is:
    chipID, channel, memoryCell, pedestal even BxID, maximum edge even BxID, pedestal odd BxID, maximum edge odd BxID, pedestal both BxIDs, maximum edge both BxIDs
    The latter two are not really needed anymore an more a relict from former times. TODO: delete this.
    The pedestal and maximum edge are extracted by the TDC processor from the TDC spectra.
*/
void TdcManager::readT0Calibration(const char* fn_T0_calibration) {
    std::ifstream in_file(fn_T0_calibration);

    int count = 0;

    if(in_file.is_open()) {
        std::string header;
        std::getline(in_file, header);

        std::cout << std::endl;
        std::cout << "Reading T0 Calibration: " << fn_T0_calibration << std::endl;
        std::cout << "Header:" << std::endl;
        std::cout << header << std::endl;


        int chipID, channel, memoryCell;
        double  tdc_min_even, tdc_max_even, tdc_min_odd, tdc_max_odd, tdc_min_all, tdc_max_all;
        while(in_file >> chipID >> channel >> memoryCell >> tdc_min_even >> tdc_max_even >> tdc_min_odd >> tdc_max_odd >> tdc_min_all >> tdc_max_all) {

            if(not (isT0(std::make_pair(chipID, channel)) || isBrokenT0(std::make_pair(chipID, channel)))) continue;

            double slope_even = -1.;
            double slope_odd = -1.;
            // calculates the slopes
            if(tdc_min_even > 0 && tdc_max_even > 0) {
                // Bunchcrossing length minus deadtime (4000ns * 0.98);
                slope_even = _bxPeriod * (1. - _rampDeadtime) / (double)(tdc_max_even - tdc_min_even);
            }

            if(tdc_min_odd > 0 && tdc_max_odd > 0) {
                slope_odd = _bxPeriod * (1. - _rampDeadtime) / (double)(tdc_max_odd - tdc_min_odd);
            }

            auto pair_even = std::make_pair(tdc_min_even, slope_even);
            auto pair_odd = std::make_pair(tdc_min_odd, slope_odd);

            _m_T0_calibration[chipID][channel][memoryCell][0] = pair_even;
            _m_T0_calibration[chipID][channel][memoryCell][1] = pair_odd;

            count++;
        }

        fHasReadT0Calibration = true;

    } else {
        std::cout << "Could not open file " << fn_T0_calibration;
        std::cout << " for reading calibration... does it exist?" << std::endl;
    }

    if(count == 0) {
        std::cout << "Something went wrong with the reading of the T0 calibration files. There are no calibration values saved." << std::endl;
    }
    assert(count > 0);

    std::cout << "T0 Calibration read in for " << count << " channels, memoryCell." << std::endl;
}

/*
    Reading correction to T0. This is a second order polynomial with a*x^2 + b*x + c
*/
void TdcManager::readT0Correction(const char* fn_T0_correction) {
    std::ifstream in_file(fn_T0_correction);

    int count = 0;

    if(in_file.is_open()) {
        std::string header;
        std::getline(in_file, header);

        std::cout << std::endl;
        std::cout << "Reading T0 Correction: " << fn_T0_correction << std::endl;
        std::cout << "Header:" << std::endl;
        std::cout << header << std::endl;

        int module, bxID;
        double  c, b, a, chi2;
        while(in_file >> module >> bxID >> c >> b >> a >> chi2) {

            _m_T0_correction[module][bxID] = std::make_tuple(c, b, a);

            count++;
        }

    } else {
        std::cout << "Could not open file " << fn_T0_correction;
        std::cout << " for reading calibration... does it exist?" << std::endl;
    }

    if(count == 0) {
        std::cout << "Something went wrong with the reading of the c, b, a T0 correction files. There are no correciton values saved." << std::endl;
    }
    assert(count > 0);

    fHasReadT0Correction = true;

    std::cout << "T0 Correction read in for " << count << " T0s, bxID." << std::endl;

}


/*
    read hardwareConnection from file
    -> get a map from ijk to chip and channel
*/
void TdcManager::readHardwareConnection(const char* fn_hardwareConnection) {
    std::ifstream in_file(fn_hardwareConnection);

    if(in_file.is_open()) {
        std::string header;
        std::getline(in_file, header);

        std::cout << std::endl;
        std::cout << "Reading hardwareConnection: " << fn_hardwareConnection << std::endl;
        std::cout << "Header:" << std::endl;
        std::cout << header << std::endl;

        int I, J, K, chipID, channel;
        while(in_file >> I >> J >> K >> chipID >> channel) {
            _m_ijk_to_ChipChan[std::make_tuple(I, J, K)] = std::make_pair(chipID, channel);
            _m_ChipChan_to_ijk[std::make_pair(chipID, channel)] = std::make_tuple(I, J, K);
        }

        fHasReadHardwareConnection = true;

    } else {
        std::cout << "Could not open file " << fn_hardwareConnection;
        std::cout << " for reading calibration... does it exist?" << std::endl;
    }

    std::cout << "Read in the Hardware connection for " << _m_ijk_to_ChipChan.size() << " channels." << std::endl;
    std::cout << "In total there should be: " << _v_chipIDs.size() * _nChannels << " channels. ";
    std::cout << "This is however only an estimate. Not including Ecal, but and maybe having some extra chips." << std::endl;
}


/*

*/
void TdcManager::readFitCalibration(const char* fn_fitCalibration) {

    std::cout << std::endl;
    std::cout << "Reading Fit calibration constants from " << fn_fitCalibration << std::endl;

    std::ifstream in_file(fn_fitCalibration);

    int count = 0;

    if(in_file.is_open()) {
        std::string header;
        std::getline(in_file, header);
        std::cout << header << std::endl;

        int chipID, channel, memCell, bxID;
        double offset, slope;

        while(in_file >> chipID >> channel >> memCell >> bxID >> offset >> slope) {
            // std::cout << chipID << "\t" << channel << "\t" << memCell << "\t" << bxID << "\t" << offset << "\t" << slope << std::endl;
            // set calibration constants
            _m_calibration_values[chipID][channel][memCell][bxID] = std::make_tuple(offset, slope, 0);

            // update calibrated channels
            if(slope > 0 && offset !=0) {
                if(std::find(_v_calibrated_channels.begin(), _v_calibrated_channels.end(), std::make_pair(channel, chipID)) == _v_calibrated_channels.end()) {
                    _v_calibrated_channels.push_back(std::make_pair(channel, chipID));
                }
                if(_m_calibrated_memoryCells[std::make_pair(chipID, channel)] < memCell) {
                    _m_calibrated_memoryCells[std::make_pair(chipID, channel)] = memCell;
                }
            }
            count++;

        }
    } else {
        std::cout << "Could not open " << fn_fitCalibration << std::endl;
    }

    in_file.close();

    std::cout << "I read in a total of " << count << " calibrated channels." << std::endl;
}

void TdcManager::extendFitCalibration() {
    int count = 0;
    for(int chipID: _v_chipIDs) {
        std::pair<double, double> chip_slope = std::make_pair(0, 0);
        for(int bxID = 0; bxID < 2; bxID ++) {
            std::pair<double, double> ChnOffset = std::make_pair(0, 0);
            for(int iChn = 0; iChn < _nChannels; iChn++) {
                std::pair<double, double> memCellOffset = std::make_pair(0, 0);
                for(int iMem = 0; iMem < _nMemoryCells; iMem++) {
                    std::tuple<double, double, double> t_calibrated = _m_calibration_values[chipID][iChn][iMem][bxID];
                    double offset = std::get<0>(t_calibrated);
                    double slope  = std::get<1>(t_calibrated);


                    if(bxID) {
                        // First memoryCell: set offset
                        if(iMem == 1 && offset != 0) { memCellOffset.first = offset; count++;}
                        // If there is not yet an offset estimate for a chip in the channel: set offset
                        if(ChnOffset.first == 0 && offset != 0) { ChnOffset.first = offset; count++;}
                        // If there is not yet a slope estimate for a chip in the channel: set offset
                        if(chip_slope.first == 0 && slope !=0 ) { chip_slope.first = slope; count++;}

                        // Set offset, if there is none, but there is a memCellOffset
                        if(offset == 0 && memCellOffset.first != 0) { offset = memCellOffset.first; count++;}
                        // Set offset, if there is none, but there is a ChannelOffset
                        else if(offset == 0 && ChnOffset.first != 0) { offset = ChnOffset.first; count++;}
                        // Set slope, if there is none, but there is a ChipSlope
                        if(slope == 0 && chip_slope.first != 0) { slope = chip_slope.first; count++;}

                    } else {
                        if(iMem == 1 && offset != 0) { memCellOffset.second = offset; count++;}
                        if(ChnOffset.second == 0 && offset != 0) { ChnOffset.first = offset; count++;}

                        if(chip_slope.second == 0 && slope != 0) { chip_slope.second = slope; count++;}

                        if(offset == 0 && memCellOffset.second != 0) { offset = memCellOffset.second; count++;}
                        else if(offset == 0 && ChnOffset.second != 0) {  offset = ChnOffset.second; count++;}

                        if(slope == 0 && chip_slope.second != 0) { slope = chip_slope.second; count++;}
                    }

                    _m_calibration_values[chipID][iChn][iMem][bxID] = std::make_tuple(offset, slope, 0);
                    if(offset != 0 && slope !=0) _m_calibrated_memoryCells[std::make_pair(chipID, iChn)] = iMem;
                }
            }
            if(bxID) {
                if(chip_slope.first == 0) std::cout << "Uncalibrated Chip: " << chipID << "\t" << (bxID ? "even": "odd") << std::endl;
            } else {
                if(chip_slope.second == 0) std::cout << "Uncalibrated Chip: " << chipID << "\t" << (bxID ? "even": "odd") << std::endl;
            }
        }
    }

    std::cout << RED << "[Extend Calibration] I extended the calibration to " << count << " channels" << NORMAL << std::endl;
}


/*

*/
void TdcManager::readNonLinearityCorrection(const char* fn_nonLinearity) {

    std::cout << std::endl;
    std::cout << "Reading Non Linearity Correction from " << fn_nonLinearity << std::endl;

    std::ifstream in_file(fn_nonLinearity);

    int count = 0;

    if(in_file.is_open()) {
        std::string header;
        std::getline(in_file, header);
        std::cout << header << std::endl;

        int chipID, bxID;
        double constant, lin, quad;

        while(in_file >> chipID >> bxID >> constant >> lin >> quad) {
            // std::cout << chipID << "\t" << bxID << "\t" << constant << "\t" << lin << "\t" << quad << std::endl;
            _m_nonLinearityCorrection[chipID][bxID] = std::make_tuple(constant, lin, quad);
            count++;
        }
    }

    in_file.close();

    std::cout << "I read in a total of " << count << " nonlinearity calibrated ramps." << std::endl;
}

/*
    - Fit Calibration and non linearity Correction needs to be done before
    Convert from TDC to Ns
*/
bool TdcManager::tdcToNs(Hit& hit, bool fNonLinearityCorrection, bool fTimewalkCorrection, bool fShiftTo0, bool fnHitsSameChipCorrection) {
    bool success = false;
    if(not _isSimulatedData) {
        auto tuple = _m_calibration_values[hit.chipID][hit.channel][hit.memoryCell][hit.bxID];
        // auto tuple_slope = _m_calibration_values[hit.chipID][36][1][hit.bxID];
        auto tuple_nonLin = _m_nonLinearityCorrection[hit.chipID][hit.bxID];
        double delay = std::get<0>(tuple);
        double slope = std::get<1>(tuple);
        // double slope = std::get<1>(tuple_slope);
        if(delay != 0 && slope != 0 ) {
            hit.ns = delay + slope*hit.tdc;
            if(fNonLinearityCorrection) {
                hit.ns += std::get<0>(tuple_nonLin) + hit.ns*std::get<1>(tuple_nonLin) + hit.ns*hit.ns*std::get<2>(tuple_nonLin);
            }
            success = true;
            if(fTimewalkCorrection) {
                double tw_correction = twCorrection(hit.mip);
                // just check that nothing is going wrong here
                if(tw_correction < 10.) {
                    hit.ns -= tw_correction;
                } else {
                    if(not fHasReported) {
                        std::cout << "Time walk correction is very big: " << tw_correction;
                        std::cout << " for mip = " << hit.mip;
                        fHasReported = true;
                    }
                    success = false;
                }
            }
        } else {
            hit.ns = -1;
            success = false;
        }

        // if(fnHitsSameChipCorrection) {
            // hit.ns = hit.ns - (0.0425 * hit.nHitsSameChip*hit.nHitsSameChip + 1.277 * hit.nHitsSameChip + 0.077);
            // hit.ns = hit.ns - (0.029 * hit.nHitsSameChip*hit.nHitsSameChip + 1.51 * hit.nHitsSameChip - 1.711);
        // }

        // TODO: What is happending here?
        // Turned of right now...
        // TODO: Remove option if not needed!
        if(fShiftTo0) {
            // if(_m_shift[hit.K] < 20 && _m_shift[hit.K] > -20) {
            //     hit.ns = hit.ns - _m_shift[hit.K];
            // }
            // if(_m_shift[hit.chipID] < 20 && _m_shift[hit.chipID] > -20) {
                // hit.ns = hit.ns - _m_shift[hit.chipID];
            // }
        }

    }

    if(not _isSimulatedData && not isMuon() && fnHitsSameChipCorrection) {
        hit.ns = hit.ns - _m_nHitsSameChipCorrection[hit.nHitsSameChip];
    }

    if(_isSimulatedData) {
        hit.ns = hit.ns - _m_simulationShiftByLayer[hit.K];
        success = true;
    }

    return success;
}


/*
    Calculate Center of Gravity in y direction
    TODO: Get Geometry from Somewhere
*/
double TdcManager::calculateCogZ(Event& evt) {

    std::map<int, double> m_layerPosition = getLayerPositions();

    double cogZ = 0.;
    double energySum = 0.;
    for(auto& hit: evt.v_hits) {
        cogZ += hit.mip * m_layerPosition[hit.K];
        energySum += hit.mip;
    }

    cogZ = cogZ / energySum;

    evt.cogZ = cogZ;

    return cogZ;
}

std::map<int, double> TdcManager::getLayerPositions() {
    //begin with absorber
    std::map<int, double> m_layerPosition; //mm
    // Ecal thickness: 58.5mm
    // double ecalThickness = 58.5;
    // Hcal layer thickness: 19.5mm
    double hcalLayerThickness = 19.5;
    // Position of Scintillator in layer: + 1.1mm
    // double positionOfScintillator = 1.1;
    double firstHcalLayer = 58.5;

    m_layerPosition[4] = firstHcalLayer + 10. + 0.5 + 0.6425 + 3.8575 + 1.1;
    m_layerPosition[5] = m_layerPosition[4] + 1*hcalLayerThickness;
    m_layerPosition[6] = m_layerPosition[5] + 1*hcalLayerThickness;
    m_layerPosition[7] = m_layerPosition[6] + 1*hcalLayerThickness;
    m_layerPosition[8] = m_layerPosition[7] + 1*hcalLayerThickness;
    m_layerPosition[9] = m_layerPosition[8] + 1*hcalLayerThickness;
    m_layerPosition[10] = m_layerPosition[9] + 1*hcalLayerThickness;
    m_layerPosition[11] = m_layerPosition[10] + 1*hcalLayerThickness;
    m_layerPosition[12] = m_layerPosition[11] + 1*hcalLayerThickness;
    m_layerPosition[13] = m_layerPosition[12] + 2*hcalLayerThickness;
    m_layerPosition[14] = m_layerPosition[13] + 8*hcalLayerThickness;
    m_layerPosition[15] = m_layerPosition[14] + 10*hcalLayerThickness;

    return m_layerPosition;

}


/*
    Sort the hit times into a map with respect to chip, channel, memCell and BxID.
    This is needed for calibration.
    Channel 36 includes all channels in this chip, while memoryCell 0 is for all
    memoryCells in the channel. BxID = 2 is for both BxIDs.
*/
void TdcManager::sortDataPerChannel() {
    if(not fHasReadRootFile || not fHasReadT0Calibration) {
        std::cout << (fHasReadRootFile ? "T0 Calibration" : "Hit Root File");
        std::cout << " was not read in ... aborting." << std::endl;
        return;
    }

    std::cout << std::endl;
    std::cout << "Filling data ..." << std::endl;

    for(auto& evt: _v_events) {

        double T0_time = evt.T0_time;

        for(auto& hit: evt.v_hits) {
            _m_tdcs[hit.chipID][hit.channel][hit.memoryCell][hit.bxID].push_back(std::pair<double, Hit&>(T0_time,hit));
            _m_tdcs[hit.chipID][hit.channel][hit.memoryCell][2].push_back(std::pair<double, Hit&>(T0_time,hit));
            _m_tdcs[hit.chipID][36][hit.memoryCell][hit.bxID].push_back(std::pair<double, Hit&>(T0_time, hit));
            _m_tdcs[hit.chipID][36][hit.memoryCell][2].push_back(std::pair<double, Hit&>(T0_time, hit));

            _m_tdcs[hit.chipID][hit.channel][0][hit.bxID].push_back(std::pair<double, Hit&>(T0_time, hit));
            _m_tdcs[hit.chipID][hit.channel][0][2].push_back(std::pair<double, Hit&>(T0_time, hit));

            _m_tdcs[hit.chipID][36][0][hit.bxID].push_back(std::pair<double, Hit&>(T0_time, hit));
            _m_tdcs[hit.chipID][36][0][2].push_back(std::pair<double, Hit&>(T0_time, hit));
        }
    }
}

void TdcManager::calibrate(bool fNonLinearityCorrection, bool fTimewalkCorrection, bool fShiftTo0, bool fnHitsSameChipCorrection) {
    std::cout << std::endl;
    std::cout << "Calibrating all hits..." << std::endl;
    std::cout << "with: " << std::endl;
    if(fNonLinearityCorrection)     std::cout << "\t* Non-linearity correction" << std::endl;
    if(fTimewalkCorrection)         std::cout << "\t* Timewalk correction" << std::endl;
    if(fShiftTo0)                   std::cout << "\t* Shift To 0" << std::endl;
    if(fnHitsSameChipCorrection)    std::cout << "\t* Number of Hits in the same chip correction" << std::endl;

    int count = 0;
    int count_successful = 0;
    for(auto& evt: _v_events) {
        for(auto& hit: evt.v_hits) {
            if(tdcToNs(hit, fNonLinearityCorrection, fTimewalkCorrection, fShiftTo0, fnHitsSameChipCorrection)) count_successful++;
            count++;
        }
    }

    std::cout << "Calibrated " << count_successful << " / " << count << " (" << count_successful / (double)count * 100. << "%) hits." << std::endl;
}

double DoubleGaussConvTS(double *x, double *par) {
    double Norm1 = par[0];
    double arg1 = (x[0] - par[1])*(x[0] - par[1])/( 2*( par[2]*par[2] + par[6]*par[6] ) );

    double Norm2 = par[3];
    double arg2 = (x[0] - par[4])*(x[0] - par[4])/( 2*( par[5]*par[5] + par[6]*par[6] ) );

    return Norm1 * TMath::Exp( - arg1 ) + Norm2 * TMath::Exp( - arg2 );
}

void TdcManager::doTimeSmearing() {
    if(not _isSimulatedData) {
        std::cout << "NOT doing time smearing, because data is not simulated." << std::endl;
    } else {
        std::cout << "Doing time smearing" << std::endl;

        std::vector<double> v_sigma = {
            2.27,  4.26,  5.69,  7.05,  8.19, 9.12, 9.98, 10.64, 11.32, 12.12, 12.78, 13.48,
            14.48, 15.08, 15.89, 16.39, 17.0, 17.0, 17.0, 17.0,  17.0,  17.0,  17.0,  17.0,
            17.0,  17.0,  17.0,  17.0,  17.0, 17.0, 17.0, 17.0,  17.0,  17.0,  17.0,  17.0
        };

        std::map<int, TF1*> m_function;
        for(int i=0; i < 36; i++) {
            std::stringstream fcn_name;
            fcn_name << "DoubleGaussConvTS_" << i;

            m_function[i] = new TF1(fcn_name.str().c_str(), DoubleGaussConvTS, -4000, 4000, 7);

            m_function[i]->FixParameter(0., 4745);
            m_function[i]->FixParameter(1, 0.048);
            m_function[i]->FixParameter(2, 3.153);
            m_function[i]->FixParameter(3., 4164);
            m_function[i]->FixParameter(4, -0.198);
            m_function[i]->FixParameter(5, 6.459);
            m_function[i]->FixParameter(6., v_sigma[i]);
            m_function[i]->SetNpx(8000);
        }

        for(auto& evt: _v_events) {
            for(auto& hit: evt.v_hits) {
                assert(hit.nHitsSameChip < 36);
                if(hit.nHitsSameChip < 1) hit.nHitsSameChip = 1;
                hit.ns = hit.ns + m_function[hit.nHitsSameChip-1]->GetRandom();
            }
        }
    }
}

// void TdcManager::writeFigures(TFile* o_file) {
//
//     if(o_file->IsOpen()) {
//         // _hitmap_rawHits->write(o_file);
//     } else {
//         std::cout << "TdcManager: Could not open output file... " << std::endl;
//         std::cout << "Figures are not written!" << std::endl;
//     }
//
// }
