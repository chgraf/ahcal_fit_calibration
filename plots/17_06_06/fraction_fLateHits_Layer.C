{
//=========Macro generated from canvas: c_fLateHits_layer/c_fLateHits_layer
//=========  (Thu Jun  8 10:57:26 2017) by ROOT version5.34/30
   TCanvas *c_fLateHits_layer = new TCanvas("c_fLateHits_layer", "c_fLateHits_layer",57,52,700,500);
   gStyle->SetOptStat(0);
   c_fLateHits_layer->SetHighLightColor(2);
   c_fLateHits_layer->Range(-2.5,-0.02324779,22.5,0.2092301);
   c_fLateHits_layer->SetFillColor(0);
   c_fLateHits_layer->SetBorderMode(0);
   c_fLateHits_layer->SetBorderSize(2);
   c_fLateHits_layer->SetFrameBorderMode(0);
   c_fLateHits_layer->SetFrameBorderMode(0);
   
   TH1D *h_fLateHits_layer = new TH1D("h_fLateHits_layer","Fraction of late hits over layer",20,0,20);
   h_fLateHits_layer->SetBinContent(5,0.1147128);
   h_fLateHits_layer->SetBinContent(8,0.1145109);
   h_fLateHits_layer->SetBinContent(9,0.1285387);
   h_fLateHits_layer->SetBinContent(10,0.1254202);
   h_fLateHits_layer->SetBinContent(11,0.1599191);
   h_fLateHits_layer->SetBinContent(12,0.1425379);
   h_fLateHits_layer->SetBinContent(14,0.177126);
   h_fLateHits_layer->SetBinContent(15,0.1731752);
   h_fLateHits_layer->SetBinContent(16,0.07316827);
   h_fLateHits_layer->SetEntries(9);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000033");
   h_fLateHits_layer->SetLineColor(ci);
   h_fLateHits_layer->SetLineWidth(2);
   h_fLateHits_layer->GetXaxis()->SetTitle("Layer");
   h_fLateHits_layer->GetXaxis()->SetLabelFont(43);
   h_fLateHits_layer->GetXaxis()->SetLabelSize(25);
   h_fLateHits_layer->GetXaxis()->SetTitleSize(25);
   h_fLateHits_layer->GetXaxis()->SetTitleFont(43);
   h_fLateHits_layer->GetYaxis()->SetTitle("Fraction of late Hits > 50ns");
   h_fLateHits_layer->GetYaxis()->SetLabelFont(43);
   h_fLateHits_layer->GetYaxis()->SetLabelSize(25);
   h_fLateHits_layer->GetYaxis()->SetTitleSize(25);
   h_fLateHits_layer->GetYaxis()->SetTitleFont(43);
   h_fLateHits_layer->GetZaxis()->SetLabelFont(42);
   h_fLateHits_layer->GetZaxis()->SetLabelSize(0.035);
   h_fLateHits_layer->GetZaxis()->SetTitleSize(0.035);
   h_fLateHits_layer->GetZaxis()->SetTitleFont(42);
   h_fLateHits_layer->Draw("");
   
   TH1D *h_fLateHits_layer = new TH1D("h_fLateHits_layer","Fraction of late hits over layer",20,0,20);
   h_fLateHits_layer->SetBinContent(5,0.07298238);
   h_fLateHits_layer->SetBinContent(8,0.04924398);
   h_fLateHits_layer->SetBinContent(9,0.05440201);
   h_fLateHits_layer->SetBinContent(10,0.05720525);
   h_fLateHits_layer->SetBinContent(11,0.07006215);
   h_fLateHits_layer->SetBinContent(12,0.04479497);
   h_fLateHits_layer->SetBinContent(14,0.09389669);
   h_fLateHits_layer->SetBinContent(15,0.09626914);
   h_fLateHits_layer->SetBinContent(16,0.09246219);
   h_fLateHits_layer->SetEntries(9);

   ci = TColor::GetColor("#3366ff");
   h_fLateHits_layer->SetLineColor(ci);
   h_fLateHits_layer->SetLineWidth(2);
   h_fLateHits_layer->GetXaxis()->SetTitle("Hit layer");
   h_fLateHits_layer->GetXaxis()->SetLabelFont(42);
   h_fLateHits_layer->GetXaxis()->SetLabelSize(0.035);
   h_fLateHits_layer->GetXaxis()->SetTitleSize(0.035);
   h_fLateHits_layer->GetXaxis()->SetTitleFont(42);
   h_fLateHits_layer->GetYaxis()->SetTitle("Fraction of late Hits > 50ns");
   h_fLateHits_layer->GetYaxis()->SetLabelFont(42);
   h_fLateHits_layer->GetYaxis()->SetLabelSize(0.035);
   h_fLateHits_layer->GetYaxis()->SetTitleSize(0.035);
   h_fLateHits_layer->GetYaxis()->SetTitleFont(42);
   h_fLateHits_layer->GetZaxis()->SetLabelFont(42);
   h_fLateHits_layer->GetZaxis()->SetLabelSize(0.035);
   h_fLateHits_layer->GetZaxis()->SetTitleSize(0.035);
   h_fLateHits_layer->GetZaxis()->SetTitleFont(42);
   h_fLateHits_layer->Draw("SAME");
   
   TLegend *leg = new TLegend(0.1005747,0.7012712,0.4008621,0.9004237,NULL,"brNDC");
   leg->SetBorderSize(1);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("h_fLateHits_layer","Simulation","lpf");
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#000033");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(1);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("h_fLateHits_layer","Data","lpf");
   entry->SetFillStyle(1001);

   ci = TColor::GetColor("#3366ff");
   entry->SetLineColor(ci);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(1);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   
   TPaveText *pt = new TPaveText(0.2450287,0.9339831,0.7549713,0.995,"blNDC");
   pt->SetName("title");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextFont(42);
   TText *text = pt->AddText("Fraction of late hits over layer");
   pt->Draw();
   c_fLateHits_layer->Modified();
   c_fLateHits_layer->cd();
   c_fLateHits_layer->SetSelected(c_fLateHits_layer);
}
