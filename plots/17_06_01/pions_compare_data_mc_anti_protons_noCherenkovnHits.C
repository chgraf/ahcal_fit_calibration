{
//=========Macro generated from canvas: c_nHits/c_nHits
//=========  (Thu Jun  1 14:52:18 2017) by ROOT version5.34/30
   TCanvas *c_nHits = new TCanvas("c_nHits", "c_nHits",0,0,700,500);
   gStyle->SetOptStat(0);
   c_nHits->SetHighLightColor(2);
   c_nHits->Range(0,0,1,1);
   c_nHits->SetFillColor(0);
   c_nHits->SetBorderMode(0);
   c_nHits->SetBorderSize(2);
   c_nHits->SetFrameBorderMode(0);
   
   TH1D *h_hits_nHits = new TH1D("h_hits_nHits","Number of Hits After Data Selection",200,0,200);
   h_hits_nHits->SetBinContent(19,0.0004702563);
   h_hits_nHits->SetBinContent(20,0.003526922);
   h_hits_nHits->SetBinContent(21,0.002351281);
   h_hits_nHits->SetBinContent(22,0.002821538);
   h_hits_nHits->SetBinContent(23,0.005643075);
   h_hits_nHits->SetBinContent(24,0.004232307);
   h_hits_nHits->SetBinContent(25,0.003526922);
   h_hits_nHits->SetBinContent(26,0.005407947);
   h_hits_nHits->SetBinContent(27,0.005407947);
   h_hits_nHits->SetBinContent(28,0.005643075);
   h_hits_nHits->SetBinContent(29,0.005407947);
   h_hits_nHits->SetBinContent(30,0.004702563);
   h_hits_nHits->SetBinContent(31,0.005172819);
   h_hits_nHits->SetBinContent(32,0.007053844);
   h_hits_nHits->SetBinContent(33,0.006113332);
   h_hits_nHits->SetBinContent(34,0.006113332);
   h_hits_nHits->SetBinContent(35,0.003056666);
   h_hits_nHits->SetBinContent(36,0.005407947);
   h_hits_nHits->SetBinContent(37,0.005878204);
   h_hits_nHits->SetBinContent(38,0.007994357);
   h_hits_nHits->SetBinContent(39,0.005643075);
   h_hits_nHits->SetBinContent(40,0.005172819);
   h_hits_nHits->SetBinContent(41,0.004232307);
   h_hits_nHits->SetBinContent(42,0.005643075);
   h_hits_nHits->SetBinContent(43,0.008699741);
   h_hits_nHits->SetBinContent(44,0.00634846);
   h_hits_nHits->SetBinContent(45,0.006818716);
   h_hits_nHits->SetBinContent(46,0.006818716);
   h_hits_nHits->SetBinContent(47,0.005878204);
   h_hits_nHits->SetBinContent(48,0.00634846);
   h_hits_nHits->SetBinContent(49,0.005407947);
   h_hits_nHits->SetBinContent(50,0.006113332);
   h_hits_nHits->SetBinContent(51,0.007759229);
   h_hits_nHits->SetBinContent(52,0.006818716);
   h_hits_nHits->SetBinContent(53,0.005407947);
   h_hits_nHits->SetBinContent(54,0.005407947);
   h_hits_nHits->SetBinContent(55,0.005643075);
   h_hits_nHits->SetBinContent(56,0.00634846);
   h_hits_nHits->SetBinContent(57,0.006113332);
   h_hits_nHits->SetBinContent(58,0.008464613);
   h_hits_nHits->SetBinContent(59,0.007524101);
   h_hits_nHits->SetBinContent(60,0.006113332);
   h_hits_nHits->SetBinContent(61,0.007288972);
   h_hits_nHits->SetBinContent(62,0.006818716);
   h_hits_nHits->SetBinContent(63,0.006113332);
   h_hits_nHits->SetBinContent(64,0.007053844);
   h_hits_nHits->SetBinContent(65,0.007994357);
   h_hits_nHits->SetBinContent(66,0.004937691);
   h_hits_nHits->SetBinContent(67,0.005407947);
   h_hits_nHits->SetBinContent(68,0.004937691);
   h_hits_nHits->SetBinContent(69,0.005643075);
   h_hits_nHits->SetBinContent(70,0.007053844);
   h_hits_nHits->SetBinContent(71,0.006818716);
   h_hits_nHits->SetBinContent(72,0.004937691);
   h_hits_nHits->SetBinContent(73,0.004937691);
   h_hits_nHits->SetBinContent(74,0.008229485);
   h_hits_nHits->SetBinContent(75,0.008464613);
   h_hits_nHits->SetBinContent(76,0.006113332);
   h_hits_nHits->SetBinContent(77,0.006113332);
   h_hits_nHits->SetBinContent(78,0.005878204);
   h_hits_nHits->SetBinContent(79,0.007053844);
   h_hits_nHits->SetBinContent(80,0.006113332);
   h_hits_nHits->SetBinContent(81,0.008464613);
   h_hits_nHits->SetBinContent(82,0.008699741);
   h_hits_nHits->SetBinContent(83,0.008464613);
   h_hits_nHits->SetBinContent(84,0.004232307);
   h_hits_nHits->SetBinContent(85,0.008229485);
   h_hits_nHits->SetBinContent(86,0.01128615);
   h_hits_nHits->SetBinContent(87,0.007759229);
   h_hits_nHits->SetBinContent(88,0.01269692);
   h_hits_nHits->SetBinContent(89,0.007288972);
   h_hits_nHits->SetBinContent(90,0.005407947);
   h_hits_nHits->SetBinContent(91,0.01105102);
   h_hits_nHits->SetBinContent(92,0.007053844);
   h_hits_nHits->SetBinContent(93,0.009640254);
   h_hits_nHits->SetBinContent(94,0.01081589);
   h_hits_nHits->SetBinContent(95,0.007759229);
   h_hits_nHits->SetBinContent(96,0.00893487);
   h_hits_nHits->SetBinContent(97,0.00893487);
   h_hits_nHits->SetBinContent(98,0.01128615);
   h_hits_nHits->SetBinContent(99,0.007994357);
   h_hits_nHits->SetBinContent(100,0.01222666);
   h_hits_nHits->SetBinContent(101,0.01034564);
   h_hits_nHits->SetBinContent(102,0.01034564);
   h_hits_nHits->SetBinContent(103,0.01058077);
   h_hits_nHits->SetBinContent(104,0.009169998);
   h_hits_nHits->SetBinContent(105,0.008464613);
   h_hits_nHits->SetBinContent(106,0.008699741);
   h_hits_nHits->SetBinContent(107,0.008229485);
   h_hits_nHits->SetBinContent(108,0.009875382);
   h_hits_nHits->SetBinContent(109,0.01128615);
   h_hits_nHits->SetBinContent(110,0.01152128);
   h_hits_nHits->SetBinContent(111,0.009640254);
   h_hits_nHits->SetBinContent(112,0.01128615);
   h_hits_nHits->SetBinContent(113,0.01128615);
   h_hits_nHits->SetBinContent(114,0.01034564);
   h_hits_nHits->SetBinContent(115,0.01199154);
   h_hits_nHits->SetBinContent(116,0.01034564);
   h_hits_nHits->SetBinContent(117,0.007759229);
   h_hits_nHits->SetBinContent(118,0.01246179);
   h_hits_nHits->SetBinContent(119,0.01011051);
   h_hits_nHits->SetBinContent(120,0.009875382);
   h_hits_nHits->SetBinContent(121,0.01011051);
   h_hits_nHits->SetBinContent(122,0.01034564);
   h_hits_nHits->SetBinContent(123,0.01152128);
   h_hits_nHits->SetBinContent(124,0.01128615);
   h_hits_nHits->SetBinContent(125,0.009169998);
   h_hits_nHits->SetBinContent(126,0.007524101);
   h_hits_nHits->SetBinContent(127,0.01081589);
   h_hits_nHits->SetBinContent(128,0.008699741);
   h_hits_nHits->SetBinContent(129,0.007288972);
   h_hits_nHits->SetBinContent(130,0.00893487);
   h_hits_nHits->SetBinContent(131,0.01246179);
   h_hits_nHits->SetBinContent(132,0.007759229);
   h_hits_nHits->SetBinContent(133,0.008229485);
   h_hits_nHits->SetBinContent(134,0.01105102);
   h_hits_nHits->SetBinContent(135,0.005878204);
   h_hits_nHits->SetBinContent(136,0.007759229);
   h_hits_nHits->SetBinContent(137,0.009169998);
   h_hits_nHits->SetBinContent(138,0.007288972);
   h_hits_nHits->SetBinContent(139,0.009169998);
   h_hits_nHits->SetBinContent(140,0.005878204);
   h_hits_nHits->SetBinContent(141,0.006113332);
   h_hits_nHits->SetBinContent(142,0.004467435);
   h_hits_nHits->SetBinContent(143,0.005643075);
   h_hits_nHits->SetBinContent(144,0.004937691);
   h_hits_nHits->SetBinContent(145,0.003526922);
   h_hits_nHits->SetBinContent(146,0.003526922);
   h_hits_nHits->SetBinContent(147,0.005407947);
   h_hits_nHits->SetBinContent(148,0.004702563);
   h_hits_nHits->SetBinContent(149,0.00258641);
   h_hits_nHits->SetBinContent(150,0.003056666);
   h_hits_nHits->SetBinContent(151,0.00258641);
   h_hits_nHits->SetBinContent(152,0.002821538);
   h_hits_nHits->SetBinContent(153,0.001881025);
   h_hits_nHits->SetBinContent(154,0.003056666);
   h_hits_nHits->SetBinContent(155,0.001881025);
   h_hits_nHits->SetBinContent(156,0.001175641);
   h_hits_nHits->SetBinContent(157,0.00258641);
   h_hits_nHits->SetBinContent(158,0.002116153);
   h_hits_nHits->SetBinContent(159,0.0007053844);
   h_hits_nHits->SetBinContent(160,0.001645897);
   h_hits_nHits->SetBinContent(161,0.001410769);
   h_hits_nHits->SetBinContent(162,0.001645897);
   h_hits_nHits->SetBinContent(163,0.001175641);
   h_hits_nHits->SetBinContent(164,0.0009405126);
   h_hits_nHits->SetBinContent(165,0.0002351281);
   h_hits_nHits->SetBinContent(166,0.001410769);
   h_hits_nHits->SetBinContent(167,0.0009405126);
   h_hits_nHits->SetBinContent(168,0.0004702563);
   h_hits_nHits->SetBinContent(169,0.0002351281);
   h_hits_nHits->SetBinContent(171,0.0004702563);
   h_hits_nHits->SetBinContent(172,0.0002351281);
   h_hits_nHits->SetBinContent(173,0.0002351281);
   h_hits_nHits->SetBinContent(175,0.0007053844);
   h_hits_nHits->SetBinContent(176,0.0002351281);
   h_hits_nHits->SetBinContent(178,0.0002351281);
   h_hits_nHits->SetBinContent(184,0.0002351281);
   h_hits_nHits->SetEntries(4253);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#339999");
   h_hits_nHits->SetLineColor(ci);
   h_hits_nHits->SetLineWidth(2);
   h_hits_nHits->GetXaxis()->SetTitle("Number of Hits");
   h_hits_nHits->GetXaxis()->SetLabelFont(43);
   h_hits_nHits->GetXaxis()->SetLabelSize(20);
   h_hits_nHits->GetXaxis()->SetTitleSize(20);
   h_hits_nHits->GetXaxis()->SetTitleFont(43);
   h_hits_nHits->GetYaxis()->SetTitle("Normalized Entries");
   h_hits_nHits->GetYaxis()->SetLabelFont(43);
   h_hits_nHits->GetYaxis()->SetLabelSize(20);
   h_hits_nHits->GetYaxis()->SetTitleSize(20);
   h_hits_nHits->GetYaxis()->SetTitleOffset(1.3);
   h_hits_nHits->GetYaxis()->SetTitleFont(43);
   h_hits_nHits->GetZaxis()->SetLabelFont(42);
   h_hits_nHits->GetZaxis()->SetLabelSize(0.035);
   h_hits_nHits->GetZaxis()->SetTitleSize(0.035);
   h_hits_nHits->GetZaxis()->SetTitleFont(42);
   h_hits_nHits->Draw("");
   
   TH1D *h_hits_nHits = new TH1D("h_hits_nHits","Number of Hits After Data Selection",200,0,200);
   h_hits_nHits->SetBinContent(15,1.847746e-05);
   h_hits_nHits->SetBinContent(17,1.847746e-05);
   h_hits_nHits->SetBinContent(19,3.695492e-05);
   h_hits_nHits->SetBinContent(20,5.543237e-05);
   h_hits_nHits->SetBinContent(21,0.0003695492);
   h_hits_nHits->SetBinContent(22,0.003677014);
   h_hits_nHits->SetBinContent(23,0.004231338);
   h_hits_nHits->SetBinContent(24,0.003880266);
   h_hits_nHits->SetBinContent(25,0.004416112);
   h_hits_nHits->SetBinContent(26,0.005173688);
   h_hits_nHits->SetBinContent(27,0.005413895);
   h_hits_nHits->SetBinContent(28,0.004988914);
   h_hits_nHits->SetBinContent(29,0.005764967);
   h_hits_nHits->SetBinContent(30,0.005432373);
   h_hits_nHits->SetBinContent(31,0.005432373);
   h_hits_nHits->SetBinContent(32,0.006226903);
   h_hits_nHits->SetBinContent(33,0.006504065);
   h_hits_nHits->SetBinContent(34,0.007021434);
   h_hits_nHits->SetBinContent(35,0.006892092);
   h_hits_nHits->SetBinContent(36,0.006411678);
   h_hits_nHits->SetBinContent(37,0.007039911);
   h_hits_nHits->SetBinContent(38,0.006485588);
   h_hits_nHits->SetBinContent(39,0.006522542);
   h_hits_nHits->SetBinContent(40,0.006744272);
   h_hits_nHits->SetBinContent(41,0.00740946);
   h_hits_nHits->SetBinContent(42,0.006855137);
   h_hits_nHits->SetBinContent(43,0.006651885);
   h_hits_nHits->SetBinContent(44,0.006670362);
   h_hits_nHits->SetBinContent(45,0.006430155);
   h_hits_nHits->SetBinContent(46,0.007298596);
   h_hits_nHits->SetBinContent(47,0.007520325);
   h_hits_nHits->SetBinContent(48,0.006818182);
   h_hits_nHits->SetBinContent(49,0.007261641);
   h_hits_nHits->SetBinContent(50,0.007169254);
   h_hits_nHits->SetBinContent(51,0.007150776);
   h_hits_nHits->SetBinContent(52,0.007187731);
   h_hits_nHits->SetBinContent(53,0.00755728);
   h_hits_nHits->SetBinContent(54,0.007298596);
   h_hits_nHits->SetBinContent(55,0.00631929);
   h_hits_nHits->SetBinContent(56,0.007039911);
   h_hits_nHits->SetBinContent(57,0.006799704);
   h_hits_nHits->SetBinContent(58,0.006633407);
   h_hits_nHits->SetBinContent(59,0.006097561);
   h_hits_nHits->SetBinContent(60,0.00646711);
   h_hits_nHits->SetBinContent(61,0.005820399);
   h_hits_nHits->SetBinContent(62,0.005968219);
   h_hits_nHits->SetBinContent(63,0.006522542);
   h_hits_nHits->SetBinContent(64,0.005857354);
   h_hits_nHits->SetBinContent(65,0.00545085);
   h_hits_nHits->SetBinContent(66,0.005561715);
   h_hits_nHits->SetBinContent(67,0.005561715);
   h_hits_nHits->SetBinContent(68,0.005358463);
   h_hits_nHits->SetBinContent(69,0.004970436);
   h_hits_nHits->SetBinContent(70,0.006116038);
   h_hits_nHits->SetBinContent(71,0.00537694);
   h_hits_nHits->SetBinContent(72,0.005709534);
   h_hits_nHits->SetBinContent(73,0.005469327);
   h_hits_nHits->SetBinContent(74,0.005801922);
   h_hits_nHits->SetBinContent(75,0.005764967);
   h_hits_nHits->SetBinContent(76,0.005857354);
   h_hits_nHits->SetBinContent(77,0.006374723);
   h_hits_nHits->SetBinContent(78,0.006042129);
   h_hits_nHits->SetBinContent(79,0.005875831);
   h_hits_nHits->SetBinContent(80,0.005764967);
   h_hits_nHits->SetBinContent(81,0.006337768);
   h_hits_nHits->SetBinContent(82,0.006171471);
   h_hits_nHits->SetBinContent(83,0.006762749);
   h_hits_nHits->SetBinContent(84,0.006781227);
   h_hits_nHits->SetBinContent(85,0.006744272);
   h_hits_nHits->SetBinContent(86,0.006670362);
   h_hits_nHits->SetBinContent(87,0.006799704);
   h_hits_nHits->SetBinContent(88,0.006984479);
   h_hits_nHits->SetBinContent(89,0.007206208);
   h_hits_nHits->SetBinContent(90,0.006781227);
   h_hits_nHits->SetBinContent(91,0.007612712);
   h_hits_nHits->SetBinContent(92,0.007834442);
   h_hits_nHits->SetBinContent(93,0.007575758);
   h_hits_nHits->SetBinContent(94,0.007871397);
   h_hits_nHits->SetBinContent(95,0.00748337);
   h_hits_nHits->SetBinContent(96,0.007095344);
   h_hits_nHits->SetBinContent(97,0.008185514);
   h_hits_nHits->SetBinContent(98,0.008425721);
   h_hits_nHits->SetBinContent(99,0.00872136);
   h_hits_nHits->SetBinContent(100,0.009127864);
   h_hits_nHits->SetBinContent(101,0.008056171);
   h_hits_nHits->SetBinContent(102,0.009127864);
   h_hits_nHits->SetBinContent(103,0.009497413);
   h_hits_nHits->SetBinContent(104,0.008961567);
   h_hits_nHits->SetBinContent(105,0.008924612);
   h_hits_nHits->SetBinContent(106,0.009626755);
   h_hits_nHits->SetBinContent(107,0.009571323);
   h_hits_nHits->SetBinContent(108,0.01034738);
   h_hits_nHits->SetBinContent(109,0.009922395);
   h_hits_nHits->SetBinContent(110,0.01095713);
   h_hits_nHits->SetBinContent(111,0.009497413);
   h_hits_nHits->SetBinContent(112,0.01005174);
   h_hits_nHits->SetBinContent(113,0.009774575);
   h_hits_nHits->SetBinContent(114,0.01023651);
   h_hits_nHits->SetBinContent(115,0.009756098);
   h_hits_nHits->SetBinContent(116,0.01064302);
   h_hits_nHits->SetBinContent(117,0.01031042);
   h_hits_nHits->SetBinContent(118,0.01097561);
   h_hits_nHits->SetBinContent(119,0.009405026);
   h_hits_nHits->SetBinContent(120,0.01043976);
   h_hits_nHits->SetBinContent(121,0.01036585);
   h_hits_nHits->SetBinContent(122,0.01031042);
   h_hits_nHits->SetBinContent(123,0.009866962);
   h_hits_nHits->SetBinContent(124,0.009996305);
   h_hits_nHits->SetBinContent(125,0.00995935);
   h_hits_nHits->SetBinContent(126,0.01025499);
   h_hits_nHits->SetBinContent(127,0.01025499);
   h_hits_nHits->SetBinContent(128,0.009552846);
   h_hits_nHits->SetBinContent(129,0.009719143);
   h_hits_nHits->SetBinContent(130,0.009608278);
   h_hits_nHits->SetBinContent(131,0.008776792);
   h_hits_nHits->SetBinContent(132,0.008444198);
   h_hits_nHits->SetBinContent(133,0.009312639);
   h_hits_nHits->SetBinContent(134,0.008407243);
   h_hits_nHits->SetBinContent(135,0.008148559);
   h_hits_nHits->SetBinContent(136,0.00849963);
   h_hits_nHits->SetBinContent(137,0.007298596);
   h_hits_nHits->SetBinContent(138,0.008019217);
   h_hits_nHits->SetBinContent(139,0.007686622);
   h_hits_nHits->SetBinContent(140,0.006411678);
   h_hits_nHits->SetBinContent(141,0.00740946);
   h_hits_nHits->SetBinContent(142,0.006300813);
   h_hits_nHits->SetBinContent(143,0.006356245);
   h_hits_nHits->SetBinContent(144,0.006079084);
   h_hits_nHits->SetBinContent(145,0.005617147);
   h_hits_nHits->SetBinContent(146,0.005487805);
   h_hits_nHits->SetBinContent(147,0.004951959);
   h_hits_nHits->SetBinContent(148,0.005192166);
   h_hits_nHits->SetBinContent(149,0.004305248);
   h_hits_nHits->SetBinContent(150,0.004674797);
   h_hits_nHits->SetBinContent(151,0.003510717);
   h_hits_nHits->SetBinContent(152,0.003566149);
   h_hits_nHits->SetBinContent(153,0.003917221);
   h_hits_nHits->SetBinContent(154,0.003658537);
   h_hits_nHits->SetBinContent(155,0.003455285);
   h_hits_nHits->SetBinContent(156,0.003067258);
   h_hits_nHits->SetBinContent(157,0.002845528);
   h_hits_nHits->SetBinContent(158,0.002790096);
   h_hits_nHits->SetBinContent(159,0.002235772);
   h_hits_nHits->SetBinContent(160,0.002217295);
   h_hits_nHits->SetBinContent(161,0.00203252);
   h_hits_nHits->SetBinContent(162,0.001681449);
   h_hits_nHits->SetBinContent(163,0.001256467);
   h_hits_nHits->SetBinContent(164,0.001348854);
   h_hits_nHits->SetBinContent(165,0.001348854);
   h_hits_nHits->SetBinContent(166,0.001459719);
   h_hits_nHits->SetBinContent(167,0.0008314856);
   h_hits_nHits->SetBinContent(168,0.0009977827);
   h_hits_nHits->SetBinContent(169,0.0008684405);
   h_hits_nHits->SetBinContent(170,0.0007206208);
   h_hits_nHits->SetBinContent(171,0.0009793052);
   h_hits_nHits->SetBinContent(172,0.0006651885);
   h_hits_nHits->SetBinContent(173,0.0006097561);
   h_hits_nHits->SetBinContent(174,0.000646711);
   h_hits_nHits->SetBinContent(175,0.0007390983);
   h_hits_nHits->SetBinContent(176,0.000646711);
   h_hits_nHits->SetBinContent(177,0.0005728012);
   h_hits_nHits->SetBinContent(178,0.0004619364);
   h_hits_nHits->SetBinContent(179,0.0003695492);
   h_hits_nHits->SetBinContent(180,0.0004619364);
   h_hits_nHits->SetBinContent(181,0.0002956393);
   h_hits_nHits->SetBinContent(182,0.0004065041);
   h_hits_nHits->SetBinContent(183,0.0002586844);
   h_hits_nHits->SetBinContent(184,0.0002771619);
   h_hits_nHits->SetBinContent(185,0.0002217295);
   h_hits_nHits->SetBinContent(186,0.0003141168);
   h_hits_nHits->SetBinContent(187,0.000203252);
   h_hits_nHits->SetBinContent(188,0.0001478197);
   h_hits_nHits->SetBinContent(189,0.0002402069);
   h_hits_nHits->SetBinContent(190,0.0002402069);
   h_hits_nHits->SetBinContent(191,0.0001478197);
   h_hits_nHits->SetBinContent(192,0.000203252);
   h_hits_nHits->SetBinContent(193,0.0001293422);
   h_hits_nHits->SetBinContent(194,0.0001108647);
   h_hits_nHits->SetBinContent(195,9.238729e-05);
   h_hits_nHits->SetBinContent(196,0.0001108647);
   h_hits_nHits->SetBinContent(197,0.0001293422);
   h_hits_nHits->SetBinContent(198,0.0001293422);
   h_hits_nHits->SetBinContent(199,0.0001478197);
   h_hits_nHits->SetBinContent(200,0.0001293422);
   h_hits_nHits->SetBinContent(201,0.002956393);
   h_hits_nHits->SetEntries(54120);

   ci = TColor::GetColor("#3366ff");
   h_hits_nHits->SetLineColor(ci);
   h_hits_nHits->SetLineWidth(2);
   h_hits_nHits->GetXaxis()->SetTitle("# Hits");
   h_hits_nHits->GetXaxis()->SetLabelFont(42);
   h_hits_nHits->GetXaxis()->SetLabelSize(0.035);
   h_hits_nHits->GetXaxis()->SetTitleSize(0.035);
   h_hits_nHits->GetXaxis()->SetTitleFont(42);
   h_hits_nHits->GetYaxis()->SetTitle("# Entries");
   h_hits_nHits->GetYaxis()->SetLabelFont(42);
   h_hits_nHits->GetYaxis()->SetLabelSize(0.035);
   h_hits_nHits->GetYaxis()->SetTitleSize(0.035);
   h_hits_nHits->GetYaxis()->SetTitleFont(42);
   h_hits_nHits->GetZaxis()->SetLabelFont(42);
   h_hits_nHits->GetZaxis()->SetLabelSize(0.035);
   h_hits_nHits->GetZaxis()->SetTitleSize(0.035);
   h_hits_nHits->GetZaxis()->SetTitleFont(42);
   h_hits_nHits->Draw("SAME");
   
   TLegend *leg = new TLegend(-2.353437e-185,-2.353437e-185,-2.353437e-185,-2.353437e-185,NULL,"brNDC");
   leg->SetBorderSize(1);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("h_hits_nHits","70GeV Anti Protons","lpf");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry=leg->AddEntry("h_hits_nHits","70GeV Pions Data noCherenkov","lpf");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   leg->Draw();
   c_nHits->Modified();
   c_nHits->cd();
   c_nHits->SetSelected(c_nHits);
}
