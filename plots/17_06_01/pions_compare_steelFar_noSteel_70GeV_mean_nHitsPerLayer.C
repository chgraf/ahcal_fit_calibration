{
//=========Macro generated from canvas: c_mean_nHitsPerLayer/c_mean_nHitsPerLayer
//=========  (Thu Jun  1 14:16:19 2017) by ROOT version5.34/30
   TCanvas *c_mean_nHitsPerLayer = new TCanvas("c_mean_nHitsPerLayer", "c_mean_nHitsPerLayer",57,52,700,500);
   gStyle->SetOptStat(0);
   c_mean_nHitsPerLayer->SetHighLightColor(2);
   c_mean_nHitsPerLayer->Range(0,0,1,1);
   c_mean_nHitsPerLayer->SetFillColor(0);
   c_mean_nHitsPerLayer->SetBorderMode(0);
   c_mean_nHitsPerLayer->SetBorderSize(2);
   c_mean_nHitsPerLayer->SetFrameBorderMode(0);
   
   TH1D *h_hits_mean_nHitsPerLayer = new TH1D("h_hits_mean_nHitsPerLayer","Mean Number of Hits per Layer After Data Selection",20,0,20);
   h_hits_mean_nHitsPerLayer->SetBinContent(4,5.358153);
   h_hits_mean_nHitsPerLayer->SetBinContent(7,3.094283);
   h_hits_mean_nHitsPerLayer->SetBinContent(8,7.982231);
   h_hits_mean_nHitsPerLayer->SetBinContent(9,6.267376);
   h_hits_mean_nHitsPerLayer->SetBinContent(10,7.471292);
   h_hits_mean_nHitsPerLayer->SetBinContent(11,4.407954);
   h_hits_mean_nHitsPerLayer->SetBinContent(12,11.69854);
   h_hits_mean_nHitsPerLayer->SetBinContent(13,12.8315);
   h_hits_mean_nHitsPerLayer->SetBinContent(14,12.24574);
   h_hits_mean_nHitsPerLayer->SetBinContent(15,7.583706);
   h_hits_mean_nHitsPerLayer->SetEntries(16);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000033");
   h_hits_mean_nHitsPerLayer->SetLineColor(ci);
   h_hits_mean_nHitsPerLayer->SetLineWidth(2);
   h_hits_mean_nHitsPerLayer->GetXaxis()->SetTitle("Layer");
   h_hits_mean_nHitsPerLayer->GetXaxis()->SetLabelFont(43);
   h_hits_mean_nHitsPerLayer->GetXaxis()->SetLabelSize(20);
   h_hits_mean_nHitsPerLayer->GetXaxis()->SetTitleSize(20);
   h_hits_mean_nHitsPerLayer->GetXaxis()->SetTitleFont(43);
   h_hits_mean_nHitsPerLayer->GetYaxis()->SetTitle("<# Hits per Layer>");
   h_hits_mean_nHitsPerLayer->GetYaxis()->SetLabelFont(43);
   h_hits_mean_nHitsPerLayer->GetYaxis()->SetLabelSize(20);
   h_hits_mean_nHitsPerLayer->GetYaxis()->SetTitleSize(20);
   h_hits_mean_nHitsPerLayer->GetYaxis()->SetTitleOffset(1.3);
   h_hits_mean_nHitsPerLayer->GetYaxis()->SetTitleFont(43);
   h_hits_mean_nHitsPerLayer->GetZaxis()->SetLabelFont(42);
   h_hits_mean_nHitsPerLayer->GetZaxis()->SetLabelSize(0.035);
   h_hits_mean_nHitsPerLayer->GetZaxis()->SetTitleSize(0.035);
   h_hits_mean_nHitsPerLayer->GetZaxis()->SetTitleFont(42);
   h_hits_mean_nHitsPerLayer->Draw("");
   
   TH1D *h_hits_mean_nHitsPerLayer = new TH1D("h_hits_mean_nHitsPerLayer","Mean Number of Hits per Layer After Data Selection",20,0,20);
   h_hits_mean_nHitsPerLayer->SetBinContent(4,4.973901);
   h_hits_mean_nHitsPerLayer->SetBinContent(7,3.086914);
   h_hits_mean_nHitsPerLayer->SetBinContent(8,8.0857);
   h_hits_mean_nHitsPerLayer->SetBinContent(9,6.368415);
   h_hits_mean_nHitsPerLayer->SetBinContent(10,7.53144);
   h_hits_mean_nHitsPerLayer->SetBinContent(11,4.518087);
   h_hits_mean_nHitsPerLayer->SetBinContent(12,11.68427);
   h_hits_mean_nHitsPerLayer->SetBinContent(13,12.84013);
   h_hits_mean_nHitsPerLayer->SetBinContent(14,12.37157);
   h_hits_mean_nHitsPerLayer->SetBinContent(15,7.772518);
   h_hits_mean_nHitsPerLayer->SetEntries(16);

   ci = TColor::GetColor("#3366ff");
   h_hits_mean_nHitsPerLayer->SetLineColor(ci);
   h_hits_mean_nHitsPerLayer->SetLineWidth(2);
   h_hits_mean_nHitsPerLayer->GetXaxis()->SetTitle("Layer Number");
   h_hits_mean_nHitsPerLayer->GetXaxis()->SetLabelFont(42);
   h_hits_mean_nHitsPerLayer->GetXaxis()->SetLabelSize(0.035);
   h_hits_mean_nHitsPerLayer->GetXaxis()->SetTitleSize(0.035);
   h_hits_mean_nHitsPerLayer->GetXaxis()->SetTitleFont(42);
   h_hits_mean_nHitsPerLayer->GetYaxis()->SetTitle("<# Hits per Layer>");
   h_hits_mean_nHitsPerLayer->GetYaxis()->SetLabelFont(42);
   h_hits_mean_nHitsPerLayer->GetYaxis()->SetLabelSize(0.035);
   h_hits_mean_nHitsPerLayer->GetYaxis()->SetTitleSize(0.035);
   h_hits_mean_nHitsPerLayer->GetYaxis()->SetTitleFont(42);
   h_hits_mean_nHitsPerLayer->GetZaxis()->SetLabelFont(42);
   h_hits_mean_nHitsPerLayer->GetZaxis()->SetLabelSize(0.035);
   h_hits_mean_nHitsPerLayer->GetZaxis()->SetTitleSize(0.035);
   h_hits_mean_nHitsPerLayer->GetZaxis()->SetTitleFont(42);
   h_hits_mean_nHitsPerLayer->Draw("SAME");
   
   TLegend *leg = new TLegend(-2.353437e-185,-2.353437e-185,-2.353437e-185,-2.353437e-185,NULL,"brNDC");
   leg->SetBorderSize(1);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("h_hits_mean_nHitsPerLayer","70GeV Pions 17mm Steel 5m away","lpf");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry=leg->AddEntry("h_hits_mean_nHitsPerLayer","70GeV Pions no Steel","lpf");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   leg->Draw();
   c_mean_nHitsPerLayer->Modified();
   c_mean_nHitsPerLayer->cd();
   c_mean_nHitsPerLayer->SetSelected(c_mean_nHitsPerLayer);
}
