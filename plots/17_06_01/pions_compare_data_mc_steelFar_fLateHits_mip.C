{
//=========Macro generated from canvas: c_fLateHits_mip/c_fLateHits_mip
//=========  (Thu Jun  1 14:23:22 2017) by ROOT version5.34/30
   TCanvas *c_fLateHits_mip = new TCanvas("c_fLateHits_mip", "c_fLateHits_mip",0,0,700,500);
   gStyle->SetOptStat(0);
   c_fLateHits_mip->SetHighLightColor(2);
   c_fLateHits_mip->Range(0,0,1,1);
   c_fLateHits_mip->SetFillColor(0);
   c_fLateHits_mip->SetBorderMode(0);
   c_fLateHits_mip->SetBorderSize(2);
   c_fLateHits_mip->SetFrameBorderMode(0);
   
   TH1D *h_fLateHits_mip = new TH1D("h_fLateHits_mip","Fraction of late hits over mip",20,0,20);
   h_fLateHits_mip->SetBinContent(1,0.2328066);
   h_fLateHits_mip->SetBinContent(2,0.206955);
   h_fLateHits_mip->SetBinContent(3,0.1700038);
   h_fLateHits_mip->SetBinContent(4,0.1122657);
   h_fLateHits_mip->SetBinContent(5,0.06247081);
   h_fLateHits_mip->SetBinContent(6,0.03363646);
   h_fLateHits_mip->SetBinContent(7,0.01951713);
   h_fLateHits_mip->SetBinContent(8,0.01156872);
   h_fLateHits_mip->SetBinContent(9,0.005963303);
   h_fLateHits_mip->SetBinContent(10,0.003470294);
   h_fLateHits_mip->SetBinContent(11,0.002137103);
   h_fLateHits_mip->SetBinContent(12,0.001896813);
   h_fLateHits_mip->SetBinContent(13,0.00066313);
   h_fLateHits_mip->SetBinContent(14,0.0007349339);
   h_fLateHits_mip->SetBinContent(15,0.0005688282);
   h_fLateHits_mip->SetBinContent(16,0.000317965);
   h_fLateHits_mip->SetBinContent(17,0.0003505082);
   h_fLateHits_mip->SetEntries(176);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000033");
   h_fLateHits_mip->SetLineColor(ci);
   h_fLateHits_mip->SetLineWidth(2);
   h_fLateHits_mip->GetXaxis()->SetTitle("MIP");
   h_fLateHits_mip->GetXaxis()->SetLabelFont(43);
   h_fLateHits_mip->GetXaxis()->SetLabelSize(25);
   h_fLateHits_mip->GetXaxis()->SetTitleSize(25);
   h_fLateHits_mip->GetXaxis()->SetTitleFont(43);
   h_fLateHits_mip->GetYaxis()->SetLabelFont(43);
   h_fLateHits_mip->GetYaxis()->SetLabelSize(25);
   h_fLateHits_mip->GetYaxis()->SetTitleSize(25);
   h_fLateHits_mip->GetYaxis()->SetTitleFont(43);
   h_fLateHits_mip->GetZaxis()->SetLabelFont(42);
   h_fLateHits_mip->GetZaxis()->SetLabelSize(0.035);
   h_fLateHits_mip->GetZaxis()->SetTitleSize(0.035);
   h_fLateHits_mip->GetZaxis()->SetTitleFont(42);
   h_fLateHits_mip->Draw("");
   
   TH1D *h_fLateHits_mip = new TH1D("h_fLateHits_mip","Fraction of late hits over mip",20,0,20);
   h_fLateHits_mip->SetBinContent(1,0.1169418);
   h_fLateHits_mip->SetBinContent(2,0.1008699);
   h_fLateHits_mip->SetBinContent(3,0.08161312);
   h_fLateHits_mip->SetBinContent(4,0.056069);
   h_fLateHits_mip->SetBinContent(5,0.03807118);
   h_fLateHits_mip->SetBinContent(6,0.02582692);
   h_fLateHits_mip->SetBinContent(7,0.01932437);
   h_fLateHits_mip->SetBinContent(8,0.01601376);
   h_fLateHits_mip->SetBinContent(9,0.01401849);
   h_fLateHits_mip->SetBinContent(10,0.01337182);
   h_fLateHits_mip->SetBinContent(11,0.01293458);
   h_fLateHits_mip->SetBinContent(12,0.01200524);
   h_fLateHits_mip->SetBinContent(13,0.01147737);
   h_fLateHits_mip->SetBinContent(14,0.01119832);
   h_fLateHits_mip->SetBinContent(15,0.01115996);
   h_fLateHits_mip->SetBinContent(16,0.01029457);
   h_fLateHits_mip->SetBinContent(17,0.01072049);
   h_fLateHits_mip->SetBinContent(18,0.01137791);
   h_fLateHits_mip->SetBinContent(19,0.01143765);
   h_fLateHits_mip->SetBinContent(20,0.009905448);
   h_fLateHits_mip->SetBinContent(21,0.0102);
   h_fLateHits_mip->SetEntries(178);

   ci = TColor::GetColor("#3366ff");
   h_fLateHits_mip->SetLineColor(ci);
   h_fLateHits_mip->SetLineWidth(2);
   h_fLateHits_mip->GetXaxis()->SetLabelFont(42);
   h_fLateHits_mip->GetXaxis()->SetLabelSize(0.035);
   h_fLateHits_mip->GetXaxis()->SetTitleSize(0.035);
   h_fLateHits_mip->GetXaxis()->SetTitleFont(42);
   h_fLateHits_mip->GetYaxis()->SetLabelFont(42);
   h_fLateHits_mip->GetYaxis()->SetLabelSize(0.035);
   h_fLateHits_mip->GetYaxis()->SetTitleSize(0.035);
   h_fLateHits_mip->GetYaxis()->SetTitleFont(42);
   h_fLateHits_mip->GetZaxis()->SetLabelFont(42);
   h_fLateHits_mip->GetZaxis()->SetLabelSize(0.035);
   h_fLateHits_mip->GetZaxis()->SetTitleSize(0.035);
   h_fLateHits_mip->GetZaxis()->SetTitleFont(42);
   h_fLateHits_mip->Draw("SAME");
   
   TLegend *leg = new TLegend(-2.353437e-185,-2.353437e-185,-2.353437e-185,-2.353437e-185,NULL,"brNDC");
   leg->SetBorderSize(1);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("h_fLateHits_mip","70GeV Pions 17mm Steel 5m away","lpf");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry=leg->AddEntry("h_fLateHits_mip","70GeV Pions Data","lpf");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   leg->Draw();
   c_fLateHits_mip->Modified();
   c_fLateHits_mip->cd();
   c_fLateHits_mip->SetSelected(c_fLateHits_mip);
}
