{
//=========Macro generated from canvas: c_fLateHits_layer/c_fLateHits_layer
//=========  (Thu Jun  1 14:16:19 2017) by ROOT version5.34/30
   TCanvas *c_fLateHits_layer = new TCanvas("c_fLateHits_layer", "c_fLateHits_layer",57,24,700,500);
   gStyle->SetOptStat(0);
   c_fLateHits_layer->SetHighLightColor(2);
   c_fLateHits_layer->Range(0,0,1,1);
   c_fLateHits_layer->SetFillColor(0);
   c_fLateHits_layer->SetBorderMode(0);
   c_fLateHits_layer->SetBorderSize(2);
   c_fLateHits_layer->SetFrameBorderMode(0);
   
   TH1D *h_fLateHits_layer = new TH1D("h_fLateHits_layer","Fraction of late hits over layer",20,0,20);
   h_fLateHits_layer->SetBinContent(5,0.1033201);
   h_fLateHits_layer->SetBinContent(8,0.1090611);
   h_fLateHits_layer->SetBinContent(9,0.1211246);
   h_fLateHits_layer->SetBinContent(10,0.1209091);
   h_fLateHits_layer->SetBinContent(11,0.1545019);
   h_fLateHits_layer->SetBinContent(12,0.1393926);
   h_fLateHits_layer->SetBinContent(14,0.172565);
   h_fLateHits_layer->SetBinContent(15,0.1755844);
   h_fLateHits_layer->SetBinContent(16,0.07266549);
   h_fLateHits_layer->SetEntries(9);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000033");
   h_fLateHits_layer->SetLineColor(ci);
   h_fLateHits_layer->SetLineWidth(2);
   h_fLateHits_layer->GetXaxis()->SetTitle("Layer");
   h_fLateHits_layer->GetXaxis()->SetLabelFont(43);
   h_fLateHits_layer->GetXaxis()->SetLabelSize(25);
   h_fLateHits_layer->GetXaxis()->SetTitleSize(25);
   h_fLateHits_layer->GetXaxis()->SetTitleFont(43);
   h_fLateHits_layer->GetYaxis()->SetLabelFont(43);
   h_fLateHits_layer->GetYaxis()->SetLabelSize(25);
   h_fLateHits_layer->GetYaxis()->SetTitleSize(25);
   h_fLateHits_layer->GetYaxis()->SetTitleFont(43);
   h_fLateHits_layer->GetZaxis()->SetLabelFont(42);
   h_fLateHits_layer->GetZaxis()->SetLabelSize(0.035);
   h_fLateHits_layer->GetZaxis()->SetTitleSize(0.035);
   h_fLateHits_layer->GetZaxis()->SetTitleFont(42);
   h_fLateHits_layer->Draw("");
   
   TH1D *h_fLateHits_layer = new TH1D("h_fLateHits_layer","Fraction of late hits over layer",20,0,20);
   h_fLateHits_layer->SetBinContent(5,0.1151169);
   h_fLateHits_layer->SetBinContent(8,0.1164411);
   h_fLateHits_layer->SetBinContent(9,0.1220933);
   h_fLateHits_layer->SetBinContent(10,0.1237338);
   h_fLateHits_layer->SetBinContent(11,0.1535116);
   h_fLateHits_layer->SetBinContent(12,0.1450625);
   h_fLateHits_layer->SetBinContent(14,0.1751366);
   h_fLateHits_layer->SetBinContent(15,0.1712856);
   h_fLateHits_layer->SetBinContent(16,0.07240132);
   h_fLateHits_layer->SetEntries(9);

   ci = TColor::GetColor("#3366ff");
   h_fLateHits_layer->SetLineColor(ci);
   h_fLateHits_layer->SetLineWidth(2);
   h_fLateHits_layer->GetXaxis()->SetLabelFont(42);
   h_fLateHits_layer->GetXaxis()->SetLabelSize(0.035);
   h_fLateHits_layer->GetXaxis()->SetTitleSize(0.035);
   h_fLateHits_layer->GetXaxis()->SetTitleFont(42);
   h_fLateHits_layer->GetYaxis()->SetLabelFont(42);
   h_fLateHits_layer->GetYaxis()->SetLabelSize(0.035);
   h_fLateHits_layer->GetYaxis()->SetTitleSize(0.035);
   h_fLateHits_layer->GetYaxis()->SetTitleFont(42);
   h_fLateHits_layer->GetZaxis()->SetLabelFont(42);
   h_fLateHits_layer->GetZaxis()->SetLabelSize(0.035);
   h_fLateHits_layer->GetZaxis()->SetTitleSize(0.035);
   h_fLateHits_layer->GetZaxis()->SetTitleFont(42);
   h_fLateHits_layer->Draw("SAME");
   
   TLegend *leg = new TLegend(-2.353437e-185,-2.353437e-185,-2.353437e-185,-2.353437e-185,NULL,"brNDC");
   leg->SetBorderSize(1);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("h_fLateHits_layer","70GeV Pions 17mm Steel 5m away","lpf");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry=leg->AddEntry("h_fLateHits_layer","70GeV Pions no Steel","lpf");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   leg->Draw();
   c_fLateHits_layer->Modified();
   c_fLateHits_layer->cd();
   c_fLateHits_layer->SetSelected(c_fLateHits_layer);
}
