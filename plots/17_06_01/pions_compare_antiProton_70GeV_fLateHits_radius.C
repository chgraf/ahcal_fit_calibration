{
//=========Macro generated from canvas: c_fLateHits_radius/c_fLateHits_radius
//=========  (Thu Jun  1 11:31:15 2017) by ROOT version5.34/30
   TCanvas *c_fLateHits_radius = new TCanvas("c_fLateHits_radius", "c_fLateHits_radius",0,0,700,500);
   gStyle->SetOptStat(0);
   c_fLateHits_radius->SetHighLightColor(2);
   c_fLateHits_radius->Range(0,0,1,1);
   c_fLateHits_radius->SetFillColor(0);
   c_fLateHits_radius->SetBorderMode(0);
   c_fLateHits_radius->SetBorderSize(2);
   c_fLateHits_radius->SetFrameBorderMode(0);
   
   TH1D *h_fLateHits_radius = new TH1D("h_fLateHits_radius","Fraction of late hits over radius",24,0,24);
   h_fLateHits_radius->SetBinContent(1,0.01533282);
   h_fLateHits_radius->SetBinContent(2,0.05825131);
   h_fLateHits_radius->SetBinContent(3,0.2624799);
   h_fLateHits_radius->SetBinContent(4,0.5315595);
   h_fLateHits_radius->SetBinContent(5,1.165674);
   h_fLateHits_radius->SetBinContent(6,1.736919);
   h_fLateHits_radius->SetBinContent(7,1.962728);
   h_fLateHits_radius->SetBinContent(8,2.297592);
   h_fLateHits_radius->SetBinContent(9,3.659637);
   h_fLateHits_radius->SetBinContent(10,2.977846);
   h_fLateHits_radius->SetBinContent(11,5.258296);
   h_fLateHits_radius->SetBinContent(12,4.719267);
   h_fLateHits_radius->SetBinContent(13,5.75238);
   h_fLateHits_radius->SetBinContent(14,2.160715);
   h_fLateHits_radius->SetBinContent(15,1.167176);
   h_fLateHits_radius->SetBinContent(16,0.5924085);
   h_fLateHits_radius->SetBinContent(17,0.09803922);
   h_fLateHits_radius->SetEntries(69);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000033");
   h_fLateHits_radius->SetLineColor(ci);
   h_fLateHits_radius->SetLineWidth(2);
   h_fLateHits_radius->GetXaxis()->SetTitle("Hit Radius");
   h_fLateHits_radius->GetXaxis()->SetRange(1,240);
   h_fLateHits_radius->GetXaxis()->SetLabelFont(43);
   h_fLateHits_radius->GetXaxis()->SetLabelSize(25);
   h_fLateHits_radius->GetXaxis()->SetTitleSize(25);
   h_fLateHits_radius->GetXaxis()->SetTitleFont(43);
   h_fLateHits_radius->GetYaxis()->SetLabelFont(43);
   h_fLateHits_radius->GetYaxis()->SetLabelSize(25);
   h_fLateHits_radius->GetYaxis()->SetTitleSize(25);
   h_fLateHits_radius->GetYaxis()->SetTitleFont(43);
   h_fLateHits_radius->GetZaxis()->SetLabelFont(42);
   h_fLateHits_radius->GetZaxis()->SetLabelSize(0.035);
   h_fLateHits_radius->GetZaxis()->SetTitleSize(0.035);
   h_fLateHits_radius->GetZaxis()->SetTitleFont(42);
   h_fLateHits_radius->Draw("");
   
   TH1D *h_fLateHits_radius = new TH1D("h_fLateHits_radius","Fraction of late hits over radius",24,0,24);
   h_fLateHits_radius->SetBinContent(1,0.01486235);
   h_fLateHits_radius->SetBinContent(2,0.05444138);
   h_fLateHits_radius->SetBinContent(3,0.2306619);
   h_fLateHits_radius->SetBinContent(4,0.4771207);
   h_fLateHits_radius->SetBinContent(5,1.013766);
   h_fLateHits_radius->SetBinContent(6,1.616662);
   h_fLateHits_radius->SetBinContent(7,1.864457);
   h_fLateHits_radius->SetBinContent(8,2.195238);
   h_fLateHits_radius->SetBinContent(9,3.557804);
   h_fLateHits_radius->SetBinContent(10,2.705776);
   h_fLateHits_radius->SetBinContent(11,5.056553);
   h_fLateHits_radius->SetBinContent(12,4.56301);
   h_fLateHits_radius->SetBinContent(13,5.555216);
   h_fLateHits_radius->SetBinContent(14,2.270961);
   h_fLateHits_radius->SetBinContent(15,1.339713);
   h_fLateHits_radius->SetBinContent(16,0.8208502);
   h_fLateHits_radius->SetBinContent(17,1.25);
   h_fLateHits_radius->SetEntries(69);

   ci = TColor::GetColor("#33cccc");
   h_fLateHits_radius->SetLineColor(ci);
   h_fLateHits_radius->SetLineWidth(2);
   h_fLateHits_radius->GetXaxis()->SetRange(1,240);
   h_fLateHits_radius->GetXaxis()->SetLabelFont(42);
   h_fLateHits_radius->GetXaxis()->SetLabelSize(0.035);
   h_fLateHits_radius->GetXaxis()->SetTitleSize(0.035);
   h_fLateHits_radius->GetXaxis()->SetTitleFont(42);
   h_fLateHits_radius->GetYaxis()->SetLabelFont(42);
   h_fLateHits_radius->GetYaxis()->SetLabelSize(0.035);
   h_fLateHits_radius->GetYaxis()->SetTitleSize(0.035);
   h_fLateHits_radius->GetYaxis()->SetTitleFont(42);
   h_fLateHits_radius->GetZaxis()->SetLabelFont(42);
   h_fLateHits_radius->GetZaxis()->SetLabelSize(0.035);
   h_fLateHits_radius->GetZaxis()->SetTitleSize(0.035);
   h_fLateHits_radius->GetZaxis()->SetTitleFont(42);
   h_fLateHits_radius->Draw("SAME");
   
   TLegend *leg = new TLegend(-2.353437e-185,-2.353437e-185,-2.353437e-185,-2.353437e-185,NULL,"brNDC");
   leg->SetBorderSize(1);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   TLegendEntry *entry=leg->AddEntry("h_fLateHits_radius","70GeV Pion","lpf");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry=leg->AddEntry("h_fLateHits_radius","70GeV Anti Proton ","lpf");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   leg->Draw();
   c_fLateHits_radius->Modified();
   c_fLateHits_radius->cd();
   c_fLateHits_radius->SetSelected(c_fLateHits_radius);
}
