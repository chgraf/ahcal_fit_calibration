import ROOT
from ROOT import TFile, TH1D, TCanvas, gPad, TLegend, gStyle

f_Electrons = TFile("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Electrons_Sim/electrons_002_hcal.root");
t_Electrons = f_Electrons.Get("bigtree;1")
h_Electrons = TH1D("h_Electrons", "Tungsten 20GeV Electrons Simulation vs Data", 2000, -500, 1500)
t_Electrons.Draw("ahc_hitTime>>h_Electrons")

h_Electrons.Scale(1./float(h_Electrons.GetEntries()))
h_Electrons.GetXaxis().SetTitle("Hit Time [ns]")
h_Electrons.GetYaxis().SetTitle("# Entries")
h_Electrons.SetLineColor(ROOT.kBlue+4)
h_Electrons.SetLineWidth(2)


f_Data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_03_06_electrons.root");
# t_Electrons = f_Electrons.Get("bigtree;1")
# h_Electrons = TH1D("h_Electrons", "Tungsten Electrons vs Electrons QGSP_BERT", 2000, -500, 1500)
h_Data = f_Data.Get("h_resolution")
h_Data.Draw()



h_Data.Scale(1./float(h_Data.GetEntries()))
h_Data.SetLineColor(ROOT.kYellow+2)
h_Data.SetLineWidth(2)
h_Data.Rebin(10)

# h_QGSP_BERT_HP.SetTitle("QGSP_BERT_HP")
# h_QGSP_BERT.SetTitle("QGSP_BERT")

c = TCanvas("c", "c")
gPad.SetLogy()
h_Electrons.Draw()
h_Data.Draw("Same")

gStyle.SetOptStat(0);

leg = TLegend(0.60, 0.70, 0.9, 0.9)
leg.AddEntry(h_Electrons, "Simulation")
leg.AddEntry(h_Data, "Data")
leg.Draw()

raw_input("Press Enter to continue...")
