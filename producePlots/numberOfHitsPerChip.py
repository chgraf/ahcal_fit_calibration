import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt

import ROOT
from ROOT import TFile, TGraph, TH1D, TCanvas, gPad, TLegend, gStyle

x = np.linspace(1,19,19)
#y = np.array([0, 2, 4, 6, 7, 9, 11, 12, 15, 15, 17, 21, 20, 26, 31, 30, 31, 35, 39])
# y = np.array([0, 2, 5, 7, 8, 10, 13, 13, 16, 15, 17, 22, 19, 28, 35, 31, 31, 35, 42])
y = np.array([-1, 2, 3, 5, 6, 9, 11, 13, 14, 16, 17, 22, 20, 26, 30, 27, 34, 34, 38])

def lin(x, a, b):
    return a+b*x

def quad(x, a, b, c):
    return a*pow(x,2) + b*x + c

fit = opt.curve_fit(lin, x, y, p0=[-1, 1])
fit_quad = opt.curve_fit(quad, x[:19], y[:19], p0=[0.1, 1, -2])
print(fit)
print(fit_quad)
plt.plot(x,y, linestyle='', marker="x")
plt.plot(x, lin(x, fit[0][0], fit[0][1]))
plt.plot(x, quad(x, fit_quad[0][0], fit_quad[0][1], fit_quad[0][2]))
plt.show()

gr = TGraph(len(x))
gr2 = TGraph(len(x))

for i in range(0, len(x)):
    gr.SetPoint(i, x[i], y[i])
    gr2.SetPoint(i, x[i], quad(x[i], fit_quad[0][0], fit_quad[0][1], fit_quad[0][2]))

c = TCanvas("c", "c")
gr.Draw("A*")
gr2.Draw("L SAME")


quad(x, fit_quad[0][0], fit_quad[0][1], fit_quad[0][2])

# RMS
RMS = np.array([3.2, 5.1, 6.7, 8.4, 9.7, 10.6, 11.6, 11.8, 11.3, 12.8])
gr_rms = TGraph(len(x[:10]))

for i in range(0, len(x[:10])):
    gr_rms.SetPoint(i, x[i], RMS[i])

c_rms = TCanvas("c_rms", "c_rms")
gr_rms.Draw("A*")
gr2.Draw("L SAME")
raw_input("Press Enter to continue...")
