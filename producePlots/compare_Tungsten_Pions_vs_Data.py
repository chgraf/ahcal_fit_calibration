import ROOT
from ROOT import TFile, TH1D, TCanvas, TPad, gPad, TLegend, gStyle, gROOT
# import numpy as np

# import christiansStyle

gROOT.SetStyle("cgStyle")

fWrite = False
writePath = "../plots/17_07_05_double_particle_rejection/"
writePrefix = "compare_pion_data_mc_"

legendData = "Data reject double particles"
legendSim = "Data no rejection"
colorData = ROOT.kAzure-2
# colorSim = ROOT.kBlue+4
colorSim = ROOT.kRed+2

# f_Muons = TFile("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Muons_Sim/muons_003_hcal.root");
# t_Muons = f_Muons.Get("bigtree;1")
# h_Muons = TH1D("h_Muons", "Tungsten 120GeV Muons Simulation vs Data", 2000, -500, 1500)
# t_Muons.Draw("ahc_hitTime>>h_Muons")

# gStyle.SetTitleFont(43)
# gStyle.SetTitleSize(20)
# gStyle.SetLabelFont(43)
# gStyle.SetLabelSize(20)

# f_Muons = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/mc_aug_muons_006.root");
# f_Muons = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_03_10_muons_sim_evtsmearing.root");
# f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_03_12_pions_simulation_noSelection.root");
# f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_08_pions_sim_noSteel_no56.root");
# f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_08_pions_sim_withSteelFar_no56_norm.root");
# f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_11_pions_sim_moreSteel.root");
# f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_08_pions_sim_10GeV_withSteelFar_no56.root");
# f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_06_pions_data.root");
# h_Muons = f_Muons.Get("h_resolution")


# f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_12_pions_sim.root");
#f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_08_pions_sim_10GeV_withSteelFar_no56.root");
#f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_08_pions_sim_10GeV_withSteelFar_no56.root");
#f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_15_pions_70Gev_sim_withSteelFar.root");
#f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_15_pions_70Gev_sim_withSteelFar.root");
#f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_26_pions_10GeV_sim.root");
#f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_31_pions_70GeV_data_cherenkov0.root");

# f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_06_01_pions_70GeV_sim_steelFarAway.root");
# f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_06_01_pions_70GeV_sim_steelNear.root");
#f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_06_01_pions_70GeV_sim_noSteel.root");
#f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_06_01_pions_70GeV_sim_QGSP_BERT.root");
# f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_06_01_antiProton_70GeV.root");
# f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_06_15_pions_70GeV_sim_rejectDoubleParticles.root");
# f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_06_21_pions_sim.root");
# f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_06_29_pions_sim_rejectDoubleParticles_30ns.root");
f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_06_30_pions_data_noRejection.root");

h_sim_timing = f_sim.Get("h_resolution_t0Time")
h_sim_timing.Draw()
h_sim_timing.Rebin(100)


h_sim_timing.Scale(1./float(h_sim_timing.GetEntries()))
h_sim_timing.GetXaxis().SetTitle("Hit Time [ns]")
h_sim_timing.GetYaxis().SetTitle("# Entries");
h_sim_timing.SetLineColor(colorSim)
h_sim_timing.SetLineWidth(2)

f_data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_06_29_pions_data_rejectDoubleParticles_30ns.root");
#f_data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_06_15_pions_70GeV_data_rejectDoubleParticles.root");
# f_data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_06_01_pions_70GeV_sim_50mmSteel.root");
# f_data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_06_01_pions_70GeV_sim_steelFarAway.root");
# f_data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_06_01_pions_70GeV_sim_QGSP_BERT.root");

# f_data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_08_pions_data_no56.root");

# f_data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_31_pions_70GeV_data_cherenkov1.root");

# f_data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_31_pions_70GeV_data_cherenkov0.root");
# f_data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_06_01_kaons_70GeV.root");
# f_data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_06_01_pions_70GeV_sim_50mmSteel.root");
#f_data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_15_pions_70GeV_data.root");
#f_data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_20_pions_70Gev_data_extendedCalibration.root");

# f_data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_11_pions_sim.root");
# f_data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_03_12_pions_data_noSelection.root");
# f_data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_08_pions_sim_noSteel_no56.root");
# t_Pions = f_Pions.Get("bigtree;1")
# h_Pions = TH1D("h_Pions", "Tungsten Muons vs Pions QGSP_BERT", 2000, -500, 1500)
# h_Data = f_Data.Get("h_resolution_evtTime")
h_data_timing = f_data.Get("h_resolution_t0Time")
# h_Data.Draw()

h_data_timing.Scale(1./float(h_data_timing.GetEntries()))
h_data_timing.SetLineColor(colorData)
h_data_timing.SetLineWidth(2)
h_data_timing.Rebin(100)

h_sim_timing.GetYaxis().SetLabelFont(43)
h_sim_timing.GetYaxis().SetLabelSize(25)
h_sim_timing.GetYaxis().SetTitleFont(43)
h_sim_timing.GetYaxis().SetTitleSize(25)
h_sim_timing.GetYaxis().SetTitleOffset(1.0)

h_sim_timing.GetXaxis().SetLabelFont(43)
h_sim_timing.GetXaxis().SetLabelSize(25)
h_sim_timing.GetXaxis().SetTitleFont(43)
h_sim_timing.GetXaxis().SetTitleSize(25)
h_sim_timing.GetXaxis().SetTitleOffset(1.0)
h_sim_timing.GetXaxis().SetTitle("Hit Time [ns]")



# h_sim_timing.SetMaximum(2*10e-2)
# h_sim_timing.SetMinimum(2*10e-6)
# h_Muons.SetOptStat(0)


#h_sim_timing.SetTitleSize(50)

# h_Muons.UseCurrentStyle()

h_sim_timing.SetAxisRange(-100,500)

# h_QGSP_BERT_HP.SetTitle("QGSP_BERT_HP")
# h_QGSP_BERT.SetTitle("QGSP_BERT")

c = TCanvas("c", "c", 800, 600)
'''
pad1 = TPad("pad1", "pad1", 0.0, 0.3, 1.0, 1.0)
pad1.SetBottomMargin(0.005)
pad1.Draw()
pad1.cd()
'''


gPad.SetLogy()
h_sim_timing.Draw()
h_data_timing.Draw("Same")

leg = TLegend(0.60, 0.70, 0.9, 0.9)
leg.AddEntry(h_sim_timing, legendSim)
leg.AddEntry(h_data_timing, legendData)
leg.Draw()

'''
h_residuals = TH1D("h_residuals", "", h_sim_timing.GetNbinsX(), h_sim_timing.GetXaxis().GetXmin(), h_sim_timing.GetXaxis().GetXmax())

for i in range(1,h_sim_timing.GetNbinsX()+1):
    if(h_data_timing.GetBinContent(i) > 0):
        # h_residuals.SetBinContent(i, (h_Muons.GetBinContent(i)-h_70GeV Anti Proton .GetBinContent(i))/h_Muons.GetBinContent(i))
        h_residuals.SetBinContent(i, h_sim_timing.GetBinContent(i)/h_data_timing.GetBinContent(i))


h_residuals.GetYaxis().SetLabelFont(43)
h_residuals.GetYaxis().SetLabelSize(25)
h_residuals.GetYaxis().SetTitleFont(43)
h_residuals.GetYaxis().SetTitleSize(25)
h_residuals.GetYaxis().SetTitleOffset(1.5)
h_residuals.GetYaxis().SetNdivisions(5,5,0)
h_residuals.GetYaxis().SetTitle("MC / 70GeV Anti Proton ")

h_residuals.GetXaxis().SetLabelFont(43)
h_residuals.GetXaxis().SetLabelSize(25)
h_residuals.GetXaxis().SetTitleFont(43)
h_residuals.GetXaxis().SetTitleSize(25)
h_residuals.GetXaxis().SetTitleOffset(4.2)
h_residuals.GetXaxis().SetTitle("Hit Time [ns]")

h_residuals.SetAxisRange(-100, 500)
h_residuals.SetMinimum(0.5)
h_residuals.SetMaximum(1.5)

h_residuals.SetMarkerColor(colorData)
h_residuals.SetMarkerSize(1.3)


c.cd()
pad2 = TPad("pad2", "pad2", 0.0, 0.05, 1.0, 0.3)
pad2.SetGridx()
pad2.SetGridy()
pad2.SetTopMargin(0.05)
pad2.SetBottomMargin(0.3)
pad2.Draw()
pad2.cd()
h_residuals.SetMarkerStyle(3)
# h_residuals.Draw("P")
'''

gStyle.SetOptStat(0);

if fWrite:
    c.SaveAs(writePath+writePrefix+"timeDistribution.root")
    c.SaveAs(writePath+writePrefix+"timeDistribution.C")
    c.SaveAs(writePath+writePrefix+"timeDistribution.pdf")
    c.SaveAs(writePath+writePrefix+"timeDistribution.svg")
# -------------------------------
# -------- nHits & CoG Z --------
# -------------------------------

# h2_sim_nHits_cogZ = f_sim.Get("h2_nHits_cogZ")
# h2_sim_nHits_cogZ.Scale(1./float(h2_sim_nHits_cogZ.GetEntries()))
# h2_sim_nHits_cogZ.SetLineColor(colorSim)
# h2_sim_nHits_cogZ.SetLineWidth(2)
#
# h2_sim_nHits_cogZ.GetYaxis().SetLabelFont(43)
# h2_sim_nHits_cogZ.GetYaxis().SetLabelSize(25)
# h2_sim_nHits_cogZ.GetYaxis().SetTitleFont(43)
# h2_sim_nHits_cogZ.GetYaxis().SetTitleSize(25)
# h2_sim_nHits_cogZ.GetYaxis().SetTitleOffset(1.6)
#
# h2_sim_nHits_cogZ.SetTitleSize(50)
#
# h2_data_nHits_cogZ = f_data.Get("h2_nHits_cogZ")
# h2_data_nHits_cogZ.Scale(1./float(h2_data_nHits_cogZ.GetEntries()))
# h2_data_nHits_cogZ.SetLineColor(ROOT.kRed)
# h2_data_nHits_cogZ.SetLineWidth(2)
#
# c_nHits_cogZ = TCanvas("c_nHits_cogZ", "c_nHits_cogZ")
# h2_sim_nHits_cogZ.Draw("BOX")
# h2_data_nHits_cogZ.Draw("BOX SAME")
#
# leg = TLegend(0.60, 0.70, 0.9, 0.9)
# leg.AddEntry(h2_sim_nHits_cogZ, "70GeV Pion")
# leg.AddEntry(h2_data_nHits_cogZ, "70GeV Anti Proton ")
# leg.Draw()

# -------------------------------
# ------------ nHits ------------
# -------------------------------

h_sim_nHits = f_sim.Get("h_hits_nHits")

h_sim_nHits.Scale(1./float(h_sim_nHits.GetEntries()))
h_sim_nHits.SetLineColor(colorSim)
h_sim_nHits.SetLineWidth(2)

h_sim_nHits.GetYaxis().SetTitle("Normalized Entries")
h_sim_nHits.GetYaxis().SetLabelFont(43)
h_sim_nHits.GetYaxis().SetLabelSize(20)
h_sim_nHits.GetYaxis().SetTitleFont(43)
h_sim_nHits.GetYaxis().SetTitleSize(20)
h_sim_nHits.GetYaxis().SetTitleOffset(1.3)

h_sim_nHits.GetXaxis().SetTitle("Number of Hits")
h_sim_nHits.GetXaxis().SetLabelFont(43)
h_sim_nHits.GetXaxis().SetLabelSize(20)
h_sim_nHits.GetXaxis().SetTitleFont(43)
h_sim_nHits.GetXaxis().SetTitleSize(20)
# h_sim_nHits.GetXaxis().SetTitleOffset(1.6)

h_data_nHits = f_data.Get("h_hits_nHits")
h_data_nHits.Scale(1./float(h_data_nHits.GetEntries()))
h_data_nHits.SetLineColor(colorData)
h_data_nHits.SetLineWidth(2)

c_nHits = TCanvas("c_nHits", "c_nHits")
h_sim_nHits.Draw()
h_data_nHits.Draw("SAME")

leg_nHits = TLegend(0.60, 0.70, 0.9, 0.9)
leg_nHits.AddEntry(h_sim_nHits, legendSim)
leg_nHits.AddEntry(h_data_nHits, legendData)
leg_nHits.Draw()

if fWrite:
    c_nHits.SaveAs(writePath+writePrefix+"nHits.root")
    c_nHits.SaveAs(writePath+writePrefix+"nHits.C")
    c_nHits.SaveAs(writePath+writePrefix+"nHits.pdf")
    c_nHits.SaveAs(writePath+writePrefix+"nHits.svg")

# -------------------------------
# ------------ CoGZ -------------
# -------------------------------

h_sim_cogZ = f_sim.Get("h_hits_cogZ")

h_sim_cogZ.Scale(1./float(h_sim_cogZ.GetEntries()))
h_sim_cogZ.SetLineColor(colorSim)
h_sim_cogZ.SetLineWidth(2)

h_sim_cogZ.GetYaxis().SetTitle("Normalized Entries")
h_sim_cogZ.GetYaxis().SetLabelFont(43)
h_sim_cogZ.GetYaxis().SetLabelSize(20)
h_sim_cogZ.GetYaxis().SetTitleFont(43)
h_sim_cogZ.GetYaxis().SetTitleSize(20)
h_sim_cogZ.GetYaxis().SetTitleOffset(1.3)

h_sim_cogZ.GetXaxis().SetTitle("CoG Z [mm]")
h_sim_cogZ.GetXaxis().SetLabelFont(43)
h_sim_cogZ.GetXaxis().SetLabelSize(20)
h_sim_cogZ.GetXaxis().SetTitleFont(43)
h_sim_cogZ.GetXaxis().SetTitleSize(20)
# h_sim_cogZ.GetXaxis().SetTitleOffset(1.6)


h_data_cogZ = f_data.Get("h_hits_cogZ")
h_data_cogZ.Scale(1./float(h_data_cogZ.GetEntries()))
h_data_cogZ.SetLineColor(colorData)
h_data_cogZ.SetLineWidth(2)

c_cogZ = TCanvas("c_cogZ", "c_cogZ")
h_sim_cogZ.Draw()
h_data_cogZ.Draw("SAME")

leg_cogZ = TLegend(0.60, 0.70, 0.9, 0.9)
leg_cogZ.AddEntry(h_sim_cogZ, legendSim)
leg_cogZ.AddEntry(h_data_cogZ, legendData)
leg_cogZ.Draw()

if fWrite:
    c_cogZ.SaveAs(writePath+writePrefix+"cogZ.root")
    c_cogZ.SaveAs(writePath+writePrefix+"cogZ.C")
    c_cogZ.SaveAs(writePath+writePrefix+"cogZ.pdf")
    c_cogZ.SaveAs(writePath+writePrefix+"cogZ.svg")

# -------------------------------
# ---------- energySum ----------
# -------------------------------

h_sim_energySum = f_sim.Get("h_hits_energySum")

h_sim_energySum.Scale(1./float(h_sim_energySum.GetEntries()))
h_sim_energySum.SetLineColor(colorSim)
h_sim_energySum.SetLineWidth(2)

h_sim_energySum.GetYaxis().SetLabelFont(43)
h_sim_energySum.GetYaxis().SetLabelSize(25)
h_sim_energySum.GetYaxis().SetTitleFont(43)
h_sim_energySum.GetYaxis().SetTitleSize(25)
h_sim_energySum.GetYaxis().SetTitleOffset(1.6)

h_sim_energySum.SetTitleSize(50)

h_data_energySum = f_data.Get("h_hits_energySum")
h_data_energySum.Scale(1./float(h_data_energySum.GetEntries()))
h_data_energySum.SetLineColor(colorData)
h_data_energySum.SetLineWidth(2)

c_energySum = TCanvas("c_energySum", "c_energySum")
h_sim_energySum.Draw()
h_data_energySum.Draw("SAME")

leg_energySum = TLegend(0.60, 0.70, 0.9, 0.9)
leg_energySum.AddEntry(h_sim_energySum, legendSim)
leg_energySum.AddEntry(h_data_energySum, legendData)
leg_energySum.Draw()

if fWrite:
    c_energySum.SaveAs(writePath+writePrefix+"energySum.root")
    c_energySum.SaveAs(writePath+writePrefix+"energySum.C")
    c_energySum.SaveAs(writePath+writePrefix+"energySum.pdf")
    c_energySum.SaveAs(writePath+writePrefix+"energySum.svg")

# -------------------------------
# ------- nHits per Layer -------
# -------------------------------

h_sim_mean_nHitsPerLayer = f_sim.Get("h_hits_mean_nHitsPerLayer")

h_sim_mean_nHitsPerLayer.SetLineColor(colorSim)
h_sim_mean_nHitsPerLayer.SetLineWidth(2)

h_sim_mean_nHitsPerLayer.GetYaxis().SetTitle("<# Hits per Layer>")
h_sim_mean_nHitsPerLayer.GetYaxis().SetLabelFont(43)
h_sim_mean_nHitsPerLayer.GetYaxis().SetLabelSize(20)
h_sim_mean_nHitsPerLayer.GetYaxis().SetTitleFont(43)
h_sim_mean_nHitsPerLayer.GetYaxis().SetTitleSize(20)
h_sim_mean_nHitsPerLayer.GetYaxis().SetTitleOffset(1.3)

h_sim_mean_nHitsPerLayer.GetXaxis().SetTitle("Layer")
h_sim_mean_nHitsPerLayer.GetXaxis().SetLabelFont(43)
h_sim_mean_nHitsPerLayer.GetXaxis().SetLabelSize(20)
h_sim_mean_nHitsPerLayer.GetXaxis().SetTitleFont(43)
h_sim_mean_nHitsPerLayer.GetXaxis().SetTitleSize(20)
# h_sim_mean_nHitsPerLayer.GetXaxis().SetTitleOffset(1.6)


h_data_mean_nHitsPerLayer = f_data.Get("h_hits_mean_nHitsPerLayer")
h_data_mean_nHitsPerLayer.SetLineColor(colorData)
h_data_mean_nHitsPerLayer.SetLineWidth(2)

c_mean_nHitsPerLayer = TCanvas("c_mean_nHitsPerLayer", "c_mean_nHitsPerLayer")
h_sim_mean_nHitsPerLayer.Draw()
h_data_mean_nHitsPerLayer.Draw("SAME")

leg_mean_nHitsPerLayer = TLegend(0.60, 0.70, 0.9, 0.9)
leg_mean_nHitsPerLayer.AddEntry(h_sim_mean_nHitsPerLayer, legendSim)
leg_mean_nHitsPerLayer.AddEntry(h_data_mean_nHitsPerLayer, legendData)
leg_mean_nHitsPerLayer.Draw()

if fWrite:
    c_mean_nHitsPerLayer.SaveAs(writePath+writePrefix+"mean_nHitsPerLayer.root")
    c_mean_nHitsPerLayer.SaveAs(writePath+writePrefix+"mean_nHitsPerLayer.C")
    c_mean_nHitsPerLayer.SaveAs(writePath+writePrefix+"mean_nHitsPerLayer.pdf")
    c_mean_nHitsPerLayer.SaveAs(writePath+writePrefix+"mean_nHitsPerLayer.svg")

# -------------------------------
# ------ nHits same Chip -------
# -------------------------------

h_sim_mean_nHitsPerChip = f_sim.Get("h_hits_mean_nHitsPerChip")

h_sim_mean_nHitsPerChip.SetLineColor(colorSim)
h_sim_mean_nHitsPerChip.SetLineWidth(2)

h_sim_mean_nHitsPerChip.GetYaxis().SetLabelFont(43)
h_sim_mean_nHitsPerChip.GetYaxis().SetLabelSize(25)
h_sim_mean_nHitsPerChip.GetYaxis().SetTitleFont(43)
h_sim_mean_nHitsPerChip.GetYaxis().SetTitleSize(25)
h_sim_mean_nHitsPerChip.GetYaxis().SetTitleOffset(1.6)

h_sim_mean_nHitsPerChip.SetTitleSize(50)

h_data_mean_nHitsPerChip = f_data.Get("h_hits_mean_nHitsPerChip")
h_data_mean_nHitsPerChip.SetLineColor(colorData)
h_data_mean_nHitsPerChip.SetLineWidth(2)

c_mean_nHitsPerChip = TCanvas("c_mean_nHitsPerChip", "c_mean_nHitsPerChip")
h_sim_mean_nHitsPerChip.Draw()
h_data_mean_nHitsPerChip.Draw("SAME")

leg_mean_nHitsPerChip = TLegend(0.60, 0.70, 0.9, 0.9)
leg_mean_nHitsPerChip.AddEntry(h_sim_mean_nHitsPerChip, legendSim)
leg_mean_nHitsPerChip.AddEntry(h_data_mean_nHitsPerChip, legendData)
leg_mean_nHitsPerChip.Draw()

if fWrite:
    c_mean_nHitsPerChip.SaveAs(writePath+writePrefix+"mean_nHitsPerChip.root")
    c_mean_nHitsPerChip.SaveAs(writePath+writePrefix+"mean_nHitsPerChip.C")
    c_mean_nHitsPerChip.SaveAs(writePath+writePrefix+"mean_nHitsPerChip.pdf")
    c_mean_nHitsPerChip.SaveAs(writePath+writePrefix+"mean_nHitsPerChip.svg")

# -------------------------------
# ------ LateHits over radius -------
# -------------------------------

h_sim_fLateHits_radius = f_sim.Get("h_fLateHits_radius")

h_sim_fLateHits_radius.SetLineColor(colorSim)
h_sim_fLateHits_radius.SetLineWidth(2)

h_sim_fLateHits_radius.GetYaxis().SetLabelFont(43)
h_sim_fLateHits_radius.GetYaxis().SetLabelSize(25)
h_sim_fLateHits_radius.GetYaxis().SetTitleFont(43)
h_sim_fLateHits_radius.GetYaxis().SetTitleSize(25)
h_sim_fLateHits_radius.GetYaxis().SetTitleOffset(1.0)

h_sim_fLateHits_radius.GetXaxis().SetLabelFont(43)
h_sim_fLateHits_radius.GetXaxis().SetLabelSize(25)
h_sim_fLateHits_radius.GetXaxis().SetTitleFont(43)
h_sim_fLateHits_radius.GetXaxis().SetTitleSize(25)
h_sim_fLateHits_radius.GetXaxis().SetTitleOffset(1.0)
h_sim_fLateHits_radius.GetXaxis().SetTitle("Hit Radius")


h_data_fLateHits_radius = f_data.Get("h_fLateHits_radius")
h_data_fLateHits_radius.SetLineColor(colorData)
h_data_fLateHits_radius.SetLineWidth(2)

h_sim_fLateHits_radius.Rebin(10)
h_data_fLateHits_radius.Rebin(10)

c_fLateHits_radius = TCanvas("c_fLateHits_radius", "c_fLateHits_radius")
h_sim_fLateHits_radius.Draw()
h_data_fLateHits_radius.Draw("SAME")

leg_fLateHits_radius = TLegend(0.60, 0.70, 0.9, 0.9)
leg_fLateHits_radius.AddEntry(h_sim_fLateHits_radius, legendSim)
leg_fLateHits_radius.AddEntry(h_data_fLateHits_radius, legendData)
leg_fLateHits_radius.Draw()


if fWrite:
    c_fLateHits_radius.SaveAs(writePath+writePrefix+"fLateHits_radius.root")
    c_fLateHits_radius.SaveAs(writePath+writePrefix+"fLateHits_radius.C")
    c_fLateHits_radius.SaveAs(writePath+writePrefix+"fLateHits_radius.pdf")
    c_fLateHits_radius.SaveAs(writePath+writePrefix+"fLateHits_radius.svg")

# -------------------------------
# ------ LateHits over mip -------
# -------------------------------

h_sim_fLateHits_mip = f_sim.Get("h_fLateHits_mip")

h_sim_fLateHits_mip.SetLineColor(colorSim)
h_sim_fLateHits_mip.SetLineWidth(2)

h_sim_fLateHits_mip.GetYaxis().SetLabelFont(43)
h_sim_fLateHits_mip.GetYaxis().SetLabelSize(25)
h_sim_fLateHits_mip.GetYaxis().SetTitleFont(43)
h_sim_fLateHits_mip.GetYaxis().SetTitleSize(25)
h_sim_fLateHits_mip.GetYaxis().SetTitleOffset(1.0)

h_sim_fLateHits_mip.GetXaxis().SetLabelFont(43)
h_sim_fLateHits_mip.GetXaxis().SetLabelSize(25)
h_sim_fLateHits_mip.GetXaxis().SetTitleFont(43)
h_sim_fLateHits_mip.GetXaxis().SetTitleSize(25)
h_sim_fLateHits_mip.GetXaxis().SetTitleOffset(1.0)
h_sim_fLateHits_mip.GetXaxis().SetTitle("MIP")


h_data_fLateHits_mip = f_data.Get("h_fLateHits_mip")
h_data_fLateHits_mip.SetLineColor(colorData)
h_data_fLateHits_mip.SetLineWidth(2)

c_fLateHits_mip = TCanvas("c_fLateHits_mip", "c_fLateHits_mip")
h_sim_fLateHits_mip.Draw()
h_data_fLateHits_mip.Draw("SAME")

leg_fLateHits_mip = TLegend(0.60, 0.70, 0.9, 0.9)
leg_fLateHits_mip.AddEntry(h_sim_fLateHits_mip, legendSim)
leg_fLateHits_mip.AddEntry(h_data_fLateHits_mip, legendData)
leg_fLateHits_mip.Draw()

if fWrite:
    c_fLateHits_mip.SaveAs(writePath+writePrefix+"fLateHits_mip.root")
    c_fLateHits_mip.SaveAs(writePath+writePrefix+"fLateHits_mip.C")
    c_fLateHits_mip.SaveAs(writePath+writePrefix+"fLateHits_mip.pdf")
    c_fLateHits_mip.SaveAs(writePath+writePrefix+"fLateHits_mip.svg")

# -------------------------------
# ------ LateHits over Layer -------
# -------------------------------

h_sim_fLateHits_layer = f_sim.Get("h_fLateHits_layer")

h_sim_fLateHits_layer.SetLineColor(colorSim)
h_sim_fLateHits_layer.SetLineWidth(2)

h_sim_fLateHits_layer.GetYaxis().SetLabelFont(43)
h_sim_fLateHits_layer.GetYaxis().SetLabelSize(25)
h_sim_fLateHits_layer.GetYaxis().SetTitleFont(43)
h_sim_fLateHits_layer.GetYaxis().SetTitleSize(25)
h_sim_fLateHits_layer.GetYaxis().SetTitleOffset(1.0)

h_sim_fLateHits_layer.GetXaxis().SetLabelFont(43)
h_sim_fLateHits_layer.GetXaxis().SetLabelSize(25)
h_sim_fLateHits_layer.GetXaxis().SetTitleFont(43)
h_sim_fLateHits_layer.GetXaxis().SetTitleSize(25)
h_sim_fLateHits_layer.GetXaxis().SetTitleOffset(1.0)
h_sim_fLateHits_layer.GetXaxis().SetTitle("Layer")

h_data_fLateHits_layer = f_data.Get("h_fLateHits_layer")
h_data_fLateHits_layer.SetLineColor(colorData)
h_data_fLateHits_layer.SetLineWidth(2)

c_fLateHits_layer = TCanvas("c_fLateHits_layer", "c_fLateHits_layer")
h_sim_fLateHits_layer.Draw()
h_data_fLateHits_layer.Draw("SAME")

leg_fLateHits_layer = TLegend(0.60, 0.70, 0.9, 0.9)
leg_fLateHits_layer.AddEntry(h_sim_fLateHits_layer, legendSim)
leg_fLateHits_layer.AddEntry(h_data_fLateHits_layer, legendData)
leg_fLateHits_layer.Draw()

if fWrite:
    c_fLateHits_layer.SaveAs(writePath+writePrefix+"fLateHits_layer.root")
    c_fLateHits_layer.SaveAs(writePath+writePrefix+"fLateHits_layer.C")
    c_fLateHits_layer.SaveAs(writePath+writePrefix+"fLateHits_layer.pdf")
    c_fLateHits_layer.SaveAs(writePath+writePrefix+"fLateHits_layer.svg")

# -------------------------------
# - Fraction of Late Hits per Event-
# -------------------------------

h_sim_fLateHits_event = f_sim.Get("h_fraction_of_late_hits")

h_sim_fLateHits_event.SetLineColor(colorSim)
h_sim_fLateHits_event.SetLineWidth(2)

h_sim_fLateHits_event.GetYaxis().SetLabelFont(43)
h_sim_fLateHits_event.GetYaxis().SetLabelSize(25)
h_sim_fLateHits_event.GetYaxis().SetTitleFont(43)
h_sim_fLateHits_event.GetYaxis().SetTitleSize(25)
h_sim_fLateHits_event.GetYaxis().SetTitleOffset(1.0)

h_sim_fLateHits_event.GetXaxis().SetLabelFont(43)
h_sim_fLateHits_event.GetXaxis().SetLabelSize(25)
h_sim_fLateHits_event.GetXaxis().SetTitleFont(43)
h_sim_fLateHits_event.GetXaxis().SetTitleSize(25)
h_sim_fLateHits_event.GetXaxis().SetTitleOffset(1.0)
h_sim_fLateHits_event.GetXaxis().SetTitle("Fraction of Late Hits")

h_data_fLateHits_event = f_data.Get("h_fraction_of_late_hits")
h_data_fLateHits_event.SetLineColor(colorData)
h_data_fLateHits_event.SetLineWidth(2)

h_data_fLateHits_event.Scale(1./float(h_data_fLateHits_event.GetEntries()))
h_sim_fLateHits_event.Scale(1./float(h_sim_fLateHits_event.GetEntries()))

c_fLateHits_event = TCanvas("c_fLateHits_event", "c_fLateHits_event")
h_data_fLateHits_event.Draw()
h_sim_fLateHits_event.Draw("SAME")


leg_fLateHits_event = TLegend(0.60, 0.70, 0.9, 0.9)
leg_fLateHits_event.AddEntry(h_sim_fLateHits_event, legendSim)
leg_fLateHits_event.AddEntry(h_data_fLateHits_event, legendData)
leg_fLateHits_event.Draw()

if fWrite:
    c_fLateHits_event.SaveAs(writePath+writePrefix+"fLateHits_event.root")
    c_fLateHits_event.SaveAs(writePath+writePrefix+"fLateHits_event.C")
    c_fLateHits_event.SaveAs(writePath+writePrefix+"fLateHits_event.pdf")
    c_fLateHits_event.SaveAs(writePath+writePrefix+"fLateHits_event.svg")

raw_input("Press Enter to continue...")
