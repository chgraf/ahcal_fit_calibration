import ROOT
from ROOT import TFile, TH1D, TCanvas, TPad, gPad, TLegend, gStyle

f_Muons = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_03_02_muons_nf_tracks.root");
h_Muons_nHits = f_Muons.Get("h_nHits")
h_Muons_nHits.SetTitle("Number of Hits per Event (Data)")

h_Muons_nHits.Scale(1./float(h_Muons_nHits.GetEntries()))
h_Muons_nHits.GetXaxis().SetTitle("# Hits")
h_Muons_nHits.GetYaxis().SetTitle("# Entries")
h_Muons_nHits.SetLineColor(ROOT.kRed+2)
h_Muons_nHits.SetLineWidth(2)

h_Muons_cogZ = f_Muons.Get("h_cogZ")
h_Muons_cogZ.SetTitle("CoG Z (Data)")

h_Muons_cogZ.Scale(1./float(h_Muons_cogZ.GetEntries()))
h_Muons_cogZ.GetXaxis().SetTitle("CoG Z")
h_Muons_cogZ.GetYaxis().SetTitle("# Entries")
h_Muons_cogZ.SetLineColor(ROOT.kRed+2)
h_Muons_cogZ.SetLineWidth(2)



f_Electrons = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_03_02_electrons_nf.root");
h_Electrons_nHits = f_Electrons.Get("h_nHits")

h_Electrons_nHits.Scale(1./float(h_Electrons_nHits.GetEntries()))
h_Electrons_nHits.GetXaxis().SetTitle("# Hits")
h_Electrons_nHits.GetYaxis().SetTitle("# Entries")
h_Electrons_nHits.SetLineColor(ROOT.kGreen+2)
h_Electrons_nHits.SetLineWidth(2)

h_Electrons_cogZ = f_Electrons.Get("h_cogZ")
h_Electrons_cogZ.SetTitle("CoG Z (Data)")

h_Electrons_cogZ.Scale(1./float(h_Electrons_cogZ.GetEntries()))
h_Electrons_cogZ.GetXaxis().SetTitle("CoG Z")
h_Electrons_cogZ.GetYaxis().SetTitle("# Entries")
h_Electrons_cogZ.SetLineColor(ROOT.kGreen+2)
h_Electrons_cogZ.SetLineWidth(2)



f_Pions = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_03_02_pions_nf.root");
h_Pions_nHits = f_Pions.Get("h_nHits")

h_Pions_nHits.Scale(1./float(h_Pions_nHits.GetEntries()))
h_Pions_nHits.GetXaxis().SetTitle("# Hits")
h_Pions_nHits.GetYaxis().SetTitle("# Entries")
h_Pions_nHits.SetLineColor(ROOT.kBlue+4)
h_Pions_nHits.SetLineWidth(2)

h_Pions_cogZ = f_Pions.Get("h_cogZ")
h_Pions_cogZ.SetTitle("CoG Z (Data)")

h_Pions_cogZ.Scale(1./float(h_Pions_cogZ.GetEntries()))
h_Pions_cogZ.GetXaxis().SetTitle("CoG Z")
h_Pions_cogZ.GetYaxis().SetTitle("# Entries")
h_Pions_cogZ.SetLineColor(ROOT.kBlue+4)
h_Pions_cogZ.SetLineWidth(2)

gStyle.SetOptStat(0)


c_nHits = TCanvas("c_nHits", "c_nHits")
h_Muons_nHits.Draw()
h_Electrons_nHits.Draw("SAME")
h_Pions_nHits.Draw("SAME")

leg = TLegend(0.60, 0.70, 0.9, 0.9)
leg.AddEntry(h_Muons_nHits, "Muons")
leg.AddEntry(h_Electrons_nHits, "Electrons")
leg.AddEntry(h_Pions_nHits, "Pions")
leg.Draw()


c_cogZ = TCanvas("c_cogZ", "c_cogZ")
h_Muons_cogZ.Draw()
h_Electrons_cogZ.Draw("SAME")
h_Pions_cogZ.Draw("SAME")

leg = TLegend(0.60, 0.70, 0.9, 0.9)
leg.AddEntry(h_Muons_cogZ, "Muons")
leg.AddEntry(h_Electrons_cogZ, "Electrons")
leg.AddEntry(h_Pions_cogZ, "Pions")
leg.Draw()

raw_input("Press Enter to continue...")
