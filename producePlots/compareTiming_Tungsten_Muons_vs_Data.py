import ROOT
from ROOT import TFile, TH1D, TCanvas, TPad, gPad, TLegend, gStyle

# f_Muons = TFile("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Muons_Sim/muons_003_hcal.root");
# t_Muons = f_Muons.Get("bigtree;1")
# h_Muons = TH1D("h_Muons", "Tungsten 120GeV Muons Simulation vs Data", 2000, -500, 1500)
# t_Muons.Draw("ahc_hitTime>>h_Muons")

# gStyle.SetTitleFont(43)
# gStyle.SetTitleSize(20)
# gStyle.SetLabelFont(43)
# gStyle.SetLabelSize(20)

# f_Muons = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/mc_aug_muons_006.root");
# f_Muons = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_03_10_muons_sim_evtsmearing.root");
f_Muons = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_03_10_muons_sim_T0smearing.root");
# h_Muons = f_Muons.Get("h_resolution")
h_Muons = f_Muons.Get("h_resolution")
h_Muons.Draw()
h_Muons.Rebin(10)


h_Muons.Scale(1./float(h_Muons.GetEntries()))
h_Muons.GetXaxis().SetTitle("Hit Time [ns]")
h_Muons.GetYaxis().SetTitle("# Entries")
h_Muons.SetLineColor(ROOT.kRed+2)
h_Muons.SetLineWidth(2)


f_Data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_03_10_muons_data.root");
# t_Pions = f_Pions.Get("bigtree;1")
# h_Pions = TH1D("h_Pions", "Tungsten Muons vs Pions QGSP_BERT", 2000, -500, 1500)
# h_Data = f_Data.Get("h_resolution_evtTime")
h_Data = f_Data.Get("h_resolution")
# h_Data.Draw()

h_Data.Scale(1./float(h_Data.GetEntries()))
h_Data.SetLineColor(ROOT.kOrange+1)
h_Data.SetLineWidth(2)
h_Data.Rebin(10)

h_Muons.GetYaxis().SetLabelFont(43)
h_Muons.GetYaxis().SetLabelSize(25)
h_Muons.GetYaxis().SetTitleFont(43)
h_Muons.GetYaxis().SetTitleSize(25)
h_Muons.GetYaxis().SetTitleOffset(1.6)

h_Muons.SetMaximum(2*10e-2)
h_Muons.SetMinimum(2*10e-6)
# h_Muons.SetOptStat(0)

h_Muons.SetTitleSize(50)

# h_Muons.UseCurrentStyle()

h_Muons.SetAxisRange(-40,40)

# h_QGSP_BERT_HP.SetTitle("QGSP_BERT_HP")
# h_QGSP_BERT.SetTitle("QGSP_BERT")

c = TCanvas("c", "c", 800, 800)
pad1 = TPad("pad1", "pad1", 0.0, 0.3, 1.0, 1.0)
pad1.SetBottomMargin(0.005)
pad1.Draw()
pad1.cd()


gPad.SetLogy()
h_Muons.Draw()
h_Data.Draw("Same")

leg = TLegend(0.60, 0.70, 0.9, 0.9)
leg.AddEntry(h_Muons, "Simulation")
leg.AddEntry(h_Data, "Data")
leg.Draw()

h_residuals = TH1D("h_residuals", "", h_Muons.GetNbinsX(), h_Muons.GetXaxis().GetXmin(), h_Muons.GetXaxis().GetXmax())

for i in range(1,h_Muons.GetNbinsX()+1):
    if(h_Data.GetBinContent(i) > 0):
        # h_residuals.SetBinContent(i, (h_Muons.GetBinContent(i)-h_Data.GetBinContent(i))/h_Muons.GetBinContent(i))
        h_residuals.SetBinContent(i, h_Muons.GetBinContent(i)/h_Data.GetBinContent(i))


h_residuals.GetYaxis().SetLabelFont(43)
h_residuals.GetYaxis().SetLabelSize(25)
h_residuals.GetYaxis().SetTitleFont(43)
h_residuals.GetYaxis().SetTitleSize(25)
h_residuals.GetYaxis().SetTitleOffset(1.5)
h_residuals.GetYaxis().SetNdivisions(5,5,0)
h_residuals.GetYaxis().SetTitle("MC / Data")

h_residuals.GetXaxis().SetLabelFont(43)
h_residuals.GetXaxis().SetLabelSize(25)
h_residuals.GetXaxis().SetTitleFont(43)
h_residuals.GetXaxis().SetTitleSize(25)
h_residuals.GetXaxis().SetTitleOffset(4.2)
h_residuals.GetXaxis().SetTitle("Hit Time [ns]")

h_residuals.SetAxisRange(-40, 40)
h_residuals.SetMinimum(0.5)
h_residuals.SetMaximum(1.5)

h_residuals.SetMarkerColor(ROOT.kBlue-2)
h_residuals.SetMarkerSize(1.3)


c.cd()
pad2 = TPad("pad2", "pad2", 0.0, 0.05, 1.0, 0.3)
pad2.SetGridx()
pad2.SetGridy()
pad2.SetTopMargin(0.05)
pad2.SetBottomMargin(0.3)
pad2.Draw()
pad2.cd()
h_residuals.SetMarkerStyle(3)
h_residuals.Draw("P")


gStyle.SetOptStat(0);


raw_input("Press Enter to continue...")
