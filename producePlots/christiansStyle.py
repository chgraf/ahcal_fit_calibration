import ROOT
from ROOT import TFile, TH1D, TCanvas, TPad, TLegend, TStyle, gStyle, gROOT

global cgStyle

cgStyle = TStyle("Default", "cgStyle")

cgStyle.SetHistLineWidth(2)
