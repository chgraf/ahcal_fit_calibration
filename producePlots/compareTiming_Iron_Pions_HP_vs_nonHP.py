import ROOT
from ROOT import TFile, TH1D, TCanvas, gPad, TLegend, gStyle

f_QGSP_BERT_HP = TFile("/remote/pcilc2/cgraf/Data/Testbeam/2015_July/MonteCarlo/Pions_Sim/pion_QGSP_BERT_HP_reco_hcal_001.root");
t_QGSP_BERT_HP = f_QGSP_BERT_HP.Get("bigtree;1")
h_QGSP_BERT_HP = TH1D("h_QGSP_BERT_HP", "Iron Pions QGSP_BERT HP vs non-HP", 2000, -500, 1500)
t_QGSP_BERT_HP.Draw("ahc_hitTime>>h_QGSP_BERT_HP")

h_QGSP_BERT_HP.Scale(1./float(h_QGSP_BERT_HP.GetEntries()))
h_QGSP_BERT_HP.GetXaxis().SetTitle("Hit Time [ns]")
h_QGSP_BERT_HP.GetYaxis().SetTitle("# Entries")
h_QGSP_BERT_HP.SetLineColor(ROOT.kAzure-3)
h_QGSP_BERT_HP.SetLineWidth(2)


f_QGSP_BERT = TFile("/remote/pcilc2/cgraf/Data/Testbeam/2015_July/MonteCarlo/Pions_Sim/pion_QGSP_BERT_reco_hcal_001.root");
t_QGSP_BERT = f_QGSP_BERT.Get("bigtree;1")
h_QGSP_BERT = TH1D("h_QGSP_BERT", "Iron Pions QGSP_BERT HP vs non-HP", 2000, -500, 1500)
t_QGSP_BERT.Draw("ahc_hitTime>>h_QGSP_BERT")



h_QGSP_BERT.Scale(1./float(h_QGSP_BERT.GetEntries()))
h_QGSP_BERT.SetLineColor(ROOT.kAzure+8)
h_QGSP_BERT.SetLineWidth(2)

# h_QGSP_BERT_HP.SetTitle("QGSP_BERT_HP")
# h_QGSP_BERT.SetTitle("QGSP_BERT")

c = TCanvas("c", "c")
gPad.SetLogy()
h_QGSP_BERT_HP.Draw()
h_QGSP_BERT.Draw("Same")

gStyle.SetOptStat(0);

leg = TLegend(0.60, 0.70, 0.9, 0.9)
leg.AddEntry(h_QGSP_BERT_HP, "QGSP_BERT_HP")
leg.AddEntry(h_QGSP_BERT, "QGSP_BERT")
leg.Draw()

raw_input("Press Enter to continue...")
