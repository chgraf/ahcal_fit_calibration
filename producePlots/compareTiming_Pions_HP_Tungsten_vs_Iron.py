import ROOT
from ROOT import TFile, TH1D, TCanvas, gPad, TLegend

# f_pion_tungsten_HP = TFile("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_QGSP_BERT_HD_reco_hcal_002.root");
f_pion_tungsten_HP = TFile("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_QGSP_BERT_HD_reco_hcal_001.root");
t_pion_tungsten_HP = f_pion_tungsten_HP.Get("bigtree;1")
h_pion_tungsten_HP = TH1D("h_pion_tungsten_HP", "70GeV Pions QGSP_BERT_HP Tungsten vs Iron", 2000, -500, 1500)
t_pion_tungsten_HP.Draw("ahc_hitTime>>h_pion_tungsten_HP")

h_pion_tungsten_HP.Scale(1./float(h_pion_tungsten_HP.GetEntries()))
h_pion_tungsten_HP.GetXaxis().SetTitle("Hit Time [ns]")
h_pion_tungsten_HP.GetYaxis().SetTitle("# Entries")
h_pion_tungsten_HP.SetLineColor(ROOT.kBlue+3)
h_pion_tungsten_HP.SetLineWidth(2)
# h_pion_tungsten_HP.Rebin(10)


f_pion_iron_HP = TFile("/remote/pcilc2/cgraf/Data/Testbeam/2015_July/MonteCarlo/Pions_Sim/pion_QGSP_BERT_HP_reco_hcal_001.root");
t_pion_iron_HP = f_pion_iron_HP.Get("bigtree;1")
h_pion_iron_HP = TH1D("h_pion_iron_HP", "70GeV Pions QGSP_BERT Tungsten vs Iron", 2000, -500, 1500)
t_pion_iron_HP.Draw("ahc_hitTime>>h_pion_iron_HP")



h_pion_iron_HP.Scale(1./float(h_pion_iron_HP.GetEntries()))
h_pion_iron_HP.SetLineColor(ROOT.kAzure-3)
h_pion_iron_HP.SetLineWidth(2)
# h_pion_iron_HP.Rebin(10)

# h_pion_tungsten_nonHP.SetTitle("pion_tungsten_nonHP")
# h_pion_iron_nonHP.SetTitle("pion_iron_nonHP")

c = TCanvas("c", "c")
gPad.SetLogy()
h_pion_tungsten_HP.Draw()
h_pion_iron_HP.Draw("Same")

#gStyle.SetOptStat(0);

leg = TLegend(0.60, 0.70, 0.9, 0.9)
leg.AddEntry(h_pion_tungsten_HP, "Pions Tungsten")
leg.AddEntry(h_pion_iron_HP, "Pions Steel")
leg.Draw()

raw_input("Press Enter to continue...")
