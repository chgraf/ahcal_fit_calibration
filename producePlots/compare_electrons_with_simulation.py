import ROOT
from ROOT import TFile, TH1D, TCanvas, gPad, TLegend, gStyle


f_Data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_03_18_electrons_data.root");

h_reso_D = f_Data.Get("h_resolution")
h_reso_D.Scale(1./float(h_reso_D.GetEntries()))
h_reso_D.SetLineColor(ROOT.kGreen-7)
h_reso_D.SetLineWidth(2)

h_nHits_D = f_Data.Get("h_nHits")
h_nHits_D.Scale(1./float(h_nHits_D.GetEntries()))
h_nHits_D.SetLineColor(ROOT.kGreen-7)
h_nHits_D.SetLineWidth(2)

h_cogZ_D = f_Data.Get("h_cogZ")
h_cogZ_D.Scale(1./float(h_cogZ_D.GetEntries()))
h_cogZ_D.SetLineColor(ROOT.kGreen-7)
h_cogZ_D.SetLineWidth(2)


# f_Sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_03_09_electrons_simulation.root");
f_Sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_03_18_electrons_sim.root");

h_reso_S = f_Sim.Get("h_resolution")
h_reso_S.Scale(1./float(h_reso_S.GetEntries()))
h_reso_S.SetLineColor(ROOT.kGreen+3)
h_reso_S.SetLineWidth(2)

h_nHits_S = f_Sim.Get("h_nHits")
h_nHits_S.Scale(1./float(h_nHits_S.GetEntries()))
h_nHits_S.SetLineColor(ROOT.kGreen+3)
h_nHits_S.SetLineWidth(2)

h_cogZ_S = f_Sim.Get("h_cogZ")
h_cogZ_S.Scale(1./float(h_cogZ_S.GetEntries()))
h_cogZ_S.SetLineColor(ROOT.kGreen+3)
h_cogZ_S.SetLineWidth(2)



c_reso = TCanvas("c_reso", "c_reso")
h_residuals = TH1D("h_residuals", "", h_sim_timing.GetNbinsX(), h_sim_timing.GetXaxis().GetXmin(), h_sim_timing.GetXaxis().GetXmax())

for i in range(1,h_sim_timing.GetNbinsX()+1):
    if(h_data_timing.GetBinContent(i) > 0):
        # h_residuals.SetBinContent(i, (h_Muons.GetBinContent(i)-h_Data.GetBinContent(i))/h_Muons.GetBinContent(i))
        h_residuals.SetBinContent(i, h_sim_timing.GetBinContent(i)/h_data_timing.GetBinContent(i))


h_residuals.GetYaxis().SetLabelFont(43)
h_residuals.GetYaxis().SetLabelSize(25)
h_residuals.GetYaxis().SetTitleFont(43)
h_residuals.GetYaxis().SetTitleSize(25)
h_residuals.GetYaxis().SetTitleOffset(1.5)
h_residuals.GetYaxis().SetNdivisions(5,5,0)
h_residuals.GetYaxis().SetTitle("MC / Data")

h_residuals.GetXaxis().SetLabelFont(43)
h_residuals.GetXaxis().SetLabelSize(25)
h_residuals.GetXaxis().SetTitleFont(43)
h_residuals.GetXaxis().SetTitleSize(25)
h_residuals.GetXaxis().SetTitleOffset(4.2)
h_residuals.GetXaxis().SetTitle("Hit Time [ns]")

h_residuals.SetAxisRange(-50, 200)
h_residuals.SetMinimum(0.5)
h_residuals.SetMaximum(1.5)

h_residuals.SetMarkerColor(ROOT.kBlue-2)
h_residuals.SetMarkerSize(1.3)


c.cd()
pad2 = TPad("pad2", "pad2", 0.0, 0.05, 1.0, 0.3)
pad2.SetGridx()
pad2.SetGridy()
pad2.SetTopMargin(0.05)
pad2.SetBottomMargin(0.3)
pad2.Draw()
pad2.cd()
h_residuals.SetMarkerStyle(3)
h_residuals.Draw("P")
h_reso_S.Draw()
h_reso_D.Draw("SAME")






c_nHits = TCanvas("c_nHits", "c_nHits")
h_nHits_S.Draw()
h_nHits_D.Draw("SAME")

c_cogZ = TCanvas("c_cogZ", "c_cogZ")
h_cogZ_S.Draw()
h_cogZ_D.Draw("SAME")

raw_input("Press Enter to continue...")
