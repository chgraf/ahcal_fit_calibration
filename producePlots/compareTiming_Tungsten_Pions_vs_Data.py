import ROOT
from ROOT import TFile, TH1D, TCanvas, gPad, TLegend, gStyle

f_Pions = TFile("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_QGSP_BERT_HD_reco_hcal_001.root");
t_Pions = f_Pions.Get("bigtree;1")
h_Pions = TH1D("h_Pions", "Tungsten 70GeV Pions QGSP_BERT_HD Simulation vs Data", 2000, -500, 1500)
t_Pions.Draw("ahc_hitTime>>h_Pions")

h_Pions.Scale(1./float(h_Pions.GetEntries()))
h_Pions.GetXaxis().SetTitle("Hit Time [ns]")
h_Pions.GetYaxis().SetTitle("# Entries")
h_Pions.SetLineColor(ROOT.kBlue+4)
h_Pions.SetLineWidth(2)


f_Data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_02_22_pions.root");
# t_Pions = f_Pions.Get("bigtree;1")
# h_Pions = TH1D("h_Pions", "Tungsten Pions vs Pions QGSP_BERT", 2000, -500, 1500)
h_Data = f_Data.Get("h_resolution")
h_Data.Draw()



h_Data.Scale(1./float(h_Data.GetEntries()))
h_Data.SetLineColor(ROOT.kGreen+2)
h_Data.SetLineWidth(2)
h_Data.Rebin(10)

# h_QGSP_BERT_HP.SetTitle("QGSP_BERT_HP")
# h_QGSP_BERT.SetTitle("QGSP_BERT")

c = TCanvas("c", "c")
gPad.SetLogy()
h_Pions.Draw()
h_Data.Draw("Same")

gStyle.SetOptStat(0);

leg = TLegend(0.60, 0.70, 0.9, 0.9)
leg.AddEntry(h_Pions, "Simulation")
leg.AddEntry(h_Data, "Data")
leg.Draw()

raw_input("Press Enter to continue...")
