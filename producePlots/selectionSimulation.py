import ROOT
from ROOT import TFile, TH1D, TH2D, TCanvas, gPad, TLegend, gStyle

f_Muons = TFile("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Muons_Sim/muons_006_hcal.root");
t_Muons = f_Muons.Get("bigtree;1")

h_Muons_cogZ = TH1D("h_Muons_cogZ", "CogZ (Simulation)", 1000, 0, 1000)
t_Muons.Draw("ahc_cogZ>>h_Muons_cogZ")
h_Muons_nHits = TH1D("h_Muons_nHits", "Number of Hits per Event (Simulation)", 500, 0, 500)
t_Muons.Draw("ahc_nHits>>h_Muons_nHits")
h_Muons_radius = TH1D("h_Muons_radius", "Radius (Simulation)", 200, 0, 200)
t_Muons.Draw("ahc_radius>>h_Muons_radius")
h_Muons_energySum = TH1D("h_Muons_energySum", "energySum (Simulation)", 2000, 0, 2000)
t_Muons.Draw("ahc_energySum>>h_Muons_energySum")

h2_Muons_cogz_nHits = TH2D("h2_Muons_cogz_nHits", "CogZ and nHits (Simulation)", 50, 0, 1000, 50, 0, 500)
t_Muons.Draw("ahc_nHits:ahc_cogZ>>h2_Muons_cogz_nHits")
h2_Muons_cogz_nHits.GetXaxis().SetTitle("Center of Gravity Z [mm]")
h2_Muons_cogz_nHits.GetYaxis().SetTitle("# Hits")

h2_Muons_radius_energySum = TH2D("h2_Muons_radius_energySum", "Radius and EnergySum (Simulation)", 50, 0, 200, 50, 0, 2000)
t_Muons.Draw("ahc_energySum:ahc_radius>>h2_Muons_radius_energySum")
h2_Muons_radius_energySum.GetXaxis().SetTitle("Radius")
h2_Muons_radius_energySum.GetYaxis().SetTitle("EnergySum")

h_Muons_cogZ.SetLineColor(ROOT.kRed+2)
h_Muons_nHits.SetLineColor(ROOT.kRed+2)
h_Muons_radius.SetLineColor(ROOT.kRed+2)
h_Muons_energySum.SetLineColor(ROOT.kRed+2)
h2_Muons_cogz_nHits.SetLineColor(ROOT.kRed+2)
h2_Muons_radius_energySum.SetLineColor(ROOT.kRed+2)

h_Muons_cogZ.SetLineWidth(2)
h_Muons_nHits.SetLineWidth(2)
h_Muons_radius.SetLineWidth(2)
h_Muons_energySum.SetLineWidth(2)
h2_Muons_cogz_nHits.SetLineWidth(2)
h2_Muons_radius_energySum.SetLineWidth(2)

h_Muons_cogZ.Scale(1./10.)
h_Muons_nHits.Scale(1./10.)
h_Muons_radius.Scale(1./10.)
h_Muons_energySum.Scale(1./10.)
h2_Muons_cogz_nHits.Scale(1./10.)
h2_Muons_radius_energySum.Scale(1./10.)

f_Electrons = TFile("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Electrons_Sim/electrons_002_hcal.root");
t_Electrons = f_Electrons.Get("bigtree;1")

h_Electrons_cogZ = TH1D("h_Electrons_cogZ", "CogZ for Electrons", 1000, 0, 1000)
t_Electrons.Draw("ahc_cogZ>>h_Electrons_cogZ")
h_Electrons_nHits = TH1D("h_Electrons_nHits", "nHits for Electrons", 500, 0, 500)
t_Electrons.Draw("ahc_nHits>>h_Electrons_nHits")
h_Electrons_radius = TH1D("h_Electrons_radius", "Radius for Electrons", 200, 0, 200)
t_Electrons.Draw("ahc_radius>>h_Electrons_radius")
h_Electrons_energySum = TH1D("h_Electrons_energySum", "energySum for Electrons", 2000, 0, 2000)
t_Electrons.Draw("ahc_energySum>>h_Electrons_energySum")
h2_Electrons_cogz_nHits = TH2D("h2_cogz_nHits", "CogZ and nHits", 1000, 0, 1000, 500, 0, 500)
t_Electrons.Draw("ahc_cogZ:ahc_nHits>>h2_Electrons_cogz_nHits")

h2_Electrons_cogz_nHits = TH2D("h2_Electrons_cogz_nHits", "CogZ and nHits", 50, 0, 1000, 50, 0, 500)
t_Electrons.Draw("ahc_nHits:ahc_cogZ>>h2_Electrons_cogz_nHits")
h2_Electrons_radius_energySum = TH2D("h2_Electrons_radius_energySum", "Radius and EnergySum", 50, 0, 200, 50, 0, 2000)
t_Electrons.Draw("ahc_energySum:ahc_radius>>h2_Electrons_radius_energySum")

h_Electrons_cogZ.SetLineColor(ROOT.kGreen+2)
h_Electrons_nHits.SetLineColor(ROOT.kGreen+2)
h_Electrons_radius.SetLineColor(ROOT.kGreen+2)
h_Electrons_energySum.SetLineColor(ROOT.kGreen+2)
h2_Electrons_cogz_nHits.SetLineColor(ROOT.kGreen+2)
h2_Electrons_radius_energySum.SetLineColor(ROOT.kGreen+2)

h_Electrons_cogZ.SetLineWidth(2)
h_Electrons_nHits.SetLineWidth(2)
h_Electrons_radius.SetLineWidth(2)
h_Electrons_energySum.SetLineWidth(2)
h2_Electrons_cogz_nHits.SetLineWidth(2)
h2_Electrons_radius_energySum.SetLineWidth(2)

f_Pions = TFile("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pion_QGSP_BERT_HD_reco_hcal_001.root");
t_Pions = f_Pions.Get("bigtree;1")

h_Pions_cogZ = TH1D("h_Pions_cogZ", "CogZ for Pions", 1000, 0, 1000)
t_Pions.Draw("ahc_cogZ>>h_Pions_cogZ")
h_Pions_nHits = TH1D("h_Pions_nHits", "nHits for Pions", 500, 0, 500)
t_Pions.Draw("ahc_nHits>>h_Pions_nHits")
h_Pions_radius = TH1D("h_Pions_radius", "Radius for Pions", 200, 0, 200)
t_Pions.Draw("ahc_radius>>h_Pions_radius")
h_Pions_energySum = TH1D("h_Pions_energySum", "energySum for Pions", 2000, 0, 2000)
t_Pions.Draw("ahc_energySum>>h_Pions_energySum")

h2_Pions_cogz_nHits = TH2D("h2_Pions_cogz_nHits", "CogZ and nHits", 50, 0, 1000, 50, 0, 500)
t_Pions.Draw("ahc_nHits:ahc_cogZ>>h2_Pions_cogz_nHits")
h2_Pions_radius_energySum = TH2D("h2_Pions_radius_energySum", "Radius and EnergySum", 50, 0, 200, 50, 0, 2000)
t_Pions.Draw("ahc_energySum:ahc_radius>>h2_Pions_radius_energySum")

h_Pions_cogZ.SetLineColor(ROOT.kBlue+4)
h_Pions_nHits.SetLineColor(ROOT.kBlue+4)
h_Pions_radius.SetLineColor(ROOT.kBlue+4)
h_Pions_energySum.SetLineColor(ROOT.kBlue+4)
h2_Pions_cogz_nHits.SetLineColor(ROOT.kBlue+4)
h2_Pions_radius_energySum.SetLineColor(ROOT.kBlue+4)

h_Pions_cogZ.SetLineWidth(2)
h_Pions_nHits.SetLineWidth(2)
h_Pions_radius.SetLineWidth(2)
h_Pions_energySum.SetLineWidth(2)
h2_Pions_cogz_nHits.SetLineWidth(2)
h2_Pions_radius_energySum.SetLineWidth(2)


leg = TLegend(0.60, 0.70, 0.9, 0.9)
leg.AddEntry(h_Muons_cogZ, "Muons")
leg.AddEntry(h_Electrons_cogZ, "Electrons")
leg.AddEntry(h_Pions_cogZ, "Pions")

gStyle.SetOptStat(0)

c_cogZ_nHits = TCanvas("c_cogZ_nHits", "c_cogZ_nHits")
c_cogZ_nHits.cd()
h2_Muons_cogz_nHits.Draw("BOX")
h2_Electrons_cogz_nHits.Draw("BOXSAME")
h2_Pions_cogz_nHits.Draw("BOXSAME")
leg.Draw()

c_radius_energySum = TCanvas("c_radius_energySum", "c_radius_energySum")
c_radius_energySum.cd()
h2_Muons_radius_energySum.Draw("BOX")
h2_Electrons_radius_energySum.Draw("BOX SAME")
h2_Pions_radius_energySum.Draw("BOX SAME")
leg.Draw()


c_cogZ = TCanvas("c_cogz", "c_cogZ")
c_cogZ.cd()
h_Muons_cogZ.Draw()
h_Electrons_cogZ.Draw("SAME")
h_Pions_cogZ.Draw("SAME")
leg.Draw()

c_nHits = TCanvas("c_nHits", "c_nHits")
c_nHits.cd()
h_Muons_nHits.Draw()
h_Electrons_nHits.Draw("SAME")
h_Pions_nHits.Draw("SAME")
leg.Draw()

c_radius = TCanvas("c_radius", "c_radius")
c_radius.cd()
h_Muons_radius.Draw()
h_Electrons_radius.Draw("SAME")
h_Pions_radius.Draw("SAME")
leg.Draw()

c_energySum = TCanvas("c_energySum", "c_energySum")
c_energySum.cd()
h_Muons_energySum.Draw()
h_Electrons_energySum.Draw("SAME")
h_Pions_energySum.Draw("SAME")
leg.Draw()

raw_input("Press Enter to continue...")
