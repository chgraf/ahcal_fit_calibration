import ROOT
from ROOT import TFile, TH1D, TCanvas, TPad, gPad, TLegend, gStyle, gROOT, TLegendEntry, TLatex
# import numpy as np

# import christiansStyle

#gROOT.SetStyle("cgStyle")

def rescaleaxis(g,scale=50e-12/1e-9):
    """This function rescales the x-axis on a TGraph."""
    N = g.GetN()
    x = g.GetX()
    for i in range(N):
        x[i] *= scale
    g.GetHistogram().Delete()
    g.SetHistogram(0)
    return

def beautifyPlot(h, fNormalizing):
    if(fNormalizing):
        # h.Scale(1./float(h.GetEntries()))
        h.Scale(1./float(h.Integral()))
    h.GetYaxis().SetLabelFont(43)
    h.GetYaxis().SetLabelSize(25)
    h.GetYaxis().SetTitleFont(43)
    h.GetYaxis().SetTitleSize(25)
    h.GetYaxis().SetTitleOffset(1.0)

    h.GetXaxis().SetLabelFont(43)
    h.GetXaxis().SetLabelSize(25)
    h.GetXaxis().SetTitleFont(43)
    h.GetXaxis().SetTitleSize(25)
    h.GetXaxis().SetTitleOffset(1.0)

    h.SetLineColor(colors[i])
    h.SetMarkerColor(colors[i])
    h.SetLineWidth(2)

    return h


fWrite = True
writePath = "../plots/17_09_19_forCaliceTokyo/compareDataMC/"
writePrefix = "compare_pion_data_MC_70GeV_reduced_"
writePrefixForOthers = "otherFiles/" #Additional path for .C .svg and .root files, leave empty for same folder

legendEntry = []
colors = []
files = []

energy = 70

if energy == 90:
    colors.append(ROOT.kRed+3)
    colors.append(ROOT.kRed+2)
    colors.append(ROOT.kRed)
    legendHeader = "#bf{Pion 90GeV}"
    legendEntry.append("Pion QGSP_BERT_HP 90 GeV")
    legendEntry.append("Pion QGSP_BERT 90 GeV")
    legendEntry.append("Pion Data 90 GeV")
    files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_08_aug_sim_pions_90.root"))
    files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_08_aug_sim_QGSP_BERT_pions_90.root"))
    files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_08_aug_data_pions_90.root"))

elif energy == 70:
    legendHeader = "#bf{Pion 70GeV}"
    legendEntry.append("QGSP_BERT_HP")
    # legendEntry.append("QGSP_BERT")
    # legendEntry.append("FTFP_BERT_HP")
    # legendEntry.append("FTFP_BERT")
    legendEntry.append("QBBC")
    # legendEntry.append("QGSP_BIC_HP")
    # legendEntry.append("QGSP_BIC")
    legendEntry.append("Data")

    colors.append(ROOT.kRed+1)

    # colors.append(ROOT.kOrange+3)
    # colors.append(ROOT.kOrange+4)
    # colors.append(ROOT.kRed-4)
    # colors.append(ROOT.kRed-7)
    colors.append(ROOT.kGreen-1)
    # colors.append(ROOT.kBlue+3)
    # colors.append(ROOT.kBlue-3)
    colors.append(ROOT.kOrange-3)

    files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_14_aug_sim_QGSP_BERT_HP_70.root"))
    # files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_14_aug_sim_QGSP_BERT_70.root"))
    # files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_08_aug_sim_FTFP_BERT_HP_pions_70.root"))
    # files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_08_aug_sim_FTFP_BERT_pions_70.root"))
    files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_14_aug_sim_QBBC_pions_70.root"))
    # files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_14_aug_sim_QGSP_BIC_HP_pions_70.root"))
    # files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_14_aug_sim_QGSP_BIC_pions_70.root"))
    files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_14_aug_data_pions_70.root"))

elif energy == 50:
    legendEntry.append("Pion QGSP_BERT_HP 50 GeV")
    legendEntry.append("Pion QGSP_BERT 50 GeV")
    legendEntry.append("Pion Data 50 GeV")
    colors.append(ROOT.kGreen+4)
    colors.append(ROOT.kGreen+3)
    colors.append(ROOT.kGreen+1)
    files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_08_aug_sim_pions_50.root"))
    files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_08_aug_sim_QGSP_BERT_pions_50.root"))
    files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_08_aug_data_pions_50.root"))

elif energy == 30:
    legendEntry.append("Pion QGSP_BERT_HP 30 GeV")
    legendEntry.append("Pion QGSP_BERT 30 GeV")
    legendEntry.append("Pion Data 30 GeV")
    colors.append(ROOT.kAzure-6)
    colors.append(ROOT.kAzure+2)
    colors.append(ROOT.kAzure+8)
    files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_08_aug_sim_pions_30.root"))
    files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_08_aug_sim_QGSP_BERT_pions_30.root"))
    files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_08_aug_data_pions_30.root"))

elif energy == 10:
    legendEntry.append("Pion QGSP_BERT_HP 10 GeV")
    legendEntry.append("Pion QGSP_BERT 10 GeV")
    legendEntry.append("Pion Data 10 GeV")
    colors.append(ROOT.kMagenta+4)
    colors.append(ROOT.kMagenta+2)
    colors.append(ROOT.kMagenta-7)
    files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_08_aug_sim_pions_10.root"))
    files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_08_aug_sim_QGSP_BERT_pions_10.root"))
    files.append(TFile("/home/iwsatlas1/cgraf/Analysis/timingAnalysis/fit_calibration/cali_output/17_08_08_aug_data_pions_10.root"))

gStyle.SetOptStat(0);

i = 0

canvases = dict()

canvases['c_timing'] = TCanvas("c_timing", "c_timing", 800, 600)
canvases['c_timing_normalized'] = TCanvas("c_timing_normalized", "c_timing_normalized", 800, 600)

canvases['c_nHits'] = TCanvas("c_nHits", "c_nHits", 800, 600)
canvases['c_cogZ'] = TCanvas("c_cogZ", "c_cogZ", 800, 600)
canvases['c_energySum'] = TCanvas("c_energySum", "c_energySum", 800, 600)
canvases['c_nHitsPerLayer'] = TCanvas("c_nHitsPerLayer", "c_nHitsPerLayer", 800, 600)
canvases['c_nHitsPerChip'] = TCanvas("c_nHitsPerChip", "c_nHitsPerChip", 800, 600)

canvases['c_hitEnergy'] = TCanvas("c_hitEnergy", "c_hitEnergy", 800, 600)
canvases['c_hitRadius'] = TCanvas("c_hitRadius", "c_hitRadius", 800, 600)
canvases['c_showerStart'] = TCanvas("c_showerStart", "c_showerStart", 800, 600)
canvases['c_mip_over_hitRadius_fraction'] = TCanvas("c_mip_over_hitRadius_fraction", "c_mip_over_hitRadius_fraction", 800, 600)

canvases['c_fLateHits_radius'] = TCanvas("c_fLateHits_radius", "c_fLateHits_radius", 800, 600)
canvases['c_fLateHits_mip'] = TCanvas("c_fLateHits_mip", "c_fLateHits_mip", 800, 600)
canvases['c_fLateHits_layer'] = TCanvas("c_fLateHits_layer", "c_fLateHits_layer", 800, 600)
canvases['c_fLateHits_event'] = TCanvas("c_fLateHits_event", "c_fLateHits_event", 800, 600)

canvases['c_timeMean_overShowerDepth_50To200'] = TCanvas("c_timeMean_overShowerDepth_50To200", "c_timeMean_overShowerDepth_50To200", 800, 600)
# canvases['c_timeMean_overRadius'] = TCanvas("c_timeMean_overRadius", "c_timeMean_overRadius", 800, 600)
# canvases['c_timeMean_overshowerDepth_50To200'] = TCanvas("c_timeMean_overshowerDepth_50To200", "c_timeMean_overshowerDepth_50To200", 800, 600)
canvases['c_timeMean_overRadius_50To200'] = TCanvas("c_timeMean_overRadius_50To200", "c_timeMean_overRadius_50To200", 800, 600)
canvases['c_timeMean_overRadius_50To200_smallLayers'] = TCanvas("c_timeMean_overRadius_50To200_smallLayers", "c_timeMean_overRadius_50To200_smallLayers", 800, 600)
canvases['c_timeMean_overRadius_50To200_bigLayers'] = TCanvas("c_timeMean_overRadius_50To200_bigLayers", "c_timeMean_overRadius_50To200_bigLayers", 800, 600)
# canvases['c_timeMedian_overRadius'] = TCanvas("c_timeMedian_overRadius", "c_timeMedian_overRadius", 800, 600)

# canvases['c_timeFit_radius_Afast_over_Aslow'] = TCanvas("c_timeFit_radius_Afast_over_Aslow", "c_timeFit_radius_Afast_over_Aslow", 800, 600)
# canvases['c_timeFit_radius_Aslow'] = TCanvas("c_timeFit_radius_Aslow", "c_timeFit_radius_Aslow", 800, 600)
# canvases['c_timeFit_radius_Tfast'] = TCanvas("c_timeFit_radius_Tfast", "c_timeFit_radius_Tfast", 800, 600)
# canvases['c_timeFit_radius_Tslow'] = TCanvas("c_timeFit_radius_Tslow", "c_timeFit_radius_Tslow", 800, 600)

legLeft = TLegend(0.1, 0.55, 0.40, 0.9)
legRight = TLegend(0.60, 0.55, 0.9, 0.9)

leg = TLegend(0.6, 0.55, 0.9, 0.9)


# entry = TLegendEntry()
# entry = legLeft.AddEntry(0, legendHeader, "")
# entry.SetTextFont(entry.GetTextFont()+1)
# entry = legRight.AddEntry(0, legendHeader, "")
# entry.SetTextFont(entry.GetTextFont()+1)
# entry = leg.AddEntry(0, legendHeader, "")
# entry.SetTextFont(entry.GetTextFont()+1)

legLeft.SetHeader(legendHeader)
legRight.SetHeader(legendHeader)
# legLeft.SetTitleFont(0)


# leg.AddEntry(0, legendHeader, "");
# legRight.AddEntry(0, legendHeader, "");
# gStyle.SetLegendFont(gStyle.GetLegendFont()-1)


l_leftLegend = dict()
l_drawGrid = dict()
l_calicePosition = dict()

for f in files:

    hs = dict()

    # timing
    hs['h_timing'] = f.Get("h_resolution_t0Time")
    hs['h_timing'] = beautifyPlot(hs['h_timing'], True)
    hs['h_timing'].GetXaxis().SetTitle("Hit Time [ns]")
    hs['h_timing'].GetYaxis().SetTitle("Normalized Entries");
    hs['h_timing'].SetAxisRange(-50,500)
    hs['h_timing'].SetMinimum(4e-6)
    hs['h_timing'].SetMaximum(1e-1)
    l_leftLegend['timing'] = False
    l_drawGrid['timing'] = True
    l_calicePosition['timing'] = 2

    # Event Normalized timing
    hs['h_timing_normalized'] = f.Get("h_resolution_t0Time_normalized")
    hs['h_timing_normalized'] = beautifyPlot(hs['h_timing_normalized'], False)
    hs['h_timing_normalized'].GetXaxis().SetTitle("Hit Time [ns]")
    hs['h_timing_normalized'].GetYaxis().SetTitle("# Entries / Event / 1ns");
    hs['h_timing_normalized'].SetAxisRange(-50,500)
    hs['h_timing_normalized'].SetMinimum(3e-4);
    hs['h_timing_normalized'].SetMaximum(5);
    l_leftLegend['timing_normalized'] = False
    l_drawGrid['timing_normalized'] = True
    l_calicePosition['timing_normalized'] = 3

    # nHits
    hs['h_nHits'] = f.Get("h_hits_nHits")
    hs['h_nHits'] = beautifyPlot(hs['h_nHits'], True)
    hs['h_nHits'].GetXaxis().SetTitle("Number of Hits per Event")
    hs['h_nHits'].GetYaxis().SetTitle("Normalized Entries");
    hs['h_nHits'].SetMinimum(0);
    hs['h_nHits'].SetMaximum(0.06);
    l_leftLegend['nHits'] = False
    l_drawGrid['nHits'] = True
    l_calicePosition['nHits'] = 0

    # cogZ
    hs['h_cogZ'] = f.Get("h_hits_cogZ")
    hs['h_cogZ'] = beautifyPlot(hs['h_cogZ'], True)
    hs['h_cogZ'].GetXaxis().SetTitle("CoG Z [mm]")
    hs['h_cogZ'].GetYaxis().SetTitle("Normalized Entries");
    hs['h_cogZ'].SetAxisRange(0,800);
    hs['h_cogZ'].SetMinimum(0);
    hs['h_cogZ'].SetMaximum(0.012);
    l_leftLegend['cogZ'] = False
    l_drawGrid['cogZ'] = True
    l_calicePosition['cogZ'] = 0

    # energySum
    hs['h_energySum'] = f.Get("h_hits_energySum")
    hs['h_energySum'] = beautifyPlot(hs['h_energySum'], True)
    hs['h_energySum'].GetXaxis().SetTitle("Energy Sum [MIP]")
    hs['h_energySum'].GetYaxis().SetTitle("Normalized Entries");
    hs['h_energySum'].SetMinimum(0);
    hs['h_energySum'].SetMaximum(0.012);
    l_leftLegend['energySum'] = False
    l_drawGrid['energySum'] = True
    l_calicePosition['energySum'] = 0

    # single hit energy
    hs['h_hitEnergy'] = f.Get("h_hits_singleHitEnergy")
    hs['h_hitEnergy'] = beautifyPlot(hs['h_hitEnergy'], True)
    hs['h_hitEnergy'].GetXaxis().SetTitle("Energy  [MIP]")
    hs['h_hitEnergy'].GetYaxis().SetTitle("Normalized Entries");
    hs['h_hitEnergy'].SetAxisRange(0,200);
    hs['h_hitEnergy'].SetMinimum(1e-6);
    hs['h_hitEnergy'].SetMaximum(1e-1);
    l_leftLegend['hitEnergy'] = False
    l_drawGrid['hitEnergy'] = False
    l_calicePosition['hitEnergy'] = 0

    # hit radius
    hs['h_hitRadius'] = f.Get("h_hits_hitRadius")
    hs['h_hitRadius'] = beautifyPlot(hs['h_hitRadius'], True)
    hs['h_hitRadius'].GetXaxis().SetTitle("Radius [mm]")
    hs['h_hitRadius'].GetYaxis().SetTitle("Normalized Entries");
    hs['h_hitRadius'].SetAxisRange(0,450);
    hs['h_hitRadius'].SetMinimum(1e-3);
    hs['h_hitRadius'].SetMaximum(2e-1);
    l_leftLegend['hitRadius'] = False
    l_drawGrid['hitRadius'] = False
    l_calicePosition['hitRadius'] = 3

    # hit shower Start
    hs['h_showerStart'] = f.Get("h_hits_showerStart")
    hs['h_showerStart'] = beautifyPlot(hs['h_showerStart'], True)
    hs['h_showerStart'].GetXaxis().SetTitle("Shower Start [Layer]")
    hs['h_showerStart'].GetYaxis().SetTitle("Normalized Entries");
    l_leftLegend['showerStart'] = False
    l_drawGrid['showerStart'] = False
    l_calicePosition['showerStart'] = 2

    # hit shower Start
    hs['h_mip_over_hitRadius_fraction'] = f.Get("h2_hits_mip_over_hitRadius_fraction")
    hs['h_mip_over_hitRadius_fraction'] = beautifyPlot(hs['h_mip_over_hitRadius_fraction'], False)
    hs['h_mip_over_hitRadius_fraction'].GetXaxis().SetTitle("Radius [mm]")
    hs['h_mip_over_hitRadius_fraction'].GetYaxis().SetTitle("Energy Fraction");
    l_leftLegend['mip_over_hitRadius_fraction'] = True
    l_drawGrid['mip_over_hitRadius_fraction'] = False
    l_calicePosition['mip_over_hitRadius_fraction'] = 2

    # nHitsPerLayer
    hs['h_nHitsPerLayer'] = f.Get("h_hits_mean_nHitsPerLayer")
    hs['h_nHitsPerLayer'] = beautifyPlot(hs['h_nHitsPerLayer'], False)
    hs['h_nHitsPerLayer'].GetXaxis().SetTitle("Layer")
    hs['h_nHitsPerLayer'].GetYaxis().SetTitle("<# Hits>");
    hs['h_nHitsPerLayer'].SetMinimum(0);
    hs['h_nHitsPerLayer'].SetMaximum(18);
    hs['h_nHitsPerLayer'].SetTitle("Mean Number of Hits per Layer");
    l_leftLegend['nHitsPerLayer'] = True
    l_drawGrid['nHitsPerLayer'] = False
    l_calicePosition['nHitsPerLayer'] = 1

    # nHitsPerChip
    hs['h_nHitsPerChip'] = f.Get("h_hits_mean_nHitsPerChip")
    hs['h_nHitsPerChip'] = beautifyPlot(hs['h_nHitsPerChip'], False)
    hs['h_nHitsPerChip'].GetXaxis().SetTitle("ChipID")
    hs['h_nHitsPerChip'].GetYaxis().SetTitle("<# Hits>");
    hs['h_nHitsPerChip'].SetMinimum(0);
    hs['h_nHitsPerChip'].SetMaximum(5);
    hs['h_nHitsPerChip'].SetTitle("Mean Number of Hits per Chip");
    l_leftLegend['nHitsPerChip'] = False
    l_drawGrid['nHitsPerChip'] = False
    l_calicePosition['nHitsPerChip'] = 0

    # Late Hits over Radius
    hs['h_fLateHits_radius'] = f.Get("h_fLateHits_radius")
    hs['h_fLateHits_radius'] = beautifyPlot(hs['h_fLateHits_radius'], False)
    hs['h_fLateHits_radius'].GetXaxis().SetTitle("Hit Radius [mm]")
    hs['h_fLateHits_radius'].GetYaxis().SetTitle("Fraction of Late Hits > 50ns");
    hs['h_fLateHits_radius'].SetAxisRange(0,300);
    hs['h_fLateHits_radius'].SetMinimum(0);
    hs['h_fLateHits_radius'].SetMaximum(1.2);
    hs['h_fLateHits_radius'].SetTitle("");
    l_leftLegend['fLateHits_radius'] = True
    l_drawGrid['fLateHits_radius'] = True
    l_calicePosition['fLateHits_radius'] = 2
    l_calicePosition['fLateHits_radius'] = 1

    # Late Hits over MIP
    hs['h_fLateHits_mip'] = f.Get("h_fLateHits_mip")
    hs['h_fLateHits_mip'] = beautifyPlot(hs['h_fLateHits_mip'], False)
    hs['h_fLateHits_mip'].GetXaxis().SetTitle("Hit Energy [MIP]")
    hs['h_fLateHits_mip'].GetYaxis().SetTitle("Fraction of Late Hits > 50ns");
    hs['h_fLateHits_mip'].GetYaxis().SetTitleOffset(1.18);
    hs['h_fLateHits_mip'].SetAxisRange(0,20);
    hs['h_fLateHits_mip'].SetMinimum(0);
    hs['h_fLateHits_mip'].SetMaximum(0.23);
    hs['h_fLateHits_mip'].SetTitle("");
    l_leftLegend['fLateHits_mip'] = False
    l_drawGrid['fLateHits_mip'] = False
    l_calicePosition['fLateHits_mip'] = 2

    # Late Hits over Layer
    hs['h_fLateHits_layer'] = f.Get("h_fLateHits_layer")
    hs['h_fLateHits_layer'] = beautifyPlot(hs['h_fLateHits_layer'], False)
    hs['h_fLateHits_layer'].GetXaxis().SetTitle("Layer")
    hs['h_fLateHits_layer'].GetYaxis().SetTitle("Fraction of Late Hits > 50ns");
    hs['h_fLateHits_layer'].GetYaxis().SetTitleOffset(1.18);
    hs['h_fLateHits_layer'].SetAxisRange(0,20);
    hs['h_fLateHits_layer'].SetMinimum(0);
    hs['h_fLateHits_layer'].SetMaximum(0.25);
    hs['h_fLateHits_layer'].SetBinContent(13,0.);
    hs['h_fLateHits_layer'].SetTitle("");
    l_leftLegend['fLateHits_layer'] = False
    l_drawGrid['fLateHits_layer'] = False
    l_calicePosition['fLateHits_layer'] = 0

    # Late Hits over Event
    hs['h_fLateHits_event'] = f.Get("h_fraction_of_late_hits")
    hs['h_fLateHits_event'] = beautifyPlot(hs['h_fLateHits_event'], True)
    hs['h_fLateHits_event'].GetXaxis().SetTitle("Fraction of Late Hits > 50ns")
    hs['h_fLateHits_event'].GetYaxis().SetTitle("Normalized Entries");
    hs['h_fLateHits_event'].GetYaxis().SetTitleOffset(1.18);
    hs['h_fLateHits_event'].SetAxisRange(0,0.5);
    hs['h_fLateHits_event'].SetMinimum(0);
    hs['h_fLateHits_event'].SetMaximum(0.25);
    l_leftLegend['fLateHits_event'] = False
    l_drawGrid['fLateHits_event'] = False
    l_calicePosition['fLateHits_event'] = 0

    # Mean of Time distribution over Layer
    hs['gr_timeMean_overShowerDepth_50To200'] = f.Get("gr_timeMean_overShowerDepth_50To200")
    hs['gr_timeMean_overShowerDepth_50To200'] = beautifyPlot(hs['gr_timeMean_overShowerDepth_50To200'], False)
    hs['gr_timeMean_overShowerDepth_50To200'].GetXaxis().SetTitle("ShowerDepth [mm]");
    hs['gr_timeMean_overShowerDepth_50To200'].GetYaxis().SetTitle("Mean Hit Time (-50,200) [ns]")
    hs['gr_timeMean_overShowerDepth_50To200'].GetXaxis().SetLimits(0,650);
    hs['gr_timeMean_overShowerDepth_50To200'].SetMaximum(12);
    hs['gr_timeMean_overShowerDepth_50To200'].SetMinimum(0);
    hs['gr_timeMean_overShowerDepth_50To200'].SetTitle("");
    l_leftLegend['timeMean_overShowerDepth_50To200'] = True
    l_drawGrid['timeMean_overShowerDepth_50To200'] = True
    l_calicePosition['timeMean_overShowerDepth_50To200'] = 1

    # Mean of Time distribution over Radius
    # hs['gr_timeMean_overLayer_50To200'] = f.Get("gr_timeMean_overLayer_50To200")
    # hs['gr_timeMean_overLayer_50To200'] = beautifyPlot(hs['gr_timeMean_overLayer_50To200'], False)
    # hs['gr_timeMean_overLayer_50To200'].GetXaxis().SetTitle("Hit Layer (to center of detector) [ij]");
    # hs['gr_timeMean_overLayer_50To200'].GetYaxis().SetTitle("Mean Hit Time (-50, 200ns) [ns]")

    # Mean of Time distribution over Radius
    # hs['gr_timeMean_overRadius'] = f.Get("gr_timeMean_overRadius")
    # hs['gr_timeMean_overRadius'] = beautifyPlot(hs['gr_timeMean_overRadius'], False)
    # hs['gr_timeMean_overRadius'].GetXaxis().SetTitle("Hit Radius [mm]");
    # hs['gr_timeMean_overRadius'].GetYaxis().SetTitle("Mean Hit Time [ns]")
    # l_leftLegend['gr_timeMean_overRadius'] = True

    # Mean of Time distribution over Radius
    hs['gr_timeMean_overRadius_50To200'] = f.Get("gr_timeMean_overRadius_50To200")
    hs['gr_timeMean_overRadius_50To200'] = beautifyPlot(hs['gr_timeMean_overRadius_50To200'], False)
    hs['gr_timeMean_overRadius_50To200'].GetXaxis().SetTitle("Hit Radius [mm]");
    hs['gr_timeMean_overRadius_50To200'].GetYaxis().SetTitle("Mean Hit Time (-50, 200ns) [ns]")
    hs['gr_timeMean_overRadius_50To200'].GetXaxis().SetLimits(0,350);
    hs['gr_timeMean_overRadius_50To200'].SetMaximum(40);
    hs['gr_timeMean_overRadius_50To200'].SetMinimum(0);
    hs['gr_timeMean_overRadius_50To200'].SetTitle("");
    l_leftLegend['timeMean_overRadius_50To200'] = True
    l_drawGrid['timeMean_overRadius_50To200'] = True
    l_calicePosition['timeMean_overRadius_50To200'] = 2

    # Mean of Time distribution over Radius
    hs['gr_timeMean_overRadius_50To200_smallLayers'] = f.Get("gr_timeMean_overRadius_50To200_smallLayers")
    rescaleaxis(hs['gr_timeMean_overRadius_50To200_smallLayers'], scale=30)
    hs['gr_timeMean_overRadius_50To200_smallLayers'] = beautifyPlot(hs['gr_timeMean_overRadius_50To200_smallLayers'], False)
    hs['gr_timeMean_overRadius_50To200_smallLayers'].GetXaxis().SetTitle("Hit Radius [mm]");
    hs['gr_timeMean_overRadius_50To200_smallLayers'].GetYaxis().SetTitle("Mean Hit Time (-50, 200ns) [ns]")
    hs['gr_timeMean_overRadius_50To200_smallLayers'].GetXaxis().SetLimits(0,400);
    # hs['gr_timeMean_overRadius_50To200_smallLayers'].GetXaxis().SetTitleOffset(1.2);
    hs['gr_timeMean_overRadius_50To200_smallLayers'].SetMaximum(40);
    hs['gr_timeMean_overRadius_50To200_smallLayers'].SetMinimum(0);
    hs['gr_timeMean_overRadius_50To200_smallLayers'].SetTitle("Small Layers");
    l_leftLegend['timeMean_overRadius_50To200_smallLayers'] = True
    l_drawGrid['timeMean_overRadius_50To200_smallLayers'] = True
    l_calicePosition['timeMean_overRadius_50To200_smallLayers'] = 2

    # Mean of Time distribution over Radius
    hs['gr_timeMean_overRadius_50To200_bigLayers'] = f.Get("gr_timeMean_overRadius_50To200_bigLayers")
    rescaleaxis(hs['gr_timeMean_overRadius_50To200_bigLayers'], scale=30)
    hs['gr_timeMean_overRadius_50To200_bigLayers'] = beautifyPlot(hs['gr_timeMean_overRadius_50To200_bigLayers'], False)
    hs['gr_timeMean_overRadius_50To200_bigLayers'].GetXaxis().SetTitle("Hit Radius [mm]");
    hs['gr_timeMean_overRadius_50To200_bigLayers'].GetYaxis().SetTitle("Mean Hit Time (-50, 200ns) [ns]")
    hs['gr_timeMean_overRadius_50To200_bigLayers'].GetXaxis().SetLimits(0,400);
    # hs['gr_timeMean_overRadius_50To200_bigLayers'].GetXaxis().SetTicks("+-");
    # hs['gr_timeMean_overRadius_50To200_bigLayers'].GetXaxis().SetTitleOffset(1.2);
    hs['gr_timeMean_overRadius_50To200_bigLayers'].SetMaximum(40);
    hs['gr_timeMean_overRadius_50To200_bigLayers'].SetMinimum(0);
    hs['gr_timeMean_overRadius_50To200_bigLayers'].SetTitle("Big Layers");

    l_leftLegend['timeMean_overRadius_50To200_bigLayers'] = True
    l_drawGrid['timeMean_overRadius_50To200_bigLayers'] = True
    l_calicePosition['timeMean_overRadius_50To200_bigLayers'] = 2


    # # Median of Time distribution over Radius
    # hs['gr_timeMedian_overRadius'] = f.Get("gr_timeMedian_overRadius")
    # hs['gr_timeMedian_overRadius'] = beautifyPlot(hs['gr_timeMedian_overRadius'], False)
    # hs['gr_timeMedian_overRadius'].GetXaxis().SetTitle("Hit Radius [mm]");
    # hs['gr_timeMedian_overRadius'].GetYaxis().SetTitle("Median Hit Time [ns]")
    # l_leftLegend['gr_timeMean_overRadius'] = True

    # Fast Time Constant of Double Exponential Fit
    # hs['gr_timeFit_radius_Tfast'] = f.Get("gr_timeFit_radius_Tfast")
    # hs['gr_timeFit_radius_Tfast'] = beautifyPlot(hs['gr_timeFit_radius_Tfast'], False)
    # hs['gr_timeFit_radius_Tfast'].GetXaxis().SetTitle("Hit Radius [mm]");
    # hs['gr_timeFit_radius_Tfast'].GetYaxis().SetTitle("Tfast [ns]")
    # hs['gr_timeFit_radius_Tfast'].GetXaxis().SetLimits(0,350);
    # hs['gr_timeFit_radius_Tfast'].SetMinimum(0);
    # hs['gr_timeFit_radius_Tfast'].SetMaximum(30);

    # Slow Time Constant of Double Exponential Fit
    # hs['gr_timeFit_radius_Tslow'] = f.Get("gr_timeFit_radius_Tslow")
    # hs['gr_timeFit_radius_Tslow'] = beautifyPlot(hs['gr_timeFit_radius_Tslow'], False)
    # hs['gr_timeFit_radius_Tslow'].GetXaxis().SetTitle("Hit Radius [mm]");
    # hs['gr_timeFit_radius_Tslow'].GetYaxis().SetTitle("Tslow [ns]")
    # hs['gr_timeFit_radius_Tfast'].GetXaxis().SetLimits(0,350);
    # hs['gr_timeFit_radius_Tfast'].SetMinimum(0);
    # hs['gr_timeFit_radius_Tfast'].SetMaximum(800);

    # Fast Time Constant of Double Exponential Fit
    # hs['gr_timeFit_radius_Afast_over_Aslow'] = f.Get("gr_timeFit_radius_Afast_over_Aslow")
    # hs['gr_timeFit_radius_Afast_over_Aslow'] = beautifyPlot(hs['gr_timeFit_radius_Afast_over_Aslow'], False)
    # hs['gr_timeFit_radius_Afast_over_Aslow'].GetXaxis().SetTitle("Hit Radius [mm]");
    # hs['gr_timeFit_radius_Afast_over_Aslow'].GetYaxis().SetTitle("Afast / Aslow")
    # hs['gr_timeFit_radius_Afast_over_Aslow'].SetMinimum(0);

    # Slow Time Constant of Double Exponential Fit
    # hs['gr_timeFit_radius_Aslow'] = f.Get("gr_timeFit_radius_Aslow")
    # hs['gr_timeFit_radius_Aslow'] = beautifyPlot(hs['gr_timeFit_radius_Aslow'], False)
    # hs['gr_timeFit_radius_Aslow'].GetXaxis().SetTitle("Hit Radius [mm]");
    # hs['gr_timeFit_radius_Aslow'].GetYaxis().SetTitle("Aslow [ns]")

    if(i == 0):
        canvases['c_timing'].cd()
        gPad.SetLogy()

        canvases['c_timing_normalized'].cd()
        gPad.SetLogy()

        canvases['c_hitRadius'].cd()
        gPad.SetLogy()

        canvases['c_hitEnergy'].cd()
        gPad.SetLogy()


    # Draw Histo
    for key,val in canvases.items():
        c = val
        c.cd()
        c.SetTickx()
        c.SetTicky()

        if(l_drawGrid[key[2:]]):
            c.SetGrid(1,1)

        if(key == "c_timeMean_overRadius" or key == "c_timeMedian_overRadius" or key == "c_timeMean_overShowerDepth"
            or key == "c_timeMean_overRadius_50To200" or key == "c_timeMean_overShowerDepth_50To200"
            or key == "c_timeFit_radius_Tfast" or key == "c_timeFit_radius_Tslow"
            or key == "c_timeFit_radius_Afast" or key == "c_timeFit_radius_Aslow"
            or key == "c_timeMean_overRadius_50To200_smallLayers" or key == "c_timeMean_overRadius_50To200_bigLayers"
            or key == "c_timeFit_radius_Afast_over_Aslow"):
            if(i==0):
                hs[('gr'+key[1:])].Draw("AL*SAME")
            else:
                hs[('gr'+key[1:])].Draw("L*SAME")

        else:
            hs[('h'+key[1:])].Draw("SAME")



    leg.AddEntry(hs['h_timing'], legendEntry[i])
    legLeft.AddEntry(hs['h_timing'], legendEntry[i])
    legRight.AddEntry(hs['h_timing'], legendEntry[i])



    i = i+1

texRightBottom = TLatex(0.62,0.17,"#splitline{CALICE AHCAL}{Work in progress}");
texRightBottom.SetNDC(True)
texRightBottom.SetTextColor(15);
texRightBottom.SetTextSize(0.04545455);
texRightBottom.SetLineWidth(2);

texLeftBottom = TLatex(0.14,0.17,"#splitline{CALICE AHCAL}{Work in progress}");
texLeftBottom.SetNDC(True)
texLeftBottom.SetTextColor(15);
texLeftBottom.SetTextSize(0.04545455);
texLeftBottom.SetLineWidth(2);

texLeftTop = TLatex(0.14,0.81,"#splitline{CALICE AHCAL}{Work in progress}");
texLeftTop.SetNDC(True)
texLeftTop.SetTextColor(15);
texLeftTop.SetTextSize(0.04545455);
texLeftTop.SetLineWidth(2);

texRightTop = TLatex(0.62,0.81,"#splitline{CALICE AHCAL}{Work in progress}");
texRightTop.SetNDC(True)
texRightTop.SetTextColor(15);
texRightTop.SetTextSize(0.04545455);
texRightTop.SetLineWidth(2);

# Draw Legend
for key,val in canvases.items():
    c = val
    c.cd()
    # leg.Draw()
    if(l_leftLegend[key[2:]]):
        legLeft.Draw()
    else:
        legRight.Draw()

    if(l_calicePosition[key[2:]] == 0):
        texLeftTop.Draw()
    elif(l_calicePosition[key[2:]] == 1):
        texRightTop.Draw()
    elif(l_calicePosition[key[2:]] == 2):
        texRightBottom.Draw()
    elif(l_calicePosition[key[2:]] == 3):
        texLeftBottom.Draw()



if fWrite:
    for key,val in canvases.items():
        c = val
        c.SaveAs(writePath+writePrefixForOthers+writePrefix+key[2:]+".root")
        c.SaveAs(writePath+writePrefixForOthers+writePrefix+key[2:]+".C")
        c.SaveAs(writePath+writePrefix+key[2:]+".pdf")
        c.SaveAs(writePath+writePrefixForOthers+writePrefix+key[2:]+".svg")

raw_input("Press Enter to continue...")
