import ROOT
from ROOT import TFile, TH1D, TCanvas, gPad, TLegend, gStyle

f_pion_tungsten_nonHP = TFile("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Pions_Sim/pions_001_hcal.root");
t_pion_tungsten_nonHP = f_pion_tungsten_nonHP.Get("bigtree;1")
h_pion_tungsten_nonHP = TH1D("h_pion_tungsten_nonHP", "70GeV Pions QGSP_BERT Tungsten vs Iron", 2000, -500, 1500)
t_pion_tungsten_nonHP.Draw("ahc_hitTime>>h_pion_tungsten_nonHP")

h_pion_tungsten_nonHP.Scale(1./float(h_pion_tungsten_nonHP.GetEntries()))
h_pion_tungsten_nonHP.GetXaxis().SetTitle("Hit Time [ns]")
h_pion_tungsten_nonHP.GetYaxis().SetTitle("# Entries")
h_pion_tungsten_nonHP.SetLineColor(ROOT.kBlue+2)
h_pion_tungsten_nonHP.SetLineWidth(2)


f_pion_iron_nonHP = TFile("/remote/pcilc2/cgraf/Data/Testbeam/2015_July/MonteCarlo/Pion_Sim/pion_QGSP_BERT_reco_hcal_001.root");
t_pion_iron_nonHP = f_pion_iron_nonHP.Get("bigtree;1")
h_pion_iron_nonHP = TH1D("h_pion_iron_nonHP", "70GeV Pions QGSP_BERT Tungsten vs Iron", 2000, -500, 1500)
t_pion_iron_nonHP.Draw("ahc_hitTime>>h_pion_iron_nonHP")



h_pion_iron_nonHP.Scale(1./float(h_pion_iron_nonHP.GetEntries()))
h_pion_iron_nonHP.SetLineColor(ROOT.kAzure+8)
h_pion_iron_nonHP.SetLineWidth(2)

# h_pion_tungsten_nonHP.SetTitle("pion_tungsten_nonHP")
# h_pion_iron_nonHP.SetTitle("pion_iron_nonHP")

c = TCanvas("c", "c")
gPad.SetLogy()
h_pion_tungsten_nonHP.Draw()
h_pion_iron_nonHP.Draw("Same")

gStyle.SetOptStat(0);

leg = TLegend(0.60, 0.70, 0.9, 0.9)
leg.AddEntry(h_pion_tungsten_nonHP, "Pions Tungsten")
leg.AddEntry(h_pion_iron_nonHP, "Pions Iron")
leg.Draw()

raw_input("Press Enter to continue...")
