import ROOT
from ROOT import TFile, TH1D, TCanvas, gPad, TLegend, gStyle

f_Muons = TFile("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Muons_Sim/muons_001_hcal.root")
t_Muons = f_Muons.Get("bigtree;1")
h_Muons = TH1D("h_Muons", "Tungsten 120GeV Muons vs 20GeV Electrons QGSP_BERT", 2000, -500, 1500)
t_Muons.Draw("ahc_hitTime>>h_Muons")

h_Muons.Scale(1./float(h_Muons.GetEntries()))
h_Muons.GetXaxis().SetTitle("Hit Time [ns]")
h_Muons.GetYaxis().SetTitle("# Entries")
h_Muons.SetLineColor(ROOT.kRed+2)
h_Muons.SetLineWidth(2)


f_Electrons = TFile("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Electrons_Sim/electrons_001_hcal.root");
t_Electrons = f_Electrons.Get("bigtree;1")
h_Electrons = TH1D("h_Electrons", "Tungsten Muons vs Electrons QGSP_BERT_HD", 2000, -500, 1500)
t_Electrons.Draw("ahc_hitTime>>h_Electrons")



h_Electrons.Scale(1./float(h_Electrons.GetEntries()))
h_Electrons.SetLineColor(ROOT.kMagenta+2)
h_Electrons.SetLineWidth(2)

# h_QGSP_BERT_HP.SetTitle("QGSP_BERT_HP")
# h_QGSP_BERT.SetTitle("QGSP_BERT")

c = TCanvas("c", "c")
gPad.SetLogy()
h_Muons.Draw()
h_Electrons.Draw("Same")

gStyle.SetOptStat(0);

leg = TLegend(0.60, 0.70, 0.9, 0.9)
leg.AddEntry(h_Muons, "Muons")
leg.AddEntry(h_Electrons, "Electrons")
leg.Draw()

raw_input("Press Enter to continue...")
