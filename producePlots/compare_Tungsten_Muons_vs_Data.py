import ROOT
from ROOT import TFile, TH1D, TCanvas, TPad, gPad, TLegend, gStyle


fWrite = False
writePath = "../plots/17_05_08/"
writePrefix = "muons_"


# f_Muons = TFile("/remote/pcilc2/cgraf/Data/Testbeam/2015_August/MonteCarlo/Muons_Sim/muons_003_hcal.root");
# t_Muons = f_Muons.Get("bigtree;1")
# h_Muons = TH1D("h_Muons", "Tungsten 120GeV Muons Simulation vs Data", 2000, -500, 1500)
# t_Muons.Draw("ahc_hitTime>>h_Muons")

# gStyle.SetTitleFont(43)
# gStyle.SetTitleSize(20)
# gStyle.SetLabelFont(43)
# gStyle.SetLabelSize(20)

# f_Muons = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/mc_aug_muons_006.root");
# f_Muons = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_03_10_muons_sim_evtsmearing.root");
# f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_03_12_muons_simulation_noSelection.root");
f_sim = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_10_muons_sim.root")
# h_Muons = f_Muons.Get("h_resolution")
h_sim_timing = f_sim.Get("h_resolution_t0Time")
h_sim_timing.Draw()
h_sim_timing.Rebin(10)


h_sim_timing.Scale(1./float(h_sim_timing.GetEntries()))
h_sim_timing.GetXaxis().SetTitle("Hit Time [ns]")
h_sim_timing.GetYaxis().SetTitle("Normalized Entries")
h_sim_timing.SetLineColor(ROOT.kRed+2)
h_sim_timing.SetLineWidth(2)


f_data = TFile("/home/iwsatlas1/cgraf/Analysis/2015_August_Testbeam_tdc/fit_calibration/cali_output/17_05_10_muons_data.root");
# t_Pions = f_Pions.Get("bigtree;1")
# h_Pions = TH1D("h_Pions", "Tungsten Muons vs Pions QGSP_BERT", 2000, -500, 1500)
# h_Data = f_Data.Get("h_resolution_evtTime")
h_data_timing = f_data.Get("h_resolution_t0Time")
# h_Data.Draw()

h_data_timing.Scale(1./float(h_data_timing.GetEntries()))
h_data_timing.SetLineColor(ROOT.kOrange+1)
h_data_timing.SetLineWidth(2)
h_data_timing.Rebin(10)

h_sim_timing.GetYaxis().SetLabelFont(43)
h_sim_timing.GetYaxis().SetLabelSize(30)
h_sim_timing.GetYaxis().SetTitleFont(43)
h_sim_timing.GetYaxis().SetTitleSize(35)
h_sim_timing.GetYaxis().SetTitleOffset(1.3)

# h_sim_timing.SetMaximum(2*10e-2)
# h_sim_timing.SetMinimum(2*10e-6)
# h_Muons.SetOptStat(0)

h_sim_timing.SetTitleSize(50)

# h_Muons.UseCurrentStyle()

h_sim_timing.SetAxisRange(-20, 20)

# h_QGSP_BERT_HP.SetTitle("QGSP_BERT_HP")
# h_QGSP_BERT.SetTitle("QGSP_BERT")

c = TCanvas("c", "c", 1000, 800)
pad1 = TPad("pad1", "pad1", 0.0, 0.3, 1.0, 1.0)
pad1.SetBottomMargin(0.000)
pad1.Draw()
pad1.cd()


gPad.SetLogy()
h_sim_timing.SetTitle("Muon Hit Time Distribution")
h_sim_timing.Draw()
h_data_timing.Draw("Same")

leg_timing = TLegend(0.60, 0.70, 0.9, 0.9)
leg_timing.AddEntry(h_sim_timing, "Simulation")
leg_timing.AddEntry(h_data_timing, "Data")
leg_timing.Draw()

h_residuals = TH1D("h_residuals", "", h_sim_timing.GetNbinsX(), h_sim_timing.GetXaxis().GetXmin(), h_sim_timing.GetXaxis().GetXmax())

for i in range(1,h_sim_timing.GetNbinsX()+1):
    if(h_data_timing.GetBinContent(i) > 0):
        # h_residuals.SetBinContent(i, (h_Muons.GetBinContent(i)-h_Data.GetBinContent(i))/h_Muons.GetBinContent(i))
        h_residuals.SetBinContent(i, h_sim_timing.GetBinContent(i)/h_data_timing.GetBinContent(i))


h_residuals.GetYaxis().SetLabelFont(43)
h_residuals.GetYaxis().SetLabelSize(30)
h_residuals.GetYaxis().SetTitleFont(43)
h_residuals.GetYaxis().SetTitleSize(35)
h_residuals.GetYaxis().SetTitleOffset(1.3)
h_residuals.GetYaxis().SetNdivisions(5,5,0)
h_residuals.GetYaxis().SetTitle("MC / Data")

h_residuals.GetXaxis().SetLabelFont(43)
h_residuals.GetXaxis().SetLabelSize(30)
h_residuals.GetXaxis().SetTitleFont(43)
h_residuals.GetXaxis().SetTitleSize(35)
h_residuals.GetXaxis().SetTitleOffset(4.2)
h_residuals.GetXaxis().SetTitle("Hit Time [ns]")

h_residuals.SetAxisRange(-20, 20)
h_residuals.SetMinimum(0.5)
h_residuals.SetMaximum(1.5)

h_residuals.SetMarkerColor(ROOT.kBlue-2)
h_residuals.SetMarkerSize(1.3)


c.cd()
pad2 = TPad("pad2", "pad2", 0.0, 0.05, 1.0, 0.3)
pad2.SetGridx()
pad2.SetGridy()
pad2.SetTopMargin(0.00)
pad2.SetBottomMargin(0.3)
pad2.Draw()
pad2.cd()
h_residuals.SetMarkerStyle(3)
h_residuals.Draw("P")

gStyle.SetOptStat(0);

if fWrite:
    c.SaveAs(writePath+writePrefix+"timeDistribution.root")
    c.SaveAs(writePath+writePrefix+"timeDistribution.C")
    c.SaveAs(writePath+writePrefix+"timeDistribution.pdf")
    c.SaveAs(writePath+writePrefix+"timeDistribution.svg")

# -------------------------------
# -------- nHits & CoG Z --------
# -------------------------------

# h2_sim_nHits_cogZ = f_sim.Get("h2_nHits_cogZ")
# h2_sim_nHits_cogZ.Scale(1./float(h2_sim_nHits_cogZ.GetEntries()))
# h2_sim_nHits_cogZ.SetLineColor(ROOT.kBlue+4)
# h2_sim_nHits_cogZ.SetLineWidth(2)
#
# h2_sim_nHits_cogZ.GetYaxis().SetLabelFont(43)
# h2_sim_nHits_cogZ.GetYaxis().SetLabelSize(25)
# h2_sim_nHits_cogZ.GetYaxis().SetTitleFont(43)
# h2_sim_nHits_cogZ.GetYaxis().SetTitleSize(25)
# h2_sim_nHits_cogZ.GetYaxis().SetTitleOffset(1.6)
#
# h2_sim_nHits_cogZ.SetTitleSize(50)
#
# h2_data_nHits_cogZ = f_data.Get("h2_nHits_cogZ")
# h2_data_nHits_cogZ.Scale(1./float(h2_data_nHits_cogZ.GetEntries()))
# h2_data_nHits_cogZ.SetLineColor(ROOT.kRed)
# h2_data_nHits_cogZ.SetLineWidth(2)
#
# c_nHits_cogZ = TCanvas("c_nHits_cogZ", "c_nHits_cogZ")
# h2_sim_nHits_cogZ.Draw("BOX")
# h2_data_nHits_cogZ.Draw("BOX SAME")
#
# leg = TLegend(0.60, 0.70, 0.9, 0.9)
# leg.AddEntry(h2_sim_nHits_cogZ, "Simulation")
# leg.AddEntry(h2_data_nHits_cogZ, "Data")
# leg.Draw()

# -------------------------------
# ------------ nHits ------------
# -------------------------------

h_sim_nHits = f_sim.Get("h_hits_nHits")

h_sim_nHits.Scale(1./float(h_sim_nHits.GetEntries()))
h_sim_nHits.SetLineColor(ROOT.kRed+2)
h_sim_nHits.SetLineWidth(2)

h_sim_nHits.GetYaxis().SetTitle("Normalized Entries")
h_sim_nHits.GetYaxis().SetLabelFont(43)
h_sim_nHits.GetYaxis().SetLabelSize(20)
h_sim_nHits.GetYaxis().SetTitleFont(43)
h_sim_nHits.GetYaxis().SetTitleSize(20)
# h_sim_nHits.GetYaxis().SetTitleOffset(1.6)

h_sim_nHits.GetXaxis().SetTitle("Number of Hits")
h_sim_nHits.GetXaxis().SetLabelFont(43)
h_sim_nHits.GetXaxis().SetLabelSize(20)
h_sim_nHits.GetXaxis().SetTitleFont(43)
h_sim_nHits.GetXaxis().SetTitleSize(20)
# h_sim_nHits.GetXaxis().SetTitleOffset(1.6)

h_data_nHits = f_data.Get("h_hits_nHits")
h_data_nHits.Scale(1./float(h_data_nHits.GetEntries()))
h_data_nHits.SetLineColor(ROOT.kOrange+1)
h_data_nHits.SetLineWidth(2)

h_sim_nHits.SetAxisRange(0,40)

c_nHits = TCanvas("c_nHits", "c_nHits")
h_sim_nHits.Draw()
h_data_nHits.Draw("SAME")

leg = TLegend(0.60, 0.70, 0.9, 0.9)
leg.AddEntry(h_sim_nHits, "Simulation")
leg.AddEntry(h_data_nHits, "Data")
leg.Draw()

if fWrite:
    c_nHits.SaveAs(writePath+writePrefix+"nHits.root")
    c_nHits.SaveAs(writePath+writePrefix+"nHits.C")
    c_nHits.SaveAs(writePath+writePrefix+"nHits.pdf")
    c_nHits.SaveAs(writePath+writePrefix+"nHits.svg")

# -------------------------------
# ------------ CoGZ -------------
# -------------------------------

h_sim_cogZ = f_sim.Get("h_hits_cogZ")

h_sim_cogZ.Scale(1./float(h_sim_cogZ.GetEntries()))
h_sim_cogZ.SetLineColor(ROOT.kRed+2)
h_sim_cogZ.SetLineWidth(2)

h_sim_cogZ.GetYaxis().SetTitle("Normalized Entries")
h_sim_cogZ.GetYaxis().SetLabelFont(43)
h_sim_cogZ.GetYaxis().SetLabelSize(20)
h_sim_cogZ.GetYaxis().SetTitleFont(43)
h_sim_cogZ.GetYaxis().SetTitleSize(20)
# h_sim_cogZ.GetYaxis().SetTitleOffset(1.6)

h_sim_cogZ.GetXaxis().SetTitle("CoG Z [mm]")
h_sim_cogZ.GetXaxis().SetLabelFont(43)
h_sim_cogZ.GetXaxis().SetLabelSize(20)
h_sim_cogZ.GetXaxis().SetTitleFont(43)
h_sim_cogZ.GetXaxis().SetTitleSize(20)
# h_sim_cogZ.GetXaxis().SetTitleOffset(1.6)


h_data_cogZ = f_data.Get("h_hits_cogZ")
h_data_cogZ.Scale(1./float(h_data_cogZ.GetEntries()))
h_data_cogZ.SetLineColor(ROOT.kOrange+1)
h_data_cogZ.SetLineWidth(2)

c_cogZ = TCanvas("c_cogZ", "c_cogZ")
h_sim_cogZ.Draw()
h_data_cogZ.Draw("SAME")

leg = TLegend(0.60, 0.70, 0.9, 0.9)
leg.AddEntry(h_sim_cogZ, "Simulation")
leg.AddEntry(h_data_cogZ, "Data")
leg.Draw()

if fWrite:
    c_cogZ.SaveAs(writePath+writePrefix+"cogZ.root")
    c_cogZ.SaveAs(writePath+writePrefix+"cogZ.C")
    c_cogZ.SaveAs(writePath+writePrefix+"cogZ.pdf")
    c_cogZ.SaveAs(writePath+writePrefix+"cogZ.svg")


# -------------------------------
# ---------- energySum ----------
# -------------------------------

h_sim_energySum = f_sim.Get("h_hits_energySum")

h_sim_energySum.Scale(1./float(h_sim_energySum.GetEntries()))
h_sim_energySum.SetLineColor(ROOT.kRed+2)
h_sim_energySum.SetLineWidth(2)

h_sim_energySum.GetYaxis().SetTitle("Normalized Entries")
h_sim_energySum.GetYaxis().SetLabelFont(43)
h_sim_energySum.GetYaxis().SetLabelSize(20)
h_sim_energySum.GetYaxis().SetTitleFont(43)
h_sim_energySum.GetYaxis().SetTitleSize(20)
h_sim_energySum.GetYaxis().SetTitleOffset(0.85)

h_sim_energySum.GetXaxis().SetTitle("Energy Sum [MIP]")
h_sim_energySum.GetXaxis().SetLabelFont(43)
h_sim_energySum.GetXaxis().SetLabelSize(20)
h_sim_energySum.GetXaxis().SetTitleFont(43)
h_sim_energySum.GetXaxis().SetTitleSize(20)
h_sim_energySum.GetYaxis().SetTitleOffset(0.85)

# h_sim_energySum.SetTitleSize(50)

h_data_energySum = f_data.Get("h_hits_energySum")
h_data_energySum.Scale(1./float(h_data_energySum.GetEntries()))
h_data_energySum.SetLineColor(ROOT.kOrange+1)
h_data_energySum.SetLineWidth(2)

c_energySum = TCanvas("c_energySum", "c_energySum")
h_sim_energySum.Draw()
h_data_energySum.Draw("SAME")

leg_energySum = TLegend(0.60, 0.70, 0.9, 0.9)
leg_energySum.AddEntry(h_sim_energySum, "Simulation")
leg_energySum.AddEntry(h_data_energySum, "Data")
leg_energySum.Draw()

h_sim_energySum.SetMaximum(0.14)
h_sim_energySum.SetAxisRange(0,80)

if fWrite:
    c_energySum.SaveAs(writePath+writePrefix+"energySum.root")
    c_energySum.SaveAs(writePath+writePrefix+"energySum.C")
    c_energySum.SaveAs(writePath+writePrefix+"energySum.pdf")
    c_energySum.SaveAs(writePath+writePrefix+"energySum.svg")

# -------------------------------
# ------ nHits same Layer -------
# -------------------------------

h_sim_mean_nHitsPerLayer = f_sim.Get("h_hits_mean_nHitsPerLayer")

h_sim_mean_nHitsPerLayer.SetLineColor(ROOT.kRed+2)
h_sim_mean_nHitsPerLayer.SetLineWidth(2)

h_sim_mean_nHitsPerLayer.GetYaxis().SetTitle("<# Hits per Layer>")
h_sim_mean_nHitsPerLayer.GetYaxis().SetLabelFont(43)
h_sim_mean_nHitsPerLayer.GetYaxis().SetLabelSize(20)
h_sim_mean_nHitsPerLayer.GetYaxis().SetTitleFont(43)
h_sim_mean_nHitsPerLayer.GetYaxis().SetTitleSize(20)
h_sim_mean_nHitsPerLayer.GetYaxis().SetTitleOffset(0.85)

h_sim_mean_nHitsPerLayer.GetXaxis().SetTitle("Layer")
h_sim_mean_nHitsPerLayer.GetXaxis().SetLabelFont(43)
h_sim_mean_nHitsPerLayer.GetXaxis().SetLabelSize(20)
h_sim_mean_nHitsPerLayer.GetXaxis().SetTitleFont(43)
h_sim_mean_nHitsPerLayer.GetXaxis().SetTitleSize(20)
h_sim_mean_nHitsPerLayer.GetXaxis().SetTitleOffset(0.9)

# h_sim_mean_nHitsPerLayer.SetTitleSize(50)

h_data_mean_nHitsPerLayer = f_data.Get("h_hits_mean_nHitsPerLayer")
h_data_mean_nHitsPerLayer.SetLineColor(ROOT.kOrange+1)
h_data_mean_nHitsPerLayer.SetLineWidth(2)

c_mean_nHitsPerLayer = TCanvas("c_mean_nHitsPerLayer", "c_mean_nHitsPerLayer")
h_sim_mean_nHitsPerLayer.Draw()
h_data_mean_nHitsPerLayer.Draw("SAME")

leg_mean_nHitsPerLayer = TLegend(0.60, 0.70, 0.9, 0.9)
leg_mean_nHitsPerLayer.AddEntry(h_sim_mean_nHitsPerLayer, "Simulation")
leg_mean_nHitsPerLayer.AddEntry(h_data_mean_nHitsPerLayer, "Data")
leg_mean_nHitsPerLayer.Draw()

h_sim_mean_nHitsPerLayer.SetMaximum(1)

if fWrite:
    c_mean_nHitsPerLayer.SaveAs(writePath+writePrefix+"mean_nHitsPerLayer.root")
    c_mean_nHitsPerLayer.SaveAs(writePath+writePrefix+"mean_nHitsPerLayer.C")
    c_mean_nHitsPerLayer.SaveAs(writePath+writePrefix+"mean_nHitsPerLayer.pdf")
    c_mean_nHitsPerLayer.SaveAs(writePath+writePrefix+"mean_nHitsPerLayer.svg")

# -------------------------------
# ------ nHits same Chip -------
# -------------------------------

h_sim_mean_nHitsPerChip = f_sim.Get("h_hits_mean_nHitsPerChip")

h_sim_mean_nHitsPerChip.SetLineColor(ROOT.kRed+2)
h_sim_mean_nHitsPerChip.SetLineWidth(2)

h_sim_mean_nHitsPerChip.GetYaxis().SetTitle("<# Hits per Chip>")
h_sim_mean_nHitsPerChip.GetYaxis().SetLabelFont(43)
h_sim_mean_nHitsPerChip.GetYaxis().SetLabelSize(20)
h_sim_mean_nHitsPerChip.GetYaxis().SetTitleFont(43)
h_sim_mean_nHitsPerChip.GetYaxis().SetTitleSize(20)
h_sim_mean_nHitsPerChip.GetYaxis().SetTitleOffset(0.85)

h_sim_mean_nHitsPerChip.GetXaxis().SetTitle("ChipID")
h_sim_mean_nHitsPerChip.GetXaxis().SetLabelFont(43)
h_sim_mean_nHitsPerChip.GetXaxis().SetLabelSize(20)
h_sim_mean_nHitsPerChip.GetXaxis().SetTitleFont(43)
h_sim_mean_nHitsPerChip.GetXaxis().SetTitleSize(20)
h_sim_mean_nHitsPerChip.GetXaxis().SetTitleOffset(0.9)

# h_sim_mean_nHitsPerChip.SetTitleSize(50)

h_data_mean_nHitsPerChip = f_data.Get("h_hits_mean_nHitsPerChip")
h_data_mean_nHitsPerChip.SetLineColor(ROOT.kOrange+1)
h_data_mean_nHitsPerChip.SetLineWidth(2)

c_mean_nHitsPerChip = TCanvas("c_mean_nHitsPerChip", "c_mean_nHitsPerChip")
h_sim_mean_nHitsPerChip.Draw()
h_data_mean_nHitsPerChip.Draw("SAME")

leg_mean_nHitsPerChip = TLegend(0.60, 0.70, 0.9, 0.9)
leg_mean_nHitsPerChip.AddEntry(h_sim_mean_nHitsPerChip, "Simulation")
leg_mean_nHitsPerChip.AddEntry(h_data_mean_nHitsPerChip, "Data")
leg_mean_nHitsPerChip.Draw()

if fWrite:
    c_mean_nHitsPerChip.SaveAs(writePath+writePrefix+"mean_nHitsPerChip.root")
    c_mean_nHitsPerChip.SaveAs(writePath+writePrefix+"mean_nHitsPerChip.C")
    c_mean_nHitsPerChip.SaveAs(writePath+writePrefix+"mean_nHitsPerChip.pdf")
    c_mean_nHitsPerChip.SaveAs(writePath+writePrefix+"mean_nHitsPerChip.svg")

raw_input("Press Enter to continue...")
