CC = g++ -std=c++0x -g
CFLAGS += -Wall -c $(shell root-config --cflags)
CPPFLAGS += -Iinclude
LFLAGS += $(shell root-config --ldflags)
LDLIBS += $(shell root-config --glibs) -lMinuit -Llib

SRC_DIR = src
OBJ_DIR = obj

SOURCES = $(wildcard $(SRC_DIR)/*.cpp)
OBJECTS = $(SOURCES:$(SRC_DIR)/%.cpp=$(OBJ_DIR)/%.o)
EXECUTABLE = calibrateTDC

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $^ $(LDLIBS) -o $@ #-fopenmp

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC) $(CPPFLAGS) $(CFLAGS) $< -o $@

# .cpp.o:
# 	$(CC) $(CFLAGS) $< -o $@ -fopenmp

clean:
	rm $(OBJECTS)

.PHONY: all clean
